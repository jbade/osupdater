package org.iinc.osbot.scripts;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.scripts.fishing.lumbridge_swamp.LumbridgeSwampFishing;
import org.iinc.osbot.scripts.tutorial.TutorialIsland;
import org.iinc.osbot.util.AccountCreator;
import org.iinc.osbot.util.Email;
import org.iinc.osbot.util.Email.TYPE;
import org.iinc.osbot.util.Memory;

public class NewAccountScript extends Script {

	long startTime;
	String username, password, displayName;
	TutorialIsland ti = new TutorialIsland();
	LumbridgeSwampFishing fi = new LumbridgeSwampFishing();
	boolean emailSent = false;

	@Override
	public int loop() {
		if (!Login.isLoggedIn()) {
			Login.login();
			Time.sleep(10000, 15000);
			Camera.setAngle(Camera.getCameraAngle(), true);
			return 0;
		}

		Interfaces.closeAll();

		if (ti.activate()) {
			ti.execute();
			return 0;
		}

		if (!emailSent) {
			Email.sendEmail(Email.TYPE.INFO, "Starter\nFinished tutorial island\n\n" + username + " " + password + " "
					+ displayName + "\n\n" + "minutes: " + (System.currentTimeMillis() - startTime) / 1000 / 60.0);
			emailSent = true;
		}

		if (fi.activate())
			fi.execute();
		
		return 100;
	}

	@Override
	public void onStop() {
	}

	@Override
	public void onStart() {
		Bot.LOGGER.log(Level.FINE, "Started NewAccountScript");
		
		startTime = System.currentTimeMillis();
		boolean success = false;
		for (int i = 0; i < 5 && !success; i++) {
			String captcha = AccountCreator.getCaptchaSolution();
			Bot.LOGGER.log(Level.FINEST, "Starter: solved captcha " + captcha);
			if (captcha != null) {
				displayName = AccountCreator.getFreeName();
				Bot.LOGGER.log(Level.FINEST, "Starter: got displayname " + displayName);
				if (displayName != null) {
					username = shuffle(displayName.toLowerCase()) + "@gmail.com";
					password = "youtube1";
					Bot.LOGGER.log(Level.FINEST, "Starter: attempted to create account '" + username + "' " + password);
					if (AccountCreator.createAccount(username, displayName, password, captcha)) {
						Bot.LOGGER.log(Level.FINEST, "Successfully created account " + username + " " + displayName);
						Bot.username = username;
						Bot.password = password;
						success = true;
					} else {
						Time.sleep(10000);
					}
				}
			}
		}
		
		if (!success){
			Email.sendEmail(TYPE.SEVERE, "Failed to create an account after 5 tries.");
			System.exit(Settings.DISABLED_EXIT_CODE);
		}
	}

	// https://stackoverflow.com/questions/1519736/random-shuffling-of-an-array
	static String shuffle(String cs) {
		char[] c = cs.toCharArray();
		Random rnd = ThreadLocalRandom.current();
		for (int i = c.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);
			char a = c[index];
			c[index] = c[i];
			c[i] = a;
		}

		return String.valueOf(c);
	}

}
