package org.iinc.osbot.scripts.gdk;

import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.base.worlds.World;
import org.iinc.osbot.api.base.worlds.Worlds;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.NpcDefinition;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.renderable.Players;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class CombatNode implements GDKNode {

    @Override
    public boolean activate(int wildernessLevel, Tile playerLocation) {
        return wildernessLevel > 0 && GDK.DRAGON_AREA.contains(playerLocation);
    }

    @Override
    public void execute(int wildernessLevel, Tile playerLocation) {
        Bot.LOGGER.log(Level.INFO, this.getClass().getName());

        if (!Client.getLocalPlayer().isInteracting()) {

            List<Npc> filtered = Npcs.getAll().parallelStream().filter((npc) -> {
                if (npc == null)
                    return false;
                NpcDefinition def = npc.getNpcDefinition();
                if (def == null)
                    return false;
                String name = def.getName();
                return name != null && name.equals("Green dragon") && npc.getAnimation() != 92
                        && !npc.isBeingInteractedWith(Players.getAll());
            }).sorted().collect(Collectors.toList());

            if (filtered.size() != 0) {

                List<Npc> attackingPlayer = filtered.parallelStream()
                        .filter((n) -> n.getInteractingReference() == Client.getLocalPlayer().getReference()).sorted()
                        .collect(Collectors.toList());

                Npc npc = attackingPlayer.size() > 0 ? attackingPlayer.get(0) : filtered.get(0);

                if (Calculations.onViewport(npc.toScreen())) {
                    Bot.LOGGER.log(Level.FINEST, "Attacking dragon");
                    if (Clicking.interact(npc, "Attack", "Green dragon  (level-79)")) {
                        Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting(), 600, 800);
                    }
                } else {
                    Tile npcTile = npc.getTile();
                    npcTile = Walking.getClosestTile(npcTile, Client.getLocalPlayer().getTile().distanceTo(npcTile));

                    Walking.walk(npcTile, () -> {
                        if (Calculations.onViewport(npc.toScreen()))
                            return true;

                        List<Npc> filtered2 = Npcs.getAll().parallelStream().filter((Npc n) -> {
                            if (n == null)
                                return false;

                            NpcDefinition def = n.getNpcDefinition();
                            if (def == null)
                                return false;

                            String name = def.getName();
                            return name != null && name.equals("Green dragon") && n.getAnimation() != 92
                                    && !n.isBeingInteractedWith(Players.getAll());
                        }).sorted().collect(Collectors.toList());

                        if (filtered2.size() == 0)
                            return true;

                        List<Npc> attackingPlayer2 = filtered2.stream()
                                .filter((n) -> n.getInteractingReference() == Client.getLocalPlayer().getReference())
                                .collect(Collectors.toList());
                        Npc n = attackingPlayer2.size() > 0 ? attackingPlayer2.get(0) : filtered2.get(0);

                        return npc.getReference() != n.getReference();
                    }, 2);
                }
            } else {
                // no dragons
                if (playerLocation.distanceTo(GDK.CENTER_DRAGON_TILE) > 8) {
                    Bot.LOGGER.log(Level.FINEST, "No dragons. Walking to center");
                    Walking.walk(Walking.getClosestTileOnMiniMap(GDK.CENTER_DRAGON_TILE,
                            playerLocation.distanceTo(GDK.CENTER_DRAGON_TILE) - 5), WalkingCondition.get(), 3);
                } else {
                    Time.sleep(200, 600);
                    World w = Worlds.getRandomNormalWorld(true);
                    if (w != null) {
                        Logout.switchWorld(w.getId());
                        Time.sleep(800, 1000);
                    }
                }
            }
            // } else {
            // Bot.LOGGER.log(Level.FINEST, "Walking path");
            // Walking.walkPath(lumbridgeToSwamp, false);
            // }
        } else {
            Time.sleep(400, 800);
        }

    }
}
