package org.iinc.osbot.scripts.gdk;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.function.BooleanSupplier;
import java.util.function.IntSupplier;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Equipment;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Keyboard;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.scripts.common.Navigation;
import org.iinc.osbot.util.ArrayUtil;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Memory;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class BankingNode implements GDKNode {

	private static final InvSupply[] INV_SUPPLIES = new InvSupply[] {
			new InvSupply(() -> 1, Items.GAMES_NECKLACE_2, Items.GAMES_NECKLACE_3, Items.GAMES_NECKLACE_4,
					Items.GAMES_NECKLACE_5, Items.GAMES_NECKLACE_6, Items.GAMES_NECKLACE_7, Items.GAMES_NECKLACE_8),
			new InvSupply(() -> (int) (-1 * Math.pow(Levels.getStrength(), 3) / 1749300.0
					+ (572.0 * Math.pow(Levels.getStrength(), 2)) / 145775.0
					- (272497.0 * Levels.getStrength()) / 349860.0 + 30325.0 / 686.0), Items.LOBSTER) };
	// https://www.wolframalpha.com/input/?i=cubic+fit+(50,+15),+(65,+10),+(85,+6),+(99,+5)

	private static Equip HELMET = new Equip(); // Items.ADAMANT_FULL_HELM
	private static Equip SHIELD = new Equip(Items.ANTI_DRAGON_SHIELD);
	private static Equip BODY = new Equip(Items.DRAGON_CHAINBODY, Items.RUNE_CHAINBODY, Items.ADAMANT_PLATEBODY);
	private static Equip LEGS = new Equip(Items.DRAGON_PLATESKIRT, Items.DRAGON_PLATELEGS, Items.RUNE_PLATESKIRT,
			Items.RUNE_PLATELEGS);
	private static Equip WEAPON = new Equip(Items.ABYSSAL_WHIP, Items.RUNE_SCIMITAR);
	private static Equip AMULET = new Equip(Items.AMULET_OF_GLORY_1, Items.AMULET_OF_GLORY_2, Items.AMULET_OF_GLORY_3,
			Items.AMULET_OF_GLORY_4, Items.AMULET_OF_GLORY_5, Items.AMULET_OF_GLORY_6);
	// TODO add just normal glory? need to check if bank has charged

	private static final InvSupply ENERGY_POT = new InvSupply(() -> 1, Items.ENERGY_POTION_1, Items.ENERGY_POTION_2,
			Items.ENERGY_POTION_3, Items.ENERGY_POTION_4);

	private static final InvSupply ATTACK_POT = new InvSupply(() -> 1, Items.SUPER_ATTACK_POTION_1,
			Items.SUPER_ATTACK_POTION_2, Items.SUPER_ATTACK_POTION_3, Items.SUPER_ATTACK_POTION_4);
	private static final InvSupply STRENGTH_POT = new InvSupply(() -> 1, Items.SUPER_STRENGTH_POTION_1,
			Items.SUPER_STRENGTH_POTION_2, Items.SUPER_STRENGTH_POTION_3, Items.SUPER_STRENGTH_POTION_4);
	private static final InvSupply DEFENCE_POT = new InvSupply(() -> 1, Items.SUPER_DEFENCE_POTION_1,
			Items.SUPER_DEFENCE_POTION_2, Items.SUPER_DEFENCE_POTION_3, Items.SUPER_DEFENCE_POTION_4);

	private static int ENERGY_MIN = 90;

	private static final class InvSupply {
		int[] ids;
		IntSupplier quantity;

		public InvSupply(IntSupplier quantity, RSItemConstant... items) {
			this.quantity = quantity;

			ids = new int[items.length];
			for (int i = 0; i < items.length; i++)
				ids[i] = items[i].getId();
		}
	}

	private static final class Equip {
		int[] ids;

		public Equip(RSItemConstant... items) {
			ids = new int[items.length];
			for (int i = 0; i < items.length; i++)
				ids[i] = items[i].getId();
		}
	}

	private void sleepAfterDrink() {
		Time.sleep(Random.modifiedRandomGuassianRange(1200, 1800, ConfidenceIntervals.CI_60, 300,
				Modifiers.get("GDK.sleepAfterDrink")));
	}

	@Override
	public boolean activate(int wildernessLevel, Tile playerLocation) {
		return wildernessLevel < 1
				&& (playerLocation.distanceTo(GDK.BARBARIAN_OUTPOST_WALK_TILE) < 3 || Navigation.atLumbridgeBank()
						|| playerLocation.distanceTo(GDK.EDGEVILLE_WALK_TILE) < 3 || Navigation.atGrandExchange());
	}

	boolean outOfAttackPotions = false;
	boolean outOfStrengthPotions = false;
	boolean outOfDefencePotions = false;
	boolean outOfEnergyPotions = false;
	boolean outOfHelmet = false;
	boolean outOfBody = false;
	boolean outOfLegs = false;
	boolean outOfWeapon = false;
	boolean outOfAmulet = false;

	ArrayList<BooleanSupplier> inventoryActions = new ArrayList<BooleanSupplier>();

	boolean acted = false;
	boolean equipped = false;

	boolean needRunEnergy = false;
	boolean needAttack = false;
	boolean needStrength = false;
	boolean needDefence = false;
	boolean needHelmet = false;
	boolean needShield = false;
	boolean needBody = false;
	boolean needLegs = false;
	boolean needWeapon = false;
	boolean needAmulet = false;

	{

		inventoryActions.add(() -> {
			if (needRunEnergy && Inventory.contains(ENERGY_POT.ids)) {
				Bot.LOGGER.log(Level.FINEST, "Drinking energy pots");
				Timer t = new Timer(Random.randomRange(15000, 20000));
				while (t.isRunning() && Walking.getRunEnergy() <= ENERGY_MIN && Inventory.contains(ENERGY_POT.ids)) {
					Item item = Inventory.getFirst(ENERGY_POT.ids);
					if (Clicking.interact(item, "Drink"))
						sleepAfterDrink();
				}
				acted = true;
			}
			return false;
		});

		inventoryActions.add(() -> {
			if (needAttack && Inventory.contains(ATTACK_POT.ids)) {
				Bot.LOGGER.log(Level.FINEST, "Drinking attack pots");
				Timer t = new Timer(Random.randomRange(15000, 20000));
				while (t.isRunning() && Levels.getCurrentAttack() - Levels.getAttack() < 5) {
					Item item = Inventory.getFirst(ATTACK_POT.ids);
					if (Clicking.interact(item, "Drink"))
						sleepAfterDrink();
				}
				acted = true;
			}
			return false;
		});
		inventoryActions.add(() -> {
			if (needStrength && Inventory.contains(STRENGTH_POT.ids)) {
				Bot.LOGGER.log(Level.FINEST, "Drinking strength pots");
				Timer t = new Timer(Random.randomRange(15000, 20000));
				while (t.isRunning() && Levels.getCurrentStrength() - Levels.getStrength() < 5) {
					Item item = Inventory.getFirst(STRENGTH_POT.ids);
					if (Clicking.interact(item, "Drink"))
						sleepAfterDrink();
				}
				acted = true;
			}
			return false;
		});
		inventoryActions.add(() -> {
			if (needDefence && Inventory.contains(DEFENCE_POT.ids)) {
				Bot.LOGGER.log(Level.FINEST, "Drinking defence pots");
				Timer t = new Timer(Random.randomRange(15000, 20000));
				while (t.isRunning() && Levels.getCurrentDefence() - Levels.getDefence() < 5) {
					Item item = Inventory.getFirst(DEFENCE_POT.ids);
					if (Clicking.interact(item, "Drink"))
						sleepAfterDrink();
				}
				acted = true;
			}
			return false;
		});
		inventoryActions.add(() -> {
			if (needHelmet && Inventory.contains(HELMET.ids)) {
				Clicking.interact(Inventory.getFirst(HELMET.ids), "Wear");
				acted = true;
				equipped = true;
				Time.sleep(Random.randomGuassianRange(0, 400, ConfidenceIntervals.CI_60));
			}
			return false;
		});
		inventoryActions.add(() -> {
			if (needShield && Inventory.contains(SHIELD.ids)) {
				Clicking.interact(Inventory.getFirst(SHIELD.ids), "Wear");
				acted = true;
				equipped = true;
				Time.sleep(Random.randomGuassianRange(0, 400, ConfidenceIntervals.CI_60));
			}
			return false;
		});
		inventoryActions.add(() -> {
			if (needBody && Inventory.contains(BODY.ids)) {
				Clicking.interact(Inventory.getFirst(BODY.ids), "Wear");
				acted = true;
				equipped = true;
				Time.sleep(Random.randomGuassianRange(0, 400, ConfidenceIntervals.CI_60));
			}
			return false;
		});
		inventoryActions.add(() -> {
			if (needLegs && Inventory.contains(LEGS.ids)) {
				Clicking.interact(Inventory.getFirst(LEGS.ids), "Wear");
				acted = true;
				equipped = true;
				Time.sleep(Random.randomGuassianRange(0, 400, ConfidenceIntervals.CI_60));
			}
			return false;
		});
		inventoryActions.add(() -> {
			if (needWeapon && Inventory.contains(WEAPON.ids)) {
				Clicking.interact(Inventory.getFirst(WEAPON.ids), "Wield");
				acted = true;
				equipped = true;
				Time.sleep(Random.randomGuassianRange(0, 400, ConfidenceIntervals.CI_60));
			}
			return false;
		});
		inventoryActions.add(() -> {
			if (needAmulet && Inventory.contains(AMULET.ids)) {
				Clicking.interact(Inventory.getFirst(AMULET.ids), "Wear");
				acted = true;
				equipped = true;
				Time.sleep(Random.randomGuassianRange(0, 400, ConfidenceIntervals.CI_60));
			}
			return false;
		});
	}

	boolean outOfSupplies = false;

	ArrayList<BooleanSupplier> bankActions = new ArrayList<BooleanSupplier>();

	{
		bankActions.add(() -> {
			if (needRunEnergy) {
				if (Bank.contains(ENERGY_POT.ids)) {
					int energyRequired = ENERGY_MIN - Walking.getRunEnergy();
					int total = 0;
					int potions = 0;
					for (int i = 0; i < ENERGY_POT.ids.length; i++) {
						int quantity = Bank.getQuantity(ENERGY_POT.ids[i]);
						int dose = i + 1;
						if (quantity * dose * 10 + total >= energyRequired) {
							potions += Math.ceil((energyRequired - total) / (dose * 10.0));
							break;
						}
						potions += quantity;
					}
					System.out.println("potions " + potions);
					if (!Bank.withdraw(ENERGY_POT.ids, potions))
						return true;
					Time.sleep(Random.randomGuassianRange(0, 400, ConfidenceIntervals.CI_60));
				} else {
					outOfEnergyPotions = true;
					Bot.LOGGER.log(Level.FINEST, "Out of ENERGY_POT");
				}
			}
			return false;
		});
		bankActions.add(() -> {
			if (needAttack) {
				if (Bank.contains(ATTACK_POT.ids)) {
					if (!Bank.withdraw(ATTACK_POT.ids, 1))
						return true;
					Time.sleep(Random.randomGuassianRange(0, 400, ConfidenceIntervals.CI_60));
				} else {
					outOfAttackPotions = true;
					Bot.LOGGER.log(Level.FINEST, "Out of ATTACK_POT");
				}
			}
			return false;
		});
		bankActions.add(() -> {
			if (needStrength) {
				if (Bank.contains(STRENGTH_POT.ids)) {
					if (!Bank.withdraw(STRENGTH_POT.ids, 1))
						return true;
					Time.sleep(Random.randomGuassianRange(0, 400, ConfidenceIntervals.CI_60));
				} else {
					outOfStrengthPotions = true;
					Bot.LOGGER.log(Level.FINEST, "Out of STRENGTH_POT");
				}
			}
			return false;
		});
		bankActions.add(() -> {
			if (needDefence) {
				if (Bank.contains(DEFENCE_POT.ids)) {
					if (!Bank.withdraw(DEFENCE_POT.ids, 1))
						return true;
					Time.sleep(Random.randomGuassianRange(0, 400, ConfidenceIntervals.CI_60));
				} else {
					outOfDefencePotions = true;
					Bot.LOGGER.log(Level.FINEST, "Out of DEFENCE_POT");
				}
			}
			return false;
		});
		bankActions.add(() -> {
			if (needHelmet) {
				if (Bank.contains(HELMET.ids)) {
					if (!Bank.withdraw(HELMET.ids, 1))
						return true;
					Time.sleep(Random.randomGuassianRange(0, 400, ConfidenceIntervals.CI_60));
				} else {
					outOfHelmet = true;
					Bot.LOGGER.log(Level.FINEST, "Out of HELMET");
				}
			}
			return false;
		});
		bankActions.add(() -> {
			if (needShield) {
				if (Bank.contains(SHIELD.ids)) {
					if (!Bank.withdraw(SHIELD.ids, 1))
						return true;
					Time.sleep(Random.randomGuassianRange(0, 400, ConfidenceIntervals.CI_60));
				} else {
					outOfSupplies = true;
					Bot.LOGGER.log(Level.FINEST, "Out of SHIELD");
				}
			}
			return false;
		});
		bankActions.add(() -> {
			if (needBody) {
				if (Bank.contains(BODY.ids)) {
					if (!Bank.withdraw(BODY.ids, 1))
						return true;
				} else {
					outOfBody = true;
					Bot.LOGGER.log(Level.FINEST, "Out of BODY");
				}
			}
			return false;
		});
		bankActions.add(() -> {
			if (needLegs) {
				if (Bank.contains(LEGS.ids)) {
					if (!Bank.withdraw(LEGS.ids, 1))
						return true;
					Time.sleep(Random.randomGuassianRange(0, 400, ConfidenceIntervals.CI_60));
				} else {
					outOfLegs = true;
					Bot.LOGGER.log(Level.FINEST, "Out of LEGS");
				}
			}
			return false;
		});
		bankActions.add(() -> {
			if (needWeapon) {
				if (Bank.contains(WEAPON.ids)) {
					if (!Bank.withdraw(WEAPON.ids, 1))
						return true;
					Time.sleep(Random.randomGuassianRange(0, 400, ConfidenceIntervals.CI_60));
				} else {
					outOfSupplies = true;
					Bot.LOGGER.log(Level.FINEST, "Out of WEAPON");
				}
			}
			return false;
		});
		bankActions.add(() -> {
			if (needAmulet) {
				if (Bank.contains(AMULET.ids)) {
					if (!Bank.withdraw(AMULET.ids, 1))
						return true;
					Time.sleep(Random.randomGuassianRange(0, 400, ConfidenceIntervals.CI_60));
				} else {
					outOfAmulet = true;
					Bot.LOGGER.log(Level.FINEST, "Out of AMULET");
				}
			}
			return false;
		});
	}

	@Override
	public void execute(int wildernessLevel, Tile playerLocation) {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		Memory.checkMemory(1000000); // 1 MB

		if (BreakHandler.shouldBreak()) {
			Interfaces.closeAll();
			Timer t = new Timer(Random.randomRange(10000, 15000));
			while (Login.isLoggedIn() && t.isRunning())
				Logout.clickLogout();
			BreakHandler.doBreak();
			return;
		}

		needRunEnergy = !outOfEnergyPotions && Walking.getRunEnergy() < ENERGY_MIN;
		needAttack = !outOfAttackPotions && Levels.getCurrentAttack() - Levels.getAttack() < 5;
		needStrength = !outOfStrengthPotions && Levels.getCurrentStrength() - Levels.getStrength() < 5;
		needDefence = !outOfDefencePotions && Levels.getCurrentDefence() - Levels.getDefence() < 5;
		needHelmet = !outOfHelmet && !ArrayUtil.contains(HELMET.ids, Equipment.getHelmet());
		needShield = !ArrayUtil.contains(SHIELD.ids, Equipment.getShield());
		needBody = !outOfBody && !ArrayUtil.contains(BODY.ids, Equipment.getBody());
		needLegs = !outOfLegs && !ArrayUtil.contains(LEGS.ids, Equipment.getLegs());
		needWeapon = !outOfWeapon && !ArrayUtil.contains(WEAPON.ids, Equipment.getWeapon());
		needAmulet = !outOfAmulet && !ArrayUtil.contains(AMULET.ids, Equipment.getAmulet());

		if (needHelmet || needShield || needBody || needLegs || needWeapon || needAmulet) {
			Time.sleep(800, 1200);
			needHelmet = !ArrayUtil.contains(HELMET.ids, Equipment.getHelmet());
			needShield = !ArrayUtil.contains(SHIELD.ids, Equipment.getShield());
			needBody = !ArrayUtil.contains(BODY.ids, Equipment.getBody());
			needLegs = !ArrayUtil.contains(LEGS.ids, Equipment.getLegs());
			needWeapon = !ArrayUtil.contains(WEAPON.ids, Equipment.getWeapon());
			needAmulet = !ArrayUtil.contains(AMULET.ids, Equipment.getAmulet());
		}

		acted = false;
		equipped = false;

		Collections.shuffle(inventoryActions);

		for (BooleanSupplier bs : inventoryActions) {
			if (bs.getAsBoolean())
				return;
		}

		if (equipped) {
			Time.sleep(Random.modifiedRandomGuassianRange(600, 900, ConfidenceIntervals.CI_70, 400,
					Modifiers.get("GDK.equippedSleep")));
		}

		boolean needSupplies = false;
		for (InvSupply inv : INV_SUPPLIES) {
			int quantity = 0;
			for (int i = 0; i < inv.ids.length; i++) {
				quantity += Inventory.getCount(inv.ids[i]);
			}
			if (quantity != inv.quantity.getAsInt())
				needSupplies = true;
		}

		Bot.LOGGER.log(Level.FINEST,
				String.format(
						"Need [helmet: %B, shield: %B, body: %B, legs: %B, weapon: %B, amulet: %B] [energy: %B, attack: %B, strength: %B, defence: %B] [supplies: %B] acted: %B",
						needHelmet, needShield, needBody, needLegs, needWeapon, needAmulet, needRunEnergy, needAttack,
						needStrength, needDefence, needSupplies, acted));
		if (acted) {
			return;
		}

		if (needHelmet || needShield || needBody || needLegs || needWeapon || needAmulet || needRunEnergy || needAttack
				|| needStrength || needDefence || needSupplies) {
			Bot.LOGGER.log(Level.FINEST, "Opening bank");

			if (Bank.openBooth("Bank", GDK.LUMBRIDGE_BANK_TILE_1, GDK.LUMBRIDGE_BANK_TILE_2)
					|| Bank.openBooth("Use", GDK.BARBARIAN_OUTPOST_BANK_TILE)
					|| Bank.openBooth("Bank", GDK.EDGEVILLE_BANK_TILE_1, GDK.EDGEVILLE_BANK_TILE_2)
					|| Bank.openBanker()) {
				if (!Bank.isOpen())
					return;

				// deposit items or wait for bank to load
				if (Inventory.getCount() == 0) {
					Time.sleep(600, 1200);
				} else if ((!Bank.depositInventory() || !Time.sleepUntil(() -> Inventory.getCount() == 0, 1200, 1700)))
					return;

				outOfSupplies = false;

				if (needRunEnergy || needAttack || needStrength || needDefence || needHelmet || needShield || needBody
						|| needLegs || needWeapon || needAmulet) {
					Collections.shuffle(bankActions);

					for (BooleanSupplier bs : bankActions) {
						if (bs.getAsBoolean())
							return;
					}
				} else { // supplies
					for (InvSupply inv : INV_SUPPLIES) {
						if (Inventory.isFull())
							return;
						if (Bank.getQuantity(inv.ids) >= inv.quantity.getAsInt()) {
							if (!Bank.withdraw(inv.ids, inv.quantity.getAsInt()))
								return;
							Time.sleep(Random.randomGuassianRange(0, 400, ConfidenceIntervals.CI_60));
						} else {
							outOfSupplies = true;
						}
					}
					Time.sleep(500, 800);
				}

				if (outOfSupplies) {
					if (!Navigation.atGrandExchange() && Bank.contains(GDK.VARROCK_TAB_ID)) {
						if (Inventory.getCount() == 0) {
							Time.sleep(600, 1200);
						} else if ((!Bank.depositInventory()
								|| !Time.sleepUntil(() -> Inventory.getCount() == 0, 1200, 1700)))
							return;

						if (!Bank.withdraw(GDK.VARROCK_TAB_ID, 1))
							return;

						if (!Bank.close())
							return;

						Time.sleep(Random.randomRange(400, 500));

						Item item = Inventory.getFirst(GDK.VARROCK_TAB_ID);
						if (item != null) {
							if (Clicking.interact(item, "Break")) {
								Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 4069
										|| Client.getLocalPlayer().getAnimation() == 4071, 600, 1200);
								Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == -1, 3000, 4000);
								GrandExchangeNode.start();
								return;
							}
						}
					} else {
						GrandExchangeNode.start();
						return;
					}
				}
			}
		} else {
			// done banking
			if (Clicking.interact(Inventory.getFirst(GDK.GAMES_NECKLACES_IDS), "Rub")) {
				Time.sleepUntil(() -> Widgets.exists(GDK.GAMES_NECK_WIDGET), 3000, 6000, 600);
				if (Widgets.exists(GDK.GAMES_NECK_WIDGET)) {
					Time.sleepReactionTime();
					Keyboard.sendKey(KeyEvent.VK_3);
					Time.sleepUntil(() -> Client.getLocalPlayer().getTile().distanceTo(GDK.EXIT_CORP_TILE) < 20, 3000,
							4000);
					Time.sleep(300, 600); // wait for cave to load
					return;
				}
			}
		}

	}

}
