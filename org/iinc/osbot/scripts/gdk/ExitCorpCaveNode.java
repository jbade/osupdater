package org.iinc.osbot.scripts.gdk;

import java.awt.event.KeyEvent;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Keyboard;

public class ExitCorpCaveNode implements GDKNode {

	static final int[] EXIT_CORP_WIDGET = new int[] { 219, 0 };

	@Override
	public boolean activate(int wildernessLevel, Tile playerLocation) {
		return wildernessLevel < 1 && playerLocation.distanceTo(GDK.EXIT_CORP_TILE) < 20;
	}

	@Override
	public void execute(int wildernessLevel, Tile playerLocation) {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		if (Widgets.exists(EXIT_CORP_WIDGET)) {
			Keyboard.sendKey(KeyEvent.VK_1);
			Time.sleepUntil(() -> Client.getWildernessLevel() != -1, 3000, 4000);
			return;
		}

		if (Interacting.interact(GDK.EXIT_CORP_TILE, "Exit", "Cave exit")) {
			Time.sleepUntil(() -> Widgets.exists(EXIT_CORP_WIDGET), 3000, 6000, 600);
			if (Widgets.exists(EXIT_CORP_WIDGET)) {
				Time.sleepReactionTime();
				Keyboard.sendKey(KeyEvent.VK_1);
				Time.sleepUntil(() -> Client.getWildernessLevel() > 0, 3000, 4000);
				Time.sleep(600, 1000);
			}
		}
	}
}
