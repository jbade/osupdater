package org.iinc.osbot.scripts.gdk;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.grounditem.GroundItem;
import org.iinc.osbot.api.injection.accessors.grounditem.GroundItems;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.NpcDefinition;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class LootNode implements GDKNode {

	static final RSItemConstant[] LOOT = new RSItemConstant[] { Items.GREEN_DRAGONHIDE, Items.DRAGON_BONES,
			Items.NATURE_RUNE, Items.ADAMANT_FULL_HELM, Items.MITHRIL_KITESHIELD, Items.RUNE_DAGGER,
			Items.ADAMANTITE_ORE, Items.ENSOULED_DRAGON_HEAD, Items.ENSOULED_DRAGON_HEAD_DROP, Items.GRIMY_RANARR_WEED,
			Items.GRIMY_CADANTINE, Items.GRIMY_LANTADYME, Items.GRIMY_DWARF_WEED, Items.GRIMY_KWUARM,
			Items.GRIMY_IRIT_LEAF, Items.GRIMY_AVANTOE };

	@Override
	public boolean activate(int wildernessLevel, Tile playerLocation) {
		if (wildernessLevel < 0 || Inventory.isFull() && !Inventory.contains(GDK.FOOD_IDS))
			return false;

		if (!Client.getLocalPlayer().isInteracting() && Npcs.getAll().stream().filter((npc) -> {
			if (npc == null)
				return false;
			NpcDefinition def = npc.getNpcDefinition();
			if (def == null)
				return false;
			String name = def.getName();
			return name != null && name.equals("Green dragon") && npc.getAnimation() == 92
					&& playerLocation.distanceTo(npc.getTile()) < 5;
		}).collect(Collectors.toList()).size() > 0)
			return true;

		return GroundItems.getAll(10).stream()
				.filter((gi) -> gi != null
						&& Arrays.asList(LOOT).parallelStream()
								.anyMatch((ric) -> gi.getItemId() == ric.getId() || gi.getItemId() == ric.getNotedId())
						&& GDK.DRAGON_AREA.contains(gi.getTile()))
				.collect(Collectors.toList()).size() > 0;
	}

	@Override
	public void execute(int wildernessLevel, Tile playerLocation) {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		List<GroundItem> items = GroundItems.getAll(10).stream()
				.filter((gi) -> gi != null
						&& Arrays.asList(LOOT).parallelStream()
								.anyMatch((ric) -> gi.getItemId() == ric.getId() || gi.getItemId() == ric.getNotedId())
						&& GDK.DRAGON_AREA.contains(gi.getTile()))
				.sorted().collect(Collectors.toList());

		if (items.size() > 0) {
			if (Inventory.isFull()) {
				for (int i = GDK.FOOD_IDS.length - 1; i >= 0; i--) {
					if (Inventory.contains(GDK.FOOD_IDS[i])) {
						if (Clicking.interact(Inventory.getFirst(GDK.FOOD_IDS[i]), "Eat")) {
							Time.sleep(Random.modifiedRandomGuassianRange(400, 700, ConfidenceIntervals.CI_70, 400,
									Modifiers.get("GDK.eatLootSleep")));
							break;
						}
					}
				}
				return;
			}

			GroundItem item = items.get(0);

			String name = null;
			for (RSItemConstant ric : LOOT) {
				if (ric.getId() == item.getItemId() || ric.getNotedId() == item.getItemId()) {
					name = ric.getName();
					if (Random.roll(0.35))
						break;
				}
			}

			Bot.LOGGER.log(Level.FINEST, "Looting " + name);
			if (Interacting.interact(items.get(0).getTile(), "Take", name)) {
				int ct = Inventory.getTotalQuantities();
				if (Time.sleepUntil(() -> Inventory.getTotalQuantities() != ct || Walking.isMoving(), 600, 800))
					Time.sleepUntil(() -> Inventory.getTotalQuantities() != ct || !Walking.isMoving(), 2000, 3000);
			}

		} else if (!Client.getLocalPlayer().isInteracting()) {
			List<Npc> filtered = Npcs.getAll().stream().filter((npc) -> {
				if (npc == null)
					return false;
				NpcDefinition def = npc.getNpcDefinition();
				if (def == null)
					return false;
				String name = def.getName();
				return name != null && name.equals("Green dragon") && npc.getAnimation() == 92
						&& playerLocation.distanceTo(npc.getTile()) < 5;
			}).sorted().collect(Collectors.toList());
			if (filtered.size() > 0) {
				Tile loot = filtered.get(0).getTile();
				loot = new Tile(loot.getX() - 2, loot.getY() - 2, loot.getPlane());

				if (Random.modifiedRandomGuassianRange(0, 100, ConfidenceIntervals.CI_70, 50,
						Modifiers.get("GDK.premptiveLoot")) > 40 && playerLocation.distanceTo(loot) > 0) {
					if (!Calculations.onViewport(loot.toScreen()))
						Camera.turnTo(loot);
					if (Clicking.interact(loot, "Walk here", null)
							&& Time.sleepUntil(() -> Walking.isMoving(), 600, 700)) {
						Time.sleepUntil(() -> !Walking.isMoving(), 1800, 2400);
						return;
					}
				}
			}
			Time.sleep(200, 400);
		}
	}
}
