package org.iinc.osbot.scripts.gdk;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.interfaces.tabs.CombatOptions;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.bot.Bot;

public class CombatStyleNode implements GDKNode {

	private int style = 0;

	@Override
	public boolean activate(int wildernessLevel, Tile playerLocation) {
		if (Levels.getAttack() < 70) {
			style = 0;
		} else {
			style = 1;
		}
		
		return CombatOptions.getStyle() != -1 && CombatOptions.getStyle() != style;
	}

	@Override
	public void execute(int wildernessLevel, Tile playerLocation) {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		CombatOptions.setStyle(style);
	}

}
