package org.iinc.osbot.scripts.gdk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Player;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.Random;

public class GDK implements Node {

	// 1 - random camera
	// 2 - not on viewport camera
	// 3 - drink sleep time
	// 4 - equip sleep time
	// 5 - exit corp turn camera
	// 6 - run enable % min
	// 7 - walk to loot when dragon dying
	// 8 - eating food sleep

	private ArrayList<GDKNode> nodes = new ArrayList<>();

	static boolean playerSpotted = false;

	static final int[] FOOD_IDS = new int[] { 379, 361, 333, };
	static final String[] FOOD_NAMES = new String[] { "Lobster", "Tuna", "Trout", };
	static final int[] FOOD_HEAL_AMOUNTS = new int[] { 12, 10, 7, };

	// 1 charge = 3867
	static final int[] GAMES_NECKLACES_IDS = new int[] { 3867, 3865, 3863, 3861, 3859, 3857, 3855, 3853 };
	static final int[] CHARGED_GLORY_IDS = new int[] { 1706, 1708, 1710, 1712, 11976, 11978 };
	static final int[] GAMES_NECK_WIDGET = new int[] { 219, 0 };

	static final int COINS_ID = 995;
	static final int HERB_ID = 249;
	static final int GRIMY_HERB_ID = 199;
	static final int VARROCK_TAB_ID = 8007;
	static final int SWAMP_TAR_ID = 1939;
	static final int PESTLE_AND_MOTAR_ID = 233;
	static final int BOND_ID = 13192;

	static final String VARROCK_TAB_NAME = "Varrock teleport";
	static final String SWAMP_TAR_NAME = "Swamp tar";
	static final String GRIMY_HERB_NAME = "Grimy guam leaf";
	static final String PESTLE_AND_MOTAR_NAME = "Pestle and mortar";
	static final String DUELING_RING_NAME = "Ring of dueling(8)";
	static final String STAMINA_POT_NAME = "Stamina potion(4)";
	static final String BOND_NAME = "Old school bond";

	static final Tile EXIT_CORP_TILE = new Tile(2963, 4382, 2);
	static final Tile WILDERNESS_DITCH_TILE = new Tile(3102, 3519, 0);

	static final Tile LUMBRIDGE_BANK_WALK_TILE = new Tile(3209, 3220, 2);
	static final Tile BARBARIAN_OUTPOST_WALK_TILE = new Tile(2535, 3572, 0);
	static final Tile EDGEVILLE_WALK_TILE = new Tile(3093, 3490, 0);

	static final Tile LUMBRIDGE_BANK_TILE_1 = new Tile(3209, 3221, 2);
	static final Tile LUMBRIDGE_BANK_TILE_2 = new Tile(3208, 3221, 2);
	static final Tile BARBARIAN_OUTPOST_BANK_TILE = new Tile(2537, 3573, 0);
	static final Tile EDGEVILLE_BANK_TILE_1 = new Tile(3095, 3491, 0);
	static final Tile EDGEVILLE_BANK_TILE_2 = new Tile(3095, 3489, 0);

	static final Area DRAGON_AREA = new Area(new Tile(3168, 3717, 0), new Tile(3166, 3706, 0), new Tile(3161, 3701, 0),
			new Tile(3161, 3695, 0), new Tile(3158, 3690, 0), new Tile(3133, 3690, 0), new Tile(3128, 3699, 0),
			new Tile(3128, 3717, 0));

	static final Tile CENTER_DRAGON_TILE = new Tile(3145, 3704, 0);

	{
		Bot.LOGGER.log(Level.INFO, "Starting GDK");
		Collections.addAll(nodes, new EscapeNode(), new EatingNode(), new PlayerAttackNode(), new LootNode(),
				new ExitWildernessNode(), new CombatStyleNode(), new CombatNode(), new ToDragons(),
				new ExitCorpCaveNode(), new GrandExchangeNode(), new ToBankNode(), new BankingNode());
	}

	@Override
	public boolean activate() {
		return Levels.getAttack() >= 40 && Levels.getDefence() >= 40;
	}

	@Override
	public void execute() {
		if (Walking.getRunEnergy() > Random.randomRange(20, 30))
			Walking.enableRun();

		if (Camera.getCameraPitch() < Random.randomRange(225, 375)) {
			Camera.setAngle(Camera.getCameraAngle(), true);
		}

		if (!Client.isInWilderness() && Login.getMembershipDaysLeft() < 0) {
			if (Login.isLoggedIn()) {
				Logout.clickLogout();
				Time.sleepUntil(() -> !Login.isLoggedIn(), 1200, 2000);
			}
			System.exit(0);
		}
		
		Interfaces.closeAll();

		int wildernessLevel = Client.getWildernessLevel();
		Player p = Client.getLocalPlayer();
		Tile loc = p != null ? p.getTile() : new Tile(-1, -1, -1);

		for (GDKNode node : nodes) {
			if (node.activate(wildernessLevel, loc)) {
				node.execute(wildernessLevel, loc);
				return;
			}
		}
	}

}
