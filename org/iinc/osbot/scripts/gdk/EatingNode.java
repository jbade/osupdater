package org.iinc.osbot.scripts.gdk;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class EatingNode implements GDKNode {

	@Override
	public boolean activate(int wildernessLevel, Tile playerLocation) {
		if (Bank.isOpen() || !Inventory.contains(GDK.FOOD_IDS))
			return false;

		int diff = Levels.getHitpoints() - Levels.getCurrentHitpoints();

		if (Client.getLocalPlayer().isInteracting()) {
			if (Levels.getCurrentHitpoints() <= 16) {
				for (int i = 0; i < GDK.FOOD_HEAL_AMOUNTS.length; i++) {
					if (diff >= GDK.FOOD_HEAL_AMOUNTS[i] && Inventory.contains(GDK.FOOD_IDS[i])) {
						return true;
					}
				}
			}
		} else {
			for (int i = 0; i < GDK.FOOD_HEAL_AMOUNTS.length; i++) {
				if (diff >= GDK.FOOD_HEAL_AMOUNTS[i] && Inventory.contains(GDK.FOOD_IDS[i])) {
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public void execute(int wildernessLevel, Tile playerLocation) {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		
		int diff = Levels.getHitpoints() - Levels.getCurrentHitpoints();
		for (int i = 0; i < GDK.FOOD_HEAL_AMOUNTS.length; i++) {
			if (diff >= GDK.FOOD_HEAL_AMOUNTS[i] && Inventory.contains(GDK.FOOD_IDS[i])) {
				if (Clicking.interact(Inventory.getFirst(GDK.FOOD_IDS[i]), "Eat")){
					Time.sleep(Random.modifiedRandomGuassianRange(300, 800, ConfidenceIntervals.CI_60, 200, Modifiers.get("GDK.eat")));
					return;
				}
			}
		}
	}

}
