package org.iinc.osbot.scripts.gdk;

import org.iinc.osbot.api.base.positioning.Tile;

public interface GDKNode {
	public boolean activate(int wildernessLevel, Tile playerLocation);

	public void execute(int wildernessLevel, Tile playerLocation);
}
