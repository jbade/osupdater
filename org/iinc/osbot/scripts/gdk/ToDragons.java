package org.iinc.osbot.scripts.gdk;

import java.util.logging.Level;

import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.bot.Bot;

public class ToDragons implements GDKNode {
	
	Tile[] path = new Tile[] { new Tile(3202, 3678, 0), new Tile(3183, 3693, 0), new Tile(3154, 3702, 0), };

	@Override
	public boolean activate(int wildernessLevel, Tile playerLocation) {
		return wildernessLevel > 0 && !GDK.DRAGON_AREA.contains(playerLocation);
	}

	@Override
	public void execute(int wildernessLevel, Tile playerLocation) {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		Walking.walkPath(Walking.generatePath(path, 3, true), WalkingCondition.get(), true); //TODO exact???????
	}

}
