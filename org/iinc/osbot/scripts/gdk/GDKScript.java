package org.iinc.osbot.scripts.gdk;

import java.util.ArrayList;
import java.util.Collections;

import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.input.MouseMovementHandler;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class GDKScript extends Script {

	private ArrayList<Node> nodes = new ArrayList<>();

	@Override
	public int loop() {

		if (!Login.isLoggedIn()) {
			Login.login();
			return 1000;
		}

		Interfaces.closeAll();

		for (Node node : nodes) {
			if (node.activate()) {
				node.execute();
				return 0;
			}
		}

		return Random.randomRange(50, 100);
	}

	@Override
	public void onStop() {

	}

	@Override
	public void onStart() {
		//MouseMovementHandler.DEFAULT_MOUSE_SPEED = 6;
		Modifiers.init(Bot.username.hashCode());
		BreakHandler.init(Bot.username.hashCode());

		Collections.addAll(nodes, //new RatFrog(50, 50, 50), 
				new GDK());
	}

}
