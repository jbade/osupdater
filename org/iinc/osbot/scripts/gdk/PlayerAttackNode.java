package org.iinc.osbot.scripts.gdk;

import java.util.logging.Level;

import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;

public class PlayerAttackNode implements GDKNode {
	int setting = 1107;
	
	@Override
	public boolean activate(int wildernessLevel, Tile playerLocation) {
		return Client.getSetting(1107) != 3;
	}

	@Override
	public void execute(int wildernessLevel, Tile playerLocation) {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		
		// TODO is this worth improving?
		
		if (Clicking.interact(Widgets.getWidget(548, 34))){
			Time.sleep(600, 800);
			if (Clicking.interact(Widgets.getWidget(261, 1, 6))){
				Time.sleep(600, 800);
				if (Clicking.interact(Widgets.getWidget(261, 67, 2))){
					Time.sleep(600, 800);
					if (Clicking.interact(Widgets.getWidget(261, 83, 4))){
						Time.sleep(600, 800);
					}	
				}
			}
		}
	}

}
