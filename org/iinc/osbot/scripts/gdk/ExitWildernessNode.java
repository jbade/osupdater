package org.iinc.osbot.scripts.gdk;

import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.logging.Level;

import org.iinc.osbot.api.base.interfaces.tabs.Equipment;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Keyboard;

public class ExitWildernessNode implements GDKNode {

	@Override
	public boolean activate(int wildernessLevel, Tile playerLocation) {
		return wildernessLevel > 0 && Inventory.getCount(GDK.FOOD_IDS) == 0;
	}
	
	private static int teleportAttempts = 0;

	private static int incrementTeleportAttempts() {
		return ++teleportAttempts;
	}
	
	@Override
	public void execute(int wildernessLevel, Tile playerLocation) {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		teleportAttempts = 0;
		
		Walking.blindWalk(GDK.WILDERNESS_DITCH_TILE, () -> {
			if (WalkingCondition.get().check())
				return true;

			int level = Client.getWildernessLevel();
			if (level != -1 && level <= 20 && incrementTeleportAttempts() <= 3) {
				boolean inInventory = Inventory.contains(GDK.GAMES_NECKLACES_IDS);

				if (inInventory) {
					if (Widgets.exists(GDK.GAMES_NECK_WIDGET)) {
						Keyboard.sendKey(KeyEvent.VK_2);
						if (Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 714, 600, 700)) {
							Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != 714, 3000, 4000);
							return true;
						}
					}

					if (Clicking.interact(Inventory.getFirst(GDK.GAMES_NECKLACES_IDS), "Rub")) {
						Time.sleepUntil(() -> Widgets.exists(GDK.GAMES_NECK_WIDGET), 1200, 1700, 200);
						if (Widgets.exists(GDK.GAMES_NECK_WIDGET)) {
							Time.sleepReactionTime();
							Keyboard.sendKey(KeyEvent.VK_2);
							if (Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 714, 600, 700)) {
								Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != 714, 3000, 4000);
								return true;
							}
						}
					}
				} else {
					boolean equipped = Arrays.asList(GDK.GAMES_NECKLACES_IDS).contains(Equipment.getAmulet());

					if (equipped && Clicking.interact(Equipment.getAmuletWidget(), "Barbarian Outpost")
							&& Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 714, 1200, 1400)) {
						return Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != 714, 3000, 4000);
					}
				}
			}

			return level == -1;
		});

	}
}
