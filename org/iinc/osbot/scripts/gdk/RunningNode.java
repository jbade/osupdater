package org.iinc.osbot.scripts.gdk;

import java.util.logging.Level;

import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class RunningNode implements GDKNode {

	@Override
	public boolean activate(int wildernessLevel, Tile playerLocation) {
		return !Walking.isRunEnabled() && Walking.getRunEnergy() > Random.modifiedRandomRange(15, 25, 15, Modifiers.get("GDK.run"));
	}

	@Override
	public void execute(int wildernessLevel, Tile playerLocation) {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		Walking.enableRun();
	}

}
