package org.iinc.osbot.scripts.gdk;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Condition;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Player;
import org.iinc.osbot.api.injection.accessors.renderable.Players;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.scripts.common.Navigation;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class WalkingCondition implements Condition {
	private static WalkingCondition condition = new WalkingCondition();

	public static WalkingCondition get() {
		return condition;
	}

	// return true to stop walking
	@Override
	public boolean check() {
		// we died TODO check animation
		if (Client.getLocalPlayer().getTile().distanceTo(Navigation.LUMBRIDGE_STAIRS_GROUND_FLOOR_TILE) < 70)
			return true;

		// check for players
		if (!GDK.playerSpotted) {
			int cb = Client.getLocalPlayer().getCombatLevel();
			int wild = Client.getWildernessLevel();

			int minLevel = cb - wild;
			int maxLevel = cb - wild;

			for (Player p : Players.getAll()) {
				if (!p.isSkulled())
					continue;
				
				int c = p.getCombatLevel();
				if (c >= minLevel && c <= maxLevel) {
					GDK.playerSpotted = true;
					return true;
				}
			}
		}

		// eat food
		int diff = Levels.getHitpoints() - Levels.getCurrentHitpoints();
		for (int i = 0; i < GDK.FOOD_HEAL_AMOUNTS.length; i++) {
			if (diff >= GDK.FOOD_HEAL_AMOUNTS[i] && Inventory.contains(GDK.FOOD_IDS[i])) {
				Clicking.interact(Inventory.getFirst(GDK.FOOD_IDS[i]), "Eat");
				break;
			}
		}

		if (!Walking.isRunEnabled() && Walking.getRunEnergy() > Random.modifiedRandomRange(15, 25, 15, Modifiers.get("GDK.runWalkCondition"))) {
			Walking.enableRun();
		}

		return GDK.playerSpotted;
	}

}
