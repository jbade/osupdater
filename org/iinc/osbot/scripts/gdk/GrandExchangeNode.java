package org.iinc.osbot.scripts.gdk;

import java.util.ArrayList;
import java.util.Collections;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.scripts.common.BankGoal;

public class GrandExchangeNode implements GDKNode {

	private static RSItemConstant[] sell = new RSItemConstant[0];
	static {
		ArrayList<RSItemConstant> tmp = new ArrayList<>();
		Collections.addAll(tmp, LootNode.LOOT);
		tmp.add(Items.AMULET_OF_GLORY);
		sell = tmp.toArray(sell);
	}

	private static org.iinc.osbot.scripts.common.GrandExchangeNode gen = new org.iinc.osbot.scripts.common.GrandExchangeNode(
			sell, true,
			new BankGoal[] { new BankGoal(Items.VARROCK_TELEPORT, 5, () -> true),
					new BankGoal(Items.ENERGY_POTION_4, 50, () -> true),
					new BankGoal(Items.SUPER_ATTACK_POTION_4, 10, () -> true),
					new BankGoal(Items.SUPER_STRENGTH_POTION_4, 10, () -> true),
					new BankGoal(Items.SUPER_DEFENCE_POTION_4, 10, () -> true),
					new BankGoal(Items.LOBSTER, 500, () -> true),
					new BankGoal(Items.RUNE_SCIMITAR, 4, () -> true),
					new BankGoal(Items.ANTI_DRAGON_SHIELD, 5, () -> true),
					new BankGoal(Items.RUNE_CHAINBODY, 1, () -> true),
					new BankGoal(Items.RUNE_PLATELEGS, 1, () -> true),
					new BankGoal(Items.AMULET_OF_GLORY_6, 10, () -> true),
					new BankGoal(Items.GAMES_NECKLACE_8, 15, () -> true) });

	public static void start() {
		gen.start();
	}

	@Override
	public boolean activate(int wildernessLevel, Tile playerLocation) {
		return gen.activate();
	}

	@Override
	public void execute(int wildernessLevel, Tile playerLocation) {
		gen.execute();
	}

}
