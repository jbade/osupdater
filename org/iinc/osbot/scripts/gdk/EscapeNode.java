package org.iinc.osbot.scripts.gdk;

import java.awt.event.KeyEvent;
import java.util.logging.Level;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Equipment;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.base.worlds.World;
import org.iinc.osbot.api.base.worlds.Worlds;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Player;
import org.iinc.osbot.api.injection.accessors.renderable.Players;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Keyboard;
import org.iinc.osbot.input.MouseMovementHandler;
import org.iinc.osbot.scripts.common.Navigation;
import org.iinc.osbot.util.ArrayUtil;
import org.iinc.osbot.util.Timer;

public class EscapeNode implements GDKNode {

	@Override
	public boolean activate(int wildernessLevel, Tile playerLocation) {
		if (wildernessLevel < 1)
			return false;

		if (GDK.playerSpotted)
			return true;

		int cb = Client.getLocalPlayer().getCombatLevel();
		int minLevel = cb - wildernessLevel; // TODO are these correct?
		int maxLevel = cb + wildernessLevel;

		Object localPlayerReference = Client.getLocalPlayer().getReference();
		for (Player p : Players.getAll()) {
			if (!p.isSkulled())
				continue;

			int c = p.getCombatLevel();
			if (c >= minLevel && c <= maxLevel && p.getReference() != localPlayerReference) {
				GDK.playerSpotted = true;
				return true;
			}
		}

		return false;
	}

	private static int teleportAttempts = 0;

	private static int incrementTeleportAttempts() {
		return ++teleportAttempts;
	}

	@Override
	public void execute(int wildernessLevel, Tile playerLocation) {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		//MouseMovementHandler.DEFAULT_MOUSE_SPEED = 1;
		Settings.MAKE_CLICKING_ERRORS = false;
		teleportAttempts = 0;

		while (Client.isLoggedIn() && Client.getWildernessLevel() > 0) {
			// Logout.clickLogout();
			Walking.blindWalk(GDK.WILDERNESS_DITCH_TILE, () -> {
				int level = Client.getWildernessLevel();
				if (level != -1 && incrementTeleportAttempts() <= 3) {
					int amulet = Equipment.getAmulet();
					boolean gloryEquipped = ArrayUtil.contains(GDK.CHARGED_GLORY_IDS, amulet);
					boolean gamesEquipped = ArrayUtil.contains(GDK.GAMES_NECKLACES_IDS, amulet);

					if (gamesEquipped && level <= 20) {
						if (Clicking.interact(Equipment.getAmuletWidget(), "Barbarian Outpost")
								&& Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 714, 600, 700)) {
							Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != 714, 3000, 4000);
							return true;
						}
					} else if (gloryEquipped && level <= 30) {
						if (Clicking.interact(Equipment.getAmuletWidget(), "Edgeville")
								&& Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 714, 600, 700)) {
							Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != 714, 3000, 4000);
							return true;
						}
					}

					if (!gamesEquipped && level <= 20 && Inventory.contains(GDK.GAMES_NECKLACES_IDS)) {
						// Clicking.interact(Inventory.getFirst(GDK.GAMES_NECKLACES_IDS), "Wear");
						// int invCount = Inventory.getCount();
						// Time.sleepUntil(() -> invCount != Inventory.getCount(), 600, 800);
						if (Widgets.exists(GDK.GAMES_NECK_WIDGET)) {
							Keyboard.sendKey(KeyEvent.VK_2);
							if (Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 714, 600, 700)) {
								Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != 714, 3000, 4000);
								return true;
							}
						}

						if (Clicking.interact(Inventory.getFirst(GDK.GAMES_NECKLACES_IDS), "Rub")) {
							Time.sleepUntil(() -> Widgets.exists(GDK.GAMES_NECK_WIDGET), 1200, 1700, 200);
							if (Widgets.exists(GDK.GAMES_NECK_WIDGET)) {
								Keyboard.sendKey(KeyEvent.VK_2);
								if (Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 714, 600, 700)) {
									Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != 714, 3000, 4000);
									return true;
								}
							}
						}
					} else if (!gloryEquipped && Inventory.contains(GDK.CHARGED_GLORY_IDS)) {
						Clicking.interact(Inventory.getFirst(GDK.CHARGED_GLORY_IDS), "Wear");
						int invCount = Inventory.getCount();
						Time.sleepUntil(() -> invCount != Inventory.getCount(), 600, 800);
					}

				}

				// Logout.clickLogout();
				WalkingCondition.get().check();

				return Client.getLocalPlayer().getTile().distanceTo(Navigation.LUMBRIDGE_STAIRS_GROUND_FLOOR_TILE) < 70
						|| Client.getLocalPlayer().getTile().distanceTo(GDK.BARBARIAN_OUTPOST_WALK_TILE) < 70
						|| Client.getLocalPlayer().getTile().distanceTo(GDK.EDGEVILLE_WALK_TILE) < 11;
			});
		}


		Bot.LOGGER.log(Level.FINE, "Switching worlds");
		
		Time.sleep(8000, 15000);
		
		Timer t = new Timer(60 * 1000);
		while (t.isRunning()){
			World w = Worlds.getRandomNormalWorld(true);
			if (w != null) {
				if (Logout.switchWorld(w.getId())){
					break;
				}
			}
			Time.sleep(800, 1000);
		}

		
		GDK.playerSpotted = false;
		
		if (!t.isRunning())
			System.exit(0);
	}

}
