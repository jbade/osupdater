package org.iinc.osbot.scripts.ratfrog;

import java.util.logging.Level;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class IdleNode implements Node {

	@Override
	public boolean activate() {
		return true;
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		if (Mouse.isFocused() && Random.modifiedRandomRange(0, 100, 15, Modifiers.get("RatFrog.idle1")) >= 80) {
			Mouse.moveOffscreen();
			Time.sleep(Random.modifiedRandomGuassianRange(2000, 3000, ConfidenceIntervals.CI_70, 1000,
					Modifiers.get("RatFrog.idle2")));
		} else {
			Time.sleep(400, 1500);
		}
	}
}
