package org.iinc.osbot.scripts.ratfrog;

import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.NpcDefinition;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.renderable.Players;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;

public class CombatNode implements Node {
	// handle attacking

	@Override
	public boolean activate() {
		return Client.getLocalPlayer().getAnimation() != 836 && !Client.getLocalPlayer().isInteracting();
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		
		if (Interacting.interact(() -> {

			 List<Npc> filtered = Npcs.getAll().parallelStream().filter((npc) -> {
				NpcDefinition def = npc.getNpcDefinition();
				if (def == null)
					return false;
				String name = def.getName();
				if (name == null)
					return false;
				return (name.toLowerCase().contains("frog") || name.toLowerCase().contains("rat"))
						&& npc.getAnimation() == -1 && npc.getTile().getY() < 3198
						&& !npc.isBeingInteractedWith(Players.getAll());
			}).sorted().collect(Collectors.toList());

			return filtered.parallelStream().filter((npc) -> {
				if (npc == null || npc.getInteractingReference() != Client.getLocalPlayer().getReference())
					return false;
				NpcDefinition def = npc.getNpcDefinition();
				if (def == null)
					return false;
				String name = def.getName();
				return name != null && (name.toLowerCase().contains("frog") || name.toLowerCase().contains("rat"));
			}).sorted().findFirst().orElse(filtered.parallelStream().findFirst().orElse(null));

		}, new Tile(3223, 3185, 0), new String[]{"Attack", "Dismiss"}, null)) {

			if (Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting(), 600, 800))
				Time.sleepUntil(() -> !Client.getLocalPlayer().isInteracting(), 3000, 4000);

			return;

		}
	}

}
