package org.iinc.osbot.scripts.ratfrog;

import java.util.logging.Level;

import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Navigation;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class ToRatsNode implements Node {

	static Area RAT_AREA = new Area(new Tile(3132, 3205, 0), new Tile(3160, 3206, 0), new Tile(3186, 3206, 0),
			new Tile(3194, 3198, 0), new Tile(3199, 3198, 0), new Tile(3200, 3199, 0), new Tile(3223, 3198, 0),
			new Tile(3228, 3196, 0), new Tile(3237, 3195, 0), new Tile(3237, 3190, 0), new Tile(3239, 3165, 0),
			new Tile(3228, 3150, 0), new Tile(3173, 3153, 0));
	static Area LUMBRIDGE_AREA = new Area(new Tile(3134, 3206, 0), new Tile(3137, 3263, 0), new Tile(3265, 3254, 0),
			new Tile(3261, 3160, 0), new Tile(3243, 3139, 0), new Tile(3137, 3138, 0));

	Tile[] points = new Tile[] { new Tile(3220, 3219, 0), new Tile(3230, 3219, 0), new Tile(3236, 3207, 0),
			new Tile(3244, 3191, 0), new Tile(3223, 3185, 0), };

	@Override
	public boolean activate() {
		return !RAT_AREA.contains(Client.getLocalPlayer().getTile());
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		
		if (Walking.getRunEnergy() > Random.modifiedRandomGuassianRange(20, 50, ConfidenceIntervals.CI_90, 20,
				Modifiers.get("RatFrog.enableRun")))
			Walking.enableRun();

		if (LUMBRIDGE_AREA.contains(Client.getLocalPlayer().getTile())) {
			Walking.walkPath(Walking.generatePath(points, 1, true), true);
		} else {
			Navigation.toLumbridge();
		}
	}

}
