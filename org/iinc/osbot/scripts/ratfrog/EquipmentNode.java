package org.iinc.osbot.scripts.ratfrog;

import java.util.ArrayList;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.tabs.Equipment;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.RSItem;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.GrandExchangeNode;
import org.iinc.osbot.scripts.common.Navigation;
import org.iinc.osbot.scripts.fishing.DropNode;
import org.iinc.osbot.util.Tuple;

public class EquipmentNode implements Node {
	private RSItem weapon;
	static final Tile LUMBRIDGE_BANK_TILE_1 = new Tile(3209, 3221, 2);
	static final Tile LUMBRIDGE_BANK_TILE_2 = new Tile(3208, 3221, 2);

	@Override
	public boolean activate() {
		int attack = Levels.getAttack();

		for (Tuple<Integer, RSItem> conf : RatFrog.WEAPONS) {
			if (conf.x <= attack) {
				weapon = conf.y;
			}
		}

		Time.sleepUntil(() -> Equipment.getWeapon() != -1, 300); // wait for equipment to load

		return Equipment.getWeapon() != weapon.getId() || !Inventory.containsOnly(weapon.getId());
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		if (Navigation.atGrandExchange() && Inventory.getCount() > 1) {
			if (Bank.openBanker())
				Bank.depositInventory();
			return;
		}

		if (Inventory.contains(weapon.getId())) {
			Clicking.interact(Inventory.getFirst(weapon.getId()));
			return;
		}

		if (!Navigation.atGrandExchange() && Client.getLocalPlayer().getTile().distanceTo(new Tile(3206, 3209, 0)) > 5
				&& ToRatsNode.LUMBRIDGE_AREA.contains(Client.getLocalPlayer().getTile())) {
			Walking.walkPath(Walking.generatePath(Client.getLocalPlayer().getTile(), new Tile(3206, 3209, 0), 3),
					false);
		} else if (Navigation.atGrandExchange() || Navigation.toLumbridgeBank()) {
			if (Bank.openBankLumbridge() || Bank.openBanker()) {
				if (!Bank.depositEquipment() || !Bank.depositInventory())
					return;

				Time.sleep(600, 1200);

				if (Bank.contains(weapon.getId())) {
					Bank.withdraw(weapon.getId(), 1);
				} else {
					GrandExchangeNode.start();
				}
			}
		}
	}

}
