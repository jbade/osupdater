package org.iinc.osbot.scripts.ratfrog;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.interfaces.tabs.CombatOptions;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;

public class CombatStyleNode implements Node {
	// handle style switching
	
	private int attackGoal, strengthGoal, defenceGoal;
	private int style = 0;
	
	public CombatStyleNode(int attackGoal, int strengthGoal, int defenceGoal) {
		this.attackGoal = attackGoal;
		this.strengthGoal = strengthGoal;
		this.defenceGoal = defenceGoal;
	}

	@Override
	public boolean activate() {
		int attack = Levels.getAttack();
		int strength = Levels.getStrength();
		int defence = Levels.getDefence();
		
		if (attackGoal >= 5 && attack < 5){
			style = 0;
		} else {
			for (int i = 1; i <= 99; i ++){
				if (strength == i && strength < strengthGoal){
					style = 1;
					break;
				}
				
				if (attack == i && attack < attackGoal){
					style = 0;
					break;
				}
				
				if (defence == i && defence < defenceGoal){
					style = 3;
					break;
				}
			}
		}
		
		return CombatOptions.getStyle() != style;
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
	
		CombatOptions.setStyle(style);
	}

}
