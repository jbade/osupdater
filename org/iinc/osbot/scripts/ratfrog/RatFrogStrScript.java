package org.iinc.osbot.scripts.ratfrog;

import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.Memory;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class RatFrogStrScript extends Script {

	RatFrog rf = new RatFrog(40, 99, -1);

	@Override
	public int loop() {
		if (!Login.isLoggedIn()) {
			Login.login();
			return 0;
		}

		Interfaces.closeAll();
		Memory.checkMemory(1000000L);

		if (BreakHandler.shouldBreak()) {
			Interfaces.closeAll();
			Timer t = new Timer(Random.randomRange(10000, 15000));
			while (Login.isLoggedIn() && t.isRunning())
				Logout.clickLogout();
			BreakHandler.doBreak();
		}

		if (rf.activate())
			rf.execute();

		return Random.randomRange(50, 100);
	}

	@Override
	public void onStop() {

	}

	@Override
	public void onStart() {
		Modifiers.init(Bot.username.hashCode());
		BreakHandler.init(Bot.username.hashCode());
	}

}
