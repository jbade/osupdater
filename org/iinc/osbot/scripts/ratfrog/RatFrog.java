package org.iinc.osbot.scripts.ratfrog;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.item.RSItem;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.BankGoal;
import org.iinc.osbot.scripts.common.GrandExchangeNode;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.Memory;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Tuple;

public class RatFrog implements Node {
	private int attackGoal, strengthGoal, defenceGoal;
	private List<Node> nodes = new LinkedList<Node>();

	// weapons to use. must have standard combat styles. sorted attack ^. Tuple(item, attack
	// requirement)
	@SuppressWarnings({ "unchecked", "rawtypes" })
	static Tuple<Integer, RSItem>[] WEAPONS = new Tuple[] { new Tuple(0, new RSItem("Iron scimitar", 1323)),
			new Tuple(5, new RSItem("Steel scimitar", 1325)), new Tuple(20, new RSItem("Mithril scimitar", 1329)),
			new Tuple(30, new RSItem("Adamant scimitar", 1331)), new Tuple(40, new RSItem("Rune scimitar", 1333)) };

	public RatFrog(int attackGoal, int strengthGoal, int defenceGoal) {
		this.attackGoal = attackGoal;
		this.strengthGoal = strengthGoal;
		this.defenceGoal = defenceGoal;

		Collections.addAll(nodes,
				new GrandExchangeNode(new RSItemConstant[] {}, false,
						new BankGoal[] { new BankGoal(Items.IRON_SCIMITAR, 1, () -> attackGoal >= 0),
								new BankGoal(Items.STEEL_SCIMITAR, 1, () -> attackGoal >= 5),
								new BankGoal(Items.MITHRIL_SCIMITAR, 1, () -> attackGoal >= 20),
								new BankGoal(Items.ADAMANT_SCIMITAR, 1, () -> attackGoal >= 30),
								new BankGoal(Items.RUNE_SCIMITAR, 1, () -> attackGoal >= 40) }),
				new EquipmentNode(), new ToRatsNode(),
				new CombatStyleNode(this.attackGoal, this.strengthGoal, this.defenceGoal), new CombatNode(),
				new IdleNode());
	}

	@Override
	public boolean activate() {
		return Levels.getAttack() < attackGoal || Levels.getStrength() < strengthGoal
				|| Levels.getDefence() < defenceGoal;
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		Interfaces.closeAll();
		Memory.checkMemory(1000000L);

		if (BreakHandler.shouldBreak())
			BreakHandler.doBreak();

		if (Walking.getRunEnergy() > Random.randomRange(20, 30))
			Walking.enableRun();
		
		for (Node node : nodes) {
			if (node.activate()) {
				node.execute();
				return;
			}
		}
	}

}
