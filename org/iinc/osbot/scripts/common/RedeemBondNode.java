package org.iinc.osbot.scripts.common;

import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;

public class RedeemBondNode implements Node {

	@Override
	public boolean activate() {
		return Inventory.contains(Items.BOND_UNTRADABLE.getId());
	}

	@Override
	public void execute() {
		Interfaces.closeAll();

		if (Clicking.interact(Inventory.getFirst(Items.BOND_UNTRADABLE.getId()), "Redeem")) {
			
			Time.sleepUntil(() -> Widgets.exists(66, 0), 3000, 4000);
			
			Widget[] w = Widgets.getWidgets()[66];
			if (w == null)
				return;

			boolean clicked = false;
			for (Widget child : w) {
				if (child != null && !child.isHidden()) {
					String text = child.getText();
					if (text != null && text.toLowerCase().contains("14")) {
						clicked = true;
						if (!Clicking.interact(child))
							return;
					}
				}
			}
			if (!clicked)
				return;

			Time.sleep(600, 1000);

			clicked = false;

			for (Widget child : w) {
				if (child != null && !child.isHidden()) {
					String text = child.getText();
					if (text != null && text.toLowerCase().contains("confirm")) {
						clicked = true;
						if (!Clicking.interact(child))
							return;
					}
				}
			}

			if (!clicked)
				return;

			Time.sleep(10000, 15000);
			Bot.LOGGER.info("Attempted to redeem bond.");
			if (!Inventory.contains(Items.BOND_UNTRADABLE.getId())) {
				Login.incrementMembershipDaysLeft(14);
				return;
			}
		}
	}

}
