package org.iinc.osbot.scripts.common;

import java.awt.Point;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.LongSupplier;

import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.interfaces.tabs.Stats;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.InputListeners;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class Antiban extends Script {

	private AntibanMethod[] techniques = new AntibanMethod[] {
			// alt tab
			new AntibanMethod(() -> Random.modifiedRandomGuassianRange(3 * 60 * 1000, 15 * 60 * 1000,
					ConfidenceIntervals.CI_50, 8 * 60 * 1000, Modifiers.get("antibanAltTabTime")),
					() -> Mouse.moveOffscreen()),
			// hover skill
			new AntibanMethod(() -> Random.modifiedRandomGuassianRange(30 * 60 * 1000, 90 * 60 * 1000,
					ConfidenceIntervals.CI_50, 30 * 60 * 1000, Modifiers.get("antibanHoverSkillTime")), () -> {
						if (Stats.open()) {
							Widget widget = Widgets.getWidget(320, 0);
							if (widget != null && !widget.isHidden()) {
								Mouse.move(widget.toScreen());
								Time.sleep(500, 5000);
								if (Random.roll(0.5)){
									Inventory.open();
								}
							}
						}
					}),
			// turn camera
			new AntibanMethod(
					() -> Random.modifiedRandomGuassianRange(3 * 60 * 1000, 20 * 60 * 1000, ConfidenceIntervals.CI_50,
							7 * 60 * 1000, Modifiers.get("antibanTurnCameraTime")),
					() -> Camera.setAngle(ThreadLocalRandom.current().nextDouble(0, 2 * Math.PI), true)),
			// examine
			new AntibanMethod(() -> Random.modifiedRandomGuassianRange(60 * 1000, 15 * 60 * 1000,
					ConfidenceIntervals.CI_50, 7 * 60 * 1000, Modifiers.get("antibanExamineTime")), () -> {
						int x = Random.randomGuassianRange(0, Client.getViewportWidth(), ConfidenceIntervals.CI_95);
						int y = Random.randomGuassianRange(0, Client.getViewportHeight(), ConfidenceIntervals.CI_95);

						Mouse.move(new Point(x, y));
						Time.sleepReactionTime();
						if (Menu.contains("Examine", null)) {
							Menu.interact("Examine");
						} else {
							Mouse.click(3);
							Time.sleep(100, 1000);
							Menu.close();
						}
					}),
			// random mouse movement
			new AntibanMethod(
					() -> Random.modifiedRandomGuassianRange(5 * 1000, 60 * 1000, ConfidenceIntervals.CI_50, 40 * 1000,
							Modifiers.get("antibanRandomMouseMovement")),
					() -> Mouse.moveMouse(Mouse.getLocation(), 50, 50)),
			// random tiny mouse movement
			new AntibanMethod(
					() -> Random.modifiedRandomGuassianRange(5 * 1000, 30 * 1000, ConfidenceIntervals.CI_50, 20 * 1000,
							Modifiers.get("antibanTinyRandomMouseMovement")),
					() -> Mouse.moveMouse(Mouse.getLocation(), 10, 10)), };

	@Override
	public int loop() {
		if (Bot.mouseLock.tryLock()) {
			try {
				long now = System.currentTimeMillis();
				for (AntibanMethod am : techniques) {
					if (now >= am.time) {
						am.time = now + am.delay.getAsLong();
						if (Mouse.isFocused() && Login.isLoggedIn() && !InputListeners.isInputEnabled())
							am.execute.run();
						return Random.randomRange(50, 100);
					}
				}
			} finally {
				Bot.mouseLock.unlock();
			}
		}

		return Random.randomRange(100, 300);
	}

	@Override
	public void onStop() {

	}

	@Override
	public void onStart() {
		long now = System.currentTimeMillis();
		for (AntibanMethod am : techniques) {
			am.time = now + am.delay.getAsLong();
		}
	}

	static class AntibanMethod {
		long time;
		LongSupplier delay;
		Runnable execute;

		public AntibanMethod(LongSupplier delay, Runnable execute) {
			this.delay = delay;
			this.execute = execute;
		}

	}
}
