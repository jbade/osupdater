package org.iinc.osbot.scripts.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.grandexchange.GrandExchangeInterface;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.grandexchange.GrandExchange;
import org.iinc.osbot.api.injection.accessors.grandexchange.GrandExchangeOffer;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.Email;
import org.iinc.osbot.util.Tuple;

public class GrandExchangeNode implements Node {

	private final RSItemConstant[] sell;
	private final boolean members;
	private final BankGoal[] goals;

	private static boolean active = false;
	private static boolean emptiedInventory = false;
	private static boolean emptiedEquipment = false;
	private boolean finishedSelling = false;
	private Collection<Tuple<String, Integer>> buy = new ArrayList<>();
	private RedeemBondNode redeemBondNode = new RedeemBondNode();

	public GrandExchangeNode(RSItemConstant sell[], boolean members, BankGoal[] goals) {
		this.sell = sell;
		this.members = members;
		this.goals = goals;
	}

	public static void start() {
		active = true;
	}

	@Override
	public boolean activate() {
		return active || redeemBondNode.activate() || members && Login.getMembershipDaysLeft() <= 1;
	}
	
	
	@Override
	public void execute() {
		if (redeemBondNode.activate()) {
			redeemBondNode.execute();
			return;
		}
		
		if(Bank.isOpen()){
			Bank.close();
			return;
		}

		if (GrandExchangeInterface.isOpen()){
			GrandExchangeInterface.close();
			return;
		}
		
		if (Login.getMembershipDaysLeft() == -1){
			// logout
			System.exit(0);
		}
		
		if (!Navigation.toGrandExchange())
			return;

		if (GrandExchange.getOffers().size() != 0) {
			if (GrandExchangeInterface.openClerk()) {
				GrandExchangeInterface.abortAndCollect(false);
				GrandExchangeInterface.close();
			}
			return;
		}

		if (emptiedInventory && emptiedEquipment && !finishedSelling && !Inventory.containsOnly(Items.COINS.getId())) {
			// sell items in ge
			if (GrandExchangeInterface.openClerk()) {
				if (!GrandExchangeInterface.abortAndCollect(false))
					return;
				GrandExchangeInterface.sellInventory();
				GrandExchangeInterface.collect(false);
			}
		} else if (emptiedInventory && emptiedEquipment && finishedSelling && Inventory.containsOnly(Items.COINS.getId())) {
			// buy stuff

			finishedSelling = false;
			emptiedInventory = false;

			if (!GrandExchangeInterface.openClerk())
				return;
			
			Time.sleep(400, 600);
			
			if (!GrandExchangeInterface.abortAndCollect(false))
				return;

			for (Tuple<String, Integer> item : buy) {
				Time.sleep(600, 800);
				
				int coins = Inventory.getCount(Items.COINS.getId());
				int each = coins / item.y;

				if (!GrandExchangeInterface.createBuyOffer(item.x, each, item.y)) {
					return;
				}

				if (!Time.sleepUntil(() -> {
					boolean allFinished = true;
					for (GrandExchangeOffer geo : GrandExchange.getOffers()) {
						allFinished = allFinished && geo.getState() == GrandExchangeOffer.STATES.COMPLETE;
					}
					return allFinished;
				}, 4 * 60 * 1000, 5 * 60 * 1000)) {
					Bot.LOGGER.log(Level.SEVERE, "Out of supplies");
					Email.sendEmail(Email.TYPE.SEVERE,
							"GrandExchangeNode: Unable to purchase item from ge\n" + item.x + "\n" + coins);
					System.exit(Settings.DISABLED_EXIT_CODE);
				}

				while (Login.isLoggedIn() && GrandExchangeInterface.isOpen() && !GrandExchangeInterface.collect(true)) {
					Time.sleep(500, 1200);
				}
			}

		} else {
			// withdraw
			if (Bank.openBanker()) {
				if (!emptiedEquipment) {
					emptiedEquipment = Bank.depositEquipment();
				}
				
				emptiedInventory = Bank.depositInventory();
				
				if (!emptiedEquipment || !emptiedInventory)
					return;
				
				
				updateToBuy();

				if (Bank.contains(sell)) {
					if (!Bank.withdrawAsNote())
						return;

					for (RSItemConstant item : sell) {
						if (Inventory.isFull() || Bank.contains(item.getId()) && !Bank.withdraw(item.getId(), -1))
							return;
					}
					
				} else {
					if (buy.size() != 0) {
						if (Bank.contains(Items.COINS.getId()) && !Bank.withdraw(Items.COINS.getId(), -1))
							return;
						finishedSelling = true;
					} else {
						active = false;
						finishedSelling = false;
						emptiedInventory = false;
						emptiedEquipment = false;
					}
				}
			}
		}

	}

	private void updateToBuy() {
		if (!Bank.isOpen())
			return;

		buy.clear();

		if (members && Login.getMembershipDaysLeft() <= 2) {
			buy.add(new Tuple<String, Integer>(Items.BOND_UNTRADABLE.getName(), 1));
		}

		int goal;
		for (BankGoal bg : goals) {
			if (bg.include.get()) {
				goal = bg.quantity - Bank.getQuantity(bg.id);
				if (goal > 0) {
					buy.add(new Tuple<String, Integer>(bg.name, goal));
				}
			}
		}
	}

}
