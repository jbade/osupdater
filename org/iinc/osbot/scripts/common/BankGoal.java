package org.iinc.osbot.scripts.common;

import java.util.function.Supplier;

import org.iinc.osbot.api.base.item.Items.RSItemConstant;

public class BankGoal {
	int quantity, id;
	String name;
	Supplier<Boolean> include;

	public BankGoal(RSItemConstant itemConstant, int quantity, Supplier<Boolean> include) {
		this.quantity = quantity;
		this.id = itemConstant.getId();
		this.name = itemConstant.getName();
		this.include = include;
	}
}