package org.iinc.osbot.scripts.common;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.ChatBox;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.interfaces.tabs.Magic;
import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.util.ArrayUtil;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;
import org.iinc.osbot.util.Tuple;

public class Navigation {

	public final static Tile LUMBRIDGE_STAIRS_GROUND_FLOOR_TILE = new Tile(3206, 3209, 0);
	final static Tile LUMBRIDGE_STAIRS_MIDDLE_FLOOR_TILE = new Tile(3205, 3209, 1);
	final static Tile LUMBRIDGE_STAIRS_TOP_FLOOR_TILE = new Tile(3205, 3209, 2);
	final static int LUMBRIDGE_STAIRS_GROUND_FLOOR_ID = 16671;
	final static int LUMBRIDGE_STAIRS_MIDDLE_FLOOR_ID = 16672;
	final static int LUMBRIDGE_STAIRS_TOP_FLOOR_ID = 16673;
	final static Area LUMBRIDGE_CASTLE_AREA = new Area(new Tile(3198, 3234, 0), new Tile(3198, 3201, 0),
			new Tile(3224, 3200, 0), new Tile(3231, 3212, 0), new Tile(3230, 3231, 0), new Tile(3213, 3238, 0),
			new Tile(3203, 3238, 0));

	final static Tile LUMBRIDGE_BANK_TILE = new Tile(3209, 3220, 2);
	final static Tile LUMBRIDGE_STAIRS_WALK_FLOOR_TILE = new Tile(3206, 3210, 0);
	final Tile LUMBRIDGE_BANK_TILE_1 = new Tile(3209, 3221, 2);
	final Tile LUMBRIDGE_BANK_TILE_2 = new Tile(3208, 3221, 2);

	final static Area LUMBRIDGE_SWAMP_AREA = new Area(new Tile(3145, 3215, 0), new Tile(3128, 3202, 0),
			new Tile(3134, 3143, 0), new Tile(3146, 3132, 0), new Tile(3247, 3137, 0), new Tile(3255, 3180, 0),
			new Tile(3260, 3213, 0), new Tile(3246, 3221, 0), new Tile(3230, 3219, 0), new Tile(3226, 3208, 0),
			new Tile(3226, 3197, 0), new Tile(3195, 3198, 0), new Tile(3186, 3207, 0), new Tile(3161, 3208, 0),
			new Tile(3154, 3216, 0));
	final static Tile[] LUMBRIDGE_SWAMP_TO_LUMBRIDGE_CASTLE_PATH_POINTS = new Tile[] { new Tile(3243, 3186, 0),
			new Tile(3239, 3202, 0), new Tile(3235, 3210, 0), new Tile(3228, 3219, 0), new Tile(3223, 3219, 0), };

	final static Area LUMBRIDGE_RIVER_FISHING_AREA = new Area(new Tile(3226, 3231, 0), new Tile(3239, 3232, 0),
			new Tile(3235, 3262, 0), new Tile(3252, 3262, 0), new Tile(3252, 3254, 0), new Tile(3265, 3254, 0),
			new Tile(3268, 3231, 0), new Tile(3267, 3203, 0), new Tile(3264, 3200, 0), new Tile(3248, 3222, 0),
			new Tile(3238, 3215, 0), new Tile(3227, 3215, 0));
	final static Tile[] LUMBRIDGE_RIVER_FISHING_TO_LUMBRIDGE_CASTLE_PATH_POINTS = new Tile[] { new Tile(3245, 3257, 0),
			new Tile(3257, 3227, 0), new Tile(3240, 3225, 0), new Tile(3226, 3218, 0) };

	static final Tile GRAND_EXCHANGE_CENTER = new Tile(3164, 3489, 0);
	static final Area GRAND_EXCHANGE_REACHABLE_AREA = new Area(new Tile[] { new Tile(3197, 3517, 0),
			new Tile(3135, 3518, 0), new Tile(3125, 3458, 0), new Tile(3142, 3384, 0), new Tile(3155, 3345, 0),
			new Tile(3206, 3318, 0), new Tile(3197, 3199, 0), new Tile(3256, 3193, 0), new Tile(3277, 3330, 0),
			new Tile(3312, 3329, 0), new Tile(3312, 3360, 0), new Tile(3306, 3363, 0), new Tile(3304, 3383, 0),
			new Tile(3297, 3386, 0), new Tile(3296, 3454, 0), new Tile(3312, 3455, 0), new Tile(3318, 3467, 0),
			new Tile(3311, 3477, 0), new Tile(3313, 3521, 0) });

	public static boolean atLumbridgeBank() {
		return Client.getLocalPlayer().getTile().distanceTo(LUMBRIDGE_BANK_TILE) < 3;
	}

	public static boolean atGrandExchange() {
		return Client.getLocalPlayer().getTile().distanceTo(GRAND_EXCHANGE_CENTER) < 5;
	}

	public static boolean toLumbridgeBank() {
		Timer timer = new Timer(Random.randomRange(30000, 40000));

		while (timer.isRunning() && Login.isLoggedIn()
				&& Client.getLocalPlayer().getTile().distanceTo(LUMBRIDGE_BANK_TILE) >= 3) {
			Tile loc = Client.getLocalPlayer().getTile();

			if (Walking.getRunEnergy() > Random.randomRange(10, 30))
				Walking.enableRun();

			if (LUMBRIDGE_RIVER_FISHING_AREA.contains(loc)) {
				Walking.walkPath(Walking.generatePath(LUMBRIDGE_RIVER_FISHING_TO_LUMBRIDGE_CASTLE_PATH_POINTS, 3, true),
						true);
				Time.sleepUntil(() -> Walking.isMoving(), 600, 1200);
				Time.sleepUntil(() -> !Walking.isMoving(), 10000, 15000);
			} else if (LUMBRIDGE_SWAMP_AREA.contains(loc)) {
				Walking.walkPath(Walking.generatePath(LUMBRIDGE_SWAMP_TO_LUMBRIDGE_CASTLE_PATH_POINTS, 2, true), true);
				Time.sleepUntil(() -> Walking.isMoving(), 600, 1200);
				Time.sleepUntil(() -> !Walking.isMoving(), 10000, 15000);
			} else if (loc.distanceTo(LUMBRIDGE_BANK_TILE) < 30) {
				Walking.blindWalk(LUMBRIDGE_BANK_TILE);
				Time.sleepUntil(() -> Walking.isMoving(), 600, 1200);
				Time.sleepUntil(() -> !Walking.isMoving(), 10000, 15000);
			} else if (loc.distanceTo(LUMBRIDGE_STAIRS_MIDDLE_FLOOR_TILE) < 20) {
				if (Interacting.interact(() -> RSObjects.getGameObjects(5).parallelStream()
						.filter((go) -> go.getObjectId() == LUMBRIDGE_STAIRS_MIDDLE_FLOOR_ID).findFirst().orElse(null),
						LUMBRIDGE_STAIRS_MIDDLE_FLOOR_TILE, "Climb-up", "Staircase")) {
					Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
					Time.sleepUntil(() -> Client.getPlane() != 1, 1200, 1800);
				}
			} else if (LUMBRIDGE_CASTLE_AREA.contains(Client.getLocalPlayer().getTile())) {
				if (Interacting.interact(() -> RSObjects.getGameObjects(5).parallelStream()
						.filter((go) -> go.getObjectId() == LUMBRIDGE_STAIRS_GROUND_FLOOR_ID).findFirst().orElse(null),
						LUMBRIDGE_STAIRS_GROUND_FLOOR_TILE, "Climb-up", "Staircase")) {
					Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
					Time.sleepUntil(() -> Client.getPlane() != 0, 1200, 1800);
				}
			} else {
				lumbridgeHomeTeleport();
				timer.reset();
			}
		}

		return Login.isLoggedIn() && Client.getLocalPlayer().getTile().distanceTo(LUMBRIDGE_BANK_TILE) < 3;
	}

	public static boolean toLumbridge() {
		Tile loc = Client.getLocalPlayer().getTile();
		Timer timer = new Timer(Random.randomRange(90000, 100000));

		while (timer.isRunning() && Login.isLoggedIn()
				&& !LUMBRIDGE_CASTLE_AREA.contains(Client.getLocalPlayer().getTile())) {

			if (Walking.getRunEnergy() > Random.randomRange(10, 30))
				Walking.enableRun();

			loc = Client.getLocalPlayer().getTile();

			if (loc.distanceTo(LUMBRIDGE_STAIRS_TOP_FLOOR_TILE) < 40) {
				if (Interacting.interact(() -> RSObjects.getGameObjects(5).parallelStream()
						.filter((go) -> go.getObjectId() == LUMBRIDGE_STAIRS_TOP_FLOOR_ID).findFirst().orElse(null),
						LUMBRIDGE_STAIRS_TOP_FLOOR_TILE, "Climb-down", "Staircase")) {
					Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
					Time.sleepUntil(() -> Client.getPlane() != 2, 1200, 1800);
				}
			} else if (loc.distanceTo(LUMBRIDGE_STAIRS_MIDDLE_FLOOR_TILE) < 40) {
				if (Interacting.interact(() -> RSObjects.getGameObjects(5).parallelStream()
						.filter((go) -> go.getObjectId() == LUMBRIDGE_STAIRS_MIDDLE_FLOOR_ID).findFirst().orElse(null),
						LUMBRIDGE_STAIRS_MIDDLE_FLOOR_TILE, "Climb-down", "Staircase")) {
					Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
					Time.sleepUntil(() -> Client.getPlane() != 1, 1200, 1800);
				}
			} else {
				lumbridgeHomeTeleport();
			}
		}

		return LUMBRIDGE_CASTLE_AREA.contains(Client.getLocalPlayer().getTile());
	}

	public static boolean lumbridgeHomeTeleport() {
		Timer timer = new Timer(Random.randomRange(30000, 40000));
		int attempts = 0;
		List<String> prev = ChatBox.getMessages(10);

		while (timer.isRunning() && Login.isLoggedIn()
				&& !LUMBRIDGE_CASTLE_AREA.contains(Client.getLocalPlayer().getTile())) {
			if (Client.isInWilderness() && (Client.getWildernessLevel() > 20 || attempts >= 3)) {
				// TODO check plane
				int lvl = Client.getWildernessLevel();
				if (Walking.getRunEnergy() > Random.randomRange(10, 30))
					Walking.enableRun();
				Walking.walkPath(
						Walking.generatePath(Client.getLocalPlayer().getTile(),
								new Tile(Client.getLocalPlayer().getTile().getX(), 3521, 0), 5),
						() -> !Client.isInWilderness() || (lvl > 20 && Client.getWildernessLevel() <= 20), false);
				timer.reset();
			}

			List<Integer> delay = ChatBox.getNewMessages(prev).stream()
					.filter((s) -> s.startsWith("You need to wait another"))
					.map((s) -> Integer.valueOf(s.replaceAll("[^\\d]", ""))).collect(Collectors.toList());
			if (delay.size() > 0) {
				while (Login.isLoggedIn()) {
					Logout.clickLogout();
					Time.sleep(3000, 5000);
				}
				Time.sleep(delay.get(0) * 60 * 1000, (delay.get(0) + 3) * 60 * 1000);
				Login.login();
				Time.sleep(600, 1200);

				timer.reset();
				prev = ChatBox.getMessages(10);
				attempts = 0;
				continue;
			}

			if (Magic.cast(Magic.SPELL.LUMBRIDGE_HOME_TELEPORT)) {
				attempts++;
				if (Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != -1, 4000, 6000))
					Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == -1, 15000, 20000);
			}
		}

		return Login.isLoggedIn() && LUMBRIDGE_CASTLE_AREA.contains(Client.getLocalPlayer().getTile());
	}

	public static boolean toGrandExchange() {
		Timer timer = new Timer(Random.randomRange(90000, 100000));

		while (timer.isRunning() && Login.isLoggedIn()
				&& Client.getLocalPlayer().getTile().distanceTo(GRAND_EXCHANGE_CENTER) >= 5) {

			if (Walking.getRunEnergy() > Random.randomRange(10, 30))
				Walking.enableRun();

			Tile loc = Client.getLocalPlayer().getTile();

			if (GRAND_EXCHANGE_REACHABLE_AREA.contains(loc)) {
				Time.sleep(600, 1200);
				Walking.walkPath(
						ArrayUtil.concatAll(
								// to chicken pen
								Walking.generatePath(new Tile[] { new Tile(3223, 3219, 0), new Tile(3232, 3230, 0),
										new Tile(3232, 3262, 0), new Tile(3239, 3281, 0) }, 1, false),
								// between chicken pen and cows
								new Tile[] { new Tile(3237, 3285, 0), new Tile(3239, 3291, 0),
										new Tile(3239, 3301, 0) },
								// from chicken pen to grand exchange
								Walking.generatePath(new Tile[] { new Tile(3233, 3308, 0), new Tile(3210, 3336, 0),
										new Tile(3201, 3359, 0), new Tile(3182, 3380, 0), new Tile(3171, 3400, 0),
										new Tile(3175, 3445, 0), new Tile(3165, 3467, 0), new Tile(3164, 3487, 0) }, 1,
										true)),
						true);
			} else {
				toLumbridge();
			}
		}

		return Login.isLoggedIn() && Client.getLocalPlayer().getTile().distanceTo(GRAND_EXCHANGE_CENTER) < 5;
	}
}
