package org.iinc.osbot.scripts.common;

import java.util.LinkedList;
import java.util.stream.Collectors;

import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;

public class Interfaces {

	private static int[][] BLACKLIST_CLOSE_ALL = new int[][] { { 69, 3 } };

	public static void closeAll() {
		Widget[][] w = Widgets.getWidgets();

		LinkedList<Widget> queue = new LinkedList<Widget>();

		if (w != null) {
			for (int i = 0; i < w.length; i++) {
				for (int j = 0; j < w[i].length; j++) {
					if (w[i][j] != null) {
						
						boolean blacklisted = false;
						for (int[] arr : BLACKLIST_CLOSE_ALL) {
							if (arr[0] == i && (arr.length < 2 || arr[1] == j)) {
								blacklisted = true;
								break;
							}
						}
						
						if (!blacklisted)
							queue.add(w[i][j]);
						
					}
				}
			}
		}

		while (!queue.isEmpty()) {
			Widget widget = queue.remove();
			
			for (Widget w2 : widget.getChildren()) {
				if (w2 != null)
					queue.add(w2);
			}

			if (!widget.isHidden() && widget.getActions().contains("Close"))
				if (!Clicking.interact(widget, "Close"))
					return;
		}

		return;
	}
	
}
