package org.iinc.osbot.scripts.hunter;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class UpdateConfigNode implements ContinuableNode {

	private int currentSpotIndex = 0;
	
	@Override
	public boolean activate() {
		return Hunter.config == null || Random.modifiedRandomRange(0, 100, 40, Modifiers.get("Hunter.updateConfig")) >= 60;
	}

	@Override
	public boolean execute() {
		HuntingConfig next = null;
		int hunterLevel = Levels.getCurrentHunter();
		for (HuntingConfig cfg : Hunter.CONFIGS) {
			if (hunterLevel >= cfg.getLevelRequirement()) {
				next = cfg;
			}
		}

		if (next != Hunter.config) {
			Hunter.config = next;
			
			currentSpotIndex = (int)(Math.abs(Modifiers.get("Hunter.currentSpot")) * next.getSpots().size());
			Hunter.spot = next.getSpots().get(currentSpotIndex);
			Hunter.traps.clear();
			return false;
		} else if (Client.getLocalPlayer().getTile().distanceTo(Hunter.spot.getCenter()) <= 20) {

			long now = System.currentTimeMillis();
			int traps = (int) Hunter.spot.getTiles().stream().filter((ht) -> ht.getLastUsed() + 30000 > now).count();
			traps += Hunter.getTrapLimit() - Hunter.traps.size();
			
			if (traps > 5 * Hunter.spot.getSpawnTiles().size()){
				// if more than 5 traps per spawn we should move spots
				
				currentSpotIndex++;
				
				if (currentSpotIndex >= next.getSpots().size()){
					currentSpotIndex = 0;
				}
				
				if  (currentSpotIndex == (int)(Math.abs(Modifiers.get("Hunter.currentSpot")) * next.getSpots().size())){
					// hop worlds
					Bot.LOGGER.log(Level.FINEST, "Hopping worlds");
					System.exit(0);
					// TODO pick up traps	
				}
				
				Hunter.spot = Hunter.config.getSpots().get(currentSpotIndex);
				Hunter.traps.clear();
				// TODO pick up traps
			}
		}
		
		return true;
	}

}
