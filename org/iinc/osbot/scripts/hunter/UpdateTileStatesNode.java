package org.iinc.osbot.scripts.hunter;

import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;

import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.util.ArrayUtil;

public class UpdateTileStatesNode implements ContinuableNode {

	@Override
	public boolean activate() {
		return Hunter.config != null;
	}

	@Override
	public boolean execute() {
		if (Hunter.checkNextAction()) {
			Set<Tile> takenTiles = RSObjects.getGameObjects(10).parallelStream().filter((obj) -> {
				int id = obj.getObjectId();
				return id == Hunter.config.getFailedTrapObjectId() || id == Hunter.config.getSetTrapNpcId()
						|| id == Hunter.config.getSuccessTrapObjectId()
						|| id == Hunter.config.getTransitionFailedTrapObjectId()
						|| ArrayUtil.contains(Hunter.config.getTransitionSuccessTrapObjectIds(), id);
			}).map((obj) -> obj.getTile()).collect(Collectors.toSet());

			Iterator<Tile> it = Hunter.traps.iterator();
			while (it.hasNext()) {
				if (!takenTiles.contains(it.next())) {
					it.remove();
				}
			}

			long now = System.currentTimeMillis();
			for (HuntingTile tile : Hunter.spot.getTiles()) {
				if (takenTiles.contains(tile.getTile()) && !Hunter.traps.contains(tile.getTile())) {
					tile.setLastUsed(now);
				}
			}
		}
		return true;
	}

}
