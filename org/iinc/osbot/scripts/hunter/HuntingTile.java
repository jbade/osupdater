package org.iinc.osbot.scripts.hunter;

import java.util.ArrayList;

import org.iinc.osbot.api.base.positioning.Tile;

public class HuntingTile implements Comparable<HuntingTile> {
	private final Tile tile;
	private final double spawnRating;
	private double rating;
	private long lastUsed;

	public HuntingTile(Tile tile, ArrayList<Tile> spawnTiles) {
		this.tile = tile;
		int tempSpawnRating = 0;
		for (Tile spawn : spawnTiles) {
			tempSpawnRating += calculateSpawnRating(tile, spawn);
		}
		spawnRating = tempSpawnRating;
	}

	private static double calculateSpawnRating(Tile tile, Tile spawn) {
		double dist = tile.distanceTo(spawn);
		double f = 0;

		// https://www.wolframalpha.com/input/?i=fit+curve+(0,+30),+(1,+100),+(2,+50),+(3,+25),+(4,+12),+(5,6),+(6,+2)
		if (dist <= 6) {
			f = -29.0 / 120 * Math.pow(dist, 6) + 601.0 / 120 * Math.pow(dist, 5) - 983.0 / 24 * Math.pow(dist, 4)
					+ 3995.0 / 24 * Math.pow(dist, 3) - 3403.0 / 10 * Math.pow(dist, 2) + 8401.0 / 30 * dist + 30;
		}

		return f;
	}

	public void updateRating(Iterable<Tile> traps) {
		rating = spawnRating;
		double avg = 0;
		int size = 0;
		for (Tile trap : traps) {
			avg += calculateTrapRating(tile, trap);
			size++;
		}
		if (size > 0){
			avg /= size;
			rating += avg;
		}
	}

	private static double calculateTrapRating(Tile tile, Tile trap) {
		double dist = tile.distanceTo(trap);
		double f = 0;

		// https://www.wolframalpha.com/input/?i=fit+curve+(1,+35),+(2,+80),+(3,+20)
		if (dist >= 1 && dist <= 3) {
			f = -105.0 / 2 * Math.pow(dist, 2) + 405.0 / 2 * dist - 115;
		}

		return f;
	}

	public long getLastUsed() {
		return lastUsed;
	}

	public void setLastUsed(long lastUsed) {
		this.lastUsed = lastUsed;
	}

	public Tile getTile() {
		return tile;
	}

	public double getRating() {
		return rating;
	}

	
	public double getSpawnRating() {
		return spawnRating;
	}

	@Override
	public int compareTo(HuntingTile o) {
		return (int) (rating - o.rating);
	}
}
