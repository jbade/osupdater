package org.iinc.osbot.scripts.hunter;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.tabs.Equipment;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.scripts.common.Navigation;
import org.iinc.osbot.util.ArrayUtil;
import org.iinc.osbot.util.Random;

public class BankingNode implements ContinuableNode {

	@Override
	public boolean activate() {
		Tile loc = Client.getLocalPlayer().getTile();
		return loc.distanceTo(Hunter.CASTLE_WARS_BANK_WALK_TILE) <= 3 || Navigation.atLumbridgeBank()
				|| Navigation.atGrandExchange();
	}

	@Override
	public boolean execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		Tile loc = Client.getLocalPlayer().getTile();

		if (Bank.close()) {
			boolean needStamina = !Walking.isStaminaActive();
			boolean needDuelingRing = !ArrayUtil.contains(Hunter.DUELING_RING_IDS, Equipment.getRing());
			boolean needSupplies = Inventory.getCount(Hunter.config.getTrapItemId()) == 0
					|| (Levels.getCurrentHerblore() >= 19 && (Inventory.getCount(Hunter.SWAMP_TAR_ID) == 0
							|| Inventory.getCount(Hunter.GRIMY_HERB_ID) == 0
							|| Inventory.getCount(Hunter.PESTLE_AND_MOTAR_ID) == 0));
			boolean acted = false;

			if (needDuelingRing && Inventory.contains(Hunter.DUELING_RING_IDS)) {
				Clicking.interact(Inventory.getFirst(Hunter.DUELING_RING_IDS), "Wear");
				acted = true;
			}

			if (needStamina && Inventory.contains(Hunter.STAMINA_POT_IDS)) {
				Clicking.interact(Inventory.getFirst(Hunter.STAMINA_POT_IDS), "Drink");
				Time.sleep(1200, 1400);
				acted = true;
			}

			if (acted) {
				Time.sleep(600, 800);
				return false;
			}

			if (needStamina || needSupplies || needDuelingRing) {
				if ((loc.getPlane() == 2
						&& Bank.openBooth("Bank", Hunter.LUMBRIDGE_BANK_TILE_1, Hunter.LUMBRIDGE_BANK_TILE_2))
						|| (loc.getPlane() == 0 && Bank.openBooth("Use", Hunter.CASTLE_WARS_BANK_TILE_1)
								|| Bank.openBanker())) {
					if (!Bank.isOpen())
						return false;

					// deposit items or wait for bank to load
					if (Inventory.getCount() == 0) {
						Time.sleep(600, 1200);
					} else if ((!Bank.depositInventory()
							|| !Time.sleepUntil(() -> Inventory.getCount() == 0, 1200, 1700)))
						return false;

					boolean outOfSupplies = false;
					if (needDuelingRing || needStamina) {
						if (needDuelingRing) {
							if (Bank.contains(Hunter.DUELING_RING_IDS)) {
								if (!Bank.withdraw(Hunter.DUELING_RING_IDS, 1))
									return false;
							} else {
								outOfSupplies = true;
								Bot.LOGGER.log(Level.FINEST, "Out of DUELING_RING");
							}
						}

						if (needStamina) {
							if (Bank.contains(Hunter.STAMINA_POT_IDS)) {
								if (!Bank.withdraw(Hunter.STAMINA_POT_IDS, 1))
									return false;
							} else {
								outOfSupplies = true;
								Bot.LOGGER.log(Level.FINEST, "Out of STAMINA_POT");
							}
						}
					} else if (needSupplies) {

						// get 3 tick supplies
						if (Levels.getCurrentHerblore() >= 19) {
							if (Bank.contains(Hunter.GRIMY_HERB_ID)) {
								if (!Bank.withdraw(Hunter.GRIMY_HERB_ID, 8))
									return false;
							} else {
								outOfSupplies = true;
								Bot.LOGGER.log(Level.FINEST, "Out of SWAMP_TAR");
							}
							if (Bank.contains(Hunter.SWAMP_TAR_ID)) {
								if (!Bank.withdraw(Hunter.SWAMP_TAR_ID, 120))
									return false;
							} else {
								outOfSupplies = true;
								Bot.LOGGER.log(Level.FINEST, "Out of SWAMP_TAR");
							}

							if (Bank.contains(Hunter.PESTLE_AND_MOTAR_ID)) {
								if (!Bank.withdraw(Hunter.PESTLE_AND_MOTAR_ID, 1))
									return false;
							} else {
								outOfSupplies = true;
								Bot.LOGGER.log(Level.FINEST, "Out of PESTLE_AND_MOTAR");
							}
						}

						if (needSupplies) {
							if (Bank.contains(Hunter.config.getTrapItemId())) {
								Time.sleep(600, 800);
								if (!Bank.withdraw(Hunter.config.getTrapItemId(),
										28 - 2 * Hunter.config.getRequiredInvSlots() - Inventory.getCount())) {
									Time.sleep(600, 800);
									return false;
								}

							} else {
								outOfSupplies = true;
								Bot.LOGGER.log(Level.FINEST, "Out of TRAPS");
							}
						}
					}

					if (outOfSupplies) {
						Bot.LOGGER.log(Level.SEVERE, "Out of supplies");

						if (!Navigation.atGrandExchange() && Bank.contains(Hunter.VARROCK_TAB_ID)) {
							if (!Bank.withdraw(Hunter.VARROCK_TAB_ID, 1))
								return false;

							if (Inventory.getCount() == 0) {
								Time.sleep(600, 1200);
							} else if ((!Bank.depositInventory()
									|| !Time.sleepUntil(() -> Inventory.getCount() == 0, 1200, 1700)))
								return false;

							if (!Bank.close())
								return false;

							Time.sleep(Random.randomRange(400, 500));

							Item item = Inventory.getFirst(Hunter.VARROCK_TAB_ID);
							if (item != null) {
								if (Clicking.interact(item, "Break")) {
									Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 4069
											|| Client.getLocalPlayer().getAnimation() == 4071, 600, 1200);
									Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == -1, 3000, 4000);
									GrandExchangeNode.needResupply = true;
									return false;
								}
							}
						} else {
							GrandExchangeNode.needResupply = true;
							return false;
						}

					}

				}
			} else {
				if (Client.getLocalPlayer().getTile().distanceTo(Hunter.CASTLE_WARS_BANK_WALK_TILE) > 10) {
					if (ArrayUtil.contains(Hunter.DUELING_RING_IDS, Equipment.getRing())) {
						if (Clicking.interact(Equipment.getRingWidget(), "Castle Wars")
								&& Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 714, 600, 700)) {
							Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != 714, 3000, 4000);
							return false;
						}
					}
				} else {
					return true;
				}
			}
		}
		return false;
	}

}
