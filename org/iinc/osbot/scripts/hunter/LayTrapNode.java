package org.iinc.osbot.scripts.hunter;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.ChatBox;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.api.injection.accessors.renderable.Player;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class LayTrapNode implements ContinuableNode {

	private static long decreasedTrapExpiration;
	private static int decreasedTrapCount;

	@Override
	public boolean activate() {

		int hunterLevel = Levels.getCurrentHunter();
		int trapLimit = 1;
		if (hunterLevel >= 80) {
			trapLimit = 5;
		} else if (hunterLevel >= 60) {
			trapLimit = 4;
		} else if (hunterLevel >= 40) {
			trapLimit = 3;
		} else if (hunterLevel >= 20) {
			trapLimit = 2;
		}

		if (decreasedTrapExpiration > System.currentTimeMillis()) {
			trapLimit -= decreasedTrapCount;
		} else {
			decreasedTrapCount = 0;
		}

		return Hunter.traps.size() < trapLimit;
	}

	@Override
	public boolean execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		Hunter.spot.updateRating(Hunter.traps);
		long now = System.currentTimeMillis();
		HuntingTile temp = Hunter.spot.getTiles().stream()
				.filter((ht) -> ht.getLastUsed() + 30000 < now && !Hunter.traps.contains(ht.getTile()))
				.sorted((a, b) -> {
					double d = b.getRating() - a.getRating();
					if (d == 0)
						return 0;
					if (d < 0)
						return -1;
					return 1;
				}).findFirst().orElse(null);
		if (temp == null) {
			Bot.LOGGER.log(Level.SEVERE, "No place for traps?");
			return false;
		}

		Player lp = Client.getLocalPlayer();
		Tile tile = temp.getTile();

		if (!(tile.equals(Walking.getDestinationTile()) && Walking.isMoving()
				|| Walking.getDestinationTile() == null && lp.getTile().equals(tile))) {
			if (Calculations.onViewport(tile.toScreen())) {
				Timer timer = new Timer(Random.randomRange(5000, 7000));
				do {
					Mouse.move(tile.toScreen(), 3);

					Point ms = tile.toScreen();
					if (Calculations.distance(ms.x, ms.y, Mouse.getLocation().x, Mouse.getLocation().y) > 3)
						Mouse.move(tile.toScreen(), 3);

					if (Hunter.interactMenuSleep(new String[] { "Walk here" }, null)) {
						if (Inventory.contains(Hunter.GRIMY_HERB_ID, Hunter.HERB_ID)
								&& Inventory.getCount(Hunter.SWAMP_TAR_ID) >= 15
								&& Inventory.contains(Hunter.PESTLE_AND_MOTAR_ID)) {
							if (Inventory.contains(Hunter.HERB_ID))
								Mouse.move(Inventory.getBounds(Inventory.getFirst(Hunter.HERB_ID).getIndex()));
						} else {
							Mouse.move(
									Inventory.getBounds(Inventory.getFirst(Hunter.config.getTrapItemId()).getIndex()));
						}
						Time.sleepUntil(
								() -> tile.equals(Walking.getDestinationTile())
										|| Client.getLocalPlayer().getTile().equals(tile),
								Random.modifiedRandomGuassianRange(1200, 1400, ConfidenceIntervals.CI_50, 300,
										Modifiers.get("Hunter.Lay.walkSleep")));
					}

				} while (!(tile.equals(Walking.getDestinationTile())
						|| Walking.getDestinationTile() == null && Client.getLocalPlayer().getTile().equals(tile))
						&& timer.isRunning() && Calculations.onViewport(tile.toScreen()));
				// no isMoving check for do while
			} else {
				Camera.turnTo(tile);
				Hunter.sleepUntilNextAction();
				if (!Calculations.onViewport(tile.toScreen())) {
					Walking.blindWalk(tile);
				}
				return false;
			}
		}

		Tile dest = Walking.getDestinationTile();
		if (tile.equals(dest)) {
			if (!Time.sleepUntil(() -> Walking.isMoving(), Random.modifiedRandomGuassianRange(650, 800,
					ConfidenceIntervals.CI_70, 300, Modifiers.get("Hunter.Lay.movingSleep"))))
				return false;
			Time.sleepUntil(() -> lp.getTile().distanceTo(tile) == 0, 1200, 1600);
		} else if (dest == null && !lp.getTile().equals(tile)) {
			return false;
		}

		if (Walking.isMoving()) {
			if (lp.getTile().distanceTo(tile) != 0)
				return false;
		} else {
			if (!tile.equals(lp.getTile()))
				return false;
		}

		Time.sleep(Random.modifiedRandomGuassianRange(100, 300, ConfidenceIntervals.CI_50, 100,
				Modifiers.get("Hunter.Lay.sleepAfterWalking")));
		
		Inventory.open();

		boolean tmp = Settings.MAKE_CLICKING_ERRORS;
		Settings.MAKE_CLICKING_ERRORS = false;

		// 3 tick
		if (Inventory.contains(Hunter.GRIMY_HERB_ID, Hunter.HERB_ID) && Inventory.getCount(Hunter.SWAMP_TAR_ID) >= 15
				&& Inventory.contains(Hunter.PESTLE_AND_MOTAR_ID)) {

			// make sure we have just 1 clean herb
			int count = Inventory.getCount(Hunter.HERB_ID);
			if (count != 1) {
				Hunter.sleepUntilNextAction();
				if (count <= 0) {
					Clicking.interact(Inventory.getFirst(Hunter.GRIMY_HERB_ID), "Clean");
				} else if (count > 1) {
					int i = 0;
					for (Item item : Inventory.getAllWithIds(Hunter.HERB_ID)) {
						if (++i >= count) {
							break;
						}
						Clicking.interact(item, "Drop");
					}
				}
				Time.sleep(Random.modifiedRandomGuassianRange(600, 900, ConfidenceIntervals.CI_70, 200,
						Modifiers.get("Hunter.lay.singleHerbSleep")));
				return false;
			}

			if (!Inventory.getBounds(Inventory.getFirst(Hunter.HERB_ID).getIndex()).contains(Mouse.getLocation())
					|| !Menu.contains("Use", null))
				Mouse.move(Inventory.getBounds(Inventory.getFirst(Hunter.HERB_ID).getIndex()));

			Hunter.sleepUntilNextAction();
			Time.sleepUntil(() -> Walking.getDestinationTile() == null, Random.modifiedRandomGuassianRange(1300, 1500,
					ConfidenceIntervals.CI_70, 300, Modifiers.get("Hunter.Lay.destinationSleep")));

			Bot.mouseLock.lock();
			try {
				if (!Inventory.getBounds(Inventory.getFirst(Hunter.HERB_ID).getIndex()).contains(Mouse.getLocation()))
					Mouse.move(Inventory.getBounds(Inventory.getFirst(Hunter.HERB_ID).getIndex()));

				Time.sleep(20, 40);
				Mouse.click(Mouse.LEFT_BUTTON);
				Mouse.move(Inventory.getBounds(Inventory.getFirst(Hunter.SWAMP_TAR_ID).getIndex()));
				Time.sleep(20, 40);
				Mouse.click(Mouse.LEFT_BUTTON);
				Mouse.move(Inventory.getBounds(Inventory.getFirst(Hunter.config.getTrapItemId()).getIndex()));
				Time.sleep(20, 40);
			} finally {
				Bot.mouseLock.unlock();
			}
		} else {
			// normal

			if (!Inventory.getBounds(Inventory.getFirst(Hunter.config.getTrapItemId()).getIndex())
					.contains(Mouse.getLocation()) || !Menu.contains("Lay", null))
				Mouse.move(Inventory.getBounds(Inventory.getFirst(Hunter.config.getTrapItemId()).getIndex()));

			Hunter.sleepUntilNextAction();
			Time.sleepUntil(() -> Walking.getDestinationTile() == null, Random.modifiedRandomGuassianRange(1250, 1300,
					ConfidenceIntervals.CI_70, 300, Modifiers.get("Hunter.Lay.destinationSleep2")));

		}

		Settings.MAKE_CLICKING_ERRORS = tmp;

		Mouse.click(Mouse.LEFT_BUTTON);

		List<String> chat = ChatBox.getMessages(5);
		List<String> newMessages = new ArrayList<String>();

		if (Time.sleepUntil(() -> {
			newMessages.clear();
			newMessages.addAll(ChatBox.getNewMessages(chat));
			boolean limited = false;
			for (String s : newMessages) {
				if (s.startsWith("You may set up only")) {
					limited = true;
					break;
				}
			}
			return limited || newMessages.contains("You begin setting up the trap.")
					|| newMessages.contains("You can't lay a trap here.");
		}, Random.modifiedRandomGuassianRange(2000, 2400, ConfidenceIntervals.CI_70, 400,
				Modifiers.get("Hunter.Lay.cantLay")))) {
			if (newMessages.contains("You begin setting up the trap.")) {
				// sleep until trap appears

				Tile accurateTile = lp.getTile();
				Hunter.traps.add(accurateTile);

				Hunter.canPeformNextAction = () -> {
					if (RSObjects.getGameObjects(accurateTile).stream()
							.anyMatch((obj) -> obj.getObjectId() == Hunter.config.getSetTrapNpcId())) {
						Time.sleep(Random.modifiedRandomGuassianRange(100, 400, ConfidenceIntervals.CI_70, 100, Modifiers.get("Hunter.Lay.sleepAfter")));
						return true;
					}
					return false;
				};

			} else if (newMessages.contains("You can't lay a trap here.")) {
				// tile taken
				Tile loc = lp.getTile();
				for (HuntingTile ht : Hunter.spot.getTiles()) {
					if (ht.getTile().equals(loc)) { // &&
													// !Hunter.traps.contains(ht.getTile())
						ht.setLastUsed(System.currentTimeMillis());
					}
				}
			} else {
				// "You may set up only....."
				// we have a trap placed that isn't tracked
				decreasedTrapCount++;
				decreasedTrapExpiration = System.currentTimeMillis() + Random.randomRange(90000, 120000); // TODO
			}
		}
		return false;
	}

}
