package org.iinc.osbot.scripts.hunter;

import java.util.ArrayList;

public class HuntingConfig {
	private final int levelRequirement;
	private final int successTrapObjectId;
	private final int failedTrapObjectId;
	private final int[] transitionSuccessTrapObjectIds;
	private final int transitionFailedTrapObjectId;
	
	private final int setTrapNpcId;
	private final int trapItemId;
	private final int requiredInvSlots; // number of empty slots required to
										// collect a trap
	private final String trapItemName;
	private final String[] trapActivatedNames;
	private final boolean walkAway;
	private final boolean walkToward;
	private final ArrayList<HuntingSpot> spots;
	private final boolean idle;
	
	public HuntingConfig(int levelRequirement, int successTrapObjectId, int[] transitionSuccessTrapObjectId, int failedTrapObjectId, int transitionFailedTrapObjectId, int setTrapNpcId, int trapItemId,
			int requiredInvSlots, String trapItemName, String[] trapActivatedNames, boolean walkAwayFromTraps, boolean walkTowardTraps, ArrayList<HuntingSpot> spots, boolean idle) {
		this.levelRequirement = levelRequirement;
		this.successTrapObjectId = successTrapObjectId;
		this.failedTrapObjectId = failedTrapObjectId;
		this.transitionSuccessTrapObjectIds = transitionSuccessTrapObjectId;
		this.transitionFailedTrapObjectId = transitionFailedTrapObjectId;
		this.setTrapNpcId = setTrapNpcId;
		this.trapItemId = trapItemId;
		this.requiredInvSlots = requiredInvSlots;
		this.trapItemName = trapItemName;
		this.trapActivatedNames = trapActivatedNames;
		this.walkAway = walkAwayFromTraps;
		this.walkToward = walkTowardTraps;
		this.spots = spots;
		this.idle = idle;
	}

	public boolean isIdle() {
		return idle;
	}

	public int getLevelRequirement() {
		return levelRequirement;
	}

	public int[] getTransitionSuccessTrapObjectIds() {
		return transitionSuccessTrapObjectIds;
	}

	public int getTransitionFailedTrapObjectId() {
		return transitionFailedTrapObjectId;
	}

	public int getSuccessTrapObjectId() {
		return successTrapObjectId;
	}

	public int getFailedTrapObjectId() {
		return failedTrapObjectId;
	}

	public int getSetTrapNpcId() {
		return setTrapNpcId;
	}

	public int getTrapItemId() {
		return trapItemId;
	}

	public String getTrapItemName() {
		return trapItemName;
	}

	public ArrayList<HuntingSpot> getSpots() {
		return spots;
	}

	public boolean isWalkAway() {
		return walkAway;
	}
	
	public boolean isWalkToward() {
		return walkToward;
	}

	public int getRequiredInvSlots() {
		return requiredInvSlots;
	}

	public String[] getTrapActivatedNames() {
		return trapActivatedNames;
	}

	
}
