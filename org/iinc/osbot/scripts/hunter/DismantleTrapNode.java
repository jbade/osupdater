package org.iinc.osbot.scripts.hunter;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.ChatBox;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.GameObject;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.api.injection.accessors.renderable.Character;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.renderable.Player;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.util.ArrayUtil;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class DismantleTrapNode implements ContinuableNode {

	private Tile tile;
	private boolean preemptive;

	@Override
	public boolean activate() {
		if (28 - Inventory.getCount() < Hunter.config.getRequiredInvSlots())
			return false;

		// check traps
		Tile location = Client.getLocalPlayer().getTile();

		ArrayList<GameObject> allTraps = new ArrayList<GameObject>();

		for (Tile t : Hunter.traps) {
			for (GameObject obj : RSObjects.getGameObjects(t)) {
				int id = obj.getObjectId();
				if (id == Hunter.config.getSuccessTrapObjectId()
						|| ArrayUtil.contains(Hunter.config.getTransitionSuccessTrapObjectIds(), id)
						|| id == Hunter.config.getFailedTrapObjectId()
						|| id == Hunter.config.getTransitionFailedTrapObjectId()
						|| id == Hunter.config.getSetTrapNpcId()) {
					allTraps.add(obj);
				}
			}
		}

		allTraps.sort((a, b) -> {
			int amod = 10, bmod = 10;
			if (a.getObjectId() == Hunter.config.getSuccessTrapObjectId()) {
				amod = 0;
			}
			if (b.getObjectId() == Hunter.config.getSuccessTrapObjectId()) {
				bmod = 0;
			}

			return (int) (amod + location.distanceTo(a.getTile()) - bmod - location.distanceTo(b.getTile()));
		});

		Character interactingIndex = Client.getLocalPlayer().getInteracting();
		tile = allTraps.stream()
				.filter((obj) -> (obj.getObjectId() == Hunter.config.getSuccessTrapObjectId()
						|| obj.getObjectId() == Hunter.config.getFailedTrapObjectId())
						&& (interactingIndex == null || !interactingIndex.getTile().equals(obj.getTile())))
				.map((obj) -> obj.getTile()).findFirst().orElse(null);

		if (tile != null) {
			preemptive = false;
			return true;
		}

		if (!Hunter.config.isWalkToward()) { // dont walk preemptive if we should be away
			return false;
		}

		// check for transitioning traps
		tile = allTraps.stream()
				.filter((obj) -> (ArrayUtil.contains(Hunter.config.getTransitionSuccessTrapObjectIds(), obj.getObjectId())
						|| obj.getObjectId() == Hunter.config.getTransitionFailedTrapObjectId())
						&& (interactingIndex == null || !interactingIndex.getTile().equals(obj.getTile())))
				.map((obj) -> obj.getTile()).findFirst().orElse(null);
		preemptive = true;
		return tile != null;
	}

	@Override
	public boolean execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		// chance that we skip preemptive walking in favor of potentially laying
		if (preemptive && 75 < Random.modifiedRandomGuassianRange(0, 100, ConfidenceIntervals.CI_60, 20,
				Modifiers.get("Hunter.Dismantle.skipPreemptive"))) {
			return true;
		}

		Player lp = Client.getLocalPlayer();

		// chance that we walk under the trap before dismantling it
		boolean behavior = preemptive || 10 < Random.modifiedRandomGuassianRange(0, 100, ConfidenceIntervals.CI_60, 5,
				Modifiers.get("Hunter.Dismantle.walkUnder"));

		// walk under trap

		if (behavior && !(tile.equals(Walking.getDestinationTile())
				|| Walking.getDestinationTile() == null && lp.getTile().equals(tile))) {
			if (Calculations.onViewport(tile.toScreen())) {

				// use adjusted tile to click below center
				// adjust based on camera angle
				Tile tmp = new Tile(tile.getX(), tile.getY(), tile.getPlane());
				double angle = Camera.getCameraAngle();
				if (angle >= 0 && angle <= Math.PI / 2) {
					tmp.setClickingOffsets(48, -48, 0);
				} else if (angle >= Math.PI / 2 && angle <= Math.PI) {
					tmp.setClickingOffsets(48, 48, 0);
				} else if (angle >= Math.PI && angle <= Math.PI * 3 / 2) {
					tmp.setClickingOffsets(-48, 48, 0);
				} else if (angle >= Math.PI * 3 / 2 && angle <= Math.PI * 2) {
					tmp.setClickingOffsets(-48, -48, 0);
				}

				Mouse.move(tmp.toScreen());

				if (Hunter.sleepUntilNextAction()) {
					Point ms = tmp.toScreen();
					if (Calculations.distance(ms.x, ms.y, Mouse.getLocation().x, Mouse.getLocation().y) > 3)
						Mouse.move(tmp.toScreen());
				}

				if (Menu.interact(new String[] { "Walk here" }, null)
						&& Time.sleepUntil(() -> Client.didYellowClick(), 20)) {
					// destination tile is client sided
					Time.sleepUntil(() -> tile.equals(Walking.getDestinationTile()), 20);
				}
			} else {
				Camera.turnTo(tile);
				Hunter.sleepUntilNextAction();
				if (!Calculations.onViewport(tile.toScreen())) {
					Walking.blindWalk(tile);
				}
			}

		}

		if (preemptive) {
			Time.sleepUntil(() -> !Walking.isMoving(), 3000, 5000);
		} else {
			Timer timer = new Timer(Random.randomRange(10000, 12000));

			if (!Time.sleepUntil(() -> Walking.getDestinationTile() == null, 5000, 6000))
				return false;

			Time.sleep(Random.modifiedRandomGuassianRange(100, 300, ConfidenceIntervals.CI_50, 100,
					Modifiers.get("Hunter.Dismantle.sleepAfterWalking")));

			boolean successful = false;
			while (!successful && timer.isRunning() && Calculations.onViewport(tile.toScreen())) {

				Mouse.move(tile.toScreen());
				Hunter.sleepUntilNextAction();
				Mouse.move(tile.toScreen());
				
				Timer t = new Timer(Random.randomRange(600, 1200));
				while (t.isRunning()
						&& !Menu.contains(new String[] { "Check", "Dismantle" }, Hunter.config.getTrapActivatedNames()))
					Mouse.move(tile.toScreen());

				if (Menu.interact(new String[] { "Check", "Dismantle" }, Hunter.config.getTrapActivatedNames())
						&& Time.sleepUntil(() -> Client.didRedClick(), 20)) {
					List<String> chat = ChatBox.getMessages(10);
					List<String> newMessages = new ArrayList<String>();

					// wait until interacting animation or chat text
					Time.sleepUntil(() -> {
						newMessages.clear();
						newMessages.addAll(ChatBox.getNewMessages(chat));
						return newMessages.contains("This isn't your trap.")
								|| lp.getAnimation() != -1 && lp.getAnimation() != 5249;
					}, Random.modifiedRandomGuassianRange(3300, 4800, ConfidenceIntervals.CI_60, 1000,
							Modifiers.get("Hunter.Dismantle.waitForDismantle")));

					// handle not our trap
					if (newMessages.contains("This isn't your trap.")) {
						for (HuntingTile ht : Hunter.spot.getTiles()) {
							if (ht.getTile().equals(tile)) {
								ht.setLastUsed(System.currentTimeMillis());
							}
						}
						Hunter.traps.remove(tile);
						break;
					}

					// 5249 = 3 tick animation
					successful = lp.getAnimation() != -1 && lp.getAnimation() != 5249;
				}

				Time.sleep(Random.modifiedRandomGuassianRange(200, 800, ConfidenceIntervals.CI_70, 100,
						Modifiers.get("Hunter.Dismantle.sleepBetweenDismantleAttempts")));
			}

			if (successful) {
				List<String> chat = ChatBox.getMessages(5);

				// removed check for not our trap because it was check above and
				// if we are animating then it should be or trap
				Hunter.traps.remove(tile);

				Hunter.canPeformNextAction = () -> {
					if (ChatBox.getNewMessages(chat).contains("You dismantle the trap.")) {
						Time.sleep(Random.modifiedRandomGuassianRange(100, 250, ConfidenceIntervals.CI_90, 50,
								Modifiers.get("Hunter.Dismantle.sleepAfterSuccess")));
						return true;
					}

					return false;
				};

				Time.sleepUntil(() -> !Walking.isMoving(), 600, 1000);
				// return true;
			}
		}

		return false;
	}

}
