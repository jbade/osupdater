package org.iinc.osbot.scripts.hunter;

import java.awt.Point;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.grounditem.GroundItem;
import org.iinc.osbot.api.injection.accessors.grounditem.GroundItems;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class PickUpTrapNode implements ContinuableNode {

	private GroundItem item;
	private long next = 0;

	@Override
	public boolean activate() {
		if (System.currentTimeMillis() <= next)
			return false;

		if (28 - Inventory.getCount() - 1 - Hunter.traps.size() < Hunter.config.getRequiredInvSlots())
			return false;

		if (Client.getLocalPlayer().getAnimation() == 5208) // TODO remove
			return false;

		updateItem();

		return item != null;
	}

	private void updateItem() {
		Tile playerLocation = Client.getLocalPlayer().getTile();
		Tile dest = Walking.getDestinationTile();
		item = GroundItems.getAll(Hunter.spot.getCenter(), 10).stream()
				.filter((gi) -> gi.getItemId() == Hunter.config.getTrapItemId() && !gi.getTile().equals(playerLocation)
						&& !gi.getTile().equals(dest))
				.sorted((a,
						b) -> (int) (playerLocation.distanceTo(a.getTile()) - playerLocation.distanceTo(b.getTile())))
				.findFirst().orElse(null);
	}

	@Override
	public boolean execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		try {
			Timer t = new Timer(Random.randomRange(10000, 15000));

			while (t.isRunning() && item != null && !Calculations.onViewport(item.toScreen())) {
				// TODO turn camera
				if (!Hunter.sleepUntilNextAction())
					Time.sleep(300, 500);
				Walking.blindWalk(item.getTile());
				updateItem();
			}

			while (Login.isLoggedIn() && t.isRunning() && item != null && Calculations.onViewport(item.toScreen())) {
				if (Client.getLocalPlayer().getAnimation() == 5208)
					return false;

				Mouse.move(item.toScreen(), 3);
				
				if (Hunter.interactMenuSleep(new String[] { "Take" }, new String[] { Hunter.config.getTrapItemName() })
						&& Time.sleepUntil(() -> Client.didRedClick(), 20)) {
					int invCount = Inventory.getCount();
					Time.sleepUntil(() -> Walking.isMoving() || Inventory.getCount() != invCount, 600, 1000);
					Time.sleepUntil(() -> !Walking.isMoving() || Inventory.getCount() != invCount, 3000, 4000);
				} else {
					Mouse.moveMouse(new Point(Mouse.getLocation().x, Mouse.getLocation().y - 30), 40, 15);
				}
				updateItem();
			}

			return false;
		} finally {
			next = System.currentTimeMillis() + Random.randomGuassianRange(500, 1200, ConfidenceIntervals.CI_90);
		}
	}

}
