package org.iinc.osbot.scripts.hunter;

import java.util.ArrayList;
import java.util.logging.Level;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.grandexchange.GrandExchangeInterface;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.grandexchange.GrandExchange;
import org.iinc.osbot.api.injection.accessors.grandexchange.GrandExchangeOffer;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.scripts.common.Navigation;
import org.iinc.osbot.util.Email;
import org.iinc.osbot.util.Tuple;

public class GrandExchangeNode implements ContinuableNode {

	public static boolean needResupply = false;
	private static boolean emptiedBank = false;
	private static ArrayList<Tuple<String, Integer>> itemsToBuy = new ArrayList<Tuple<String, Integer>>();
	static final int[] TO_SELL_IDS = new int[] { 10034 };

	@Override
	public boolean activate() {
		return needResupply || Inventory.contains(Hunter.BOND_ID);// || Login.getMembershipDaysLeft() <= 2;
	}

	@Override
	public boolean execute() {
		if (!Navigation.toGrandExchange())
			return false;

		// sell loot

		// check what items to buy and quantity

		// buy items

		if (GrandExchange.getOffers().size() != 0) {
			if (GrandExchangeInterface.openClerk()) {
				GrandExchangeInterface.abortAndCollect(true);
				GrandExchangeInterface.close();
			}
			return false;
		}

		if (Inventory.contains(Hunter.BOND_ID)) {
			// redeem bond

			Clicking.interact(Inventory.getFirst(Hunter.BOND_ID), "Redeem");

			Time.sleep(3200, 4000);

			Widget[] w = Widgets.getWidgets()[66];
			if (w == null)
				return false;

			boolean clicked = false;
			for (Widget child : w) {
				if (child != null && !child.isHidden()) {
					String text = child.getText();
					if (text != null && text.toLowerCase().contains("14")) {
						clicked = true;
						if (!Clicking.interact(child))
							return false;
					}
				}
			}
			if (!clicked)
				return false;

			Time.sleep(1200, 1800);

			clicked = false;

			for (Widget child : w) {
				if (child != null && !child.isHidden()) {
					String text = child.getText();
					if (text != null && text.toLowerCase().contains("confirm")) {
						clicked = true;
						if (!Clicking.interact(child))
							return false;
					}
				}
			}

			if (!clicked)
				return false;

			Time.sleep(10000, 15000);
			Bot.LOGGER.info("Attempted to redeem bond.");
			if (!Inventory.contains(Hunter.BOND_ID)) {
				Login.incrementMembershipDaysLeft(14);
				return false;
			}
		} else if (Inventory.contains(TO_SELL_IDS)) {
			// sell items in ge
			if (GrandExchangeInterface.openClerk()) {
				if (!GrandExchangeInterface.abortAndCollect(true))
					return false;
				GrandExchangeInterface.sellInventory();
				GrandExchangeInterface.collect(true);
			}
		} else if (emptiedBank && Inventory.containsOnly(Hunter.COINS_ID)) {
			// buy stuff

			if (!GrandExchangeInterface.openClerk() && !GrandExchangeInterface.abortAndCollect(true))
				return false;

			for (Tuple<String, Integer> item : itemsToBuy) {
				int coins = Inventory.getCount(Hunter.COINS_ID);
				int each = coins / item.y;

				if (!GrandExchangeInterface.createBuyOffer(item.x, each, item.y))
					return false;

				if (!Time.sleepUntil(() -> {
					boolean allFinished = true;
					for (GrandExchangeOffer geo : GrandExchange.getOffers()) {
						allFinished = allFinished && geo.getState() == GrandExchangeOffer.STATES.COMPLETE;
					}
					return allFinished;
				}, 20000, 30000)) {
					GrandExchangeInterface.collect(true);
					Bot.LOGGER.log(Level.SEVERE, "Out of supplies");
					Email.sendEmail(Email.TYPE.SEVERE,
							"Hunter: Unable to purchase item from ge\n" + item.x + "\n" + coins);
					System.exit(Settings.DISABLED_EXIT_CODE);
				} else if (!GrandExchangeInterface.collect(true)) {
					return false;
				}
				while (!GrandExchangeInterface.collect(true)) {
					Time.sleep(500, 1200);
				}
			}

			needResupply = false;
			itemsToBuy.clear();
			emptiedBank = false;

		} else {
			// withdraw
			if (Bank.openBanker()) {
				if (!Bank.depositInventory() || !Bank.withdrawAsNote())
					return false;
				if (!Time.sleepUntil(() -> Inventory.getCount() == 0, 1200, 1800))
					return false;

				updateToBuy();

				if (Bank.contains(Hunter.COINS_ID) && !Bank.withdraw(Hunter.COINS_ID, -1))
					return false;

				Time.sleep(1200, 1800);
				for (int i : TO_SELL_IDS) {
					if (Inventory.isFull() || Bank.contains(i) && !Bank.withdraw(i, -1))
						return false;
					Time.sleep(1200, 1800);
				}

				emptiedBank = true;
			}
		}

		return false;
	}

	// TODO make cleaner
	
	private void updateToBuy() {
		if (!Bank.isOpen())
			return;

		itemsToBuy.clear();

		int goal;

		if (Login.getMembershipDaysLeft() <= 2) {
			itemsToBuy.add(new Tuple<String, Integer>(Hunter.BOND_NAME, 1));
		}

		goal = 10 - Bank.getQuantity(Hunter.VARROCK_TAB_ID);
		if (goal > 0) {
			itemsToBuy.add(new Tuple<String, Integer>(Hunter.VARROCK_TAB_NAME, goal));
		}

		goal = 100 - Bank.getQuantity(Hunter.config.getTrapItemId());
		if (goal > 0) {
			itemsToBuy.add(new Tuple<String, Integer>(Hunter.config.getTrapItemName(), goal));
		}

		goal = 1 - Bank.getQuantity(Hunter.DUELING_RING_IDS);
		if (goal > 0) {
			itemsToBuy.add(new Tuple<String, Integer>(Hunter.DUELING_RING_NAME, goal));
		}

		goal = 2 - Bank.getQuantity(Hunter.STAMINA_POT_IDS);
		if (goal > 0) {
			itemsToBuy.add(new Tuple<String, Integer>(Hunter.STAMINA_POT_NAME, goal));
		}

		if (Levels.getCurrentHerblore() >= 19) {
			goal = 100 - Bank.getQuantity(Hunter.GRIMY_HERB_ID);
			if (goal > 0) {
				itemsToBuy.add(new Tuple<String, Integer>(Hunter.GRIMY_HERB_NAME, goal));
			}

			goal = 1500 - Bank.getQuantity(Hunter.SWAMP_TAR_ID);
			if (goal > 0) {
				itemsToBuy.add(new Tuple<String, Integer>(Hunter.SWAMP_TAR_NAME, goal));
			}

			goal = 1 - Bank.getQuantity(Hunter.PESTLE_AND_MOTAR_ID);
			if (goal > 0) {
				itemsToBuy.add(new Tuple<String, Integer>(Hunter.PESTLE_AND_MOTAR_NAME, goal));
			}

		}

	}

}
