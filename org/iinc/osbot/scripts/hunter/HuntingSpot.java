package org.iinc.osbot.scripts.hunter;

import java.util.ArrayList;

import org.iinc.osbot.api.base.positioning.Tile;

public class HuntingSpot {
	private final Tile center;
	private final ArrayList<HuntingTile> tiles;
	private final ArrayList<Tile> spawnTiles;
	
	public HuntingSpot(Tile center, ArrayList<Tile> tiles, ArrayList<Tile> spawnTiles) {
		this.center = center;
		this.spawnTiles = spawnTiles;
		
		ArrayList<HuntingTile> t = new ArrayList<HuntingTile>();
		for (Tile tile : tiles){
			t.add(new HuntingTile(tile, spawnTiles));
		}
		this.tiles  = t;
	}

	public void updateRating(Iterable<Tile> traps){
		for  (HuntingTile tile : tiles)
			tile.updateRating(traps);
	}
	
	public Tile getCenter() {
		return center;
	}

	public ArrayList<HuntingTile> getTiles() {
		return tiles;
	}

	public ArrayList<Tile> getSpawnTiles() {
		return spawnTiles;
	}
}
