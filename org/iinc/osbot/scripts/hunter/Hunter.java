package org.iinc.osbot.scripts.hunter;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Condition;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class Hunter extends Script implements PaintListener {

	/*
	 * Script Modifiers
	 * 0 - sleepUntilNextAction
	 * 1 - When to update config
	 * 2 - Config spot starting indexs
	 * 3 - Dismantle preemptive do nothing
	 * 4 - Walk under trap before dismantling
	 * 5 - Walk away choose tile
	 * 6 - Drop items?
	 * 7 - Walk Away threshold
	 * 8 - DismantleTrapNode sleep 1
	 * 9 - DismantleTrapNode sleep 2
	 * 10-15 - LayTrapNode sleep
	 * 16-17 - WalkAwayNode
	 * 18-19 - DropJunkNode
	 * 
	 */
	static final ArrayList<HuntingConfig> CONFIGS;

	public static final Tile LUMBRIDGE_BANK_WALK_TILE = new Tile(3209, 3220, 2);
	public static final Tile CASTLE_WARS_BANK_WALK_TILE = new Tile(2443, 3083, 0);

	public static final Tile LUMBRIDGE_BANK_TILE_1 = new Tile(3209, 3221, 2);
	public static final Tile LUMBRIDGE_BANK_TILE_2 = new Tile(3208, 3221, 2);
	public static final Tile CASTLE_WARS_BANK_TILE_1 = new Tile(2444, 3083, 0);

	public static final int[] STAMINA_POT_IDS = new int[] { 12631, 12629, 12627, 12625 };
	public static final int[] DUELING_RING_IDS = new int[] { 2564, 2562, 2560, 2558, 2556, 2554, 2552 };
	// 1 charge 2566

	public static final int COINS_ID = 995;
	public static final int HERB_ID = 249;
	public static final int GRIMY_HERB_ID = 199;
	public static final int VARROCK_TAB_ID = 8007;
	public static final int SWAMP_TAR_ID = 1939;
	public static final int PESTLE_AND_MOTAR_ID = 233;
	public static final int BOND_ID = 13192;

	public static final String VARROCK_TAB_NAME = "Varrock teleport";
	public static final String SWAMP_TAR_NAME = "Swamp tar";
	public static final String GRIMY_HERB_NAME = "Grimy guam leaf";
	public static final String PESTLE_AND_MOTAR_NAME = "Pestle and mortar";
	public static final String DUELING_RING_NAME = "Ring of dueling(8)";
	public static final String STAMINA_POT_NAME = "Stamina potion(4)";
	public static final String BOND_NAME = "Old school bond";

	private ArrayList<ContinuableNode> startNodes = new ArrayList<>();
	private ArrayList<ContinuableNode> actionNodes = new ArrayList<>();
	private ArrayList<ContinuableNode> endNodes = new ArrayList<>();
	private ContinuableNode pickUpTrap = new PickUpTrapNode();
	private ContinuableNode layTrap = new LayTrapNode();
	private ContinuableNode dismantleTrap = new DismantleTrapNode();

	public static HuntingConfig config;
	public static HuntingSpot spot;
	public static HashSet<Tile> traps = new HashSet<Tile>(6);

	static Condition canPeformNextAction = () -> true;

	static {
		CONFIGS = new ArrayList<HuntingConfig>();

		// Crimson Swift
		ArrayList<Tile> spawnTiles = new ArrayList<Tile>();
		Collections.addAll(spawnTiles, new Tile(2612, 2915, 0), new Tile(2607, 2929, 0), new Tile(2615, 2933, 0),
				new Tile(2614, 2938, 0), new Tile(2606, 2921, 0), new Tile(2609, 2918, 0), new Tile(2604, 2916, 0));
		ArrayList<Tile> toRemove = new ArrayList<Tile>();
		Collections.addAll(toRemove);
		ArrayList<Tile> tiles = new ArrayList<Tile>();
		for (int i = 2603; i <= 2610; i++)
			for (int j = 2916; j <= 2933; j++)
				tiles.add(new Tile(i, j, 0));
		tiles.removeAll(toRemove);
		ArrayList<HuntingSpot> spots = new ArrayList<HuntingSpot>();
		spots.add(new HuntingSpot(new Tile(2605, 2923, 0), tiles, spawnTiles));
		CONFIGS.add(new HuntingConfig(0, 9373, new int[] { 9349 }, 9344, 9346, 9345, 10006, 4, "Bird snare",
				new String[] { "Bird snare" }, true, false, spots, true));

		spots = new ArrayList<HuntingSpot>();

		// Tropical Wagtail
		spawnTiles = new ArrayList<Tile>();
		Collections.addAll(spawnTiles, new Tile(2499, 2890, 0), new Tile(2504, 2888, 0), new Tile(2502, 2892, 0));
		toRemove = new ArrayList<Tile>();
		Collections.addAll(toRemove, new Tile(2506, 2896, 0), new Tile(2503, 2896, 0), new Tile(2500, 2896, 0),
				new Tile(2499, 2896, 0), new Tile(2498, 2896, 0), new Tile(2504, 2895, 0), new Tile(2505, 2895, 0),
				new Tile(2506, 2895, 0), new Tile(2499, 2894, 0), new Tile(2498, 2894, 0), new Tile(2505, 2892, 0),
				new Tile(2504, 2891, 0), new Tile(2505, 2891, 0), new Tile(2506, 2890, 0), new Tile(2498, 2890, 0),
				new Tile(2498, 2889, 0), new Tile(2506, 2889, 0), new Tile(2496, 2887, 0), new Tile(2497, 2887, 0),
				new Tile(2499, 2887, 0), new Tile(2504, 2887, 0), new Tile(2504, 2886, 0), new Tile(2503, 2886, 0),
				new Tile(2500, 2886, 0), new Tile(2497, 2886, 0), new Tile(2496, 2886, 0));
		tiles = new ArrayList<Tile>();
		for (int i = 2496; i <= 2506; i++)
			for (int j = 2884; j <= 2895; j++)
				tiles.add(new Tile(i, j, 0));
		tiles.removeAll(toRemove);
		spots.add(new HuntingSpot(new Tile(2500, 2891, 0), tiles, spawnTiles));

		// Tropical Wagtail
		spawnTiles = new ArrayList<Tile>();
		Collections.addAll(spawnTiles, new Tile(2545, 2882, 0), new Tile(2543, 2887, 0), new Tile(2538, 2884, 0));
		toRemove = new ArrayList<Tile>();
		Collections.addAll(toRemove, new Tile(2537, 2880, 0), new Tile(2538, 2880, 0), new Tile(2544, 2881, 0),
				new Tile(2545, 2881, 0), new Tile(2542, 2881, 0), new Tile(2541, 2883, 0), new Tile(2540, 2884, 0),
				new Tile(2542, 2884, 0), new Tile(2547, 2885, 0), new Tile(2540, 2886, 0), new Tile(2540, 2887, 0),
				new Tile(2538, 2887, 0), new Tile(2540, 2888, 0), new Tile(2539, 2890, 0), new Tile(2543, 2890, 0));
		tiles = new ArrayList<Tile>();
		for (int i = 2536; i <= 2549; i++)
			for (int j = 2880; j <= 2890; j++)
				tiles.add(new Tile(i, j, 0));
		tiles.removeAll(toRemove);
		spots.add(new HuntingSpot(new Tile(2544, 2884, 0), tiles, spawnTiles));

		CONFIGS.add(new HuntingConfig(19, 9348, new int[] { 9347 }, 9344, 9346, 9345, 10006, 4, "Bird snare",
				new String[] { "Bird snare" }, false, false, spots, true));
		// 9347 from up to caught
		// 9346 from up to fail

		// Red chin
		spots = new ArrayList<HuntingSpot>();
		spawnTiles = new ArrayList<Tile>();
		Collections.addAll(spawnTiles, new Tile(2497, 2909, 0), new Tile(2497, 2901, 0), new Tile(2501, 2906, 0));
		toRemove = new ArrayList<Tile>();
		Collections.addAll(toRemove, new Tile(2499, 2899, 0), new Tile(2506, 2900, 0), new Tile(2506, 2901, 0),
				new Tile(2501, 2901, 0), new Tile(2499, 2903, 0), new Tile(2502, 2903, 0), new Tile(2503, 2903, 0),
				new Tile(2505, 2904, 0), new Tile(2504, 2904, 0), new Tile(2503, 2904, 0), new Tile(2502, 2904, 0),
				new Tile(2498, 2904, 0), new Tile(2502, 2906, 0), new Tile(2502, 2907, 0), new Tile(2502, 2908, 0),
				new Tile(2506, 2910, 0), new Tile(2502, 2910, 0), new Tile(2501, 2911, 0), new Tile(2505, 2911, 0));
		tiles = new ArrayList<Tile>();
		for (int i = 2496; i <= 2506; i++)
			for (int j = 2899; j <= 2913; j++)
				tiles.add(new Tile(i, j, 0));
		tiles.removeAll(toRemove);
		spots.add(new HuntingSpot(new Tile(2499, 2904, 0), tiles, spawnTiles));

		// Red chin
		spawnTiles = new ArrayList<Tile>();
		Collections.addAll(spawnTiles, new Tile(2557, 2932, 0), new Tile(2557, 2936, 0), new Tile(2553, 2935, 0));
		toRemove = new ArrayList<Tile>();
		Collections.addAll(toRemove, new Tile(2554, 2932, 0), new Tile(2553, 2934, 0), new Tile(2553, 2935, 0));
		tiles = new ArrayList<Tile>();
		for (int i = 2553; i <= 2559; i++)
			for (int j = 2930; j <= 2939; j++)
				tiles.add(new Tile(i, j, 0));
		tiles.removeAll(toRemove);
		spots.add(new HuntingSpot(new Tile(2556, 2934, 0), tiles, spawnTiles));

		// Red chin
		spawnTiles = new ArrayList<Tile>();
		Collections.addAll(spawnTiles, new Tile(2559, 2918, 0), new Tile(2559, 2911, 0), new Tile(2556, 2914, 0));
		toRemove = new ArrayList<Tile>();
		Collections.addAll(toRemove, new Tile(2560, 2912, 0), new Tile(2557, 2913, 0), new Tile(2557, 2918, 0),
				new Tile(2561, 2919, 0));
		tiles = new ArrayList<Tile>();
		for (int i = 2555; i <= 2561; i++)
			for (int j = 2910; j <= 2919; j++)
				tiles.add(new Tile(i, j, 0));
		tiles.removeAll(toRemove);
		spots.add(new HuntingSpot(new Tile(2558, 2915, 0), tiles, spawnTiles));

		// Red chin
		spawnTiles = new ArrayList<Tile>();
		Collections.addAll(spawnTiles, new Tile(2507, 2885, 0), new Tile(2503, 2881, 0));
		toRemove = new ArrayList<Tile>();
		Collections.addAll(toRemove, new Tile(2507, 2883, 0), new Tile(2502, 2883, 0), new Tile(2503, 2886, 0),
				new Tile(2504, 2886, 0));
		tiles = new ArrayList<Tile>();
		for (int i = 2502; i <= 2509; i++)
			for (int j = 2880; j <= 2886; j++)
				tiles.add(new Tile(i, j, 0));
		tiles.removeAll(toRemove);
		spots.add(new HuntingSpot(new Tile(2505, 2882, 0), tiles, spawnTiles));

		CONFIGS.add(new HuntingConfig(63, 9383, new int[] { 9390, 9391, 9392, 9393 }, 9385, 9381, 9380, 10008, 2,
				"Box trap", new String[] { "Box trap", "Shaking box" }, false, true, spots, true));

	}

	// , "Shaking box"
	public static boolean checkNextAction() {
		if (canPeformNextAction == null)
			return true;
		if (canPeformNextAction.check()) {
			canPeformNextAction = null;
			return true;
		}
		return false;
	}

	private static final int[] CLICK_TO_CONTINUE = new int[] { 193, 2 };

	/**
	 * 
	 * @return if we slept at all
	 */
	public static boolean sleepUntilNextAction() {
		if (Hunter.checkNextAction())
			return false;

		Time.sleepUntil(() -> checkNextAction() || Widgets.exists(CLICK_TO_CONTINUE), 3000, 5000);
		if (Widgets.exists(CLICK_TO_CONTINUE)) {
			Clicking.interact(Widgets.getWidget(CLICK_TO_CONTINUE));
			Time.sleep(600, 800);
			return sleepUntilNextAction();
		}

		Hunter.canPeformNextAction = null;
		// TODO
		// Time.sleep(Random.modifiedRandomGuassianRange(100, 200, ConfidenceIntervals.CI_70, 50,
		// Modifiers.scriptModifiers[0]));
		return true;
	}

	public static boolean interactMenuSleep(String[] actions, String[] options) {
		Bot.mouseLock.lock();
		try {
			if (Hunter.checkNextAction()) {
				return Menu.interact(actions, options);
			} else {
				Mouse.click(Mouse.RIGHT_BUTTON);

				int i = Menu.getIndex(actions, options);
				int count = Client.getMenuCount();

				Time.sleep(20, 35);
				if (Menu.isOpen()) {
					// check again to make sure we are clicking the correct thing
					i = Menu.getIndex(actions, options);
					count = Client.getMenuCount();

					if (i != -1) {
						Mouse.move(Menu.getBounds(count - i - 1));
						if (!Hunter.sleepUntilNextAction()) {
							Mouse.sleepBeforeClick();
						}
						if (Menu.isOpen()) {
							if (!Menu.getTrueBounds(count - i - 1).contains(Mouse.getLocation())) {
								Mouse.move(Menu.getBounds(count - i - 1));
							}
							Mouse.click(Mouse.LEFT_BUTTON);
							return Menu.getTrueBounds(count - i - 1).contains(Mouse.getLocation());
						}
					} else {
						Menu.close();
					}
				}
			}

		} finally {
			Bot.mouseLock.unlock();
		}

		return false;
	}

	public static int getTrapLimit() {
		int hunterLevel = Levels.getCurrentHunter();
		int trapLimit = 1;
		if (hunterLevel >= 80) {
			trapLimit = 5;
		} else if (hunterLevel >= 60) {
			trapLimit = 4;
		} else if (hunterLevel >= 40) {
			trapLimit = 3;
		} else if (hunterLevel >= 20) {
			trapLimit = 2;
		}
		return trapLimit;
	}

	@Override
	public int loop() {
		// Memory.checkMemory(1000000); // 1 MB

		if (BreakHandler.shouldBreak()) {
			BreakHandler.doBreak();
		}

		Interfaces.closeAll();

		if (Inventory.isItemSelected()) {
			Menu.interact("Cancel");
		}

		if (Walking.getRunEnergy() > Random.randomRange(10, 30))
			Walking.enableRun();

		if (Login.isLoggedIn() && Login.getMembershipDaysLeft() == -1) {
			System.exit(0);
		}

		for (ContinuableNode node : startNodes) {
			if (node.activate()) {
				if (!node.execute())
					return Random.randomRange(50, 150);
			}
		}

		actionNodes.clear();
		actionNodes.add(pickUpTrap);
		if (70 > Random.modifiedRandomGuassianRange(0, 100, ConfidenceIntervals.CI_70, 20,
				Modifiers.get("Hunter.layFirst"))) {
			actionNodes.add(layTrap);
			actionNodes.add(dismantleTrap);
		} else {
			actionNodes.add(dismantleTrap);
			actionNodes.add(layTrap);
		}

		for (ContinuableNode node : actionNodes) {
			if (node.activate()) {
				if (!node.execute())
					return Random.randomRange(50, 150);
			}
		}

		for (ContinuableNode node : endNodes) {
			if (node.activate()) {
				if (!node.execute())
					return Random.randomRange(50, 150);
			}
		}

		return Random.randomRange(50, 150);
	}

	@Override
	public void onStop() {

	}

	@Override
	public void onStart() {
		Collections.addAll(startNodes, new LoginNode(), new UpdateTileStatesNode(), new UpdateConfigNode(),
				new GrandExchangeNode(), new ToBankNode(), new BankingNode(), new ToHuntingSpotNode());
		Collections.addAll(endNodes, new DropJunkNode(), new WalkAwayNode(), new IdleNode());

		Modifiers.init(Bot.username.hashCode());
		BreakHandler.init(Bot.username.hashCode());
		traps.clear();
		config = null;
		spot = null;

		Settings.MAKE_CLICKING_ERRORS = true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onDraw(Graphics g) {
		g.setColor(Color.green);
		for (Tile t : (Iterable<Tile>) traps.clone()) {
			Point p = t.toScreen();
			g.drawRect(p.x - 1, p.y - 1, 2, 2);
		}

		if (spot != null) {
			g.setColor(Color.red);
			for (Tile t : spot.getSpawnTiles()) {
				Point p = t.toScreen();
				g.drawRect(p.x - 1, p.y - 1, 2, 2);
			}
		}
	}
}
