package org.iinc.osbot.scripts.hunter;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.ContinuableNode;

public class LoginNode implements ContinuableNode {

	@Override
	public boolean activate() {
		return !Login.isLoggedIn();
	}

	@Override
	public boolean execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		Login.login();
		Time.sleep(1000, 2000);
		Camera.setAngle(Camera.getCameraAngle(), true);
		
		return false;
	}

}
