package org.iinc.osbot.scripts.hunter;

import java.util.logging.Level;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class IdleNode implements ContinuableNode {

	@Override
	public boolean activate() {
		return Hunter.config.isIdle(); // TODO
	}

	@Override
	public boolean execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		
		//if (Mouse.isFocused() && Random.modifiedRandomRange(0, 100, 10, Modifiers.get("Hunter.idleMoveoff")) >= 80) {
		//	Time.sleep(Random.randomGuassianRange(200, 800, ConfidenceIntervals.CI_70));
		//	Mouse.moveOffscreen();
		//}

		Time.sleep(Random.modifiedRandomGuassianRange(500, 2000, ConfidenceIntervals.CI_60, 500,
				Modifiers.get("Hunter.Idle.sleep")));
		
		return false;
	}

}
