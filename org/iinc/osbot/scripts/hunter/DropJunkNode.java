package org.iinc.osbot.scripts.hunter;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class DropJunkNode implements ContinuableNode {

	private final static int[] ITEM_IDS_TO_DROP = new int[] { Items.BONES.getId(), Items.RAW_BIRD_MEAT.getId() };
	private long next;
	private WalkAwayNode wan = new WalkAwayNode();

	@Override
	public boolean activate() {
		if (System.currentTimeMillis() <= next)
			return false;

		if (28 - Inventory.getCount() < Hunter.config.getRequiredInvSlots())
			return true;

		if (Inventory.contains(ITEM_IDS_TO_DROP) && !Hunter.traps.contains(Client.getLocalPlayer().getTile())
				&& Random.modifiedRandomGuassianRange(0, 100, ConfidenceIntervals.CI_60, 50,
						Modifiers.get("Hunter.DropJunk.activate")) >= 50)
			return true;

		return false;
	}

	@Override
	public boolean execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		Hunter.sleepUntilNextAction();

		if (28 - Inventory.getCount() >= Hunter.config.getRequiredInvSlots()) {
			Time.sleep(Random.randomGuassianRange(400, 1200, ConfidenceIntervals.CI_60));
		}
		
		if (Hunter.traps.contains(Client.getLocalPlayer().getTile())) {
			wan.execute();
		}

		if (Inventory.contains(ITEM_IDS_TO_DROP)) {
			List<Item> items = Inventory.getAllWithIds(ITEM_IDS_TO_DROP);
			if (Random.modifiedRandomGuassianRange(0, 100, ConfidenceIntervals.CI_70, 50,
					Modifiers.get("Hunter.DropJunk.shuffle")) > 50) {
				Collections.shuffle(items);
			}

			for (Item item : items) {
				Inventory.open();

				if (Inventory.isItemSelected()) {
					Menu.interact("Cancel");
				}

				Mouse.move(Inventory.getBounds(item.getIndex()));

				Menu.interact(new String[] { "Drop" }, null);
				Time.sleep(Random.modifiedRandomGuassianRange(0, 350, ConfidenceIntervals.CI_50, 50,
						Modifiers.get("Hunter.DropJunk.sleep")));

			}
		} else {
			// need space, drop trap
			Clicking.interact(Inventory.getFirst(Hunter.config.getTrapItemId()), "Drop");
		}

		next = System.currentTimeMillis() + Random.randomGuassianRange(500, 1200, ConfidenceIntervals.CI_90);

		return false;
	}

}
