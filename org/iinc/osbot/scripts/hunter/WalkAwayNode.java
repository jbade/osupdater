package org.iinc.osbot.scripts.hunter;

import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.grounditem.GroundItems;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class WalkAwayNode implements ContinuableNode {

	private final int distance = 1;
	private long next;

	@Override
	public boolean activate() {
		if (!Hunter.config.isWalkAway() || System.currentTimeMillis() <= next)
			return false;

		Tile loc = Client.getLocalPlayer().getTile();
		for (Tile t : Hunter.traps) {
			if (loc.distanceTo(t) <= distance)
				return true;
		}

		return false;
	}

	@Override
	public boolean execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		// get potential walking tiles sorted by best distance
		List<Tile> ght = Hunter.spot.getTiles().stream().map((a) -> a.getTile()).filter((ht) -> {
			if (GroundItems.getAt(ht).size() >= 4)
				return false;
			
			for (Tile t : Hunter.traps)
				if (ht.distanceTo(t) <= distance)
					return false;

			return true;
		}).sorted().collect(Collectors.toList());

		if (ght.size() == 0)
			return true;
		int size = ght.size();

		int index = Random.randomGuassianRange(0, Math.min(size, 2), ConfidenceIntervals.CI_60);
		if (index < 0)
			index = 0;
		if (index > size - 1)
			index = size - 1;

		Tile goal = ght.get(index).getTile();

		if (Calculations.onViewport(goal.toScreen())) {
			Mouse.move(goal.toScreen());

			if (Hunter.sleepUntilNextAction())
				Mouse.move(goal.toScreen());

			if (Hunter.interactMenuSleep(new String[] { "Walk here" }, null)
					&& Time.sleepUntil(() -> Client.didYellowClick(), 10, 20)) {
				Time.sleepUntil(() -> Walking.hasDestination(), 600, 800);
				Hunter.canPeformNextAction = () -> {
					if (!Walking.isMoving()) {
						Time.sleep(Random.modifiedRandomGuassianRange(400, 1200, ConfidenceIntervals.CI_80, 200,
								Modifiers.get("Hunter.WalkAway.sleepAfter")));
						return true;
					}

					return false;
				};
			}

		} else {
			Camera.turnTo(goal);
			Hunter.sleepUntilNextAction();
			if (!Calculations.onViewport(goal.toScreen())) {
				Walking.blindWalk(goal);
			}
		}

		next = System.currentTimeMillis() + Random.randomGuassianRange(3000, 5000, ConfidenceIntervals.CI_70);

		return false;
	}

}
