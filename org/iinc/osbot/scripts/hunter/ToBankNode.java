package org.iinc.osbot.scripts.hunter;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.interfaces.tabs.Equipment;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.scripts.common.Navigation;
import org.iinc.osbot.util.ArrayUtil;

public class ToBankNode implements ContinuableNode {

	static final Tile LUMBRIDGE_STAIRS_FIRST_TILE = new Tile(3204, 3208, 1);
	static final Tile LUMBRIDGE_STAIRS_FLOOR_TILE = new Tile(3204, 3208, 0);

	@Override
	public boolean activate() {
		Tile loc = Client.getLocalPlayer().getTile();

		int hunterLevel = Levels.getCurrentHunter();
		int trapLimit = 1;
		if (hunterLevel >= 80) {
			trapLimit = 5;
		} else if (hunterLevel >= 60) {
			trapLimit = 4;
		} else if (hunterLevel >= 40) {
			trapLimit = 3;
		} else if (hunterLevel >= 20) {
			trapLimit = 2;
		}

		if ((Inventory.getCount(Hunter.config.getTrapItemId()) < trapLimit - Hunter.traps.size()
				|| Levels.getCurrentHerblore() >= 19 && (Inventory.getCount(Hunter.SWAMP_TAR_ID) == 0
						|| Inventory.getCount(Hunter.HERB_ID, Hunter.GRIMY_HERB_ID) == 0
						|| Inventory.getCount(Hunter.PESTLE_AND_MOTAR_ID) == 0))
				&& loc.distanceTo(Hunter.LUMBRIDGE_BANK_WALK_TILE) >= 3
				&& loc.distanceTo(Hunter.CASTLE_WARS_BANK_WALK_TILE) >= 3 
				&& !Navigation.atGrandExchange())
			return true;

		if (loc.distanceTo(LUMBRIDGE_STAIRS_FLOOR_TILE) <= 30 || loc.distanceTo(LUMBRIDGE_STAIRS_FIRST_TILE) <= 30
				|| loc.distanceTo(Hunter.LUMBRIDGE_BANK_WALK_TILE) <= 30
						&& loc.distanceTo(Hunter.LUMBRIDGE_BANK_WALK_TILE) >= 3)
			return true;

		return false;
	}

	@Override
	public boolean execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		Tile loc = Client.getLocalPlayer().getTile();
		if (loc.distanceTo(Hunter.CASTLE_WARS_BANK_WALK_TILE) < 30
				&& loc.distanceTo(Hunter.CASTLE_WARS_BANK_WALK_TILE) >= 3) {
			Walking.blindWalk(Hunter.CASTLE_WARS_BANK_WALK_TILE);
			return false;
		} else if (Inventory.contains(Hunter.DUELING_RING_IDS)) {
			Clicking.interact(Inventory.getFirst(Hunter.DUELING_RING_IDS), "Wear");
			int invCount = Inventory.getCount();
			Time.sleepUntil(() -> invCount != Inventory.getCount(), 600, 800);
			return false;
		} else {
			if (!Equipment.open())
				return false;
			Time.sleep(600, 1200);

			if (ArrayUtil.contains(Hunter.DUELING_RING_IDS, Equipment.getRing())) {
				if (Clicking.interact(Equipment.getRingWidget(), "Castle Wars")
						&& Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 714, 600, 700)) {
					Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != 714, 3000, 4000);
					return false;
				}
			} else {
				Navigation.toLumbridgeBank();
			}
		}

		return false;
	}

}
