package org.iinc.osbot.scripts.hunter;

import java.util.logging.Level;

import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.util.Random;

public class ToHuntingSpotNode implements ContinuableNode {
	static final Area HUNTING_AREA = new Area(new Tile(2490, 2940, 0), new Tile(2645, 2940, 0), new Tile(2646, 2867, 0),
			new Tile(2490, 2867, 0));
	static final Tile[] pathVerts = new Tile[] { new Tile(2443, 3083, 0), new Tile(2453, 3088, 0),
			new Tile(2453, 3073, 0), new Tile(2441, 3047, 0), new Tile(2458, 3025, 0), new Tile(2483, 3011, 0),
			new Tile(2487, 2978, 0), new Tile(2484, 2965, 0), new Tile(2499, 2930, 0) };

	@Override
	public boolean activate() {
		return Client.getLocalPlayer().getTile().distanceTo(Hunter.spot.getCenter()) > 20;
	}

	@Override
	public boolean execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		
		Tile playerLocation = Client.getLocalPlayer().getTile();
		if (!HUNTING_AREA.contains(playerLocation)) {
			if (Walking.getRunEnergy() > Random.randomRange(10, 30))
				Walking.enableRun();
			Walking.walkPath(Walking.generatePath(pathVerts, 4, true), false);
		} else if (playerLocation.distanceTo(Hunter.spot.getCenter()) > 20) {
			Walking.enableRun();
			Walking.walkPath(Walking.generatePath(playerLocation, Hunter.spot.getCenter(), 4), false);
		}

		return false;

	}

}
