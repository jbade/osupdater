package org.iinc.osbot.scripts;

import java.util.logging.Level;

import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.script.ScriptHandler;
import org.iinc.osbot.scripts.fishing.catherby.CatherbyFishing;
import org.iinc.osbot.scripts.hunter.Hunter;
import org.iinc.osbot.util.Modifiers;

public class Multi extends Script {

	@Override
	public void onStart() {
		Class<Script>[] scripts = new Class[] { Hunter.class, CatherbyFishing.class };

		// 2 hour sections
		int section = (int) (System.currentTimeMillis() / 1000 / 60 / 60 / 2 + Modifiers.get("Multi.1") * 100000); 
		
		java.util.Random random = new java.util.Random(section);
		int index = (int) (random.nextDouble() * scripts.length);

		if (index < 0)
			index = 0;
		if (index > scripts.length - 1)
			index = scripts.length - 1;

		try {
			Bot.LOGGER.log(Level.INFO, "Multi starting script " + scripts[index].getName());
			ScriptHandler.addScript(scripts[index].newInstance());
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		this.stop();
	}

	@Override
	public int loop() {
		return 0;
	}

	@Override
	public void onStop() {

	}
}
