package org.iinc.osbot.scripts.fishing.lumbridge_river;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.NpcDefinition;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.script.Node;

public class FishingNode implements Node {

	@Override
	public boolean activate() {
		return Client.getLocalPlayer().getAnimation() == -1 || !Client.getLocalPlayer().isInteracting();
	}

	@Override
	public void execute() {
		if (Interacting.interact(() -> Npcs.getAll().parallelStream().filter((npc) -> {
			NpcDefinition def = npc.getNpcDefinition();
			return def != null && def.getId() == LumbridgeRiverFishing.FISHING_SPOT_ID && npc.getTile().getY() != 3157;
		}).sorted().findFirst().orElse(null), LumbridgeRiverFishing.FISH_CENTER_TILE, LumbridgeRiverFishing.FISHING_SPOT_ACTION, null)) {
			Time.sleepUntil(() -> Walking.isMoving(), 800, 1600);
			if (Time.sleepUntil(
					() -> !Walking.isMoving()
							|| Client.getLocalPlayer().isInteracting() && Client.getLocalPlayer().getAnimation() != -1,
					3500, 5000))
				Time.sleep(2000, 3000);
		}

	}

}
