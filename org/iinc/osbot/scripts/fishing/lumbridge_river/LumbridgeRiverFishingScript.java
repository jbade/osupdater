package org.iinc.osbot.scripts.fishing.lumbridge_river;

import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class LumbridgeRiverFishingScript extends Script {

	LumbridgeRiverFishing node = new LumbridgeRiverFishing();
	
	@Override
	public int loop() {
		if (!Login.isLoggedIn()){
			Login.login();
			return 0;
		}		
		
		if (BreakHandler.shouldBreak()) {
			Interfaces.closeAll();
			Timer t = new Timer(Random.randomRange(10000, 15000));
			while (Login.isLoggedIn() && t.isRunning())
				Logout.clickLogout();
			BreakHandler.doBreak();
		}
		
		if (Inventory.isItemSelected()) {
			Menu.interact("Cancel");
		}
		
		Interfaces.closeAll();
		
		if (Walking.getRunEnergy() > Random.randomRange(20, 30))
			Walking.enableRun();
		
		
		if (node.activate())
			node.execute();
		
		return Random.randomRange(50, 100);
	}

	@Override
	public void onStop() {
		
	}

	@Override
	public void onStart() {
		Modifiers.init(Bot.username.hashCode());
		BreakHandler.init(Bot.username.hashCode());
	}

}
