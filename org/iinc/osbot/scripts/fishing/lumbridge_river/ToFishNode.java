package org.iinc.osbot.scripts.fishing.lumbridge_river;

import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Navigation;

public class ToFishNode implements Node {

	@Override
	public boolean activate() {
		return LumbridgeRiverFishing.FISH_CENTER_TILE.distanceTo(Client.getLocalPlayer().getTile()) > 15;
	}

	@Override
	public void execute() {
		if (LumbridgeRiverFishing.REACHABLE_AREA.contains(Client.getLocalPlayer().getTile())) {
			Walking.walkPath(Walking.generatePath(LumbridgeRiverFishing.PATH_TO_FISH, 5, true), true);
		} else {
			Navigation.toLumbridge();
		}
	}

}
