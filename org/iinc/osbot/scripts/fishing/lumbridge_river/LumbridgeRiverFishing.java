package org.iinc.osbot.scripts.fishing.lumbridge_river;

import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;

import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.fishing.DropNode;
import org.iinc.osbot.scripts.fishing.IdleNode;

public class LumbridgeRiverFishing implements Node {
	static Area REACHABLE_AREA = new Area(new Tile(3197, 3130, 0), new Tile(3253, 3140, 0), new Tile(3253, 3177, 0),
			new Tile(3261, 3203, 0), new Tile(3267, 3204, 0), new Tile(3266, 3253, 0), new Tile(3265, 3256, 0),
			new Tile(3254, 3255, 0), new Tile(3253, 3265, 0), new Tile(3213, 3263, 0), new Tile(3196, 3255, 0),
			new Tile(3192, 3198, 0));

	static Tile FISH_CENTER_TILE = new Tile(3241, 3246, 0);

	static Tile[] PATH_TO_FISH = new Tile[] { new Tile(3226, 3218, 0), new Tile(3240, 3225, 0), new Tile(3257, 3227, 0),
			new Tile(3241, 3241, 0) };
	
	static int FISHING_SPOT_ID = 1527;
	static String FISHING_SPOT_ACTION = "Lure";
	

	ArrayList<Node> nodes = new ArrayList<>();
	{
		Collections.addAll(nodes, new BankNode(), new ToFishNode(), new DropNode(), new FishingNode(), new IdleNode());
	}

	@Override
	public boolean activate() {
		return true;
	}

	@Override
	public void execute() {
		for (Node node : nodes) {
			if (node.activate()) {
				Bot.LOGGER.log(Level.INFO, node.getClass().getName());
				node.execute();
				return;
			}
		}
	}

}
