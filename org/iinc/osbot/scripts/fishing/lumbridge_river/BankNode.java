package org.iinc.osbot.scripts.fishing.lumbridge_river;

import java.util.ArrayList;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Navigation;
import org.iinc.osbot.scripts.fishing.DropNode;
import org.iinc.osbot.util.Email;
import org.iinc.osbot.util.Tuple;

public class BankNode implements Node {
	static Tuple<RSItemConstant, Integer>[] equipment = new Tuple[] { new Tuple<>(Items.FLY_FISHING_ROD, 1),
			new Tuple<>(Items.FEATHER, -1) };

	static final Tile LUMBRIDGE_BANK_TILE_1 = new Tile(3209, 3221, 2);
	static final Tile LUMBRIDGE_BANK_TILE_2 = new Tile(3208, 3221, 2);

	@Override
	public boolean activate() {
		ArrayList<Integer> ids = new ArrayList<>();
		for (RSItemConstant item : DropNode.drop) {
			ids.add(item.getId());
		}
		for (Tuple<RSItemConstant, Integer> item : equipment) {
			ids.add(item.x.getId());

			if (!Inventory.contains(item.x.getId())) {
				return true;
			}
		}

		return !Inventory.containsOnly(ids.stream().mapToInt((i) -> i).toArray());
	}

	@Override
	public void execute() {
		if (Navigation.toLumbridgeBank()) {
			if (Bank.openBooth("Bank", LUMBRIDGE_BANK_TILE_1, LUMBRIDGE_BANK_TILE_2)) {
				if (!Bank.isOpen() || !Bank.depositEquipment() || !Bank.depositInventory())
					return;

				for (Tuple<RSItemConstant, Integer> item : equipment) {
					if (!Time.sleepUntil(() -> Bank.contains(item.x.getId()))) {
						Email.sendEmail(Email.TYPE.SEVERE, "NewAccountFishing\nDoesn't have " + item.x.getName());
						System.exit(Settings.DISABLED_EXIT_CODE);
					}

					if (!Bank.withdraw(item.x.getId(), item.y))
						return;
				}
			}
		}
	}

}
