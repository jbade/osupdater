package org.iinc.osbot.scripts.fishing.fishing_guild;

import java.util.logging.Level;

import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.BankGoal;

public class GrandExchangeNode implements Node {

	private static org.iinc.osbot.scripts.common.GrandExchangeNode gen = new org.iinc.osbot.scripts.common.GrandExchangeNode(
			new RSItemConstant[] { Items.RAW_ANCHOVIES, Items.RAW_LOBSTER, Items.RAW_SHARK, Items.RAW_SHRIMPS,
					Items.RAW_SWORDFISH, Items.RAW_TUNA, Items.SKILLS_NECKLACE },
			true,
			new BankGoal[] { new BankGoal(Items.LOBSTER_POT, 1, () -> true), new BankGoal(Items.HARPOON, 1, () -> true),
					new BankGoal(Items.VARROCK_TELEPORT, 5, () -> true),
					new BankGoal(Items.SKILLS_NECKLACE_4, 1, () -> true) });

	public static void start() {
		org.iinc.osbot.scripts.common.GrandExchangeNode.start();
	}

	@Override
	public boolean activate() {
		return gen.activate();
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		gen.execute();
	}

}
