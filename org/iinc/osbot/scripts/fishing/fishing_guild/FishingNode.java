package org.iinc.osbot.scripts.fishing.fishing_guild;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.NpcDefinition;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class FishingNode implements Node {

	static final Tile MIDDLE_SPOT = new Tile(2600, 3420, 0);

	@Override
	public boolean activate() {
		return Client.getLocalPlayer().getAnimation() == -1 || !Client.getLocalPlayer().isInteracting();
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		if (Interacting.interact(() -> Npcs.getAll().parallelStream().filter((npc) -> {
			NpcDefinition def = npc.getNpcDefinition();
			return def != null && def.getId() == FishingConfig.config.getFishingSpotId();
		}).sorted((a, b) -> {
			Tile loc = Client.getLocalPlayer().getTile();
			double da = Calculations.distance(a.getTile(), loc);
			double db = Calculations.distance(b.getTile(), loc);

			if (loc.getY() >= 3418 && a.getY() < 3418 || loc.getY() < 3418 && a.getY() > 3418)
				da += 30;
			if (loc.getY() >= 3418 && b.getY() < 3418 || loc.getY() < 3418 && b.getY() > 3418)
				db += 30;

			if (da - db > 0) {
				return 1;
			} else if (da - db < 0) {
				return -1;
			} else {
				return 0;
			}
		}).findFirst().orElse(null), MIDDLE_SPOT, FishingConfig.config.getFishingSpotAction(), null)) {
			Time.sleepUntil(() -> Walking.isMoving(), 800, 1600);
			if (Time.sleepUntil(
					() -> !Walking.isMoving()
							|| Client.getLocalPlayer().isInteracting() && Client.getLocalPlayer().getAnimation() != -1,
					3500, 5000))
				Time.sleep(2000, 3000);
		}
	}

}
