package org.iinc.osbot.scripts.fishing.fishing_guild;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.BoundaryObject;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Navigation;

public class ToBankNode implements Node {

	static final Tile[] PATH_POINTS_BANK_TO_FISH = new Tile[] { new Tile(2607, 3400, 0), new Tile(2593, 3415, 0),
			new Tile(2587, 3420, 0) };
	static final Area FULL_AREA = new Area(new Tile[] { new Tile(2575, 3431, 0), new Tile(2626, 3425, 0),
			new Tile(2627, 3387, 0), new Tile(2576, 3392, 0) });
	static final Area HOUSE = new Area(new Tile[] { new Tile(2601, 3393, 0), new Tile(2607, 3399, 0),
			new Tile(2617, 3401, 0), new Tile(2616, 3393, 0), });
	static final Area OUTSIDE = new Area(new Tile[] { new Tile(2622, 3396, 0), new Tile(2618, 3393, 0),
			new Tile(2595, 3393, 0), new Tile(2595, 3384, 0), new Tile(2625, 3379, 0), });

	@Override
	public boolean activate() {
		Tile loc = Client.getLocalPlayer().getTile();

		return ((Inventory.isFull() || Inventory.getCount(FishingConfig.config.getEquipmentId()) != 1)
				&& !(Navigation.atGrandExchange() || Navigation.atLumbridgeBank()
						|| loc.distanceTo(BankingNode.BANK_TILE) < 3))
				|| (!FULL_AREA.contains(loc) && !Navigation.atGrandExchange() && !Navigation.atLumbridgeBank());
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		Tile loc = Client.getLocalPlayer().getTile();
		if (FULL_AREA.contains(loc)) {
			if (HOUSE.contains(loc)) {
				BoundaryObject door = RSObjects.getBoundaryObject(new Tile(2611, 3398, 0));
				if (door != null) {
					if (Interacting.interact(() -> new Tile(2611, 3398, 0, 0, 64, 64), new Tile(2611, 3398, 0))
							&& Time.sleepUntil(() -> Walking.isMoving(), 1000, 1600)) {
						Time.sleepUntil(() -> !Walking.isMoving(), 4600, 6000);
						Time.sleep(600, 1200);
					}
				} else {
					Mouse.click(new Tile(2604, 3402, 0).randomized(2, 2).toMiniMap(), Mouse.LEFT_BUTTON);
					if (Time.sleepUntil(() -> Walking.isMoving(), 1000, 1600)) {
						Time.sleepUntil(() -> !Walking.isMoving(), 4600, 6000);
						Time.sleep(600, 1200);
					}
				}
			} else if (OUTSIDE.contains(loc)) {
				if (Interacting.interact(() -> new Tile(2611, 3394, 0, 0, -64, 64), new Tile(2611, 3394, 0))
						&& Time.sleepUntil(() -> Walking.isMoving(), 1000, 1600)) {
					Time.sleepUntil(() -> !Walking.isMoving(), 4600, 6000);
					Time.sleep(600, 1200);
				}
			} else {
				FishingGuildFishing.checkRun();
				Walking.walkPath(Walking.generatePath(PATH_POINTS_BANK_TO_FISH, 3, false), true);
				Time.sleepUntil(() -> !Walking.isMoving() || Walking.getDestinationTile() == null, 3600, 5000);
			}
		} else {
			Navigation.toLumbridgeBank();
		}
	}

}
