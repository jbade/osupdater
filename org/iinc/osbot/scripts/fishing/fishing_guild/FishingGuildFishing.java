package org.iinc.osbot.scripts.fishing.fishing_guild;

import java.util.ArrayList;
import java.util.Collections;

import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.scripts.fishing.IdleNode;
import org.iinc.osbot.scripts.fishing.LoginNode;
import org.iinc.osbot.util.Memory;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class FishingGuildFishing implements Node {

	/*
	 * Modifiers script
	 * 0 - BankingNode tile/npc
	 * 1 - CatherbyFisher checkRun
	 * 2 - FishingNode execute interact
	 * 3 - FishingNode execute walk path
	 * 4 - FishingNode distance from middle tile
	 * 5 - ToBankNode walk fish to bank
	 * 6 - UpdateEquipmentNode
	 * 7 - IdleNode
	 * 8 - Deposit slot
	 * 9 - IdleNode lose focus
	 */

	private ArrayList<Node> nodes = new ArrayList<>();
	
	{
		Collections.addAll(nodes, new LoginNode(), new UpdateEquipmentNode(), new GrandExchangeNode(), new ToBankNode(),
				new BankingNode(), new FishingNode(), new IdleNode());
	}

	@Override
	public boolean activate() {
		return true;
	}

	@Override
	public void execute() {
		Memory.checkMemory(1000000L);
		
		Interfaces.closeAll();

		for (Node node : nodes) {
			if (node.activate()) {
				node.execute();
				return;
			}
		}
	}

	public static void checkRun() {
		if (Random.modifiedRandomRange(0, 100, 40, Modifiers.get("FishingGuild.checkRun1")) > 50
				&& Walking.getRunEnergy() > Random.modifiedRandomRange(30, 60, 25, Modifiers.get("FishingGuild.checkRun1")))
			Walking.enableRun();
	}
	
}
