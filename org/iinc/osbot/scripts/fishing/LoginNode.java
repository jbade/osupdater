package org.iinc.osbot.scripts.fishing;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;

public class LoginNode implements Node {

	@Override
	public boolean activate() {
		return !Login.isLoggedIn();
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		Login.login();
		Time.sleep(1200, 5000);
		Camera.setAngle(Camera.getCameraAngle(), true);
		Time.sleepReactionTime();
	}

}
