package org.iinc.osbot.scripts.fishing.catherby;

import java.util.logging.Level;

import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Navigation;

public class ToBankNode implements Node {

	static final Tile[] PATH_POINTS_BANK_TO_FISH = new Tile[] { new Tile(2862, 3427, 0), new Tile(2844, 3433, 0),
			new Tile(2832, 3436, 0), new Tile(2816, 3436, 0), new Tile(2809, 3440, 0) };
	static final Area CATHERBY_AREA = new Area(new Tile[] { new Tile(2669, 3509, 0), new Tile(2675, 3506, 0),
			new Tile(2683, 3495, 0), new Tile(2677, 3479, 0), new Tile(2664, 3454, 0), new Tile(2687, 3428, 0),
			new Tile(2667, 3406, 0), new Tile(2658, 3360, 0), new Tile(2743, 3361, 0), new Tile(2765, 3420, 0),
			new Tile(2864, 3421, 0), new Tile(2864, 3436, 0), new Tile(2833, 3494, 0), new Tile(2799, 3501, 0),
			new Tile(2780, 3508, 0), new Tile(2776, 3482, 0), new Tile(2743, 3481, 0), new Tile(2741, 3513, 0) });

	@Override
	public boolean activate() {
		Tile loc = Client.getLocalPlayer().getTile();

		return ((Inventory.isFull() || Inventory.getCount(FishingConfig.config.getEquipmentId()) != 1)
				&& !(Navigation.atGrandExchange() || Navigation.atLumbridgeBank()
						|| loc.distanceTo(BankingNode.CATHERBY_BANK_TILE) < 3))
				|| (!CATHERBY_AREA.contains(loc) && !Navigation.atGrandExchange() && !Navigation.atLumbridgeBank());
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		Tile loc = Client.getLocalPlayer().getTile();
		if (CATHERBY_AREA.contains(loc)) {
			CatherbyFishing.checkRun();
			Walking.walkPath(Walking.generatePath(PATH_POINTS_BANK_TO_FISH, 3, false), true);
			Time.sleepUntil(() -> !Walking.isMoving() || Walking.getDestinationTile() == null, 3600, 5000);
		} else {
			Navigation.toLumbridgeBank();
		}
	}

}
