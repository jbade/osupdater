package org.iinc.osbot.scripts.fishing.catherby;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.logging.Level;

import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Navigation;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class BankingNode implements Node {

	static final Tile CATHERBY_BANK_TILE = new Tile(2809, 3440, 0);
	static final Tile[] CATHERBY_BANK_BOOTH_TILES = new Tile[] { new Tile(2811, 3442, 0), new Tile(2810, 3442, 0),
			new Tile(2809, 3442, 0), new Tile(2807, 3442, 0) };

	static final Tile LUMBRIDGE_BANK_TILE_1 = new Tile(3209, 3221, 2);
	static final Tile LUMBRIDGE_BANK_TILE_2 = new Tile(3208, 3221, 2);

	@Override
	public boolean activate() {
		return !ToBankNode.CATHERBY_AREA.contains(Client.getLocalPlayer().getTile())
				|| Inventory.getCount(FishingConfig.config.getEquipmentId()) != 1 || Inventory.isFull();
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		Tile loc = Client.getLocalPlayer().getTile();
		if (loc.distanceTo(CATHERBY_BANK_TILE) < 3) {

			// catherby bank

			boolean first = Random.modifiedRandomGuassianRange(0, 100, ConfidenceIntervals.CI_95, 40,
					Modifiers.get("CatherbyFishing.bankingBooth")) >= 50;
			if (first && Bank.openBooth(CATHERBY_BANK_BOOTH_TILES) || !first && Bank.openBanker()) {
				Time.sleepReactionTime();

				// deposit items
				for (int tries = 0, failures = 0; tries < 28 && failures < 3
						&& !Inventory.containsOnly(FishingConfig.config.getEquipmentId()); tries++) {
					ArrayList<Item> items = Inventory.getItems();
					HashSet<Integer> toDeposit = new HashSet<Integer>();

					for (Item item : items)
						if (FishingConfig.config.getEquipmentId() != item.getId())
							toDeposit.add(item.getId());

					for (Integer id : toDeposit) {
						items = Inventory.getAllWithIds(id);
						
						int rand = Random.modifiedRandomGuassianRange(0, items.size() - 1, ConfidenceIntervals.CI_90,
								Modifiers.get("CatherbyFishing.deposit"));
						if (rand < 0)
							rand = 0;
						if (rand > items.size() - 1)
							rand = items.size() - 1;

						if (!Bank.depositSlot(items.get(rand).getIndex(), -1))
							failures++;
					}
					
					Time.sleepUntil(() -> Inventory.getCount() == 1, 600, 1200);
				}

				if (!Inventory.containsOnly(FishingConfig.config.getEquipmentId())
						|| Inventory.getCount(FishingConfig.config.getEquipmentId()) != 1) {
					Time.sleepReactionTime();
					Bank.depositInventory();
					if (!Time.sleepUntil(() -> Inventory.getCount() == 0, 600, 1200))
						return;

					if (!Bank.contains(FishingConfig.config.getEquipmentId())) {
						needResupply();
						return;
					}

					Bank.withdraw(FishingConfig.config.getEquipmentId(), 1);
					Time.sleepReactionTime();
				}
			}

		} else {

			// lumbridge/grand exchange bank

			if (Inventory.contains(Items.CAMELOT_TELEPORT.getId())) {
				if (Clicking.interact(Inventory.getFirst(Items.CAMELOT_TELEPORT.getId()), "Break")
						&& Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != -1, 600, 800)) {
					Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == -1, 3000, 5000);
				}
				return;
			}

			// open bank
			if (Navigation.atGrandExchange()) {
				if (!Bank.openBanker())
					return;
			} else if (Navigation.atLumbridgeBank()) {
				if (!Bank.openBooth("Bank", LUMBRIDGE_BANK_TILE_1, LUMBRIDGE_BANK_TILE_2))
					return;
			}

			if (Inventory.getCount() != 0
					&& (!Bank.depositInventory() || !Time.sleepUntil(() -> Inventory.getCount() == 0, 1200, 1700)))
				return;

			// check if we have necessary equipment
			if (!Bank.contains(FishingConfig.config.getEquipmentId())) {
				needResupply();
				return;
			}

			if (Bank.contains(Items.CAMELOT_TELEPORT.getId())) {
				if (!Bank.withdraw(Items.CAMELOT_TELEPORT.getId(), 1))
					return;
			} else {
				needResupply();
				return;
			}
		}
	}

	private void needResupply() {
		Bot.LOGGER.log(Level.SEVERE, "Need resupply");

		if (Inventory.getCount() != 0
				&& (!Bank.depositInventory() || !Time.sleepUntil(() -> Inventory.getCount() == 0, 1200, 1700)))
			return;

		if (!Navigation.atGrandExchange() && Bank.contains(Items.VARROCK_TELEPORT.getId())) {
			if (!Bank.withdraw(Items.VARROCK_TELEPORT.getId(), 1))
				return;

			if (!Bank.close())
				return;

			Time.sleep(Random.randomRange(600, 1200));

			if (Clicking.interact(Inventory.getFirst(Items.VARROCK_TELEPORT.getId()), "Break")) {
				Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 4069
						|| Client.getLocalPlayer().getAnimation() == 4071, 600, 1200);
				Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == -1, 3000, 4000);
				GrandExchangeNode.start();
			}
		} else {
			GrandExchangeNode.start();
		}
	}

}
