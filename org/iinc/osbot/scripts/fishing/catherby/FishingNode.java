package org.iinc.osbot.scripts.fishing.catherby;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.NpcDefinition;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class FishingNode implements Node {

	static final Tile MIDDLE_SPOT = new Tile(2850, 3429, 0);
	static final Tile[] PATH_POINTS_FISH_TO_BANK = new Tile[] { new Tile(2809, 3441, 0), new Tile(2820, 3436, 0),
			new Tile(2836, 3433, 0) };

	@Override
	public boolean activate() {
		return Client.getLocalPlayer().getAnimation() == -1 || !Client.getLocalPlayer().isInteracting();
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());


		if (Client.getLocalPlayer().getTile().distanceTo(MIDDLE_SPOT) < Random.modifiedRandomRange(18, 22, 4,
				Modifiers.get("CatherbyFishing.middle"))) {
			CatherbyFishing.checkRun();
			if (Interacting.interact(
					() -> Npcs.getAll().parallelStream()
							.filter((npc) -> {
								NpcDefinition def = npc.getNpcDefinition();
								return def != null && def.getId() == FishingConfig.config.getFishingSpotId();
							})
							.sorted().findFirst().orElse(null),
					MIDDLE_SPOT, FishingConfig.config.getFishingSpotAction(), null)) {
				Time.sleepUntil(() -> Walking.isMoving(), 800, 1600);
				Time.sleepUntil(() -> Walking.isMoving(), 800, 1600);
				if (Time.sleepUntil(
						() -> !Walking.isMoving()
								|| Client.getLocalPlayer().isInteracting() && Client.getLocalPlayer().getAnimation() != -1,
						3500, 5000))
					Time.sleep(2000, 3000);
			}
			
		} else {
			Bot.LOGGER.log(Level.FINEST, "Walking to fish");
			CatherbyFishing.checkRun();
			Walking.walkPath(Walking.generatePath(PATH_POINTS_FISH_TO_BANK, 3, true), true);
		}
	}

}
