package org.iinc.osbot.scripts.fishing.catherby;

import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class CatherbyFishingScript extends Script {

	CatherbyFishing cf = new CatherbyFishing();

	@Override
	public int loop() {
		if (BreakHandler.shouldBreak()) {
			Interfaces.closeAll();
			Timer t = new Timer(Random.randomRange(10000, 15000));
			while (Login.isLoggedIn() && t.isRunning())
				Logout.clickLogout();
			BreakHandler.doBreak();
		}
		
		if (cf.activate())
			cf.execute();

		return Random.randomRange(50, 100);
	}

	@Override
	public void onStop() {

	}

	@Override
	public void onStart() {
		Modifiers.init(Bot.username.hashCode());
		BreakHandler.init(Bot.username.hashCode());
		
	}

}
