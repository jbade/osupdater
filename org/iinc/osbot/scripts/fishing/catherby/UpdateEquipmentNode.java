package org.iinc.osbot.scripts.fishing.catherby;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class UpdateEquipmentNode implements Node {

	@Override
	public boolean activate() {
		return FishingConfig.config == null
				|| Random.modifiedRandomRange(10, 100, 15, Modifiers.get("CatherbyFishing.updateEquip")) > 95;
	}

	@Override
	public void execute() {
		int fishingLevel = Levels.getCurrentFishing();

		FishingConfig.config = null;
		for (FishingConfig cfg : FishingConfig.CONFIGS) {
			if (FishingConfig.config == null || fishingLevel >= cfg.getFishingLevel()) {
				FishingConfig.config = cfg;
			}
		}
	}

}
