package org.iinc.osbot.scripts.fishing.catherby;

import java.util.ArrayList;
import java.util.List;

import org.iinc.osbot.api.base.item.Items;

public class FishingConfig {
	public static FishingConfig config;
	public final static List<FishingConfig> CONFIGS = new ArrayList<FishingConfig>();

	static {
		CONFIGS.add(
				new FishingConfig(0, Items.SMALL_FISHING_NET.getId(), Items.SMALL_FISHING_NET.getName(), 1518, "Net"));
		CONFIGS.add(new FishingConfig(35, Items.HARPOON.getId(), Items.HARPOON.getName(), 1519, "Harpoon"));
		CONFIGS.add(new FishingConfig(55, Items.LOBSTER_POT.getId(), Items.LOBSTER_POT.getName(), 1519, "Cage"));
		CONFIGS.add(new FishingConfig(76, Items.HARPOON.getId(), Items.HARPOON.getName(), 1520, "Harpoon"));
	}

	private int fishingLevel;
	private int equipmentId;
	private String equipmentName;
	private int fishingSpotId;
	private String fishingSpotAction;

	private FishingConfig(int fishingLevel, int equipmentId, String equipmentName, int fishingSpotId,
			String fishingSpotAction) {
		this.fishingLevel = fishingLevel;
		this.equipmentId = equipmentId;
		this.equipmentName = equipmentName;
		this.fishingSpotId = fishingSpotId;
		this.fishingSpotAction = fishingSpotAction;
	}

	public int getFishingLevel() {
		return fishingLevel;
	}

	public int getEquipmentId() {
		return equipmentId;
	}

	public String getEquipmentName() {
		return equipmentName;
	}

	public int getFishingSpotId() {
		return fishingSpotId;
	}

	public String getFishingSpotAction() {
		return fishingSpotAction;
	}
}
