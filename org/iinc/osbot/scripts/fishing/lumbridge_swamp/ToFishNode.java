package org.iinc.osbot.scripts.fishing.lumbridge_swamp;

import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Navigation;

public class ToFishNode implements Node {

	@Override
	public boolean activate() {
		return LumbridgeSwampFishing.FISH_CENTER_TILE.distanceTo(Client.getLocalPlayer().getTile()) > 15;
	}

	@Override
	public void execute() {
		if (LumbridgeSwampFishing.REACHABLE_AREA.contains(Client.getLocalPlayer().getTile())) {
			Walking.walkPath(Walking.generatePath(LumbridgeSwampFishing.PATH_TO_FISH, 5, true), true);
		} else {
			Navigation.toLumbridge();
		}
	}

}
