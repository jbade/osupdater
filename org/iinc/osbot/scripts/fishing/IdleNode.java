package org.iinc.osbot.scripts.fishing;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class IdleNode implements Node {

	@Override
	public boolean activate() {
		return true;
	}

	@Override
	public void execute() {
		if (Mouse.isFocused() && Random.modifiedRandomRange(0, 100, 30, Modifiers.get("Fishing.idle1")) >= 40) {
			Mouse.moveOffscreen();
		}

		Time.sleep(Random.modifiedRandomGuassianRange(5000, 30000, ConfidenceIntervals.CI_60, 5000,
				Modifiers.get("Fishing.idle2")));
	}

}
