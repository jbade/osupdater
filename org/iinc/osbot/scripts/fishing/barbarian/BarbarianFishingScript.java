package org.iinc.osbot.scripts.fishing.barbarian;

import java.util.ArrayList;
import java.util.Collections;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.scripts.fishing.DropNode;
import org.iinc.osbot.scripts.fishing.IdleNode;
import org.iinc.osbot.util.Memory;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class BarbarianFishingScript extends Script {

	private ArrayList<Node> nodes = new ArrayList<>();

	{
		Collections.addAll(nodes, new DropNode(), new FishingNode(), new IdleNode());
	}

	public static void checkRun() {
		if (Random.modifiedRandomRange(0, 100, 40, Modifiers.get("BarbarianFishing.checkRun1")) > 50
				&& Walking.getRunEnergy() > Random.modifiedRandomRange(30, 60, 25, Modifiers.get("BarbarianFishing.checkRun2")))
			Walking.enableRun();
	}

	@Override
	public int loop() {
		Memory.checkMemory(1000000L);

		if (!Login.isLoggedIn()) {
			Login.login();
			Camera.setAngle(Camera.getCameraAngle(), true);
			return 0;
		}
		
		if (BreakHandler.shouldBreak())
			System.exit(0);
		
		if (Inventory.isItemSelected()) {
			Menu.interact("Cancel");
		}

		Interfaces.closeAll();

		if (Inventory.getCount(Items.FEATHER.getId()) == 0){
			System.exit(Settings.DISABLED_EXIT_CODE);
		}
		
		for (Node node : nodes) {
			if (node.activate()) {
				node.execute();
				return Random.randomRange(50, 100);
			}
		}
		return Random.randomRange(50, 100);
	}

	@Override
	public void onStop() {

	}

	@Override
	public void onStart() {
		Modifiers.init(Bot.username.hashCode());
		BreakHandler.init(Bot.username.hashCode());
	}

}
