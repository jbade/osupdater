package org.iinc.osbot.scripts.fishing;

import java.awt.Point;
import java.util.List;
import java.util.stream.Collectors;

import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class DropNode implements Node {
	public static RSItemConstant[] drop = { Items.RAW_ANCHOVIES, Items.RAW_SHRIMPS, Items.RAW_TROUT, Items.RAW_SALMON,
			Items.LEAPING_SALMON, Items.LEAPING_STURGEON, Items.LEAPING_TROUT };

	@Override
	public boolean activate() {
		return Inventory.isFull();
	}

	@Override
	public void execute() {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 6; j++) {
				Inventory.open();

				int slot = i + j * 4;
				Item item = Inventory.getSlot(slot);
				if (item != null) {

					boolean cont = false;
					for (RSItemConstant i2 : drop) {
						if (i2.getId() == item.getId()) {
							cont = true;
							break;
						}
					}

					if (!cont)
						continue;

					if (Inventory.isItemSelected()) {
						Menu.interact("Cancel");
					}

					if (!Inventory.getBounds(slot).contains(Mouse.getLocation())) {
						Clicking.hover(Inventory.getBounds(slot));
						Time.sleep(Random.modifiedRandomGuassianRange(50, 300, ConfidenceIntervals.CI_70, 0,
								Modifiers.get("Fishing.drop1")));
					}

					Time.sleep(Random.modifiedRandomGuassianRange(20, 40, ConfidenceIntervals.CI_50, 0,
							Modifiers.get("Fishing.drop2")));

					Mouse.click(Mouse.RIGHT_BUTTON);
					Time.sleep(Random.modifiedRandomGuassianRange(20, 40, ConfidenceIntervals.CI_50, 0,
							Modifiers.get("Fishing.drop3")));

					Point pos = Mouse.getLocation();
					Mouse.setLocation(pos.x, pos.y + 36);

					Time.sleep(Random.modifiedRandomGuassianRange(30, 50, ConfidenceIntervals.CI_60, 0,
							Modifiers.get("Fishing.drop4")));

					if (Menu.getMenuOptions().stream().anyMatch((e) -> e.contains("eather"))) {
						Menu.close();
						continue;
					}

					Mouse.click(Mouse.LEFT_BUTTON);

					Time.sleep(Random.modifiedRandomGuassianRange(30, 50, ConfidenceIntervals.CI_50, 0,
							Modifiers.get("Fishing.drop5")));

				}
			}
		}

//		List<Item> items = Inventory.getItems().stream().filter((i) -> {
//			for (RSItemConstant item : drop) {
//				if (item.getId() == i.getId()) {
//					return true;
//				}
//			}
//
//			return false;
//		}).collect(Collectors.toList());
//
//		for (Item item : items) {
//			Inventory.open();
//
//			if (Inventory.isItemSelected()) {
//				Menu.interact("Cancel");
//			}
//
//			Mouse.move(Inventory.getBounds(item.getIndex()));
//
//			Menu.interact(new String[] { "Drop" }, null);
//			Time.sleep(Random.modifiedRandomGuassianRange(0, 350, ConfidenceIntervals.CI_50, 50,
//					Modifiers.get("Fishing.Drop.sleep")));
//
//		}
	}

}
