package org.iinc.osbot.scripts.herblore;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.grandexchange.GrandExchangeInterface;
import org.iinc.osbot.api.base.interfaces.grandexchange.GrandExchangeInventory;
import org.iinc.osbot.api.base.interfaces.grandexchange.Margin;
import org.iinc.osbot.api.base.interfaces.grandexchange.RSBuddyExchange;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.api.base.item.RSItem;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.grandexchange.GrandExchange;
import org.iinc.osbot.api.injection.accessors.grandexchange.GrandExchangeOffer;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ArrayUtil;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class GrandExchangeNode implements Node {

	private static long MIN_UPDATE_TIME = 10 * 60 * 1000;

	public static boolean purchaseItems = false;
	private long nextUpdate;
	private long lastUpdate;
	private boolean emptiedBank = false;

	@Override
	public boolean activate() {
		return purchaseItems || nextUpdate < System.currentTimeMillis();
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());

		// should update
		if (System.currentTimeMillis() - lastUpdate > MIN_UPDATE_TIME) {
			// get items to sell from bank
			if (!emptiedBank) {
				emptyBank();
				return;
			}

			// update offers
			if (!Inventory.containsOnly(Herblore.COINS_ID) || GrandExchange.getOffers().size() != 0) {
				if (!Bank.close() || !GrandExchangeInterface.openClerk()) {
					return;
				}

				// update sell offers
				for (GrandExchangeOffer offer : GrandExchange.getOffers()) {
					if (offer.isSellOffer()) {
						GrandExchangeInterface.abortOffer(offer);
						GrandExchangeInterface.collect(offer, !offer.isBuyOffer());
					}
				}

				for (Entry<RSItem, Integer> item : GrandExchangeInventory.getInventory().entrySet()) {
					if (item.getKey().getId() != Herblore.COINS_ID)
						GrandExchangeInterface.createSlowSellOffer(item.getKey().getName(), item.getKey().getId(), -1);
				}

				// update buy offers
				ArrayList<BuyOffer> buy = new ArrayList<>();

				for (GrandExchangeOffer offer : GrandExchange.getOffers()) {
					if (offer.isBuyOffer()) {
						int left = offer.getQuantity() - offer.getTransfered();
						String item = GrandExchangeInterface.getItemName(offer);
						if (left > 0 && item != null) {
							buy.add(new BuyOffer(item, offer.getItemId(), left));
						}

						GrandExchangeInterface.abortOffer(offer);
						GrandExchangeInterface.collect(offer, true);
					}
				}

				for (BuyOffer bo : buy) {
					if (!GrandExchangeInterface.createSlowBuyOffer(bo.name, bo.id, bo.quantity)) {
						PriceCalculator.addBannedItem(new RSItem(bo.name, bo.id));
					}
				}
			}

			emptiedBank = false;
			nextUpdate = System.currentTimeMillis() + Random.randomRange(20 * 60 * 1000, 30 * 60 * 1000);
			lastUpdate = System.currentTimeMillis();
			purchaseItems = false;
			return;
		}

		// need more items

		// get items to sell from bank
		if (!emptiedBank) {
			emptyBank();
			return;
		}

		if (!Bank.close())
			return;

		if (GrandExchangeInterface.openClerk()) {

			// sell items in inv
			for (GrandExchangeOffer offer : GrandExchange.getOffers()) {
				if (offer.isSellOffer()
						&& GrandExchangeInventory.getCount(GrandExchangeInterface.getItemName(offer)) > 0) {
					GrandExchangeInterface.abortOffer(offer);
					GrandExchangeInterface.collect(offer, true);
				}
			}

			for (Entry<RSItem, Integer> item : GrandExchangeInventory.getInventory().entrySet()) {
				if (item.getKey().getId() != Herblore.COINS_ID)
					GrandExchangeInterface.createSlowSellOffer(item.getKey().getName(), item.getKey().getId(), -1);
			}

			// wait if buy offers exist
			if (GrandExchange.getOffers().parallelStream().filter((o) -> o.isBuyOffer()).count() > 0) {

				Timer t = new Timer(Random.randomRange(15 * 60 * 1000, 20 * 60 * 1000));
				emptiedBank = false;

				while (GrandExchangeInterface.isOpen() && t.isRunning()
						&& System.currentTimeMillis() - lastUpdate < MIN_UPDATE_TIME
						&& GrandExchange.getOffers().parallelStream()
								.filter((o) -> o.isBuyOffer() && o.getState() != GrandExchangeOffer.STATES.COMPLETE)
								.count() > 0
						&& !Inventory.isFull()) {

					if (GrandExchangeInterface.canCollect()) {
						GrandExchangeInterface.collect(true);
						emptiedBank = false;
					}

					if (GrandExchangeInventory.getInventoryItems().parallelStream()
							.filter((i) -> !"Coins".equals(i) && GrandExchangeInventory.getCount(i) > 28).count() > 0) {
						purchaseItems = false;
						return;
					}
					
					Time.sleep(200, 500);
				}

				if (GrandExchange.getOffers().parallelStream()
						.filter((o) -> o.isBuyOffer() && o.getState() == GrandExchangeOffer.STATES.COMPLETE)
						.count() > 0) {
					if (GrandExchangeInterface.collect(true)) {
						if (GrandExchangeInventory.getInventoryItems().parallelStream()
								.filter((i) -> !"Coins".equals(i) && GrandExchangeInventory.getCount(i) > 28)
								.count() > 0) {
							purchaseItems = false;
						}
					}
				}
			} else {
				// create buy offers if buy offers do not exist

				// update margins
				for (RSItem item : PriceCalculator.getAllItems()) {
					// try using rsbuddy api
					Margin margin = RSBuddyExchange.getPriceMarginSummary(item.getId());

					// fallback to using grand exchange
					if (margin == null) {
						if (item.getName().equals("Vial of water")) {
							margin = new Margin(10, 10);
						} else {
							margin = GrandExchangeInterface.checkMargin(item.getName(), item.getId());
						}
					}

					if (margin != null) {
						PriceCalculator.updateMargin(item, margin);
					} else {
						PriceCalculator.addBannedItem(item);
					}
				}

				ArrayList<RSItem> input = PriceCalculator.getItems(Levels.getCurrentHerblore());
				System.out.println(input);

				int cost = 0;
				for (RSItem item : input) {
					if (item.getName().equals("Vial of water")) {
						cost += PriceCalculator.getMargin(item).instantBuy * 2;
					} else {
						cost += PriceCalculator.getMargin(item).instantSell;
					}
				}

				int coins = GrandExchangeInventory.getCount("Coins") - 100000;
				int quantity = Math.min(coins / cost, 2000);

				// not enough money to buy items, wait for sell offers to complete
				if (quantity < 28) {
					Timer t = new Timer(Random.randomRange(10 * 60 * 1000, 15 * 60 * 1000));
					while (quantity < 100 && GrandExchangeInterface.isOpen() && t.isRunning()
							&& System.currentTimeMillis() - lastUpdate < MIN_UPDATE_TIME
							&& GrandExchange.getOffers().parallelStream().filter((o) -> o.isSellOffer()).count() > 0
							&& !Inventory.isFull()) {

						if (GrandExchangeInterface.canCollect()) {
							GrandExchangeInterface.collect(true);
							emptiedBank = false;
						}

						coins = GrandExchangeInventory.getCount("Coins") - 100000;
						quantity = Math.min(coins / cost, 2000);
						Time.sleep(200, 500);
					}

					if (quantity < 28) {
						emptiedBank = false;
						return;
					}
				}

				// buy items
				for (RSItem item : input) {
					if (item.getName().equals("Vial of water")) {
						GrandExchangeInterface.createBuyOfferPercent(item.getName(), 1.5, quantity);
					} else {
						if (!GrandExchangeInterface.createSlowBuyOffer(item.getName(), item.getId(), quantity)) {
							PriceCalculator.addBannedItem(item);
						}
					}

				}

				purchaseItems = false;
				emptiedBank = false;

				Time.sleep(2000, 4000);
				GrandExchangeInterface.collect(false);
			}
		}
	}

	private void emptyBank() {
		if (!GrandExchangeInterface.close())
			return;

		if (Bank.openBanker()) {
			boolean invClean = true;
			for (Item item : Inventory.getItems()) {
				// -1 because items are noted
				invClean = invClean && (item.getId() == Herblore.COINS_ID
						|| ArrayUtil.contains(Herblore.UNF_POTION_IDS, item.getId() - 1)
						|| ArrayUtil.contains(Herblore.CLEAN_HERB_IDS, item.getId() - 1));
			}

			if (invClean) {
				Time.sleep(600, 1200);
			} else if ((!Bank.depositInventory() || !Time.sleepUntil(() -> Inventory.getCount() == 0, 1200, 1700)))
				return;

			if (Bank.contains(Herblore.COINS_ID) && !Bank.withdraw(Herblore.COINS_ID, -1))
				return;

			boolean more = Bank.contains(Herblore.UNF_POTION_IDS);
			int vials = Bank.getQuantity(Herblore.VIAL_OF_WATER_ID);

			for (int id : Herblore.CLEAN_HERB_IDS) {
				if (Bank.getQuantity(id) > vials) {
					more = true;
					break;
				}
			}
			if (more) {

				if (!Bank.withdrawAsNote())
					return;

				if (!Bank.withdraw(Herblore.UNF_POTION_IDS, -1))
					return;

				for (int id : Herblore.CLEAN_HERB_IDS) {
					if (Bank.getQuantity(id) > vials) {
						if (!Bank.withdraw(id, -1))
							return;
					}
				}
			}

			emptiedBank = true;
		}
	}

	private static class BuyOffer {
		int id, quantity;
		String name;

		public BuyOffer(String name, int id, int quantity) {
			this.name = name;
			this.id = id;
			this.quantity = quantity;
		}
	}
}
