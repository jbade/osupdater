package org.iinc.osbot.scripts.herblore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.iinc.osbot.api.base.interfaces.grandexchange.Margin;
import org.iinc.osbot.api.base.item.RSItem;
import org.iinc.osbot.util.internet.Internet;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class PriceCalculator {

	private static HashMap<RSItem, Long> bannedItems = new HashMap<RSItem, Long>();
	private static HashMap<RSItem, Margin> margins = new HashMap<RSItem, Margin>();

	private static ArrayList<Item> items = new ArrayList<Item>();
	
	static {
		Collections.addAll(items,
				new Item(3, 2.5, new RSItem[] { new RSItem("Grimy guam leaf", 199) },
						new RSItem[] { new RSItem("Guam leaf", 249) }, 1500),
				new Item(5, 3.8, new RSItem[] { new RSItem("Grimy marrentill", 201) },
						new RSItem[] { new RSItem("Marrentill", 251) }, 2000),
				new Item(11, 5, new RSItem[] { new RSItem("Grimy tarromin", 203) },
						new RSItem[] { new RSItem("Tarromin", 253) }, 2000),
				new Item(20, 6.3, new RSItem[] { new RSItem("Grimy harralander", 205) },
						new RSItem[] { new RSItem("Harralander", 255) }, 2000),
				//new Item(25, 7.5, new RSItem[] { new RSItem("Grimy ranarr weed", 207) },
				//		new RSItem[] { new RSItem("Ranarr weed", 257) }, 2000),
//			new Item(30, 8, new RSItem[] { new RSItem("Grimy toadflax", 3049) },
//						new RSItem[] { new RSItem("Toadflax", 2998) }, 2000),
				//new Item(40, 8.8, new RSItem[] { new RSItem("Grimy irit leaf", 209) },
				//		new RSItem[] { new RSItem("Irit leaf", 259) }, 2000),
				//new Item(48, 10, new RSItem[] { new RSItem("Grimy avantoe", 211) },
				//		new RSItem[] { new RSItem("Avantoe", 261) }, 2000),
				//new Item(54, 11.3, new RSItem[] { new RSItem("Grimy kwuarm", 213) },
				//		new RSItem[] { new RSItem("Kwuarm", 263) }, 2000),
				//new Item(59, 11.8, new RSItem[] { new RSItem("Grimy snapdragon", 3051) },
				//		new RSItem[] { new RSItem("Snapdragon", 3000) }, 2000),
//				new Item(65, 12.5, new RSItem[] { new RSItem("Grimy cadantine", 215) },
//						new RSItem[] { new RSItem("Cadantine", 265) }, 2000),
//				new Item(67, 13.1, new RSItem[] { new RSItem("Grimy lantadyme", 2485) },
//						new RSItem[] { new RSItem("Lantadyme", 2481) }, 2000),
//				new Item(70, 13.8, new RSItem[] { new RSItem("Grimy dwarf weed", 217) },
//						new RSItem[] { new RSItem("Dwarf weed", 267) }, 2000),
				//new Item(3, 0, new RSItem[] { new RSItem("Vial of water", 227), new RSItem("Guam leaf", 249) },
				//		new RSItem[] { new RSItem("Guam potion (unf)", 91) }, 1700),
				//new Item(-1, 0, new RSItem[] { new RSItem("Vial of water", 227), new RSItem("Marrentill", 251) },
				//		new RSItem[] { new RSItem("Marrentill potion (unf)", 93) }, 1700),
				//new Item(-1, 0, new RSItem[] { new RSItem("Vial of water", 227), new RSItem("Tarromin", 253) },
				//		new RSItem[] { new RSItem("Tarromin potion (unf)", 95) }, 1700),
				new Item(-1, 0, new RSItem[] { new RSItem("Vial of water", 227), new RSItem("Harralander", 255) },
						new RSItem[] { new RSItem("Harralander potion (unf)", 97) }, 1700),
				new Item(30, 0, new RSItem[] { new RSItem("Vial of water", 227), new RSItem("Ranarr weed", 257) },
						new RSItem[] { new RSItem("Ranarr potion (unf)", 99) }, 1700),
				//new Item(34, 0, new RSItem[] { new RSItem("Vial of water", 227), new RSItem("Toadflax", 2998) },
				//		new RSItem[] { new RSItem("Toadflax potion (unf)", 3002) }, 1700),
				new Item(45, 0, new RSItem[] { new RSItem("Vial of water", 227), new RSItem("Irit leaf", 259) },
						new RSItem[] { new RSItem("Irit potion (unf)", 101) }, 1700),
				new Item(50, 0, new RSItem[] { new RSItem("Vial of water", 227), new RSItem("Avantoe", 261) },
						new RSItem[] { new RSItem("Avantoe potion (unf)", 103) }, 1700),
				new Item(55, 0, new RSItem[] { new RSItem("Vial of water", 227), new RSItem("Kwuarm", 263) },
						new RSItem[] { new RSItem("Kwuarm potion (unf)", 105) }, 1700),
				new Item(63, 0, new RSItem[] { new RSItem("Vial of water", 227), new RSItem("Snapdragon", 3000) },
						new RSItem[] { new RSItem("Snapdragon potion (unf)", 3004) }, 1700)//,
//				new Item(66, 0, new RSItem[] { new RSItem("Vial of water", 227), new RSItem("Cadantine", 265) },
//						new RSItem[] { new RSItem("Cadantine potion (unf)", 107) }, 1700),
//				new Item(69, 0, new RSItem[] { new RSItem("Vial of water", 227), new RSItem("Lantadyme", 2481) },
//						new RSItem[] { new RSItem("Lantadyme potion (unf)", 2483) }, 1700),
//				new Item(72, 0, new RSItem[] { new RSItem("Vial of water", 227), new RSItem("Dwarf weed", 267) },
//						new RSItem[] { new RSItem("Dwarf weed potion (unf)", 109) }, 1700)
				);
	}

	public static void addBannedItem(RSItem item) {
		bannedItems.put(item, System.currentTimeMillis() + 60 * 60 * 1000);
	}

	public static void updateMargin(RSItem item, Margin margin) {
		margins.put(item, margin);
	}

	public static Margin getMargin(RSItem item) {
		return margins.get(item);
	}

	public static boolean isBannedItem(RSItem item) {
		return bannedItems.containsKey(item);
	}

	public static Set<RSItem> getAllItems() {
		Set<RSItem> s = new HashSet<>();
		for (Item item : items) {
			for (RSItem rsi : item.input)
				s.add(rsi);
			for (RSItem rsi : item.output)
				s.add(rsi);
		}
		return s;
	}

	public static ArrayList<RSItem> getItems(int herbloreLevel) {
		ArrayList<RSItem> result = new ArrayList<RSItem>();

		for (Item item : items) {
			int in = 0, out = 0;
			boolean skip = false;
			for (RSItem i : item.input) {
				if (bannedItems.containsKey(i))
					skip = true;
				in += margins.get(i).instantBuy + 1;
			}
			if (skip) {
				item.profitPerHour = Integer.MIN_VALUE;
				item.margin = Integer.MIN_VALUE;
				break;
			}

			for (RSItem i : item.output) {
				out += margins.get(i).instantSell - 1;
			}

			item.margin = out - in;
			item.profitPerHour = item.margin * item.processedPerHour;
		}

		List<Item> todo = items.stream().sorted((a, b) -> b.profitPerHour - a.profitPerHour)
				.collect(Collectors.toList());

		boolean first = true;
		for (Item item : todo) {
			System.out.println(item);
			if (item.levelRequirement <= herbloreLevel) {
				if (first || item.xp > 0) {
					for (RSItem i : item.input) {
						result.add(i);
					}

					return result;
				}
			}
			first = false;
		}
		return result;
	}

	public static HashMap<RSItem, Integer> getItemsRSBuddy(int herbloreLevel) {
		HashMap<RSItem, Integer> result = new HashMap<RSItem, Integer>();

		String s = null;
		while (s == null) {
			s = String.join("", Internet.read("https://rsbuddy.com/exchange/summary.json"));
		}

		JsonObject json = new JsonParser().parse(s).getAsJsonObject();

		Iterator<Map.Entry<RSItem, Long>> iter = bannedItems.entrySet().iterator();
		while (iter.hasNext()) {
			Entry<RSItem, Long> entry = iter.next();
			if (System.currentTimeMillis() > entry.getValue()) {
				iter.remove();
			}
		}

		for (Item item : items) {
			int in = 0, out = 0;
			boolean skip = false;
			for (RSItem i : item.input) {
				if (bannedItems.containsKey(i))
					skip = true;
				in += json.get(Integer.toString(i.getId())).getAsJsonObject().get("sell_average").getAsInt();
			}
			if (skip) {
				item.profitPerHour = Integer.MIN_VALUE;
				item.margin = Integer.MIN_VALUE;
				break;
			}

			for (RSItem i : item.output) {
				out += json.get(Integer.toString(i.getId())).getAsJsonObject().get("buy_average").getAsInt();
			}

			item.margin = out - in;
			item.profitPerHour = item.margin * item.processedPerHour;
		}

		List<Item> todo = items.stream().sorted((a, b) -> a.profitPerHour - b.profitPerHour)
				.collect(Collectors.toList());
		boolean first = true;
		for (Item item : todo) {
			if (item.levelRequirement <= herbloreLevel) {
				if (first || item.xp > 0) {
					for (RSItem i : item.input) {
						result.put(i,
								json.get(Integer.toString(i.getId())).getAsJsonObject().get("sell_average").getAsInt());
					}

					return result;
				}
			}
			first = false;
		}
		return result;
	}

	static class Item {
		int levelRequirement;
		RSItem[] input;
		RSItem[] output;
		double xp;
		int processedPerHour;
		int margin;
		int profitPerHour;
		int inputCost;

		public Item(int levelRequirement, double xp, RSItem[] input, RSItem[] output, int processedPerHour) {
			super();
			this.xp = xp;
			this.levelRequirement = levelRequirement;
			this.input = input;
			this.output = output;
			this.processedPerHour = processedPerHour;
		}

		@Override
		public String toString() {
			return "Item [levelRequirement=" + levelRequirement + ", input=" + Arrays.toString(input) + ", output="
					+ Arrays.toString(output) + ", xp=" + xp + ", processedPerHour=" + processedPerHour + ", margin="
					+ margin + ", profitPerHour=" + profitPerHour + ", inputCost=" + inputCost + "]";
		}
		
	}
}
