package org.iinc.osbot.scripts.herblore;

import java.awt.Point;
import java.util.logging.Level;

import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.grandexchange.GrandExchangeInterface;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class CleaningNode implements Node {

	@Override
	public boolean activate() {
		return Inventory.getCount(Herblore.GRIMY_HERB_IDS) == 28;
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		if (!Bank.close() || !GrandExchangeInterface.close())
			return;

		if (!Inventory.open())
			return;

		for (int i = 0; i < 4; i++) {
			if (!Inventory.open())
				return;

			Clicking.hover(Inventory.getBounds(i));
			Time.sleep(Random.modifiedRandomGuassianRange(100, 300, ConfidenceIntervals.CI_70, 0,
					Modifiers.get("Herblore.clean1")));

			Mouse.click(Mouse.LEFT_BUTTON);
			Time.sleep(Random.modifiedRandomGuassianRange(20, 60, ConfidenceIntervals.CI_60, 0,
					Modifiers.get("Herblore.clean2")));

			Point pos = Mouse.getLocation();

			for (int j = 1; j <= 6; j++) {
				Mouse.setLocation(pos.x, pos.y + j * 36);

				Time.sleep(Random.modifiedRandomGuassianRange(20, 60, ConfidenceIntervals.CI_60, 0,
						Modifiers.get("Herblore.clean1")));

				Mouse.click(Mouse.LEFT_BUTTON);

				Time.sleep(Random.modifiedRandomGuassianRange(20, 60, ConfidenceIntervals.CI_60, 0,
						Modifiers.get("Herblore.clean2")));
			}

			Time.sleep(Random.modifiedRandomGuassianRange(100, 300, ConfidenceIntervals.CI_70, 0,
					Modifiers.get("Herblore.clean3")));
		}

	}

}
