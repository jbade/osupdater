package org.iinc.osbot.scripts.herblore;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collections;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.grandexchange.GrandExchangeInterface;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.Memory;
import org.iinc.osbot.util.Random;

public class Herblore extends Script implements PaintListener {

	private ArrayList<Node> nodes = new ArrayList<>();

	static int[] GRIMY_HERB_IDS = new int[] { 199, 201, 203, 205, 207, 3049, 209, 211, 213, 3051, 215, 2485, 217, 219 };
	static String[] GRIMY_HERB_NAMES = new String[] { "Grimy guam leaf", "Grimy marrentill", "Grimy tarromin",
			"Grimy harralander", "Grimy ranarr weed", "Grimy toadflax", "Grimy irit leaf", "Grimy avantoe",
			"Grimy kwuarm", "Grimy snapdragon", "Grimy cadantine", "Grimy lantadyme", "Grimy dwarf weed",
			"Grimy torstol" };
	static int[] CLEAN_HERB_IDS = new int[] { 249, 251, 253, 255, 257, 2998, 259, 261, 263, 3000, 265, 2481, 267, 269 };
	static String[] CLEAN_HERB_NAMES = new String[] { "Guam leaf", "Marrentill", "Tarromin", "Harralander",
			"Ranarr weed", "Toadflax", "Irit leaf", "Avantoe", "Kwuarm", "Snapdragon", "Cadantine", "Lantadyme",
			"Dwarf weed", "Torstol" };

	static int VIAL_OF_WATER_ID = 227;
	static String VIAL_OF_WATER_NAME = "Vial of water";

	static int[] UNF_POTION_IDS = new int[] { 91, 109, 107, 103, 97, 101, 105, 2483, 99, 93, 3004, 95, 3002, 111 };
	static String[] UNF_POTION_NAMES = new String[] { "Guam potion (unf)", "Dwarf weed potion (unf)",
			"Cadantine potion (unf)", "Avantoe potion (unf)", "Harralander potion (unf)", "Irit potion (unf)",
			"Kwuarm potion (unf)", "Lantadyme potion (unf)", "Ranarr potion (unf)", "Marrentill potion (unf)",
			"Snapdragon potion (unf)", "Tarromin potion (unf)", "Toadflax potion (unf)", "Torstol potion (unf)" };

	static int COINS_ID = 995;
	static String COINS_NAME = "Coins";

	@Override
	public int loop() {
		Memory.checkMemory(1000000); // 1 MB

		//if (BreakHandler.shouldBreak())
		//	System.exit(0);

		if (Inventory.isItemSelected()) {
			Menu.interact("Cancel");
		}

		if (!GrandExchangeInterface.isOpen() && !Bank.isOpen()) {
			Interfaces.closeAll();
		}

		for (Node node : nodes) {
			if (node.activate()) {
				node.execute();
				return 0;
			}
		}

		return Random.randomRange(50, 100);
	}

	@Override
	public void onStop() {

	}

	@Override
	public void onStart() {
		Settings.MAKE_CLICKING_ERRORS = false;
		Collections.addAll(nodes, new LoginNode(), new GrandExchangeNode(), new BankingNode(), new CleaningNode(),
				new PotionNode());
	}

	@Override
	public void onDraw(Graphics g) {

	}

}
