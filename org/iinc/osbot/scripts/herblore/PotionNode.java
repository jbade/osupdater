package org.iinc.osbot.scripts.herblore;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.grandexchange.GrandExchangeInterface;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Player;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class PotionNode implements Node {

	private static final int[] MAKE_WIDGET = new int[] { 309, 3 };

	@Override
	public boolean activate() {
		return Inventory.getCount(Herblore.CLEAN_HERB_IDS) == 14 && Inventory.getCount(Herblore.VIAL_OF_WATER_ID) == 14;
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		if (!Bank.close() || !GrandExchangeInterface.close())
			return;

		if (!Inventory.open())
			return;

		if (Widgets.exists(MAKE_WIDGET) && Clicking.interact(Widgets.getWidget(MAKE_WIDGET), "Make All")) {
			Player lp = Client.getLocalPlayer();
			Time.sleepUntil(() -> lp.getAnimation() != 0, 600, 1200);
			Time.sleepUntil(() -> lp.getAnimation() == 0, 9000, 12000);
			return;
		}

		int slot1 = Random.modifiedRandomRange(4, 8, 5, Modifiers.get("Herblore.potionSlot1"));
		int slot2 = Random.modifiedRandomRange(19, 23, 5, Modifiers.get("Herblore.potionSlot2"));

		Clicking.hover(Inventory.getBounds(slot1));
		if (Menu.getMenuOptions().stream().filter((s) -> s.contains("->")).count() != 0) {
			Mouse.click(Mouse.LEFT_BUTTON);
		}

		if (Menu.interact("Use")) {
			Clicking.hover(Inventory.getBounds(slot2));
			if (Menu.getMenuOptions().stream().filter((s) -> s.contains("->")).count() > 0) {
				Menu.interact("Use");
				Time.sleepUntil(() -> Widgets.exists(MAKE_WIDGET), 1200, 1600);
				if (Widgets.exists(MAKE_WIDGET) && Clicking.interact(Widgets.getWidget(MAKE_WIDGET), "Make All")) {
					Time.sleep(600, 1200);

					Timer t = new Timer(Random.randomRange(600, 1200));
					int threshold = Math.max(Random.modifiedRandomRange(0, 1, 2, Modifiers.get("Herblore.potionThreshold")), 0);
					int prev = Inventory.getCount(Herblore.VIAL_OF_WATER_ID);

					while (prev > threshold && t.isRunning()) {
						Time.sleep(100, 200);

						int ct = Inventory.getCount(Herblore.VIAL_OF_WATER_ID);
						if (ct < prev) {
							prev = ct;
							t.reset();
						}
					}
				}
			}
		}

	}

}
