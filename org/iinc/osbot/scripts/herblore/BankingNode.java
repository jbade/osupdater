package org.iinc.osbot.scripts.herblore;

import java.util.logging.Level;

import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.grandexchange.GrandExchangeInterface;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ArrayUtil;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class BankingNode implements Node {

	@Override
	public boolean activate() {
		return Inventory.getCount(Herblore.GRIMY_HERB_IDS) != 28 && !(Inventory.getCount(Herblore.CLEAN_HERB_IDS) == 14
				&& Inventory.getCount(Herblore.VIAL_OF_WATER_ID) == 14);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		if (!GrandExchangeInterface.close())
			return;

		if (Bank.openBanker()) {

			boolean cont = false;
			for (int id : Herblore.CLEAN_HERB_IDS) {
				if (Bank.getQuantity(id) + Inventory.getCount(id, id + 1) >= 14
						&& Bank.getQuantity(Herblore.VIAL_OF_WATER_ID)
								+ Inventory.getCount(Herblore.VIAL_OF_WATER_ID, Herblore.VIAL_OF_WATER_ID + 1) >= 14) {
					cont = true;
					break;
				}
			}
			
			int grimyHerbCount = Bank.getQuantity(Herblore.GRIMY_HERB_IDS);
			for (Item item : Inventory.getItems()) {
				if (ArrayUtil.contains(Herblore.GRIMY_HERB_IDS, item.getId())
						|| ArrayUtil.contains(Herblore.GRIMY_HERB_IDS, item.getId() - 1)){
					grimyHerbCount += item.getQuantity();
				}
			}
			cont = cont || grimyHerbCount >= 28;
			
			if (!cont) {
				GrandExchangeNode.purchaseItems = true;
				return;
			}

			if (Inventory.getCount() == 0) {
				Time.sleep(600, 1200);
			} else if ((!Bank.depositInventory() || !Time.sleepUntil(() -> Inventory.getCount() == 0, 1200, 1700)))
				return;

			if (Bank.getQuantity(Herblore.GRIMY_HERB_IDS) >= 28) {
				if (Bank.withdraw(Herblore.GRIMY_HERB_IDS, 28)) {
					Bank.close();
					Time.sleepUntil(() -> Inventory.isFull(), 600, 1200);
				}
			} else {
				if (Bank.getQuantity(Herblore.VIAL_OF_WATER_ID) >= 14) {
					for (int id : Herblore.CLEAN_HERB_IDS) {
						if (Bank.getQuantity(id) >= 14) {
							boolean herbFirst = Random.modifiedRandomGuassianRange(0, 100, ConfidenceIntervals.CI_80,
									70, Modifiers.get("Herblore.herbFirst")) > 50;
							if (herbFirst && Bank.withdraw(id, 14) && Bank.withdraw(Herblore.VIAL_OF_WATER_ID, 14)
									|| !herbFirst && Bank.withdraw(Herblore.VIAL_OF_WATER_ID, 14)
											&& Bank.withdraw(id, 14)) {
								Bank.close();
								Time.sleepUntil(() -> Inventory.isFull(), 600, 1200);
							}

							return;
						}
					}
				}

				GrandExchangeNode.purchaseItems = true;
			}
		}
	}

}
