package org.iinc.osbot.scripts.herblore;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;

public class LoginNode implements Node {

	@Override
	public boolean activate() {
		return !Login.isLoggedIn();
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		Login.login();
		Time.sleep(200, 500);
		Camera.setAngle(Camera.getCameraAngle(), true);
	}

}
