package org.iinc.osbot.scripts.debug;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.injection.accessors.object.GameObject;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;

public class DebugGameObjects extends Script implements PaintListener {

	@Override
	public int loop() {
		return 200;
	}

	@Override
	public void onDraw(Graphics g) {
		g.setColor(Color.yellow);
		
		for (GameObject go : RSObjects.getGameObjects(10)){
			Point p = go.toScreen();
			if (Calculations.onViewport(p)) {
				g.drawString(
						String.format("%d", go.getObjectId()),
						p.x, p.y - 5);
				g.drawOval(p.x - 1, p.y - 1, 2, 2);
				p = go.toMiniMap();
				g.drawOval(p.x - 1, p.y - 1, 2, 2);
			}
		}
		
	}

	@Override
	public void onStop() {
	}

	@Override
	public void onStart() {
	}

}
