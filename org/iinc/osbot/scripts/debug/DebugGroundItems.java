package org.iinc.osbot.scripts.debug;

import java.awt.Color;
import java.awt.Graphics;

import org.iinc.osbot.api.injection.accessors.grounditem.GroundItem;
import org.iinc.osbot.api.injection.accessors.grounditem.GroundItems;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;

public class DebugGroundItems extends Script implements PaintListener {

	@Override
	public int loop() {
		return 1000;
	}

	@Override
	public void onDraw(Graphics g) {
		g.setColor(Color.yellow);
		//int distance = 10;
		//Player local = Client.getLocalPlayer();
		//if (local == null)
		//	return;

		//int x = local.getX();
		//int y = local.getY();
		//int plane = Client.getPlane();
		//System.out.println(GroundItems.getAt(local.getTile()));
		//		for (int i = distance * -1; i <= distance; i ++){
		//			for (int j = distance * -1; j <= distance; j ++){
		//				ArrayList<GroundItem> gi = GroundItems.getAt(x + i, y + j, plane);
		//				if (gi.size() > 0){
		//					g.drawOval(gi.get(0).toScreen().x - 1, gi.get(0).toScreen().y - 1, 2, 2);
		//					g.drawString(gi.size() + "", gi.get(0).toScreen().x, gi.get(0).toScreen().y - 8);
		//				}
		//			}
		//}

		for (GroundItem gi : GroundItems.getAllTrue()) {
			System.out.println(gi.getItemId());
			g.drawString(gi.getItemId() + "", gi.toScreen().x, gi.toScreen().y - 8);
			g.drawOval(gi.toScreen().x - 1, gi.toScreen().y - 1, 2, 2);
		}

	}

	@Override
	public void onStop() {
	}

	@Override
	public void onStart() {
	}

}