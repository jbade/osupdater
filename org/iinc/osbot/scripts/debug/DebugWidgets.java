package org.iinc.osbot.scripts.debug;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.script.ScriptHandler;

public class DebugWidgets extends Script implements PaintListener {
	private Script self = this;

	private WidgetExplorer explorer;
	private LinkedList<Widget> selected = new LinkedList<Widget>();
	private Widget[][] widgets;

	@Override
	public int loop() {
		return 1000;
	}

	@Override
	public void onStop() {
		explorer.dispose();
	}

	@Override
	public void onStart() {
		widgets = Widgets.getWidgets();
		explorer = new WidgetExplorer();
		explorer.setVisible(true);
		explorer.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				ScriptHandler.stopScript(self);
			}
		});
	}

	@Override
	public void onDraw(Graphics g) {
		g.setColor(Color.yellow);
		if (selected.size() != 0) {
			for (Widget widget : selected) {
				g.drawRect(widget.getX(), widget.getY(), widget.getWidth(), widget.getHeight());
				for (Widget child : widget.getChildren()) {
					if (child != null && !child.isHidden()) {
						g.drawRect(child.getX(), child.getY(), child.getWidth(), child.getHeight());
					}
				}
			}
		} else {
			if (widgets != null)
			for (int i = 0; i < widgets.length; i++) {
				for (int j = 0; j < widgets[i].length; j++) {
					if (widgets[i][j] != null && !widgets[i][j].isHidden()) {
						g.drawRect(widgets[i][j].getX(), widgets[i][j].getY(), widgets[i][j].getWidth(),
								widgets[i][j].getHeight());
						for (Widget child : widgets[i][j].getChildren()) {
							if (child != null && !child.isHidden()) {
								g.drawRect(child.getX(), child.getY(), child.getWidth(), child.getHeight());
							}
						}
					}

				}
			}
		}
	}

	class WidgetExplorer extends JFrame {
		private static final long serialVersionUID = 1L;

		DefaultMutableTreeNode root = new DefaultMutableTreeNode("");
		String filter = "";

		public WidgetExplorer() {
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (Throwable e) {
				e.printStackTrace();
			}
			initComponents();
			refresh(null);
		}

		private void refresh(MouseEvent e) {
			root.removeAllChildren();
			selected.clear();
			widgets = Widgets.getWidgets();

			for (int i = 0; i < widgets.length; i++) {
				if (widgets[i] != null && widgets[i].length > 0) {
					DefaultMutableTreeNode child = new DefaultMutableTreeNode("Container " + i);

					boolean hasChildren = false;

					for (int j = 0; j < widgets[i].length; j++) {
						if (widgets[i][j] != null
								&& (textField1.getText() == null || textField1.getText().equals("")
										|| (widgets[i][j].getText() != null
												&& widgets[i][j].getText().contains(textField1.getText())))
								|| (widgets[i][j].getName() != null
										&& widgets[i][j].getName().contains(textField1.getText()))) {
							DefaultMutableTreeNode child2 = new DefaultMutableTreeNode("Widget " + i + ", " + j);
							child.add(child2);
							hasChildren = true;

							Widget[] children = widgets[i][j].getChildren();
							if (children != null) {
								for (int k = 0; k < children.length; k++) {
									DefaultMutableTreeNode child3 = new DefaultMutableTreeNode(
											"Widget " + i + ", " + j + ", " + k);
									child2.add(child3);
								}

							}
						}
					}

					if (hasChildren) {
						root.add(child);
					}
				}
			}

			tree.setModel(new DefaultTreeModel(root));
			tree.expandRow(0);
		}

		private void select(TreeSelectionEvent e) {
			DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
			if (selectedNode == null)
				return;

			// System.out.println(selectedNode.toString());

			String s = selectedNode.toString();
			selected.clear();
			if (s.startsWith("Container")) {
				s = s.replaceAll("[^0-9,]", "");
				int container = Integer.parseInt(s);
				Widget[] arr = widgets[container];
				if (arr != null) {
					for (int j = 0; j < arr.length; j++) {
						if (arr[j] != null)
							selected.add(arr[j]);
					}
				}
			} else if (s.startsWith("Widget")) {
				s = s.replaceAll("[^0-9,]", "");
				String[] ss = s.split(",");

				int container = Integer.parseInt(ss[0]);
				int index = Integer.parseInt(ss[1]);

				Widget w;
				if (ss.length > 2) {
					int child = Integer.parseInt(ss[2]);
					w = widgets[container][index].getChild(child);
				} else {
					w = widgets[container][index];
				}

				selected.add(w);
			}

			if (selected.size() == 1) {
				text.setText(selected.getFirst().toString());
			} else {
				text.setText("");
			}
		}

		private void filter(KeyEvent e) {
			refresh(null);
			// System.out.println(textField1.getText());
		}

		private void initComponents() {
			// JFormDesigner - Component initialization - DO NOT MODIFY
			// //GEN-BEGIN:initComponents
			// Generated using JFormDesigner Evaluation license - a a
			button1 = new JButton();
			textField1 = new JTextField();
			scrollPane2 = new JScrollPane();
			tree = new JTree();
			scrollPane3 = new JScrollPane();
			text = new JTextPane();

			// ======== this ========
			setTitle("Widget Explorer");
			setResizable(false);
			setMinimumSize(new Dimension(500, 300));
			Container contentPane = getContentPane();
			contentPane.setLayout(null);

			// ---- button1 ----
			button1.setText("Refresh");
			button1.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					refresh(e);
				}
			});
			contentPane.add(button1);
			button1.setBounds(5, 5, button1.getPreferredSize().width, 25);

			// ---- textField1 ----
			textField1.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					filter(e);
				}
			});
			contentPane.add(textField1);
			textField1.setBounds(80, 5, 410, 25);

			// ======== scrollPane2 ========
			{
				scrollPane2.setPreferredSize(new Dimension(30, 322));

				// ---- tree ----
				tree.addTreeSelectionListener(e -> select(e));
				scrollPane2.setViewportView(tree);
			}
			contentPane.add(scrollPane2);
			scrollPane2.setBounds(0, 35, 205, 400);

			// ======== scrollPane3 ========
			{
				scrollPane3.setRequestFocusEnabled(false);
				scrollPane3.setMinimumSize(new Dimension(40, 23));
				scrollPane3.setPreferredSize(new Dimension(120, 22));
				scrollPane3.setViewportView(text);
			}
			contentPane.add(scrollPane3);
			scrollPane3.setBounds(210, 35, 285, 400);

			{ // compute preferred size
				Dimension preferredSize = new Dimension();
				for (int i = 0; i < contentPane.getComponentCount(); i++) {
					Rectangle bounds = contentPane.getComponent(i).getBounds();
					preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
					preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
				}
				Insets insets = contentPane.getInsets();
				preferredSize.width += insets.right;
				preferredSize.height += insets.bottom;
				contentPane.setMinimumSize(preferredSize);
				contentPane.setPreferredSize(preferredSize);
			}
			pack();
			setLocationRelativeTo(getOwner());
			// JFormDesigner - End of component initialization
			// //GEN-END:initComponents
		}

		// JFormDesigner - Variables declaration - DO NOT MODIFY
		// //GEN-BEGIN:variables
		// Generated using JFormDesigner Evaluation license - a a
		private JButton button1;
		private JTextField textField1;
		private JScrollPane scrollPane2;
		private JTree tree;
		private JScrollPane scrollPane3;
		private JTextPane text;
		// JFormDesigner - End of variables declaration //GEN-END:variables
	}

}
