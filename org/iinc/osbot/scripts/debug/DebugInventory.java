package org.iinc.osbot.scripts.debug;

import java.awt.Color;
import java.awt.Graphics;

import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;

public class DebugInventory extends Script implements PaintListener {

	@Override
	public int loop() {
		return 1000;
	}

	@Override
	public void onDraw(Graphics g) {
		g.setColor(Color.yellow);
		for (Item item : Inventory.getItems()) {
			g.drawString(item.getId() + "", item.toScreen().x, item.toScreen().y + 18);
		}
		
		// for (int i = 0; i < 28; i ++){
		// Rectangle b = Inventory.getBounds(i);
		// g.drawRect(b.x, b.y, b.width, b.height);
		// }

	}

	@Override
	public void onStop() {
	}

	@Override
	public void onStart() {
	}

}