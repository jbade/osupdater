package org.iinc.osbot.scripts.debug;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.injection.accessors.renderable.Player;
import org.iinc.osbot.api.injection.accessors.renderable.Players;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;

public class DebugPlayers extends Script implements PaintListener {

	@Override
	public int loop() {
		// for (Npc npc : Npcs.getNpcs()) {
		// System.out.println(npc.getNpcDefinition().getName());
		// }
		// System.out.println(Npcs.getNpcs().size());

		return 1000;
	}
	//private static final Point pt = new Point(642, 84);

	@Override
	public void onDraw(Graphics g) {
		g.setColor(Color.yellow);
		for (Player player : Players.getAll()) {
			Point p = player.toScreen();
			if (Calculations.onViewport(p)) {
				g.drawString(
						String.format("(anim: %d, tile: %d, %d, skull: %d)", player.getAnimation(), player.getX(), player.getY(), player.getSkullIcon()),
						p.x, p.y - 5);
				g.drawOval(p.x - 1, p.y - 1, 2, 2);
			}

			p = player.toMiniMap();
			if (Calculations.onMiniMap(p)) {
				g.drawOval(p.x - 1, p.y - 1, 2, 2);
			}
		}

	}

	@Override
	public void onStop() {
	}

	@Override
	public void onStart() {
	}

}