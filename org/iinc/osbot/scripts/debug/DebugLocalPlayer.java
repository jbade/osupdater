package org.iinc.osbot.scripts.debug;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Player;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;

public class DebugLocalPlayer extends Script implements PaintListener {

	@Override
	public int loop() {
		return 200;
	}

	@Override
	public void onDraw(Graphics g) {
		g.setColor(Color.yellow);
		Player localPlayer = Client.getLocalPlayer();
		if (localPlayer != null){
		g.drawString("x: " + localPlayer.getX() + " " + localPlayer.getPreciseX(), 10, 45);
		g.drawString("y: " + localPlayer.getY() + " " + localPlayer.getPreciseY(), 10, 60);
		g.drawString("plane: " + Client.getPlane(), 10, 75);
		g.drawString("animation: " + localPlayer.getAnimation(), 10, 90);
		g.drawString("health: " + Levels.getCurrentHitpoints(), 10, 105);
		g.drawString("height: " + localPlayer.getModelHeight(), 10, 120);
		}
		g.drawString("destinationX: " + Client.getDestinationX(), 10, 135);
		g.drawString("destinationY: " + Client.getDestinationY(), 10, 150);
		
		Tile t= Walking.getDestinationTile();
		if (t != null){
			Point p = t.toScreen();
			g.drawRect(p.x - 1, p.y - 1, 3, 3);
		}
	}

	@Override
	public void onStop() {
	}

	@Override
	public void onStart() {
	}

}
