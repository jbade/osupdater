package org.iinc.osbot.scripts.debug;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.script.ScriptHandler;

public class PathRecorder extends Script implements PaintListener {
	private Script self = this;
	private final DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("Paths");
	private final DefaultTreeModel model = new DefaultTreeModel(rootNode);
	private final JTree tree = new JTree(model);
	private DefaultMutableTreeNode lastAdded = null;

	private Tile tile = null;
	private boolean started;

	public void onStart() {
		new Gui().init();
	}

	public int loop() {
		if (started) {
			if (tree.getSelectionPaths() == null)
				return 100;
			if (tile == null || Client.getLocalPlayer().getTile().distanceTo(tile) >= 3) {
				DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(
						"Tile(" + Client.getLocalPlayer().getX() + ", " + Client.getLocalPlayer().getY() + ", " + Client.getPlane() + ")");
				for (TreePath path : tree.getSelectionPaths()) {
					DefaultMutableTreeNode last = (DefaultMutableTreeNode) path.getLastPathComponent();
					model.insertNodeInto(newNode, last, last.getChildCount());
					tree.expandPath(new TreePath(last.getPath()));
				}
				tile = Client.getLocalPlayer().getTile();
			}
		}
		return 100;
	}

	public void onDraw(Graphics g) {
		for (int i = 0; i < rootNode.getChildCount(); i++) {
			DefaultMutableTreeNode parent = (DefaultMutableTreeNode) rootNode.getChildAt(i);
			Point mMTD = null;
			for (int c = 0; c < parent.getChildCount(); c++) {
				DefaultMutableTreeNode childAt = (DefaultMutableTreeNode) parent.getChildAt(c);
				String nodeName = childAt.toString().substring(5);
				String[] args = nodeName.substring(0, nodeName.length() - 1).split(", ");
				Point mMTD2 = Calculations.tileToMiniMap(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
				if (mMTD != null && mMTD2 != null) {
					g.setColor(Color.BLACK);
					if (mMTD2.x != -1 && mMTD.x != -1)
						g.drawLine(mMTD.x, mMTD.y, mMTD2.x,
								mMTD2.y);
				}
				if (mMTD2 != null && mMTD2.x != -1) {
					g.setColor(Color.RED);
					g.drawOval(mMTD2.x - 1, mMTD2.y - 1, 2, 2);
				}
				mMTD = mMTD2;
			}
		}
	}

	private class Gui extends JFrame {
		private static final long serialVersionUID = 1L;

		public void init() {
			addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					ScriptHandler.stopScript(self);
				}
			});
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (Throwable e) {
				e.printStackTrace();
			}
			setTitle("Multi-Path Generator");
			setBounds(100, 100, 295, 300);
			setAlwaysOnTop(true);
			JPanel contentPane = new JPanel();
			setContentPane(contentPane);
			contentPane.setLayout(new BorderLayout(0, 0));

			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			contentPane.add(scrollPane, BorderLayout.CENTER);

			scrollPane.setViewportView(tree);

			JToolBar toolBar_1 = new JToolBar();
			toolBar_1.setFloatable(false);
			contentPane.add(toolBar_1, BorderLayout.NORTH);

			JToolBar toolBar = new JToolBar();
			toolBar.setFloatable(false);
			contentPane.add(toolBar, BorderLayout.SOUTH);

			JButton btnAdd = new JButton("Add");
			btnAdd.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String name = JOptionPane.showInputDialog(Gui.this, "Name: ");
					if (name != null && name.length() >= 0) {
						DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(name);
						model.insertNodeInto(newNode, rootNode, rootNode.getChildCount());
						tree.expandPath(new TreePath(rootNode.getPath()));
						tree.setSelectionPath(new TreePath(newNode));
						lastAdded = newNode;
						tile = null;
					}
				}
			});
			toolBar.add(btnAdd);

			JButton btnClear = new JButton("Clear");
			btnClear.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (tree.getSelectionPaths() == null)
						return;
					for (TreePath path : tree.getSelectionPaths()) {
						DefaultMutableTreeNode last = (DefaultMutableTreeNode) path.getLastPathComponent();
						for (int c = last.getChildCount() - 1; c >= 0; c--) {
							model.removeNodeFromParent((DefaultMutableTreeNode) last.getChildAt(c));
						}
					}
				}
			});
			toolBar.add(btnClear);

			JButton btnRemove = new JButton("Remove");
			btnRemove.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (tree.getSelectionPaths() == null)
						return;
					for (TreePath path : tree.getSelectionPaths()) {
						DefaultMutableTreeNode last = (DefaultMutableTreeNode) path.getLastPathComponent();
						if (last != rootNode)
							model.removeNodeFromParent(last);
					}
				}
			});
			toolBar.add(btnRemove);

			JButton btnGet = new JButton("get");
			btnGet.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					JFrame frame = new JFrame("Code");
					frame.setSize(400, 200);
					frame.setLayout(new BorderLayout());
					JTextArea codeBox = new JTextArea();

					for (int c = 0; c < rootNode.getChildCount(); c++) {
						DefaultMutableTreeNode child = (DefaultMutableTreeNode) rootNode.getChildAt(c);
						StringBuilder builder = new StringBuilder(
								"Tile[] " + child.toString() + " = new Tile[]{\n");
						for (int sc = 0; sc < child.getChildCount(); sc++) {
							DefaultMutableTreeNode subchild = (DefaultMutableTreeNode) child.getChildAt(sc);

							builder.append("    new " + subchild.toString() + ",\n");
						}
						builder.append("};\n\n");
						codeBox.append(builder.toString());
					}
					frame.add(new JScrollPane(codeBox));
					frame.setVisible(true);
				}
			});
			toolBar.add(btnGet);

			JButton btnAddPosition = new JButton("Add Current");
			btnAddPosition.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (lastAdded == null)
						return;
					DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(
							"Tile(" + Client.getLocalPlayer().getX() + ", " + Client.getLocalPlayer().getY() + ", " + Client.getPlane() + ")");
					model.insertNodeInto(newNode, lastAdded, lastAdded.getChildCount());
					tree.expandPath(new TreePath(lastAdded.getPath()));
				}
			});
			toolBar_1.add(btnAddPosition);

			JButton btnStart = new JButton("Start");
			btnStart.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					TreePath path = tree.getSelectionPath();
					if (path == null || path.getLastPathComponent() == rootNode) {
						String name = JOptionPane.showInputDialog(Gui.this, "Name: ");
						if (name != null && name.length() >= 0) {
							DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(name);
							model.insertNodeInto(newNode, rootNode, rootNode.getChildCount());
							tree.expandPath(new TreePath(rootNode.getPath()));
							tree.setSelectionPath(new TreePath(newNode));
							lastAdded = newNode;
						}
					}
					started = true;
				}
			});
			toolBar_1.add(btnStart);

			JButton btnStop = new JButton("Stop");
			btnStop.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					started = false;
					tile = null;
				}
			});
			toolBar_1.add(btnStop);

			setVisible(true);
		}
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		
	}
}
