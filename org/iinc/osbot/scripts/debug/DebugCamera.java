package org.iinc.osbot.scripts.debug;

import java.awt.Color;
import java.awt.Graphics;

import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;

public class DebugCamera extends Script implements PaintListener {

	@Override
	public int loop() {
		return 200;
	}

	@Override
	public void onDraw(Graphics g) {
		g.setColor(Color.yellow);
		g.drawString("x: " + Client.getCameraX(), 10, 45);
		g.drawString("y: " + Client.getCameraY(), 10, 60);
		g.drawString("z: " + Client.getCameraZ(), 10, 75);
		g.drawString("pitch: " + Client.getCameraPitch(), 10, 90);
		g.drawString("yaw: " + Client.getCameraYaw(), 10, 105);
		g.drawString("angle: " + Camera.getCameraAngle(), 10, 120);
	}

	@Override
	public void onStop() {
	}

	@Override
	public void onStart() {
	}

}
