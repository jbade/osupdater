package org.iinc.osbot.scripts.debug;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;

public class DebugMenu extends Script implements PaintListener {

	@Override
	public int loop() {
		System.out.println(Menu.getMenuActions());
		System.out.println(Menu.getMenuOptions());
		return 1000;
	}

	@Override
	public void onDraw(Graphics g) {
		g.setColor(Color.yellow);
		g.drawString("Menu open: " + Menu.isOpen(), 10, 45);
		g.drawString("Menu size: " + Menu.getSize(), 10, 60);
		g.drawString("Menu location: " + Menu.getLocation(), 10, 75);
		g.drawString("Menu actions: " + Menu.getMenuActions(), 10, 90);
		g.drawString("Menu options: " + Menu.getMenuOptions(), 10, 105);
		
		if (Menu.isOpen()){
			g.drawRect(Menu.getLocation().x, Menu.getLocation().y, Menu.getSize().width, Menu.getSize().height);
			
			for (int i = 0; i < Client.getMenuCount(); i ++){
				Rectangle r = Menu.getBounds(i);
				g.drawRect(r.x, r.y, r.width, r.height);
			}
		}
	}

	@Override
	public void onStop() {
	}

	@Override
	public void onStart() {
	}

}