package org.iinc.osbot.scripts.debug;

import java.awt.Color;
import java.awt.Graphics;

import org.iinc.osbot.api.base.interfaces.tabs.Equipment;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;

public class DebugEquipment extends Script implements PaintListener {

	@Override
	public int loop() {
		return 200;
	}

	@Override
	public void onDraw(Graphics g) {
		g.setColor(Color.yellow);
		g.drawString("amulet: " + Equipment.getAmulet(), 10, 45);
		g.drawString("arrow: " + Equipment.getArrow(), 10, 60);
		g.drawString("body: " + Equipment.getBody(), 10, 75);
		g.drawString("boots: " + Equipment.getBoots(), 10, 90);
		g.drawString("cape: " + Equipment.getCape(), 10, 105);
		g.drawString("gloves: " + Equipment.getGloves(), 10, 120);
		g.drawString("helmet: " + Equipment.getHelmet(), 10, 135);
		g.drawString("legs: " + Equipment.getLegs(), 10, 150);
		g.drawString("ring: " + Equipment.getRing(), 10, 165);
		g.drawString("shield: " + Equipment.getShield(), 10, 180);
		g.drawString("weapon: " + Equipment.getWeapon(), 10, 195);
		g.drawString("isOpen: " + Equipment.isOpen(), 10, 210);
	}

	@Override
	public void onStop() {
	}

	@Override
	public void onStart() {
	}

}