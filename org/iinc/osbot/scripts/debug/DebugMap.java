package org.iinc.osbot.scripts.debug;

import java.awt.Color;
import java.awt.Graphics;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;

public class DebugMap extends Script implements PaintListener {

	@Override
	public int loop() {
		return 200;
	}

	@Override
	public void onDraw(Graphics g) {
		g.setColor(Color.yellow);
		g.drawString("angle: " + Client.getMapAngle(), 10, 45);
		int distance = 68;
		g.drawOval(Calculations.MINIMAP_CENTER_POINT.x - distance, Calculations.MINIMAP_CENTER_POINT.y - distance, distance * 2, distance * 2);
	}

	@Override
	public void onStop() {
	}

	@Override
	public void onStart() {
	}

}
