package org.iinc.osbot.scripts.debug;

import java.awt.Color;
import java.awt.Graphics;

import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;

public class DebugGameState extends Script implements PaintListener {

	@Override
	public int loop() {
		return 200;
	}

	@Override
	public void onDraw(Graphics g) {
		g.setColor(Color.yellow);
		g.drawString("game state: " + Client.getGameState(), 10, 30);
		g.drawString("spellSelected: " + Client.isSpellSelected(), 10, 45);
		g.drawString("itemSelectionState: " + Client.getItemSelectionState() + " " + Integer.toBinaryString(Client.getItemSelectionState()), 10, 60);
		g.drawString("login state: " + Client.getLoginState(), 10, 75);
		g.drawString("membershipDaysLeft: " + Login.getMembershipDaysLeft(), 10, 90);
		g.drawString("cursorState: " + Client.getCursorState(), 10, 105);
	}

	@Override
	public void onStop() {
	}

	@Override
	public void onStart() {
	}

}
