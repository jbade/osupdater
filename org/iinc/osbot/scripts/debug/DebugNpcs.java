package org.iinc.osbot.scripts.debug;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;

public class DebugNpcs extends Script implements PaintListener {

	@Override
	public int loop() {
		// for (Npc npc : Npcs.getNpcs()) {
		// System.out.println(npc.getNpcDefinition().getName());
		// }
		// System.out.println(Npcs.getNpcs().size());

		return 1000;
	}
	//private static final Point pt = new Point(642, 84);

	@Override
	public void onDraw(Graphics g) {
		g.setColor(Color.yellow);
		for (Npc npc : Npcs.getAll()) {
			Point p = npc.toScreen();
			if (Calculations.onViewport(p)) {
				g.drawString(
						String.format("%s (id: %d, anim: %d, tile: %d, %d)", npc.getNpcDefinition().getName(), npc.getNpcDefinition().getId(), npc.getAnimation(), npc.getX(), npc.getY()),
						p.x, p.y - 5);
				g.drawOval(p.x - 1, p.y - 1, 2, 2);
			}

			p = npc.toMiniMap();
			if (Calculations.onMiniMap(p)) {
				g.drawOval(p.x - 1, p.y - 1, 2, 2);
			}
		}

	}

	@Override
	public void onStop() {
	}

	@Override
	public void onStart() {
	}

}
