package org.iinc.osbot.scripts.debug;

import java.util.Collection;

import org.iinc.osbot.api.injection.accessors.grandexchange.GrandExchange;
import org.iinc.osbot.api.injection.accessors.grandexchange.GrandExchangeOffer;
import org.iinc.osbot.script.Script;

public class DebugGrandExchangeOffers extends Script {

	@Override
	public int loop() {
		Collection<GrandExchangeOffer> offers = GrandExchange.getOffers();
		for (GrandExchangeOffer offer : offers) {
			System.out.println(offer);
		}
		return 100;
	}

	@Override
	public void onStop() {
	}

	@Override
	public void onStart() {
	}

}
