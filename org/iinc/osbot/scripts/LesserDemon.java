package org.iinc.osbot.scripts;

import java.awt.Graphics;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Equipment;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.base.worlds.Worlds;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.renderable.Players;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Email;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class LesserDemon extends Script implements PaintListener {

	@Override
	public void onStart() {
		Bot.LOGGER.log(Level.INFO, "Started LesserDemon");
		startTime = System.currentTimeMillis();
		Modifiers.init(Bot.username.hashCode());
		BreakHandler.init(Bot.username.hashCode());

		if (BreakHandler.shouldBreak())
			BreakHandler.doBreak();
	}
	
	int tries = 0;

	@Override
	public int loop() {
		if (BreakHandler.shouldBreak()) {
			Interfaces.closeAll();
			Timer t = new Timer(Random.randomRange(10000, 15000));
			while (Login.isLoggedIn() && t.isRunning())
				Logout.clickLogout();
			BreakHandler.doBreak();
		}

		if (Players.getAll().size() > 1) {
			Bot.LOGGER.log(Level.FINEST, "Hopping worlds");
			Logout.switchWorld(Worlds.getRandomNormalWorld(true).getId());
			Time.sleep(1000, 2000);
		}

		if (!Login.isLoggedIn()) {
			Login.login(Bot.username, Bot.password);
			Time.sleep(500, 2000);
			Equipment.open(); // THIS FIXES BUG BUT IT PREVENTS US FROM TESTING THE ACTUAL FIX
			Camera.setAngle(Camera.getCameraAngle(), true);
			return Random.randomRange(1000, 2000);
		}

		if (!Client.getLocalPlayer().isInteracting() || Widgets.exists(new int[] { 233, 0 })) { // level
																								// up
																								// widget
			Time.sleepReactionTime();
			List<Npc> filtered = Npcs.getAll().stream().filter(
					(npc) -> npc.getNpcDefinition().getName().equals("Lesser demon") && npc.getAnimation() != 67) // death
																													// animation
					.collect(Collectors.toList());
			if (filtered.size() != 0) {
				if (tries > 5 && Equipment.getArrow() == -1) {
					Bot.LOGGER.log(Level.SEVERE, "Out of arrows");
					Email.sendEmail(Email.TYPE.SEVERE, "Out of arrows " + Bot.username);
					System.exit(Settings.DISABLED_EXIT_CODE);
				}

				Bot.LOGGER.log(Level.FINEST, "Interacting");
				Tile loc = Client.getLocalPlayer().getTile();

				filtered.sort((a, b) -> {
					double dist = loc.distanceTo(a.getTile()) - loc.distanceTo(b.getTile());
					if (dist > 0)
						return 1;
					if (dist < 0)
						return -1;
					return 0;
				});

				Npc npc = filtered.get(0);
				if (Random.modifiedRandomRange(0, 100, 20, Modifiers.get("LesserDemon.camera1")) > 90
						|| !Calculations.onViewport(npc.toScreen())
								&& Random.modifiedRandomRange(0, 100, 30, Modifiers.get("LesserDemon.camera2")) > 70) {
					Camera.turnTo(npc.getTile());
				}
				if (Calculations.onViewport(npc.toScreen()) || Walking.walkTo(npc)) {
					if (Clicking.interact(npc, "Attack")) {
						tries++;
						Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting(), 600, 800);
						if (Time.sleepUntil(() -> !Client.getLocalPlayer().isInteracting(), 3000, 4000)) {
							// Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting(), 3000,
							// 4000);
							return 0;
						}
						return Random.randomRange(500, 1000);
					}
				}
			} else {
				return Random.modifiedRandomRange(2000, 5000, 3000, Modifiers.get("LesserDemon.sleep1"));
			}
		} else {
			Bot.LOGGER.log(Level.FINEST, "Fighting");
			tries = 0;
			if (Mouse.isFocused() && Random.modifiedRandomRange(0, 100, 30, Modifiers.get("LesserDemon.offscreen")) >= 40) {
				Mouse.moveOffscreen();
			}

			return Random.modifiedRandomGuassianRange(5000, 20000, ConfidenceIntervals.CI_60, 5000,
					Modifiers.get("LesserDemon.sleep2"));
		}

		return 500;
	}

	private long startTime;

	@Override
	public void onDraw(Graphics g) {
		g.drawString(org.iinc.osbot.util.Formatter.timeToString(System.currentTimeMillis() - startTime), 10, 500);
	}

	@Override
	public void onStop() {

	}
}
