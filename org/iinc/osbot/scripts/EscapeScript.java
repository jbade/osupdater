package org.iinc.osbot.scripts;

import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Player;
import org.iinc.osbot.api.injection.accessors.renderable.Players;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.MouseMovementHandler;
import org.iinc.osbot.script.Script;

public class EscapeScript extends Script {

	@Override
	public int loop() {
		int wildernessLevel = Client.getWildernessLevel();
		if (wildernessLevel == -1)
			wildernessLevel = 60;

		int cb = Client.getLocalPlayer().getCombatLevel();
		int minLevel = cb - wildernessLevel;
		int maxLevel = cb + wildernessLevel;

		Object localPlayerReference = Client.getLocalPlayer().getReference();
		for (Player p : Players.getAll()) {
			int c = p.getCombatLevel();
			if (c >= minLevel && c <= maxLevel && p.getReference() != localPlayerReference) {
				// ESCAPE
				MouseMovementHandler.DEFAULT_MOUSE_SPEED = -1000;
				while (Client.getGameState() > 10) {
					try {
						Logout.clickLogout();
					} catch (Exception e) {
					}
				}
			}
		}

		return 0;
	}

	@Override
	public void onStop() {
	}

	@Override
	public void onStart() {
	}

}
