package org.iinc.osbot.scripts.ge;

import java.awt.Color;
import java.awt.Graphics;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.grandexchange.GrandExchangeInterface;
import org.iinc.osbot.api.base.interfaces.grandexchange.GrandExchangeInventory;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.grandexchange.GrandExchange;
import org.iinc.osbot.api.injection.accessors.grandexchange.GrandExchangeOffer;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.util.Random;

public class Flipper extends Script implements PaintListener {
	private Set<ItemStatus> items = new HashSet<ItemStatus>();
	private Set<ItemStatus> limitedItems = new HashSet<ItemStatus>();

	private int itemCountGoal = 5;

	private long startTime;

	@Override
	public void onStart() {
		startTime = System.currentTimeMillis();
	}

	@Override
	public void onStop() {
	}

	@Override
	public int loop() {
		System.out.println("step " + System.currentTimeMillis());
		if (!Login.isLoggedIn()) {
			Login.login("mrch1@gmail.com", "youtube1");
			return 2000;
		}

		if (openGrandExchange()) {
			updateItems();
			collect(true);
			updateOffers();
		}
		return Random.randomRange(5000, 10000);
	}

	private void updateItems() {
		// update active items
		for (Iterator<ItemStatus> i = items.iterator(); i.hasNext();) {
			ItemStatus is = i.next();

			if (shouldCheckMargins(is))
				updateMargins(is);

			if (is.isLimited()) {
				limitedItems.add(is);
				i.remove();
			}
		}

		// update limited items
		for (Iterator<ItemStatus> i = limitedItems.iterator(); i.hasNext();) {
			ItemStatus is = i.next();

			if (!is.isLimited() && GrandExchangeInventory.getCount(is.getName()) == 0) {
				i.remove();
			}
		}

		if (items.size() < itemCountGoal) {
			LinkedList<Integer> limitedIds = new LinkedList<Integer>();
			for (ItemStatus is : limitedItems) {
				limitedIds.add(is.getId());
			}

			// ***** TEMPORARY until server keeps track of what items are active
			for (ItemStatus is : items) {
				limitedIds.add(is.getId());
			}
			// *****

			ItemContainer ic = ItemHandler.requestItem(limitedIds);
			System.out.println("New item: " + ic.name);
			items.add(new ItemStatus(ic.name, ic.id));
		}
	}

	private void updateOffers() {
		HashSet<String> inv = GrandExchangeInventory.getInventoryItems();
		int inventoryCount = Inventory.getCount();

		// handle limited offers
		for (ItemStatus item : limitedItems) {
			if (inv.contains(item.getName())) {
				GrandExchangeInterface.createSellOffer(item.getName(), 1, -1); // DUMP. TODO ?
			}
		}

		// handle active sell offers
		for (ItemStatus item : items) {
			if (item.getBuyPrice() == 0 || item.getSellPrice() == 0 || !inv.contains(item.getName()))
				continue;
			// TODO check if active?

			boolean aborted = false;
			boolean hasSellOffer = false;
			for (GrandExchangeOffer offer : GrandExchange.getOffers()) {
				if (offer.getItemId() == item.getId()) {
					if (offer.isSellOffer()) {
						hasSellOffer = true;
						// only abort and replace if inv count is > 25% of left selling
						int invCount = GrandExchangeInventory.getCount(item.getName());
						int left = offer.getQuantity() - offer.getTransfered();

						if (inventoryCount >= 24 - itemCountGoal || invCount > left * 0.25) {
							GrandExchangeInterface.abortOffer(offer);
							aborted = true;
						}

					}
				}
			}

			if (hasSellOffer) {
				if (aborted) {
					collect(true);
					if (!GrandExchangeInterface.createSellOffer(item.getName(), item.getSellPrice(), -1))
						continue;
				}
			} else {
				if (!GrandExchangeInterface.createSellOffer(item.getName(), item.getSellPrice(), -1))
					continue;
			}

		}

		// handle active offers
		for (ItemStatus item : items) {
			if (item.getBuyPrice() == 0 || item.getSellPrice() == 0)
				continue;
			// TODO check if active?

			boolean hasBuyOffer = false;
			for (GrandExchangeOffer offer : GrandExchange.getOffers()) {
				if (offer.getItemId() == item.getId()) {
					if (offer.isBuyOffer()) {
						hasBuyOffer = true;
						break;
					}
				}
			}

			if (!hasBuyOffer && item.shouldBuy()) {
				int coins = Inventory.getFirst(995).getQuantity();
				int max = 0;
				for (ItemStatus is : items) {
					if (is.getSellPrice() * 1.1 > max)
						max = (int) (is.getSellPrice() * 1.1);
				}
				coins /= itemCountGoal;
				coins -= max;

				int quantity = coins / item.getBuyPrice();
				if (quantity > 0)
					GrandExchangeInterface.createBuyOffer(item.getName(), item.getBuyPrice(), quantity);
			}
		}
	}

	private boolean shouldCheckMargins(ItemStatus is) {
		return (System.currentTimeMillis() > is.getLastMarginUpdate()
				+ Random.randomRange(20 * 60 * 1000, 30 * 60 * 1000)); // TODO
	}

	// -1 on limited, 0 on error
	private int checkBuyPrice(ItemStatus is) {
		if (is.isLimited())
			return -1;

		if (GrandExchangeInventory.getCount(is.getName()) == 0)
			return 0;

		GrandExchangeInterface.createSellOffer(is.getName(), 1, 1);

		boolean success = Time.sleepUntil(() -> {
			for (GrandExchangeOffer offer : GrandExchange.getOffers()) {
				if (offer.getItemId() == is.getId() && offer.getState() == GrandExchangeOffer.STATES.COMPLETE)
					return true;
			}
			return false;
		}, 8000, 10000);

		if (!success) {
			abortAndCollect(is.getId());
			return -1;
		}

		int buyPrice = 0;
		for (GrandExchangeOffer offer : GrandExchange.getOffers()) {
			if (offer.getItemId() == is.getId()) {
				buyPrice = offer.getSpent();
				break;
			}

		}

		if (!collect(true))
			return 0;

		return buyPrice;
	}

	// -1 on limited, 0 on error
	private int checkSellPrice(ItemStatus is) {
		int coins = Inventory.getFirst(995).getQuantity();
		if (!GrandExchangeInterface.createBuyOffer(is.getName(), coins, 1))
			return 0;

		// wait for offer to complete
		boolean success = Time.sleepUntil(() -> {
			for (GrandExchangeOffer offer : GrandExchange.getOffers()) {
				if (offer.getItemId() == is.getId() && offer.getState() == GrandExchangeOffer.STATES.COMPLETE)
					return true;
			}
			return false;
		}, 8000, 10000);

		if (!success) {
			abortAndCollect(is.getId());
			return -1;
		}

		int sellPrice = 0;
		for (GrandExchangeOffer offer : GrandExchange.getOffers()) {
			if (offer.getItemId() == is.getId()) {
				sellPrice = offer.getSpent();
				break;
			}

		}

		if (!collect(true))
			return 0;

		Time.sleepUntil(() -> Inventory.getFirst(is.getId()) != null, 2000, 2500);
		return sellPrice;
	}

	private void updateMargins(ItemStatus is) {
		System.out.println("Checking margins " + is.getName());

		boolean aborted = false;
		for (GrandExchangeOffer offer : GrandExchange.getOffers()) {
			if (offer.getItemId() == is.getId()) {
				System.out.println("\tAborting " + offer.getIndex());
				aborted = true;
				GrandExchangeInterface.abortOffer(offer);
			}
		}

		if (aborted) {
			System.out.println("\tCollecting");
			if (!collect(true))
				return;
		}

		int sellPrice = checkSellPrice(is);
		if (sellPrice == -1) {
			is.limit();
			return;
		}

		int buyPrice = checkBuyPrice(is);

		System.out.println("buy " + buyPrice + " sell" + sellPrice);
		if (buyPrice == 0 || sellPrice == 0)
			return;

		is.updateMargins(buyPrice, sellPrice);
		
		double marginPercent = (double)((sellPrice - buyPrice)) / buyPrice;
		if (marginPercent < 0.01){
			is.limit(60 * 60 * 1000); // 1 hr
		}
	}

	private void abortAndCollect(int id) {
		for (GrandExchangeOffer offer : GrandExchange.getOffers()) {
			if (offer.getItemId() == id)
				GrandExchangeInterface.abortOffer(offer);
		}
		collect(true);
	}

	private boolean collect(boolean inventory) {

		boolean success = false;

		for (int tries = 0; tries < 5 && !success; tries++) {
			GrandExchangeOffer[] offers = GrandExchange.getOffersArray();
			for (ItemStatus is : items)
				is.updateTracker(offers);

			success = GrandExchangeInterface.collect(inventory);

			offers = GrandExchange.getOffersArray();
			for (ItemStatus is : items)
				is.updateTracker(offers);
		}

		return success;
	}

	private boolean openGrandExchange() {
		if (GrandExchangeInterface.isOpen())
			return true;

		List<Npc> filtered = Npcs.getAll().stream()
				.filter((npc) -> npc.getNpcDefinition().getName().equals("Grand Exchange Clerk") && Calculations.onViewport(npc.toScreen()))
				.collect(Collectors.toList());
		if (filtered.size() > 0) {
			Npc npc = filtered.get(0);
			Clicking.interact(npc, "Exchange");
		}

		return Time.sleepUntil(() -> GrandExchangeInterface.isOpen(), 4000, 6000);
	}

	@Override
	public void onDraw(Graphics g) {

		g.drawString(org.iinc.osbot.util.Formatter.timeToString(System.currentTimeMillis() - startTime), 10, 500);

		int x = 10;
		for (ItemStatus is : items) {

			String s = is.toString();
			int maxWidth = 0;
			int h = 0;

			for (String line : s.split("\\n")) {
				int width = g.getFontMetrics().stringWidth(line);
				if (width > maxWidth)
					maxWidth = width;
				h++;
			}

			g.setColor(new Color(0, 0, 0, 127));
			g.fillRect(x - 5, 15, maxWidth + 10, h * 15 + 6);
			g.setColor(Color.green);

			int i = 0;
			for (String line : s.split("\\n")) {
				g.drawString(line, x, 30 + i++ * 15);
			}

			x += maxWidth + 20;
		}

		x = 10;
		for (ItemStatus is : limitedItems) {

			String s = is.toString();
			int maxWidth = 0;
			int h = 0;

			for (String line : s.split("\\n")) {
				int width = g.getFontMetrics().stringWidth(line);
				if (width > maxWidth)
					maxWidth = width;
				h++;
			}

			g.setColor(new Color(0, 0, 0, 127));
			g.fillRect(x - 5, 140 + 15, maxWidth + 10, h * 15 + 6);
			g.setColor(Color.green);

			int i = 0;
			for (String line : s.split("\\n")) {
				g.drawString(line, x, 140 + 30 + i++ * 15);
			}

			x += maxWidth + 20;
		}
	}

}
