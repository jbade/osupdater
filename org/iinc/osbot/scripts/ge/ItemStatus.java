package org.iinc.osbot.scripts.ge;

import java.util.ArrayList;

import org.iinc.osbot.api.injection.accessors.grandexchange.GrandExchangeOffer;
import org.iinc.osbot.util.Formatter;

public class ItemStatus {
	private final String name;
	private final int id;

	private long limitStartTime = 0;
	private long limitEndTime = 0;

	private boolean limited = false;

	private int bought = 0;
	//private int spent = 0;

	private int sold = 0;
	//private int recieved = 0;

	private long lastBought = 0;
	private long lastSold = 0;

	private int buyMargin;
	private int sellMargin;
	private long lastMarginUpdate;

	private ArrayList<OfferState> trackedOffers = new ArrayList<OfferState>(8);

	public ItemStatus(String name, int id) {
		this.name = name;
		this.id = id;

		for (int i = 0; i < 8; i++)
			trackedOffers.add(null);
	}

	public boolean shouldBuy() {
		return bought <= sold * 1.25;
	}

	public void updateMargins(int buy, int sell) {
		buyMargin = buy;
		sellMargin = sell;
		lastMarginUpdate = System.currentTimeMillis();
	}

	public boolean isLimited() {
		if (limited && System.currentTimeMillis() > limitEndTime) {
			limited = false;
			limitStartTime = 0;
			limitEndTime = 0;
		}

		return limited;
	}

	/**
	 * Should be called when an item is detected to be limited.
	 */
	public void limit() {
		limit(14400000 + 10 * 60 * 1000); //4 hours
	}

	public void limit(long time){
		if (limitStartTime == 0)
			limitStartTime = System.currentTimeMillis();

		limitEndTime = limitStartTime + time; // 4 hours + 10 min
		limited = true;
	}
	
	class OfferState {
		int quantity;
		int transfered;
		//int spent;
	}

	public void updateTracker(GrandExchangeOffer[] offers) {
		for (int i = 0; i < offers.length; i++) {
			processOffer(offers[i]);
		}
	}

	private void processOffer(GrandExchangeOffer offer){
		boolean offerEmpty = offer == null || offer.getState() == GrandExchangeOffer.STATES.EMPTY;
		boolean offerUnknown = offer.getState() == GrandExchangeOffer.STATES.UNKNOWN;

		OfferState info = trackedOffers.get(offer.getIndex());

		if (info == null && offerEmpty) {
			// do nothing
		} else if (info != null && (offerEmpty || offer.getItemId() != id || offer.getQuantity() != info.quantity
				|| offer.getTransfered() < info.transfered)) { // TODO track spent
			// complete offer

			int dtransfered = info.quantity - info.transfered;
			if (dtransfered > 0) {
				if (offer.isBuyOffer()) {
					bought += dtransfered;
					if (limitStartTime == 0)
						limitStartTime = System.currentTimeMillis();
				} else if (offer.isSellOffer()) {
					sold += dtransfered;
				}
			}
			trackedOffers.set(offer.getIndex(), null);
			
			processOffer(offer);
		} else if (info != null && !offerEmpty && offer.getItemId() == id) {
			//update offer

			int dtransfered = offer.getTransfered() - info.transfered;

			if (dtransfered > 0) {
				if (offer.isBuyOffer()) {
					bought += dtransfered;
					lastBought = System.currentTimeMillis();
					info.transfered = offer.getTransfered();
					if (limitStartTime == 0)
						limitStartTime = System.currentTimeMillis();
				} else if (offer.isSellOffer()) {
					sold += dtransfered;
					lastSold = System.currentTimeMillis();
					info.transfered = offer.getTransfered();
				}
			}

		} else if (info == null && !offerEmpty && offer.getItemId() == id) {
			// new offer
			info = new OfferState();
			info.quantity = offer.getQuantity();
			trackedOffers.set(offer.getIndex(), info);

			processOffer(offer);
		}
	}
	
	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public long getLastMarginUpdate() {
		return lastMarginUpdate;
	}

	public int getSellPrice() {
		if (sellMargin - buyMargin <= 3)
			return sellMargin;
		return sellMargin - 1;
	}

	public int getBuyPrice() {
		if (sellMargin - buyMargin <= 3)
			return buyMargin;
		return buyMargin + 1;
	}

	public long getLimitStartTime() {
		return limitStartTime;
	}

	public int getBought() {
		return bought;
	}

	public int getSold() {
		return sold;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(name + " (" + id + ")\n");
		sb.append("margin: " + buyMargin + " " + sellMargin + "\n");
		sb.append("bought: " + bought + "\n");
		sb.append("sold: " + sold + "\n");
		sb.append("limited: " + limited + "\n");
		sb.append("last sold: " + (lastSold == 0 ? 0 : Formatter.timeToString(System.currentTimeMillis() - lastSold))
				+ "\n");
		sb.append("last bought: "
				+ (lastBought == 0 ? 0 : Formatter.timeToString(System.currentTimeMillis() - lastBought)) + "\n");

		return sb.toString();
	}

}
