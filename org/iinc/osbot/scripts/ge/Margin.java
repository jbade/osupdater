package org.iinc.osbot.scripts.ge;

public class Margin {
	public int buy;
	public int sell;

	public Margin(int buy, int sell) {
		this.buy = buy;
		this.sell = sell;
	}

	@Override
	public String toString() {
		return "Margin [buy=" + buy + ", sell=" + sell + "]";
	}

}