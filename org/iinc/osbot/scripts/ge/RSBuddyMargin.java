package org.iinc.osbot.scripts.ge;
public class RSBuddyMargin {
	public int buy;
	public int sell;

	public RSBuddyMargin(int buy, int sell) {
		this.buy = buy;
		this.sell = sell;
	}

	@Override
	public String toString() {
		return "Margin [buy=" + buy + ", sell=" + sell + "]";
	}

}