package org.iinc.osbot.scripts.ge;

import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class ItemHandler {

	private static LinkedList<ItemContainer> items = new LinkedList<ItemContainer>();

	static {
		
		items.add(new ItemContainer("Mystic gloves", 4095));
		items.add(new ItemContainer("Rune platebody", 1127));
		items.add(new ItemContainer("Mystic boots", 4097));
	//	items.add(new ItemContainer("Papaya tree seed", 5288));
		items.add(new ItemContainer("Lava battlestaff", 3053));
	//	items.add(new ItemContainer("Zul-andra teleport", 12938));
	//	items.add(new ItemContainer("Snapdragon seed", 5300));
		items.add(new ItemContainer("Rune axe", 1359));
	//	items.add(new ItemContainer("Ensouled demon head", 13502));
	//	items.add(new ItemContainer("Super combat potion(3)", 12697));
	//	items.add(new ItemContainer("Stamina potion(3)", 12627));
		items.add(new ItemContainer("Amulet of glory(4)", 1712));
		items.add(new ItemContainer("Stamina potion(4)", 12625));
		items.add(new ItemContainer("Lava dragon bones", 11943));
		items.add(new ItemContainer("Dragon dagger(p++)", 5698));
		items.add(new ItemContainer("Super restore(3)", 3026));
		items.add(new ItemContainer("Super potion set", 13066));
	//	items.add(new ItemContainer("Maple sapling", 5372));
		items.add(new ItemContainer("Rune battleaxe", 1373));
		items.add(new ItemContainer("Super restore(4)", 3024));
		items.add(new ItemContainer("Ensouled abyssal head", 13508));
		items.add(new ItemContainer("Eclectic impling jar", 11248));
		items.add(new ItemContainer("Sanfew serum(4)", 10925));
		items.add(new ItemContainer("Granite maul", 4153));
		items.add(new ItemContainer("Lumberyard teleport", 12642));
		items.add(new ItemContainer("Combat bracelet", 11126));
		items.add(new ItemContainer("Fire battlestaff", 1393));
		items.add(new ItemContainer("Amulet of glory(6)", 11978));
		items.add(new ItemContainer("Nature impling jar", 11250));
		items.add(new ItemContainer("White platebody", 6617));
		items.add(new ItemContainer("Ranarr seed", 5295));
		items.add(new ItemContainer("Rune full helm", 1163));
		items.add(new ItemContainer("Lunar isle teleport", 12405));
		items.add(new ItemContainer("Black d'hide body", 2503));
		items.add(new ItemContainer("Prayer potion(4)", 2434));
		items.add(new ItemContainer("Loop half of key", 987));
		items.add(new ItemContainer("Rune boots", 4131));
		items.add(new ItemContainer("Combat bracelet(6)", 11972));
		items.add(new ItemContainer("Maple seed", 5314));
		items.add(new ItemContainer("Dragon boots", 11840));
		items.add(new ItemContainer("Crystal key", 989));
		items.add(new ItemContainer("Ensouled tzhaar head", 13499));
		items.add(new ItemContainer("Ring of wealth (5)", 11980));
		items.add(new ItemContainer("Onyx bolts (e)", 9245));
		items.add(new ItemContainer("Ancient staff", 4675));
		items.add(new ItemContainer("Ensouled dragon head", 13511));
		items.add(new ItemContainer("Super strength(4)", 2440));
		items.add(new ItemContainer("Ring of wealth", 2572));
		items.add(new ItemContainer("Rune sword", 1289));
		items.add(new ItemContainer("Earth battlestaff", 1399));
		items.add(new ItemContainer("Runite limbs", 9431));
		items.add(new ItemContainer("Ranarr weed", 257));
		items.add(new ItemContainer("Rune spear", 1247));
		items.add(new ItemContainer("Onyx bolts", 9342));
		items.add(new ItemContainer("Onyx bolt tips", 9194));
		items.add(new ItemContainer("Rune warhammer", 1347));
		items.add(new ItemContainer("Anti-venom+(4)", 12913));
		items.add(new ItemContainer("Snapdragon potion (unf)", 3004));
		items.add(new ItemContainer("Rune kiteshield", 1201));
		items.add(new ItemContainer("Palm sapling", 5502));
		items.add(new ItemContainer("Grimy snapdragon", 3051));
		items.add(new ItemContainer("Runite ore", 451));
		items.add(new ItemContainer("Rune chainbody", 1113));
		items.add(new ItemContainer("Rune platelegs", 1079));	
		//items.add(new ItemContainer("Dragon dagger(p++)", 5698));
		//items.add(new ItemContainer("Dragon axe", 6739));
		//items.add(new ItemContainer("Dragon platelegs", 4087));
		//items.add(new ItemContainer("Dragon plateskirt", 4585));
//		items.add(new ItemContainer("Dragon scimitar", 4587));
//		items.add(new ItemContainer("Dragon boots", 11840));
//		items.add(new ItemContainer("Dragon longsword", 1305));
//		items.add(new ItemContainer("Dragon mace", 1434));
//		items.add(new ItemContainer("Dragon battleaxe", 1377));
//		items.add(new ItemContainer("Dragon med helm", 1149));
//	
		
		
		//items.add(new ItemContainer("Shark", 385));
		//items.add(new ItemContainer("Diamond bolts (e)", 9243));
		//items.add(new ItemContainer("Raw lobster", 377));
		//items.add(new ItemContainer("Rune dagger", 1213));
		//items.add(new ItemContainer("Yew logs", 1515));
		//items.add(new ItemContainer("Rune 2h sword", 1319));
		//items.add(new ItemContainer("Wine of zamorak", 245));
		//items.add(new ItemContainer("Nature rune", 561));
		//items.add(new ItemContainer("Dragon bones", 536));
		//items.add(new ItemContainer("Rune crossbow", 9185));
		//items.add(new ItemContainer("Ruby bolts (e)", 9242));
		//items.add(new ItemContainer("Rune arrow", 892));
		//items.add(new ItemContainer("Amulet of glory(6)", 11978));
		//items.add(new ItemContainer("Ring of wealth (5)", 11980));
		//items.add(new ItemContainer("Lava scale", 11992));
		//items.add(new ItemContainer("Battlestaff", 1391));

//		items.add(new ItemContainer("Dragon bolts (e)", 9244));
//		items.add(new ItemContainer("Shark", 385));
//		items.add(new ItemContainer("Nature rune", 561));
//		items.add(new ItemContainer("Zulrah's scales", 12934));
//		items.add(new ItemContainer("Monkfish", 7946));
//		items.add(new ItemContainer("Dragon dagger(p++)", 5698));
//		items.add(new ItemContainer("Black d'hide chaps", 2497));
//		items.add(new ItemContainer("Astral rune", 9075));
//		items.add(new ItemContainer("Cosmic rune", 564));
//		items.add(new ItemContainer("Cannonball", 2));
//		items.add(new ItemContainer("Diamond bolts (e)", 9243));
//		items.add(new ItemContainer("Super potion set", 13066));
//		items.add(new ItemContainer("Prayer potion(4)", 2434));
//		items.add(new ItemContainer("Teleport to house", 8013));
//		items.add(new ItemContainer("Saltpetre", 13421));
//		items.add(new ItemContainer("Magic logs", 1513));

		items.add(new ItemContainer("Soul rune", 566));
		items.add(new ItemContainer("Blue dragon leather", 2505));
		items.add(new ItemContainer("Green dragonhide", 1753));
		items.add(new ItemContainer("Rune dart", 811));
		items.add(new ItemContainer("Runite bolts", 9144));
		items.add(new ItemContainer("Molten glass", 1775));
		items.add(new ItemContainer("Red chinchompa", 10034));
		items.add(new ItemContainer("Mahogany plank", 8782));
		items.add(new ItemContainer("Rune arrow", 892));
		items.add(new ItemContainer("Chinchompa", 10033));
		items.add(new ItemContainer("Shark", 385));
		//items.add(new ItemContainer("Mithril bolts (unf)", 9379));
		items.add(new ItemContainer("Soft clay", 1761));
		items.add(new ItemContainer("Cowhide", 1739));
		items.add(new ItemContainer("Cannonball", 2));
		items.add(new ItemContainer("Grapes", 1987));
		items.add(new ItemContainer("Battlestaff", 1391));
		items.add(new ItemContainer("Raw swordfish", 371));
		items.add(new ItemContainer("Blue dragonhide", 1751));
		items.add(new ItemContainer("Diamond bolts (e)", 9243));
		items.add(new ItemContainer("Dragon bones", 536));
		items.add(new ItemContainer("Raw shark", 383));
		items.add(new ItemContainer("Adamant dart", 810));
		items.add(new ItemContainer("Mahogany logs", 6332));
		items.add(new ItemContainer("Ruby bolts (e)", 9242));
		items.add(new ItemContainer("Mithril dart tip", 822));
		items.add(new ItemContainer("Gold ore", 444));
		items.add(new ItemContainer("Oak plank", 8778));
		items.add(new ItemContainer("Yew longbow (u)", 66));
		items.add(new ItemContainer("Mithril bar", 2359));
		items.add(new ItemContainer("Maple longbow (u)", 62));
		items.add(new ItemContainer("Ranarr potion (unf)", 99));
		items.add(new ItemContainer("Plank", 960));
		items.add(new ItemContainer("Black chinchompa", 11959));
		items.add(new ItemContainer("Adamant bolts", 9143));
		items.add(new ItemContainer("Redwood logs", 19669));
		items.add(new ItemContainer("Prayer potion(4)", 2434));
		items.add(new ItemContainer("Black knife", 869));
		items.add(new ItemContainer("Magic longbow", 859));
		items.add(new ItemContainer("Bow string", 1777));
		items.add(new ItemContainer("Green dragon leather", 1745));
		items.add(new ItemContainer("Leather", 1741));
		items.add(new ItemContainer("Anglerfish", 13441));
		items.add(new ItemContainer("Varrock teleport", 8007));
		items.add(new ItemContainer("Amylase crystal", 12640));
		items.add(new ItemContainer("Limpwurt root", 225));
		items.add(new ItemContainer("Air battlestaff", 1397));
		items.add(new ItemContainer("Impling jar", 11260));
		items.add(new ItemContainer("Yew longbow", 855));

	}

	/**
	 * Request a new item to flip
	 * 
	 * @param limitedIds
	 * @return
	 */
	public static ItemContainer requestItem(Collection<Integer> limitedIds) {
		return items.stream().filter((i) -> !limitedIds.contains(i.id)).collect(Collectors.toList()).get(0);
	}

}
