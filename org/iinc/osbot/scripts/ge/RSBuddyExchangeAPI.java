package org.iinc.osbot.scripts.ge;

import java.util.List;

import org.iinc.osbot.util.internet.Internet;

import com.google.gson.Gson;

public class RSBuddyExchangeAPI {

	public static void main(String[] args) {
		System.out.println(getPriceMargin(2));
		System.out.println(getVolume(2));
	}

	public static Margin getPriceMargin(int id) {
		List<String> lines = Internet.read("https://api.rsbuddy.com/grandExchange?a=guidePrice&i=" + id);
		String body = "";
		for (String line : lines)
			body += line;
		Gson gson = new Gson();
		GuidePrice gp = gson.fromJson(body, GuidePrice.class);

		if (gp == null || gp.buying <= 0 || gp.selling <= 0)
			return null;

		return new Margin(Math.min(gp.selling, gp.buying), Math.max(gp.selling, gp.buying));
	}

	public static Volume getVolume(int id) {
		List<String> lines = Internet.read("https://api.rsbuddy.com/grandExchange?a=graph&i=" + id);
		String body = "";
		for (String line : lines)
			body += line;
		Gson gson = new Gson();
		GraphElement[] elements = gson.fromJson(body, GraphElement[].class);

		if (elements == null || elements.length == 0)
			return null;

		int buyingTotal = 0;
		int sellingTotal = 0;
		long startTime = 0;
		long endTime = 0;

		for (int i = elements.length - 1; i >= 0; i--) {
			if (endTime == 0)
				endTime = elements[i].ts;
			startTime = elements[i].ts;
			buyingTotal += elements[i].buyingCompleted;
			sellingTotal += elements[i].sellingCompleted;
		}

		return new Volume((int) ((double) buyingTotal / (endTime - startTime) * 1000 * 60 * 60),
				(int) ((double) sellingTotal / (endTime - startTime) * 1000 * 60 * 60));
	}

	private class GuidePrice {
		private int overall;
		private int buying;
		private int buyingQuantity;
		private int selling;
		private int sellingQuantity;
	}

	private class GraphElement {
		private long ts;
		private int buyingPrice;
		private int buyingCompleted;
		private int sellingPrice;
		private int sellingCompleted;
		private int overallPrice;
		private int overallCompleted;
	}

	private class SummaryItem {
		private int overall_average;
		private int sp;
		private String name;
		private int sell_average;
		private int id;
		private int buy_average;
	}

	

	public static class Volume {
		private int buy;
		private int sell;

		public Volume(int buy, int sell) {
			this.buy = buy;
			this.sell = sell;
		}

		@Override
		public String toString() {
			return "Margin [buy=" + buy + ", sell=" + sell + "]";
		}

	}

}
