package org.iinc.osbot.scripts.shop.zaff;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.BoundaryObject;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.Random;

public class Zaff implements Node {

	static Area SHOP_AREA = new Area(new Tile(3205, 3438, 0), new Tile(3200, 3438, 0), new Tile(3200, 3430, 0),
			new Tile(3205, 3430, 0));
	static Tile BANK_TILE = new Tile(3185, 3436, 0);
	static Tile DOOR_TILE = new Tile(3204, 3432, 0, 72, 0, 128);
	static Tile SHOP_CENTER = new Tile(3202, 3434, 0);
	static final Tile LUMBRIDGE_BANK_TILE_1 = new Tile(3209, 3221, 2);
	static final Tile LUMBRIDGE_BANK_TILE_2 = new Tile(3208, 3221, 2);
	static final Tile VARROCK_WEST_BANK_TILE_1 = new Tile(3186, 3436, 0);
	static final Tile VARROCK_WEST_BANK_TILE_2 = new Tile(3186, 3438, 0);
	static final int NEEDED_COINS = 10000;

	private List<ContinuableNode> nodes = new LinkedList<ContinuableNode>();

	public Zaff() {
		Collections.addAll(nodes, new GrandExchangeNode(), new ToBankNode(), new BankingNode(), new ToShopNode(),
				new ShopNode());
	}

	@Override
	public boolean activate() {
		return true;
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		
		if (Walking.getRunEnergy() > Random.randomRange(20, 30))
			Walking.enableRun();
		
		if (!checkTrappedInBankCloset())
			return;
		
		for (ContinuableNode node : nodes) {
			if (node.activate()) {
				Bot.LOGGER.log(Level.INFO, node.getClass().getName());
				if (!node.execute())
					return;
			}
		}
	}

	public static boolean isDoorOpen() {
		BoundaryObject bo = RSObjects.getBoundaryObject(new Tile(3204, 3432, 0));
		return bo != null && bo.getObjectId() == 11774;
	}

	public static boolean checkTrappedInBankCloset() {
		if (!Login.isLoggedIn())
			return true;
		
		Tile loc = Client.getLocalPlayer().getTile();
		
		// closet
		if (loc.equals(new Tile(3186, 3434, 0)) || loc.equals(new Tile(3186, 3433, 0))) {
			if (RSObjects.getBoundaryObject(new Tile(3185, 3434, 0)) == null){
				Interacting.interact(new Tile(3185, 3434, 0, 72, 0, 128), "Open");
				Time.sleep(600, 1000);
				return false;
			}
		}

		// basement
		if (loc.distanceTo(new Tile(3190, 9833, 0)) < 20) {
			Interacting.interact(() -> RSObjects.getGameObjects(20).stream().filter((go) -> go.getObjectId() == 11805)
					.findAny().orElse(null), new Tile(3190, 9833, 0));
			Time.sleep(600, 1000);
			return false;
		}
		
		return true;
	}
}
