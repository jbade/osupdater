package org.iinc.osbot.scripts.shop.zaff;

import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.scripts.common.Navigation;
import org.iinc.osbot.scripts.gdk.GDK;
import org.iinc.osbot.scripts.gdk.GrandExchangeNode;
import org.iinc.osbot.util.Random;

public class BankingNode implements ContinuableNode {

	@Override
	public boolean activate() {
		return Navigation.atGrandExchange() || Navigation.atLumbridgeBank()
				|| Client.getLocalPlayer().getTile().distanceTo(Zaff.BANK_TILE) < 5;
	}

	@Override
	public boolean execute() {
		if (Bank.openBooth("Bank", Zaff.LUMBRIDGE_BANK_TILE_1, Zaff.LUMBRIDGE_BANK_TILE_2)
				|| Bank.openBooth("Bank", Zaff.VARROCK_WEST_BANK_TILE_1, Zaff.VARROCK_WEST_BANK_TILE_2)
				|| Bank.openBanker()) {
			if (!Bank.isOpen())
				return false;

			// deposit items or wait for bank to load
			if (Inventory.getCount() == 0) {
				Time.sleep(600, 1200);
			} else if ((!Bank.depositInventory() || !Time.sleepUntil(() -> Inventory.getCount() == 0, 1200, 1700)))
				return false;

			if (Bank.getQuantity(Items.COINS.getId()) < Zaff.NEEDED_COINS || Bank.getQuantity(Items.VARROCK_TELEPORT.getId()) <= 1) {
				if (!Navigation.atGrandExchange() && !(Client.getLocalPlayer().getTile().distanceTo(Zaff.BANK_TILE) < 5) && Bank.contains(Items.VARROCK_TELEPORT.getId())) {
					if (!Bank.withdraw(Items.VARROCK_TELEPORT.getId(), 1))
						return false;

					if (!Bank.close())
						return false;

					Time.sleep(Random.randomRange(400, 500));

					Item item = Inventory.getFirst(Items.VARROCK_TELEPORT.getId());
					if (item != null) {
						if (Clicking.interact(item, "Break")) {
							Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 4069
									|| Client.getLocalPlayer().getAnimation() == 4071, 600, 1200);
							Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == -1, 3000, 4000);
							GrandExchangeNode.start();
						}
					}
				} else {
					GrandExchangeNode.start();
				}
				return false;
			}
			
			if (!Bank.withdraw(Items.COINS.getId(), -1))
				return false;
			
			if (!Navigation.atGrandExchange() && !(Client.getLocalPlayer().getTile().distanceTo(Zaff.BANK_TILE) < 5)){
				if (!Bank.withdraw(Items.VARROCK_TELEPORT.getId(), 1))
					return false;

				if (!Bank.close())
					return false;

				Time.sleep(Random.randomRange(400, 500));

				Item item = Inventory.getFirst(Items.VARROCK_TELEPORT.getId());
				if (item != null) {
					if (Clicking.interact(item, "Break")) {
						Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 4069
								|| Client.getLocalPlayer().getAnimation() == 4071, 600, 1200);
						Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == -1, 3000, 4000);
						return false;
					}
				}
			} else {
				return true;
			}
		}
		
		return false;
	}

}
