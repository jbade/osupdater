package org.iinc.osbot.scripts.shop.zaff;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.scripts.common.Navigation;

public class ToShopNode implements ContinuableNode {

	static Tile UPSTAIRS = new Tile(3202, 3433, 1, 0, 76, 0);

	@Override
	public boolean activate() {
		return !Zaff.SHOP_AREA.contains(Client.getLocalPlayer().getTile());
	}

	@Override
	public boolean execute() {
		Tile loc = Client.getLocalPlayer().getTile();

		if (loc.distanceTo(UPSTAIRS) < 5) {
			if (Interacting.interact(UPSTAIRS, "Climb-down", "Ladder"))
				Time.sleep(500, 800);
		} else if (loc.distanceTo(Zaff.DOOR_TILE) >= 75) {
			Navigation.toGrandExchange();
		} else if (loc.distanceTo(Zaff.DOOR_TILE) < 75 && loc.distanceTo(Zaff.DOOR_TILE) >= 5) {
			Walking.walkPath(Walking.generatePath(loc, Zaff.DOOR_TILE, 3), true);
		} else if (loc.distanceTo(Zaff.DOOR_TILE) < 5) {
			if (!Zaff.isDoorOpen()) {
				if (Interacting.interact(Zaff.DOOR_TILE, "Open", "Door"))
					Time.sleep(500, 800);
			} else {
				Walking.walk(Zaff.SHOP_CENTER);
				Time.sleepUntil(() -> Walking.hasDestination(), 600, 800);
				Time.sleepUntil(() -> !Walking.hasDestination(), 6000, 10000);
			}
		}

		return false;
	}

}
