package org.iinc.osbot.scripts.shop.zaff;

import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.Memory;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class ZaffScript extends Script {

	Zaff z = new Zaff();

	@Override
	public int loop() {
		if (!Login.isLoggedIn()) {
			Login.login();
			Time.sleep(600, 1000);
			Camera.setAngle(Camera.getCameraAngle(), true);
			return 0;
		}

		Interfaces.closeAll();

		Memory.checkMemory(1000000L);

		if (BreakHandler.shouldBreak()) {
			Interfaces.closeAll();
			Timer t = new Timer(Random.randomRange(10000, 15000));
			while (Login.isLoggedIn() && t.isRunning())
				Logout.clickLogout();
			BreakHandler.doBreak();
		}

		if (z.activate())
			z.execute();

		return Random.randomRange(50, 100);
	}

	@Override
	public void onStop() {

	}

	@Override
	public void onStart() {
		Modifiers.init(Bot.username.hashCode());
		BreakHandler.init(Bot.username.hashCode());
	}

}
