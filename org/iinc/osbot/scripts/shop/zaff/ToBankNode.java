package org.iinc.osbot.scripts.shop.zaff;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.scripts.common.Navigation;

public class ToBankNode implements ContinuableNode {

	@Override
	public boolean activate() {
		return (Inventory.isFull() || Inventory.getCount(Items.COINS.getId()) < Zaff.NEEDED_COINS)
				&& !Navigation.atGrandExchange() && !Navigation.atLumbridgeBank()
				&& Client.getLocalPlayer().getTile().distanceTo(Zaff.BANK_TILE) >= 5;
	}

	@Override
	public boolean execute() {
		Tile loc = Client.getLocalPlayer().getTile();
		if (Zaff.SHOP_AREA.contains(loc) && !Zaff.isDoorOpen()) {
			if (Interacting.interact(Zaff.DOOR_TILE, "Open", "Door"))
				Time.sleep(500, 800);
		} else if (loc.distanceTo(Zaff.BANK_TILE) < 75 && loc.distanceTo(Zaff.BANK_TILE) >= 5) {
			Walking.walkPath(Walking.generatePath(loc, Zaff.BANK_TILE, 3), true);
		} else {
			Navigation.toLumbridgeBank();
		}

		return false;
	}

}
