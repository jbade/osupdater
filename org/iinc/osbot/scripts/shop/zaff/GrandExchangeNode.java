package org.iinc.osbot.scripts.shop.zaff;

import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.scripts.common.BankGoal;

public class GrandExchangeNode implements ContinuableNode {
	private static org.iinc.osbot.scripts.common.GrandExchangeNode gen = new org.iinc.osbot.scripts.common.GrandExchangeNode(
			new RSItemConstant[] { Items.BATTLESTAFF }, true,
			new BankGoal[] { new BankGoal(Items.VARROCK_TELEPORT, 5, () -> true) });

	public static void start() {
		org.iinc.osbot.scripts.common.GrandExchangeNode.start();
	}

	@Override
	public boolean activate() {
		return gen.activate();
	}

	@Override
	public boolean execute() {
		gen.execute();
		return false;
	}

}
