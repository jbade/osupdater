package org.iinc.osbot.scripts.shop.zaff;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.Shop;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.base.worlds.World;
import org.iinc.osbot.api.base.worlds.Worlds;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.ContinuableNode;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.Tuple;

public class ShopNode implements ContinuableNode {

	Tuple<RSItemConstant, Integer>[] buy = new Tuple[] { new Tuple(Items.BATTLESTAFF, 0) };
	boolean emptied = false;

	@Override
	public boolean activate() {
		return true;
	}

	@Override
	public boolean execute() {
		if (emptied) {
			// hop worlds
			World w = Worlds.getRandomNormalWorld(true);
			if (w != null && Logout.switchWorld(w.getId()))
				emptied = false;
		} else {
			if (Interacting.interact(() -> Npcs.getClosest("Zaff"), Zaff.SHOP_CENTER, "Trade", "Zaff")) {
				if (Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting(), 1000, 1600))
					Time.sleepUntil(() -> Shop.isOpen(), 3500, 4500);

				if (!Shop.isOpen())
					return false;

				Time.sleep(200, 300);
				
				for (Tuple<RSItemConstant, Integer> id : buy) {
					int count = Shop.getQuantity(id.x.getId());
					while (count > id.y) {
						int diff = count - id.y;
						String s;
						if (id.y == 0 || diff >= 10) {
							s = "Buy 10";
						} else if (diff >= 5) {
							s = "Buy 5";
						} else {
							s = "Buy 1";
						}

						int tq = Inventory.getTotalQuantities();
						if (Clicking.interact(Shop.getWidget(id.x.getId()), s)) {
							if (diff <= 10) {
								Time.sleepUntil(() -> tq != Inventory.getTotalQuantities(), 2000, 3000);
							} else {
								Time.sleep(200, 400);
							}
						}

						count = Shop.getQuantity(id.x.getId());
						if (Inventory.isFull() || Inventory.getCount(Items.COINS.getId()) < Zaff.NEEDED_COINS) {
							return false;
						}
					}
					emptied = true;
				}
			}
		}
		return false;
	}

}
