package org.iinc.osbot.scripts;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;

public class TestScript extends Script implements PaintListener {

	@Override
	public int loop() {
		return 1500;
	}

	@Override
	public void onDraw(Graphics g) {
		// Tile[] arr = new Tile[] { new Tile(2570, 3295, 0),
		// new Tile(2575, 3295, 0),
		// new Tile(2575, 3287, 0),
		// new Tile(2570, 3287, 0),};
		// Area house = new Area( arr);
		//
		//
		// Tile t = Client.getLocalPlayer().getTile();
		// for (int i = -10; i <= 10; i++) {
		// for (int j = -10; j <= 10; j++) {
		// if (house.contains(t.modify(i, j))) {
		// g.drawString(".", t.modify(i, j).toScreen().x, t.modify(i, j).toScreen().y);
		// }
		// }
		// }
		//
		// for (Tile t2 : arr){
		// g.drawString("x", t2.toScreen().x, t2.toScreen().y);
		// }
		//
		double angle = Camera.getCameraAngle();
		if (angle >= 0 && angle <= Math.PI / 2) {
			Tile t2 = new Tile(2543, 2888, 0, 48, -48, 0);
			g.drawString(".", t2.toScreen().x, t2.toScreen().y);
		} else if (angle >= Math.PI / 2 && angle <= Math.PI) {
			Tile t2 = new Tile(2543, 2888, 0, 48, 48, 0);
			g.drawString(".", t2.toScreen().x, t2.toScreen().y);
		} else if (angle >= Math.PI && angle <= Math.PI * 3 / 2) {
			Tile t2 = new Tile(2543, 2888, 0, -48, 48, 0);
			g.drawString(".", t2.toScreen().x, t2.toScreen().y);
		} else if (angle >= Math.PI * 3 / 2 && angle <= Math.PI * 2) {
			Tile t2 = new Tile(2543, 2888, 0, -48, -48, 0);
			g.drawString(".", t2.toScreen().x, t2.toScreen().y);
		}

	}

	@Override
	public void onStop() {

	}

	@Override
	public void onStart() {

	}

}
