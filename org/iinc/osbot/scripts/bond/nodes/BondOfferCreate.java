package org.iinc.osbot.scripts.bond.nodes;

import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.grandexchange.GrandExchangeInterface;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.grandexchange.GrandExchange;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.scripts.common.Navigation;
import org.iinc.osbot.util.Email;

public class BondOfferCreate implements Node {

	boolean success = false;
	boolean emptiedBank = false;

	@Override
	public boolean activate() {
		return !success;
	}

	@Override
	public void execute() {
		if (!Login.isLoggedIn()) {
			Login.login();
			Time.sleep(600, 1000);
			Camera.setAngle(Camera.getCameraAngle(), true);
			return;
		}

		Interfaces.closeAll();

		if (!Navigation.toGrandExchange())
			return;

		if (GrandExchange.getOffers().size() > 0) {
			if (GrandExchangeInterface.openClerk()){
				GrandExchangeInterface.abortAndCollect(false);
			}
		} else if (!emptiedBank) {
			if (Bank.openBanker()) {
				if ((!Bank.depositInventory() || !Time.sleepUntil(() -> Inventory.getCount() == 0, 1200, 1700)))
					return;

				if (!Bank.withdraw(Items.COINS.getId(), -1))
					return;

				emptiedBank = true;
			}

		} else {

			if (!GrandExchangeInterface.openClerk() && !GrandExchangeInterface.abortAndCollect(true))
				return;

			int coins = Inventory.getCount(Items.COINS.getId());

			if (GrandExchangeInterface.createBuyOffer("Old school bond", coins - 50000, 1)) {
				Email.sendEmail(Email.TYPE.INFO,
						"BondOfferCreateScript: Succesfully created offer for " + (coins - 50000) + " gp");
				success = true;
			}
		}

	}

}
