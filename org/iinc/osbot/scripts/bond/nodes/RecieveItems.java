package org.iinc.osbot.scripts.bond.nodes;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.Trading;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.scripts.common.Navigation;
import org.iinc.osbot.util.Email;
import org.iinc.osbot.util.Timer;

public class RecieveItems implements Node {

	boolean success = false;
	private static final Tile TRADE_TILE = new Tile(3165, 3486, 0); // http://i.imgur.com/kpuYlkY.png -1 y
	private static final String MULE_RIGHT_CLICK = "Cke Runs De  (level-39)";


	@Override
	public boolean activate() {
		return !success;
	}

	@Override
	public void execute() {
		if (!Login.isLoggedIn()) {
			Login.login();
			Time.sleep(600, 1000);
			Camera.setAngle(Camera.getCameraAngle(), true);
			return;
		}

		Interfaces.closeAll();

		if (!Navigation.toGrandExchange())
			return;

		if (Inventory.getCount() != 0) {
			if (Bank.openBanker()) {
				if ((!Bank.depositInventory() || !Time.sleepUntil(() -> Inventory.getCount() == 0, 1200, 1700)))
					return;
			}
		} else {

			if (Calculations.onViewport(TRADE_TILE.toScreen(30))) {
				// prevent bots from stacking on trade tile
				if (Client.getLocalPlayer().getTile().equals(TRADE_TILE)) {
					Walking.walk(TRADE_TILE.randomized());
					return;
				}

				if (Interacting.interact(TRADE_TILE, "Trade with", MULE_RIGHT_CLICK)){
					if (!Time.sleepUntil(() -> Trading.isOpen(), 5000, 10000))
						return;

					if (!MULE_RIGHT_CLICK.substring(0, MULE_RIGHT_CLICK.indexOf('(')).trim().equals(Trading.getName()))
						return;

					Timer t = new Timer(30000);
					while (Trading.isOpen() && t.isRunning()) {
						Trading.accept();
						Time.sleep(1200, 3200);
					}

					if (t.isRunning() && Inventory.getCount() != 0){
						Email.sendEmail(Email.TYPE.INFO, "RecieveItems: Succesfully recieved items from mule");
						success = true;
					}
				} else {
					Time.sleep(600, 1200);
				}

			} else {
				Walking.blindWalk(TRADE_TILE);
			}
		}

	}

}
