package org.iinc.osbot.scripts.bond.nodes;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.grandexchange.GrandExchangeInterface;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.grandexchange.GrandExchange;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.scripts.common.Navigation;
import org.iinc.osbot.util.Email;

public class BondOfferRedeem implements Node {

	boolean success = false;

	@Override
	public boolean activate() {
		return !success;
	}

	@Override
	public void execute() {
		if (!Login.isLoggedIn()) {
			Login.login();
			Time.sleep(600, 1000);
			Camera.setAngle(Camera.getCameraAngle(), true);
			return;
		}

		Interfaces.closeAll();

		if (!Navigation.toGrandExchange())
			return;

		// close bond interface
		if (Widgets.exists(66, 3, 13)) {
			Clicking.interact(Widgets.getWidget(66, 3, 13));
			return;
		}

		if (Inventory.contains(Items.BOND_UNTRADABLE.getId())) {
			if (!Clicking.interact(Inventory.getFirst(Items.BOND_UNTRADABLE.getId()), "Redeem"))
				return;

			Time.sleep(3200, 4000);

			Widget[] w = Widgets.getWidgets()[66];
			if (w == null)
				return;

			boolean clicked = false;
			for (Widget child : w) {
				if (child != null && !child.isHidden()) {
					String text = child.getText();
					if (text != null && text.toLowerCase().contains("14")) {
						clicked = true;
						if (!Clicking.interact(child))
							return;
					}
				}
			}
			if (!clicked)
				return;

			Time.sleep(1200, 1800);

			clicked = false;

			for (Widget child : w) {
				if (child != null && !child.isHidden()) {
					String text = child.getText();
					if (text != null && text.toLowerCase().contains("confirm")) {
						clicked = true;
						if (!Clicking.interact(child))
							return;
					}
				}
			}

			if (!clicked)
				return;

			Time.sleep(40000, 60000);
			if (Inventory.getCount(Items.BOND_UNTRADABLE.getId()) == 0) {
				// success
				Email.sendEmail(Email.TYPE.INFO, "Redeemed bond\n");
				success = true;
			}
		} else {
			if (GrandExchange.getOffers().size() != 0) {
				if (GrandExchangeInterface.openClerk()) {
					GrandExchangeInterface.abortAndCollect(false);
					GrandExchangeInterface.close();
				}
				return;
			}

			if (Bank.openBanker()) {
				if ((!Bank.depositInventory() || !Time.sleepUntil(() -> Inventory.getCount() == 0, 1200, 1700)))
					return;

				if (!Bank.contains(Items.BOND_UNTRADABLE.getId())) {
					Email.sendEmail(Email.TYPE.SEVERE, "BondOfferRedeem: No bond to redeem");
					System.exit(Settings.DISABLED_EXIT_CODE);
				}

				if (!Bank.withdraw(Items.BOND_UNTRADABLE.getId(), 1))
					return;

			}
		}

	}

}
