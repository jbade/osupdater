package org.iinc.osbot.scripts.bond;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.bond.nodes.BondOfferRedeem;

public class RedeemBondOfferScript extends Script {

	BondOfferRedeem bor = new BondOfferRedeem();

	@Override
	public int loop() {
		if (!Login.isLoggedIn()) {
			Login.login();
			Time.sleep(600, 1000);
			Camera.setAngle(Camera.getCameraAngle(), true);
			return 300;
		}

		if (bor.activate()) {
			bor.execute();
		} else {
			System.exit(Settings.DISABLED_EXIT_CODE);
		}

		return 300;
	}

	@Override
	public void onStop() {

	}

	@Override
	public void onStart() {
		Settings.MAKE_CLICKING_ERRORS = false;
	}

}
