package org.iinc.osbot.scripts;

import java.awt.Point;

import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.Shop;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.base.worlds.World;
import org.iinc.osbot.api.base.worlds.Worlds;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Tuple;

public class ShopScript extends Script {

	Tuple<RSItemConstant, Integer>[] buy = new Tuple[] { new Tuple<>(Items.MITHRIL_ARROWTIPS, 300), new Tuple<>(Items.STEEL_ARROWTIPS, 500) };

	boolean emptied = false;
	int minCoins = 1000;
	String name = "Hickton";

	@Override
	public int loop() {
		if (!Login.isLoggedIn()) {
			Login.login();
			Time.sleep(600, 1200);
			Camera.setAngle(Camera.getCameraAngle(), true);
			return 0;
		}

		Interfaces.closeAll();

		if (emptied) {
			// hop worlds
			World w = Worlds.getRandomNormalWorld(true);
			if (w != null && Logout.switchWorld(w.getId()))
				emptied = false;
		} else {
			if (Interacting.interact(() -> Npcs.getClosest(name), null, "Trade", name)) {
				if (Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting(), 1200, 1800))
					Time.sleepUntil(() -> Shop.isOpen(), 3500, 4500);

				if (!Shop.isOpen())
					return 0;

				Time.sleep(200, 300);

				for (Tuple<RSItemConstant, Integer> id : buy) {
					int count = Shop.getQuantity(id.x.getId());
					while (count > id.y) {
						int diff = count - id.y;
						String s;
						if (id.y == 0 || diff >= 10) {
							s = "Buy 10";
						} else if (diff >= 5) {
							s = "Buy 5";
						} else {
							s = "Buy 1";
						}

						int tq = Inventory.getTotalQuantities();
						while (Shop.getQuantity(id.x.getId()) - 30 > id.y) {
							if (!Shop.getWidget(id.x.getId()).toScreen().contains(Mouse.getLocation())) {
								Clicking.hover(Shop.getWidget(id.x.getId()));
								Time.sleep(Random.modifiedRandomGuassianRange(100, 300, ConfidenceIntervals.CI_70, 0,
										Modifiers.get("Shop.sleep1")));
							}

							Time.sleep(Random.modifiedRandomGuassianRange(20, 40, ConfidenceIntervals.CI_60, 0,
									Modifiers.get("Shop.sleep2")));

							Mouse.click(Mouse.RIGHT_BUTTON);
							Time.sleep(Random.modifiedRandomGuassianRange(20, 40, ConfidenceIntervals.CI_60, 0,
									Modifiers.get("Shop.sleep2")));

							Point pos = Mouse.getLocation();
							Mouse.setLocation(pos.x, pos.y + 72);

							Time.sleep(Random.modifiedRandomGuassianRange(20, 40, ConfidenceIntervals.CI_60, 0,
									Modifiers.get("Shop.sleep2")));

							Mouse.click(Mouse.LEFT_BUTTON);

							Time.sleep(Random.modifiedRandomGuassianRange(20, 40, ConfidenceIntervals.CI_60, 0,
									Modifiers.get("Shop.sleep3")));

							pos = Mouse.getLocation();
							Mouse.setLocation(pos.x, pos.y - 72);

							Time.sleep(Random.modifiedRandomGuassianRange(20, 40, ConfidenceIntervals.CI_60, 0,
									Modifiers.get("Shop.sleep2")));

						}
						if (Clicking.interact(Shop.getWidget(id.x.getId()), s)) {
							if (diff <= 10) {
								Time.sleepUntil(() -> tq != Inventory.getTotalQuantities(), 2000, 3000);
							} else {
								Time.sleep(200, 400);
							}
						}

						count = Shop.getQuantity(id.x.getId());
						if (Inventory.isFull() || Inventory.getCount(Items.COINS.getId()) < minCoins) {
							return 0;
						}
					}
					emptied = true;
				}
			}
		}

		return 0;
	}

	@Override
	public void onStop() {

	}

	@Override
	public void onStart() {

	}

}
