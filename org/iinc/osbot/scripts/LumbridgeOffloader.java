package org.iinc.osbot.scripts;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.Trading;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.scripts.common.Navigation;

public class LumbridgeOffloader extends Script {

	static final String NAME = "Muler323";
	static final int COMBAT = 14;
	static final RSItemConstant[] ITEMS = new RSItemConstant[] { Items.COINS, Items.RAW_LOBSTER, Items.RAW_ANCHOVIES,
			Items.RAW_SHRIMPS, Items.RAW_SHARK, Items.RAW_SWORDFISH, Items.RAW_TUNA, Items.BATTLESTAFF };

	static final Tile MULE_TILE = new Tile(3209, 3220, 2); // http://i.imgur.com/IZ4DDSv.png

	static final Tile LUMBRIDGE_BANK_TILE_1 = new Tile(3209, 3221, 2);
	static final Tile LUMBRIDGE_BANK_TILE_2 = new Tile(3208, 3221, 2);

	boolean emptiedBank = false;
	boolean withdrawnAll = false;

	@Override
	public int loop() {
		if (!Login.isLoggedIn()) {
			Login.login();
			Time.sleep(1000, 2000);
			Camera.setAngle(Camera.getCameraAngle(), true);
			return 0;
		}

		Interfaces.closeAll();

		if (Navigation.toLumbridgeBank()) {
			if (!emptiedBank) {
				if (Bank.openBooth("Bank", LUMBRIDGE_BANK_TILE_1, LUMBRIDGE_BANK_TILE_2)) {
					if (Inventory.getCount() != 0 && (!Bank.depositInventory()
							|| !Time.sleepUntil(() -> Inventory.getCount() == 0, 1200, 2000))  )
						return 0;

					if (!Bank.withdrawAsNote())
						return 0;
					Time.sleep(1000, 2000);
					for (RSItemConstant item : ITEMS) {
						if (Inventory.isFull()) {
							emptiedBank = true;
							withdrawnAll = false;
							return 0;
						}

						if (Bank.contains(item.getId()))
							if (!Bank.withdraw(item.getId(), -1))
								return 0;
							else
								Time.sleep(1200, 2000);

					}

					emptiedBank = true;
					withdrawnAll = true;

				}
			} else {
				// emptied bank
				// trade player

				// prevent bots from stacking on trade tile
				if (Client.getLocalPlayer().getTile().equals(MULE_TILE)) {
					Walking.walk(MULE_TILE.randomized());
					return 0;
				}

				if (Clicking.interact(MULE_TILE, "Trade with", NAME + "  (level-" + COMBAT + ")")) {
					if (!Time.sleepUntil(() -> Trading.isOpen(), 20000, 30000))
						return 0;

					if (!NAME.equals(Trading.getName())) {
						Time.sleepUntil(() -> !Trading.isOpen(), 30000, 60000);
						return 0;
					}

					boolean all = false;
					while (!all) {
						all = true;
						for (RSItemConstant item : ITEMS) {
							if (!Trading.offerAll(item.getName()))
								all = false;
						}
					}

					while (Trading.isOpen()) {
						Trading.accept();
						Time.sleep(1000, 2000);
					}

					if (!withdrawnAll) {
						emptiedBank = false;
					} else {
						System.exit(Settings.DISABLED_EXIT_CODE);
					}

				} else {
					Time.sleep(2000, 5000);
				}
			}
		}

		return 0;

	}

	@Override
	public void onStop() {
	}

	@Override
	public void onStart() {
	}

}
