package org.iinc.osbot.scripts.tutorial.combat;

import java.util.logging.Level;

import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class SitBackAndWatch implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Sit back and watch.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		if (!Client.getLocalPlayer().isInteracting()){
			new Attacking().execute();
		}
	}

}