package org.iinc.osbot.scripts.tutorial.combat;

import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.NpcDefinition;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.renderable.Players;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class Attacking implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Attacking.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		Tile loc = Client.getLocalPlayer().getTile();

		List<Npc> filtered = Npcs.getAll().parallelStream()
				.filter((npc) -> npc.getNpcDefinition().getName().equals("Giant rat") && npc.getAnimation() == -1
						&& !npc.isBeingInteractedWith(Players.getAll()))
				.sorted((a, b) -> {
					double dist = loc.distanceTo(a.getTile()) - loc.distanceTo(b.getTile());
					if (dist > 0)
						return 1;
					if (dist < 0)
						return -1;
					return 0;
				}).collect(Collectors.toList());

		List<Npc> attackingPlayer = filtered.parallelStream().filter((npc) -> {
			if (npc == null || npc.getInteractingReference() != Client.getLocalPlayer().getReference())
				return false;
			NpcDefinition def = npc.getNpcDefinition();
			if (def == null)
				return false;
			String name = def.getName();
			return name != null && name.equals("Giant rat");
		}).sorted((a, b) -> {
			double dist = loc.distanceTo(a.getTile()) - loc.distanceTo(b.getTile());
			if (dist > 0)
				return 1;
			if (dist < 0)
				return -1;
			return 0;
		}).collect(Collectors.toList());

		Npc npc = attackingPlayer.size() > 0 ? attackingPlayer.get(0) : filtered.size() > 0 ? filtered.get(0) : null;

		if (npc != null) {
			Bot.LOGGER.log(Level.FINEST, "Interacting");

			if (Random.modifiedRandomRange(0, 100, 20, Modifiers.get("TutorialIsland.camera1")) > 90
					|| !Calculations.onViewport(npc.toScreen())
							&& Random.modifiedRandomRange(0, 100, 30, Modifiers.get("TutorialIsland.camera2")) > 70) {
				Camera.turnTo(npc.getTile());
			}
			Tile npcTile = npc.getTile();
			npcTile = Walking.getClosestTileOnMiniMap(npcTile,
					Client.getLocalPlayer().getTile().distanceTo(npcTile) - 2);
			if (Calculations.onViewport(npc.toScreen()) || Walking.walk(npcTile)) {
				if (Clicking.interact(npc, "Attack")) {
					Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting(), 600, 800);
					if (Time.sleepUntil(() -> !Client.getLocalPlayer().isInteracting(), 3000, 4000)) {
						return;
					}
				}
			}
		}
	}

}