package org.iinc.osbot.scripts.tutorial.combat;

import java.util.Optional;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.tabs.Equipment;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.renderable.Players;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class RatRanging implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Rat ranging.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());

		if (Inventory.contains(882) && !Clicking.interact(Inventory.getFirst(882), "Wield")
				|| Inventory.contains(841) && !Clicking.interact(Inventory.getFirst(841), "Wield"))
			return;
		
		Time.sleep(300, 500);

		if (TutorialHelper.walkPrecise(new Tile(3112, 9518, 0), 1)) {
			Tile loc = Client.getLocalPlayer().getTile();
			Optional<Npc> filtered = Npcs.getAll()
					.parallelStream().filter((npc) -> npc.getNpcDefinition().getName().equals("Giant rat")
							&& npc.getAnimation() == -1 && !npc.isBeingInteractedWith(Players.getAll()))
					.sorted((a, b) -> {
						double dist = loc.distanceTo(a.getTile()) - loc.distanceTo(b.getTile());
						if (dist > 0)
							return 1;
						if (dist < 0)
							return -1;
						return 0;
					}).findFirst();

			if (filtered.isPresent()) {
				Bot.LOGGER.log(Level.FINEST, "Interacting");

				Npc npc = filtered.get();

				if (Random.modifiedRandomRange(0, 100, 20, Modifiers.get("TutorialIsland.camera1")) > 90
						|| !Calculations.onViewport(npc.toScreen())
								&& Random.modifiedRandomRange(0, 100, 30, Modifiers.get("TutorialIsland.camera2")) > 70) {
					Camera.turnTo(npc.getTile());
				}
				Tile npcTile = npc.getTile();
				npcTile = Walking.getClosestTileOnMiniMap(npcTile,
						Client.getLocalPlayer().getTile().distanceTo(npcTile) - 2);
				if (Calculations.onViewport(npc.toScreen())) {
					if (Clicking.interact(npc, "Attack")) {
						Timer t = new Timer(Random.randomRange(2500, 3000));
						while (t.isRunning()) {
							// get more arrows if out
							if (Equipment.getArrow() == -1) {
								new WellDoneYouveMadeYourFirstKill().execute();
								return;
							}

							if (Client.getLocalPlayer().getAnimation() != -1)
								t.reset();
							Time.sleep(150, 300);
						}
					}
				}
			}
		}

	}

}