package org.iinc.osbot.scripts.tutorial.combat;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class MovingOn implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Moving on.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		Tile loc = Client.getLocalPlayer().getTile();

		if (Interacting.interact(new Tile(3111, 9526, 0, 0, 0, 90), "Climb-up")
				&& Time.sleepUntil(() -> Walking.isMoving() || Client.getLocalPlayer().getTile().distanceTo(new Tile(3111, 9526, 0, 0, 0, 30)) <= 1)) {
			Time.sleepUntil(() -> Client.getLocalPlayer().getTile().distanceTo(loc) > 10, 6000, 8000);
		}
	}

}
