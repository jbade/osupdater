package org.iinc.osbot.scripts.tutorial.combat;

import java.util.logging.Level;

import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class UnequippingItems implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Unequipping items.", 421, 1);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		Inventory.open();
		Clicking.interact(Inventory.getFirst(1277), "Wield");
		Clicking.interact(Inventory.getFirst(1171), "Wield");
	}

}
