package org.iinc.osbot.scripts.tutorial.combat;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class ThisIsYourCombatInterface implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("This is your combat interface.", 421, 1);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		if (Interacting.interact(new Tile(3111, 9518, 0, -55, 64, 100), "Open")
				&& Time.sleepUntil(() -> Walking.isMoving()
						|| Client.getLocalPlayer().getTile().distanceTo(new Tile(3111, 9518, 0, -64, 0, 20)) <= 1)) {
			Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
		}
	}

}
