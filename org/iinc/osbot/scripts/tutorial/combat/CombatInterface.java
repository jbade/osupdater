package org.iinc.osbot.scripts.tutorial.combat;

import java.util.logging.Level;

import org.iinc.osbot.api.base.interfaces.tabs.CombatOptions;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class CombatInterface implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Combat interface.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		CombatOptions.open();
	}

}
