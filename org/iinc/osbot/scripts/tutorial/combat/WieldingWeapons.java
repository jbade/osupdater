package org.iinc.osbot.scripts.tutorial.combat;

import java.util.logging.Level;

import org.iinc.osbot.api.base.interfaces.tabs.Equipment;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class WieldingWeapons implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Wielding weapons.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		Equipment.open();
	}

}
