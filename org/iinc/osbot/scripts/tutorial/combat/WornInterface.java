package org.iinc.osbot.scripts.tutorial.combat;

import java.awt.Rectangle;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class WornInterface implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Worn interface", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		Inventory.open();
		
		Rectangle bounds = Inventory.getBounds(Inventory.getFirst(1205).getIndex());
		Mouse.move(bounds);
		Mouse.sleepBeforeClick();
		Menu.interact("Wield", null); // equip if when open equipment interfaces
	}
}