package org.iinc.osbot.scripts.tutorial.combat;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.interfaces.tabs.Equipment;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class WellDoneYouveMadeYourFirstKill implements Node {

	Area cage = new Area(new Tile(3106, 9524, 0), new Tile(3107, 9523, 0), new Tile(3109, 9523, 0),
			new Tile(3110, 9522, 0), new Tile(3111, 9519, 0), new Tile(3111, 9518, 0), new Tile(3110, 9515, 0),
			new Tile(3107, 9512, 0), new Tile(3104, 9509, 0), new Tile(3090, 9509, 0), new Tile(3090, 9526, 0));

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Well done, you've made your first kill!", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		// sometimes chat doesnt change
		if (Inventory.contains(882, 841) || Equipment.getArrow() != -1){ // bow/arrows
			new RatRanging().execute();
			return;
		}
		
		if (cage.contains(Client.getLocalPlayer().getTile())) {
			if (Interacting.interact(new Tile(3111, 9518, 0, -55, 64, 100), "Open")
					&& Time.sleepUntil(() -> Walking.isMoving()
							|| Client.getLocalPlayer().getTile().distanceTo(new Tile(3111, 9518, 0, -64, 0, 20)) <= 1)) {
				Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
			}
		} else {
			if (Interacting.interact(() -> Npcs.getClosest("Combat Instructor"), new Tile(3106, 9508, 0), "Talk-to")){
				Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting() && !Walking.isMoving());
			}
		}

	}

}
