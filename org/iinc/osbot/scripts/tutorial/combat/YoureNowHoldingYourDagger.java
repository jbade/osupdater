package org.iinc.osbot.scripts.tutorial.combat;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class YoureNowHoldingYourDagger implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("You're now holding your dagger.", 421, 1);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		if (Widgets.exists(84, 4)){
			Clicking.interact(Widgets.getWidget(84, 4), "Close");
		} else {
			if (Interacting.interact(() -> Npcs.getClosest("Combat Instructor"), new Tile(3106, 9508, 0), "Talk-to")){
				Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting() && !Walking.isMoving());
			}
		}
	}

}
