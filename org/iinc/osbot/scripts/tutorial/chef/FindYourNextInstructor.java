package org.iinc.osbot.scripts.tutorial.chef;

import java.util.Arrays;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class FindYourNextInstructor implements Node {

	Tile[] inside = new Tile[] { new Tile(3078, 3084, 0), new Tile(3078, 3085, 0), new Tile(3078, 3083, 0),
			new Tile(3077, 3083, 0), new Tile(3077, 3084, 0), new Tile(3077, 3085, 0), new Tile(3076, 3082, 0),
			new Tile(3076, 3083, 0), new Tile(3076, 3084, 0), new Tile(3076, 3085, 0), new Tile(3075, 3082, 0),
			new Tile(3075, 3083, 0), new Tile(3075, 3084, 0), new Tile(3075, 3085, 0), new Tile(3075, 3086, 0),
			new Tile(3075, 3087, 0), new Tile(3075, 3088, 0), new Tile(3075, 3089, 0), new Tile(3075, 3090, 0),
			new Tile(3075, 3091, 0), new Tile(3076, 3090, 0), new Tile(3074, 3085, 0), new Tile(3074, 3086, 0),
			new Tile(3074, 3087, 0), new Tile(3074, 3088, 0), new Tile(3074, 3089, 0), new Tile(3074, 3090, 0),
			new Tile(3074, 3091, 0), new Tile(3073, 3089, 0), new Tile(3073, 3090, 0), };

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Find your next instructor.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());

		if (!Arrays.asList(inside).contains(Client.getLocalPlayer().getTile())) {
			if (Interacting.interact(new Tile(3078, 3084, 0, 75, 0, 120), "Open")
					&& Time.sleepUntil(() -> Walking.isMoving())) {
				Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
			}
		} else {
			if (Interacting.interact(() -> Npcs.getClosest("Master Chef"), new Tile(3103, 3095, 0), "Talk-to")) {
				Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting() && !Walking.isMoving());
			}
		}
	}

}
