package org.iinc.osbot.scripts.tutorial.chef;

import java.util.logging.Level;

import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class Running implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Running.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());

		Widget w = Widgets.getWidget(548, 34);
		if (w != null && w.getTextureId() == -1) {
			if (Clicking.interact(w))
				Time.sleepUntil(() -> w.getTextureId() != -1, 600, 1200);
		}

		Walking.enableRun();
	}

}
