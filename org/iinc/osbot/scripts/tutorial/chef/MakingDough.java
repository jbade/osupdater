package org.iinc.osbot.scripts.tutorial.chef;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;
import org.iinc.osbot.util.Random;

public class MakingDough implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Making dough.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());

		int count = Inventory.getCount();

		if (Inventory.isItemSelected()) {
			Menu.interact("Cancel");
		}

		if (Random.roll(0.5)) {
			if (Clicking.interact(Inventory.getFirst(1929), "Use"))
				if (Clicking.interact(Inventory.getFirst(2516), "Use"))
					Time.sleepUntil(() -> count != Inventory.getCount(), 1200, 3000);
		} else {
			if (Clicking.interact(Inventory.getFirst(2516), "Use"))
				if (Clicking.interact(Inventory.getFirst(1929), "Use"))
					Time.sleepUntil(() -> count != Inventory.getCount(), 1200, 3000);
		}

	}

}
