package org.iinc.osbot.scripts.tutorial.chef;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class TheMusicPlayer implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("The music player.", 421, 1);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());

		if (Interacting.interact(new Tile(3072, 3090, 0, 55, 0, 110), "Open")
				&& Time.sleepUntil(() -> Walking.isMoving())) {
			Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
		}
	}
}
