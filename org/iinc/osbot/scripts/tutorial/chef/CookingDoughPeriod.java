package org.iinc.osbot.scripts.tutorial.chef;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.item.RSItem;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class CookingDoughPeriod implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Cooking dough.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());

		if (Interacting.useItem(() -> RSObjects.getGameObjects(15).parallelStream().filter((go) -> go.getObjectId() == 9736).findFirst()
				.orElse(null), new Tile(3079, 9497, 0), new RSItem("Bread dough", 2307),
				"Range")) {
			Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != -1, 1200, 2000);
			Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == -1, 3000, 4000);
		}
	}
}