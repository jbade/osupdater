package org.iinc.osbot.scripts.tutorial.chef;

import java.awt.Rectangle;
import java.util.logging.Level;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class Emotes2 implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Emotes.", 421, 1);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());

		if (Widgets.getWidget(548, 35).getTextureId() == -1) {
			Clicking.interact(Widgets.getWidget(548, 35));
			if (!Time.sleepUntil(() -> Widgets.getWidget(548, 35).getTextureId() != -1, 600, 1000))
				return;
		}

		Mouse.move(new Rectangle(547, 205, 190 - 18, 261));
		Mouse.click(Mouse.LEFT_BUTTON);
		Time.sleep(1200, 3200);
	}

}
