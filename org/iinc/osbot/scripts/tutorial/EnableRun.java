package org.iinc.osbot.scripts.tutorial;

import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class EnableRun implements org.iinc.osbot.script.Node {

	@Override
	public boolean activate() {
		return !Walking.isRunEnabled() && Walking.getRunEnergy() > Random.modifiedRandomGuassianRange(10, 40,
				ConfidenceIntervals.CI_70, 20, Modifiers.get("TutorialIsland.run")) && Random.roll(0.3);
	}

	@Override
	public void execute() {
		Walking.enableRun();
	}

}
