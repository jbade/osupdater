package org.iinc.osbot.scripts.tutorial;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class TutorialHelper {

	public static boolean checkWidgetText(String text, int... ids) {
		Widget w = Widgets.getWidget(ids);
		return w != null && w.getText() != null && w.getText().equals(text);
	}

	public static boolean walkPrecise(Tile tile, int maxDist) {

		if (Client.getLocalPlayer().getTile().distanceTo(tile) <= maxDist
				&& (Walking.getDestinationTile() == null || (Walking.getDestinationTile() != null
						&& Walking.getDestinationTile().distanceTo(new Tile(3112, 9518, 0)) <= maxDist))) {
			return true;
		}
		if (Random.modifiedRandomRange(0, 100, 60, Modifiers.get("TutorialIsland.camera1")) > 70
				|| !Calculations.onViewport(tile.toScreen())
						&& Random.modifiedRandomRange(0, 100, 60, Modifiers.get("TutorialIsland.camera2")) > 50) {
			Camera.turnTo(tile);
		}

		if (Calculations.onViewport(tile.toScreen())) {
			Mouse.move(tile.toMiniMap());
			Mouse.sleepBeforeClick();
			Mouse.click(Mouse.LEFT_BUTTON);
			Time.sleepUntil(() -> Walking.getDestinationTile() != null, 600, 800);
			Time.sleepUntil(() -> Walking.getDestinationTile() == null, 3000, 4000);
			return true;
		} else {
			Walking.walkPath(Walking.generatePath(Client.getLocalPlayer().getTile(), tile, 3), false);
		}
		
		return false;
	}

}
