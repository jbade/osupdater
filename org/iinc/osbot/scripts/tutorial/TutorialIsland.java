package org.iinc.osbot.scripts.tutorial;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.scripts.tutorial.banking.Banking;
import org.iinc.osbot.scripts.tutorial.banking.ContinueThroughTheNextDoor;
import org.iinc.osbot.scripts.tutorial.banking.FinancialAdvice;
import org.iinc.osbot.scripts.tutorial.banking.ThisIsAPollBooth;
import org.iinc.osbot.scripts.tutorial.banking.ThisIsYourBankBox;
import org.iinc.osbot.scripts.tutorial.chef.CookingDough;
import org.iinc.osbot.scripts.tutorial.chef.CookingDoughPeriod;
import org.iinc.osbot.scripts.tutorial.chef.Emotes1;
import org.iinc.osbot.scripts.tutorial.chef.Emotes2;
import org.iinc.osbot.scripts.tutorial.chef.FindYourNextInstructor;
import org.iinc.osbot.scripts.tutorial.chef.MakingDough;
import org.iinc.osbot.scripts.tutorial.chef.RunToTheNextGuide;
import org.iinc.osbot.scripts.tutorial.chef.Running;
import org.iinc.osbot.scripts.tutorial.chef.TheMusicPlayer;
import org.iinc.osbot.scripts.tutorial.combat.Attacking;
import org.iinc.osbot.scripts.tutorial.combat.Combat;
import org.iinc.osbot.scripts.tutorial.combat.CombatInterface;
import org.iinc.osbot.scripts.tutorial.combat.RatRanging;
import org.iinc.osbot.scripts.tutorial.combat.SitBackAndWatch;
import org.iinc.osbot.scripts.tutorial.combat.ThisIsYourCombatInterface;
import org.iinc.osbot.scripts.tutorial.combat.ThisIsYourWornInventory;
import org.iinc.osbot.scripts.tutorial.combat.UnequippingItems;
import org.iinc.osbot.scripts.tutorial.combat.WellDoneYouveMadeYourFirstKill;
import org.iinc.osbot.scripts.tutorial.combat.WieldingWeapons;
import org.iinc.osbot.scripts.tutorial.combat.WornInterface;
import org.iinc.osbot.scripts.tutorial.combat.YoureNowHoldingYourDagger;
import org.iinc.osbot.scripts.tutorial.magic.CastWindStrikeAtAChicken;
import org.iinc.osbot.scripts.tutorial.magic.OpenUpYourFinalMenu;
import org.iinc.osbot.scripts.tutorial.magic.ThisIsYourSpellsList;
import org.iinc.osbot.scripts.tutorial.magic.YouHaveAlmostCompletedTheTutorial;
import org.iinc.osbot.scripts.tutorial.mining.ItsCopper;
import org.iinc.osbot.scripts.tutorial.mining.ItsTin;
import org.iinc.osbot.scripts.tutorial.mining.Mining;
import org.iinc.osbot.scripts.tutorial.mining.MiningAndSmithing;
import org.iinc.osbot.scripts.tutorial.mining.Prospecting;
import org.iinc.osbot.scripts.tutorial.mining.Smelting;
import org.iinc.osbot.scripts.tutorial.mining.SmithingADagger;
import org.iinc.osbot.scripts.tutorial.mining.YouveFinishedInThisArea;
import org.iinc.osbot.scripts.tutorial.mining.YouveMadeABarOfBronze;
import org.iinc.osbot.scripts.tutorial.prayer.FriendsList;
import org.iinc.osbot.scripts.tutorial.prayer.Prayer;
import org.iinc.osbot.scripts.tutorial.prayer.ThisIsYourFriendsList;
import org.iinc.osbot.scripts.tutorial.prayer.ThisIsYourIgnoreList;
import org.iinc.osbot.scripts.tutorial.prayer.YourPrayerMenu1;
import org.iinc.osbot.scripts.tutorial.prayer.YourPrayerMenu2;
import org.iinc.osbot.scripts.tutorial.quest.MovingOn;
import org.iinc.osbot.scripts.tutorial.quest.OpenTheQuestJournal;
import org.iinc.osbot.scripts.tutorial.quest.TalkWithTheQuestGuide;
import org.iinc.osbot.scripts.tutorial.quest.YourQuestJournal;
import org.iinc.osbot.scripts.tutorial.start.GettingStarted;
import org.iinc.osbot.scripts.tutorial.start.InteractingWithScenery;
import org.iinc.osbot.scripts.tutorial.start.PlayerControls1;
import org.iinc.osbot.scripts.tutorial.start.PlayerControls2;
import org.iinc.osbot.scripts.tutorial.start.PlayerDesign;
import org.iinc.osbot.scripts.tutorial.survival.CatchSomeShrimp;
import org.iinc.osbot.scripts.tutorial.survival.CookingBurningYourShrimp;
import org.iinc.osbot.scripts.tutorial.survival.CutDownATree;
import org.iinc.osbot.scripts.tutorial.survival.MakingAFire;
import org.iinc.osbot.scripts.tutorial.survival.MovingAround;
import org.iinc.osbot.scripts.tutorial.survival.ViewingTheItems;
import org.iinc.osbot.scripts.tutorial.survival.WellDone;
import org.iinc.osbot.scripts.tutorial.survival.YouGainedSomeExperience;
import org.iinc.osbot.scripts.tutorial.survival.YourSkillStats;


public class TutorialIsland implements org.iinc.osbot.script.Node {

	private List<Node> nodes = new LinkedList<Node>();

	{
		Collections.addAll(nodes, 
				new ClickContinue(), 
				new PlayerDesign(), 
				new CameraNode(), 
				new InventoryHandler(), 
				new GettingStarted(), 
				new PlayerControls1(),
				new EnableRun(),
				new PlayerControls2(),
				new InteractingWithScenery(), 
				new MovingAround(), 
				new ViewingTheItems(),
				new CutDownATree(), 
				new MakingAFire(), 
				new YouGainedSomeExperience(), 
				new YourSkillStats(),
				new CatchSomeShrimp(), 
				new CookingBurningYourShrimp(), 
				new WellDone(), 
				new FindYourNextInstructor(),
				new MakingDough(), 
				new CookingDoughPeriod(), 
				new CookingDough(), 
				new TheMusicPlayer(), 
				new Emotes1(),
				new Emotes2(), 
				new Running(), 
				new RunToTheNextGuide(), 
				new TalkWithTheQuestGuide(),
				new OpenTheQuestJournal(), 
				new YourQuestJournal(), 
				new MovingOn(), 
				new MiningAndSmithing(),
				new Prospecting(), 
				new ItsTin(), 
				new ItsCopper(), 
				new Mining(), 
				new Smelting(),
				new YouveMadeABarOfBronze(), 
				new SmithingADagger(), 
				new YouveFinishedInThisArea(), 
				new Combat(),
				new WieldingWeapons(), 
				new ThisIsYourWornInventory(), 
				new WornInterface(),
				new YoureNowHoldingYourDagger(), 
				new UnequippingItems(), 
				new CombatInterface(),
				new ThisIsYourCombatInterface(), 
				new Attacking(), 
				new SitBackAndWatch(),
				new WellDoneYouveMadeYourFirstKill(),
				new RatRanging(), 
				new org.iinc.osbot.scripts.tutorial.combat.MovingOn(), 
				new Banking(), 
				new ThisIsYourBankBox(),
				new ThisIsAPollBooth(), 
				new FinancialAdvice(), 
				new ContinueThroughTheNextDoor(), 
				new Prayer(),
				new YourPrayerMenu1(), 
				new YourPrayerMenu2(), 
				new FriendsList(), 
				new ThisIsYourFriendsList(),
				new ThisIsYourIgnoreList(), 
				new org.iinc.osbot.scripts.tutorial.prayer.YourFinalInstructor(),
				new org.iinc.osbot.scripts.tutorial.magic.YourFinalInstructor(),
				new OpenUpYourFinalMenu(), 
				new ThisIsYourSpellsList(),
				new CastWindStrikeAtAChicken(), 
				new YouHaveAlmostCompletedTheTutorial()
				);
	}

	@Override
	public boolean activate() {
		return Widgets.exists(371, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		
		for (Node node : nodes) {
			if (node.activate()) {
				System.out.println("running");
				node.execute();
				System.out.println("done");
				return;
			}
		}

	}

}
