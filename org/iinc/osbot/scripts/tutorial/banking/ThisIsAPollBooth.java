package org.iinc.osbot.scripts.tutorial.banking;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class ThisIsAPollBooth implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("This is a poll booth.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());

		Widget w = Widgets.getWidget(310, 2, 11);
		if (w != null && !w.isHidden()) {
			if (Clicking.interact(w))
				Time.sleepUntil(() -> !Widgets.exists(310, 2, 11), 600, 1200);
			return;
		}

		if (Interacting.interact(new Tile(3124, 3124, 0, 74, 0, 100), "Open")
				&& Time.sleepUntil(() -> Walking.isMoving() || Client.getLocalPlayer().getTile().distanceTo(new Tile(3124, 3124, 0, 64, 0, 50)) <= 1)) {
			Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
		}
	}

}
