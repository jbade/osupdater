package org.iinc.osbot.scripts.tutorial.banking;

import java.awt.event.KeyEvent;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Keyboard;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class Banking implements Node {
	private static final Area bank = new Area(new Tile(3118, 3119, 0),
		    new Tile(3118, 3120, 0),
		    new Tile(3119, 3120, 0),
		    new Tile(3119, 3123, 0),
		    new Tile(3118, 3123, 0),
		    new Tile(3118, 3125, 0),
		    new Tile(3124, 3125, 0),
		    new Tile(3124, 3120, 0),
		    new Tile(3125, 3120, 0),
		    new Tile(3125, 3119, 0));
	
    
	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Banking.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());

		if (Widgets.exists(219, 0, 1)) {
			if (Random.modifiedRandomRange(0, 100, 50, Modifiers.get("TutorialIsland.bankKey")) > 50) {
				Keyboard.sendKey(KeyEvent.VK_1);
			} else {
				Clicking.interact(Widgets.getWidget(219, 0, 1));
			}
			Time.sleepUntil(() -> !Widgets.exists(219, 0, 1), 1200, 3000);
			return;
		}
		
		if (!bank.contains(Client.getLocalPlayer().getTile())) {
			if (RSObjects.getBoundaryObject(new Tile(3121, 3118, 0)) == null) {
				Walking.walkPath(Walking.generatePath(Client.getLocalPlayer().getTile(), new Tile(3122, 3124, 0), 3),
						true);
			} else {
				if (Interacting.interact(new Tile(3121, 3118, 0, 64, 45, 120), "Open")
						&& Time.sleepUntil(() -> Walking.isMoving() || Client.getLocalPlayer().getTile().distanceTo(new Tile(3121, 3118, 0)) <= 1)) {
					Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
				}
			}
		} else {
			if (Interacting.interact(new Tile(3122, 3124, 0), "Use")
					&& Time.sleepUntil(() -> Walking.isMoving() || Client.getLocalPlayer().getTile().distanceTo(new Tile(3122, 3124, 0)) <= 1)) {
				Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
			}
		}
	}

}
