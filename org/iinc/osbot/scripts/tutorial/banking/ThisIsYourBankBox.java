package org.iinc.osbot.scripts.tutorial.banking;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class ThisIsYourBankBox implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("This is your bank box.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());

		Widget w = Widgets.getWidget(12, 3, 11);
		if (w != null && !w.isHidden()) {
			if (Clicking.interact(w))
				Time.sleepUntil(() -> !Widgets.exists(12, 3, 11), 600, 1200);
			return;
		}

		if (Interacting.interact(new Tile(3119, 3121, 0), "Use")
				&& Time.sleepUntil(() -> Walking.isMoving() || Client.getLocalPlayer().getTile().distanceTo(new Tile(3119, 3121, 0)) <= 1)) {
			Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
		}
	}

}
