package org.iinc.osbot.scripts.tutorial;

import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class CameraNode implements org.iinc.osbot.script.Node {

	@Override
	public boolean activate() {

		return Camera.getCameraPitch() < Random.modifiedRandomGuassianRange(225, 375, ConfidenceIntervals.CI_70, 80,
				Modifiers.get("TutorialIsland.camera"));
	}

	@Override
	public void execute() {
		Camera.setAngle(Camera.getCameraAngle(), true);
	}

}
