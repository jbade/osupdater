package org.iinc.osbot.scripts.tutorial.start;

import java.util.logging.Level;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class PlayerControls1 implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Player controls", 372, 1);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		// open settings
		if (Clicking.interact(Widgets.getWidget(548, 34))){
			Time.sleepUntil(() -> Widgets.getWidget(548, 34).getTextureId() != -1);
		}
		 
	}
}
