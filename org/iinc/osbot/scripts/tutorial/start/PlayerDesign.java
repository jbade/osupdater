package org.iinc.osbot.scripts.tutorial.start;

import java.util.logging.Level;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Random;

public class PlayerDesign implements Node {

	private boolean randomizedAppearance = false;

	private static final int[] PLAYER_DESIGN_ACCEPT_WIDGET = new int[] { 269, 99 };
	private static final int[][][] PLAYER_DESIGN_APPEARANCE_WIDGETS = { { { 269, 106 }, { 269, 113 } }, // head
			{ { 269, 107 }, { 269, 114 } }, // jaw
			{ { 269, 108 }, { 269, 115 } }, // torso
			{ { 269, 109 }, { 269, 116 } }, // arms
			{ { 269, 110 }, { 269, 117 } }, // hands
			{ { 269, 111 }, { 269, 118 } }, // legs
			{ { 269, 112 }, { 269, 119 } }, // feet
			{ { 269, 105 }, { 269, 121 } }, // hair
			{ { 269, 123 }, { 269, 127 } }, // torso
			{ { 269, 122 }, { 269, 129 } }, // legs
			{ { 269, 124 }, { 269, 130 } }, // feet
			{ { 269, 125 }, { 269, 131 } }, // skin
			// {{269, 138}, {269, 139}}, // gender

	};

	@Override
	public boolean activate() {
		return Widgets.exists(PLAYER_DESIGN_ACCEPT_WIDGET);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		if (!randomizedAppearance) {
			randomizedAppearance = true;

			while (Random.roll(1.0 - 1.0 / PLAYER_DESIGN_APPEARANCE_WIDGETS.length / 4.0)) {
				int[][] wa = PLAYER_DESIGN_APPEARANCE_WIDGETS[new java.util.Random()
						.nextInt(PLAYER_DESIGN_APPEARANCE_WIDGETS.length)];
				int[] w = Random.roll(.3) ? wa[0] : wa[1];
				int times = Random.randomGuassianRange(0, 6, ConfidenceIntervals.CI_70);
				for (int i = 1; i <= times; i++) {
					if (Clicking.interact(Widgets.getWidget(w)))
						Time.sleepReactionTime();
				}
			}

		}

		if (Clicking.interact(Widgets.getWidget(PLAYER_DESIGN_ACCEPT_WIDGET)))
			Time.sleepUntil(() -> !Widgets.exists(PLAYER_DESIGN_ACCEPT_WIDGET));
	}

}
