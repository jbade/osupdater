package org.iinc.osbot.scripts.tutorial.start;

import java.awt.event.KeyEvent;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Keyboard;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class GettingStarted implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Getting started", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		if (Widgets.exists(219, 0, 1)) {
			if (Random.modifiedRandomRange(0, 100, 50, Modifiers.get("TutorialIsland.GettingStartedKey")) > 50) {
				Keyboard.sendKey(KeyEvent.VK_1);
			} else {
				Clicking.interact(Widgets.getWidget(219, 0, 1));
			}
			Time.sleep(600, 1200);
			return;
		}

		if (Interacting.interact(() -> Npcs.getClosest("RuneScape Guide"), null, "Talk-to")){
			Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting() && !Walking.isMoving());
		}
	}

}
