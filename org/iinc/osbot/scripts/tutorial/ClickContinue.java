package org.iinc.osbot.scripts.tutorial;

import java.awt.event.KeyEvent;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Keyboard;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class ClickContinue implements Node {

	int[][] spaceWidgets = new int[][] { { 231, 2 }, { 217, 2 }, { 229, 1 }, { 193, 1 }, { 193, 2 } };
	int[][] clickWidgets = new int[][] { { 162, 34 } };
	boolean space = false;
	int[] widget;

	// Widget widget;

	@Override
	public boolean activate() {
		for (int[] c : clickWidgets) {
			Widget w = Widgets.getWidget(c);
			if (w != null && !w.isHidden()) {
				String text = w.getText();
				if (text != null && (text.equals("Click here to continue") || text.equals("Click to continue"))) {
					widget = c;
					space = false;
					return true;
				}
			}
		}

		for (int[] c : spaceWidgets) {
			Widget w = Widgets.getWidget(c);
			if (w != null && !w.isHidden()) {
				String text = w.getText();
				if (text != null && (text.equals("Click here to continue") || text.equals("Click to continue"))) {
					widget = c;
					space = true;
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, "Clicking continue");

		Widget w = Widgets.getWidget(widget);
		if (w != null && !w.isHidden()) {
			Time.sleepReactionTime();
			if (!space || Random.modifiedRandomGuassianRange(0, 100, ConfidenceIntervals.CI_70, 50,
					Modifiers.get("TutorialIsland.continueClick")) < 50) {
				if (!Menu.interact("Continue"))
					Clicking.interact(Widgets.getWidget(widget));
			} else {
				Keyboard.sendKey(KeyEvent.VK_SPACE);
			}

			final Widget q = w;
			if (Time.sleepUntil(() -> q.getText() != null && q.getText().contains("Please wait..."), 300, 500)) {
				Time.sleepUntil(() -> {
					Widget e = Widgets.getWidget(widget);
					if (e != null && !e.isHidden()) {
						String text = e.getText();
						if (text != null && text.contains("lease")) {
							return false;
						}
					}
					return true;
				}, 1200, 2400);
			}
			
			Time.sleep(400, 700);

		}
	}

}
