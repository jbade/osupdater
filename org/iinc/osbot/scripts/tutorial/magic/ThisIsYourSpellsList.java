package org.iinc.osbot.scripts.tutorial.magic;

import java.util.Arrays;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class ThisIsYourSpellsList implements Node {

	Tile[] house = new Tile[]{
		    new Tile(3138, 3082, 0),
		    new Tile(3139, 3082, 0),
		    new Tile(3140, 3082, 0),
		    new Tile(3141, 3083, 0),
		    new Tile(3140, 3083, 0),
		    new Tile(3139, 3083, 0),
		    new Tile(3140, 3084, 0),
		    new Tile(3141, 3084, 0),
		    new Tile(3142, 3084, 0),
		    new Tile(3142, 3085, 0),
		    new Tile(3141, 3085, 0),
		    new Tile(3140, 3085, 0),
		    new Tile(3140, 3086, 0),
		    new Tile(3140, 3087, 0),
		    new Tile(3141, 3086, 0),
		    new Tile(3141, 3087, 0),
		    new Tile(3140, 3088, 0),
		    new Tile(3141, 3088, 0),
		    new Tile(3142, 3088, 0),
		    new Tile(3142, 3089, 0),
		    new Tile(3141, 3089, 0),
		    new Tile(3140, 3089, 0),
		    new Tile(3139, 3090, 0),
		    new Tile(3140, 3090, 0),
		    new Tile(3141, 3090, 0),
		    new Tile(3140, 3091, 0),
		    new Tile(3139, 3091, 0),
		    new Tile(3138, 3091, 0)
		};

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("This is your spells list.", 372, 1);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		if (Widgets.exists(219, 0, 1)) {
			Clicking.interact(Widgets.getWidget(219,0, 1));
			return;
		}

		if (!Arrays.asList(house).contains(Client.getLocalPlayer().getTile())) {
			Walking.walkPath(Walking.generatePath(Client.getLocalPlayer().getTile(), new Tile(3141, 3087, 0), 3),
					false);
			Time.sleepUntil(() -> Walking.getDestinationTile() == null || !Walking.isMoving(), 4000, 6000);
		} else {
			if (Interacting.interact(() -> Npcs.getClosest("Magic Instructor"), new Tile(3125, 3107, 0), "Talk-to")){
				Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting() && !Walking.isMoving());
			}
		}
	}

}
