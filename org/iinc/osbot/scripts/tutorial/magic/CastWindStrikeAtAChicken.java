package org.iinc.osbot.scripts.tutorial.magic;

import java.util.Optional;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.Magic;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.renderable.Players;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class CastWindStrikeAtAChicken implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Cast Wind Strike at a chicken.", 421, 1);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		if (TutorialHelper.walkPrecise(new Tile(3139, 3091, 0), 1)) {
			if (Client.isSpellSelected() || Magic.cast(Magic.SPELL.WIND_STRIKE)) {
				Tile loc = Client.getLocalPlayer().getTile();
				Optional<Npc> filtered = Npcs.getAll()
						.parallelStream().filter((npc) -> npc.getNpcDefinition().getName().equals("Chicken")
								&& npc.getAnimation() == -1 && !npc.isBeingInteractedWith(Players.getAll()))
						.sorted((a, b) -> {
							double dist = loc.distanceTo(a.getTile()) - loc.distanceTo(b.getTile());
							if (dist > 0)
								return 1;
							if (dist < 0)
								return -1;
							return 0;
						}).findFirst();
				if (filtered.isPresent()) {
					Npc npc = filtered.get();
					if (Clicking.interact(npc, "Cast", "Wind Strike -> Chicken  (level-3)")) {
						Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting(), 1200, 1400);
						Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == -1, 3000, 4000);
					} else {
						if (Client.isSpellSelected())
							Menu.interact("Cancel");
					}
				}
			}
		}
	}
}