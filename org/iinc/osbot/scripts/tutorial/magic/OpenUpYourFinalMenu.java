package org.iinc.osbot.scripts.tutorial.magic;

import java.util.logging.Level;

import org.iinc.osbot.api.base.interfaces.tabs.Magic;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class OpenUpYourFinalMenu implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Open up your final menu.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		Magic.open();
	}

}