package org.iinc.osbot.scripts.tutorial;

import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class TutorialIslandScript extends Script {

	TutorialIsland ti = new TutorialIsland();
	
	@Override
	public int loop() {
		if (!Login.isLoggedIn()){
			Login.login();
			return 0;
		}		
		
		Interfaces.closeAll();
		
		if (ti.activate())
			ti.execute();
		//Time.sleepReactionTime();
		return Random.randomRange(50, 100);
	}

	@Override
	public void onStop() {
		
	}

	@Override
	public void onStart() {
		Modifiers.init(Bot.username.hashCode());
		BreakHandler.init(Bot.username.hashCode());
	}

}
