package org.iinc.osbot.scripts.tutorial.prayer;

import java.util.logging.Level;

import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class YourPrayerMenu2 implements Node {

	Prayer p = new Prayer();

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Your Prayer menu.", 372, 1);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		p.execute();
	}

}
