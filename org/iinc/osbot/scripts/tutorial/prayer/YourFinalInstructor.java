package org.iinc.osbot.scripts.tutorial.prayer;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class YourFinalInstructor implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Your final instructor!", 372, 1);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		if (Interacting.interact(new Tile(3122, 3103, 0, 0, -64,  50), "Open")
				&& Time.sleepUntil(() -> Walking.isMoving() || Client.getLocalPlayer().getTile().distanceTo(new Tile(3122, 3103, 0, 0, -64,  50)) <= 1)) {
			Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
		}
	}
}