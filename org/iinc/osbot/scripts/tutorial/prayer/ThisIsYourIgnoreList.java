package org.iinc.osbot.scripts.tutorial.prayer;

import java.util.logging.Level;

import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class ThisIsYourIgnoreList implements Node {

	Prayer p = new Prayer();
	
	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("This is your ignore list.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		p.execute();
	}

}
