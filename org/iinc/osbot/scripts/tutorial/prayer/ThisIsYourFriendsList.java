package org.iinc.osbot.scripts.tutorial.prayer;

import java.util.logging.Level;

import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class ThisIsYourFriendsList implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("This is your friends list.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		Clicking.interact(Widgets.getWidget(548, 39));
	}

}
