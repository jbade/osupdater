package org.iinc.osbot.scripts.tutorial.prayer;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class Prayer implements Node {
	static final Area church = new Area(new Tile(3120, 3103, 0), new Tile(3120, 3110, 0),
			new Tile(3128, 3110, 0), new Tile(3128, 3103, 0));

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Prayer.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		if (!church.contains(Client.getLocalPlayer().getTile())) {
			if (RSObjects.getBoundaryObject(new Tile(3129, 3107, 0)) == null) {
				Walking.walkPath(Walking.generatePath(Client.getLocalPlayer().getTile(), new Tile(3125, 3107, 0), 3),
						true);
			} else {
				if (Interacting.interact(new Tile(3129, 3107, 0, -64, -64, 60), "Open")
						&& Time.sleepUntil(() -> Walking.isMoving() || Client.getLocalPlayer().getTile().distanceTo(new Tile(3129, 3107, 0)) <= 1)) {
					Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
				}
			}
		} else {
			if (Interacting.interact(() -> Npcs.getClosest("Brother Brace"), new Tile(3125, 3107, 0), "Talk-to")){
				Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting() && !Walking.isMoving());
			}
		}

	}

}