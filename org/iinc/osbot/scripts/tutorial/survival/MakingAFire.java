package org.iinc.osbot.scripts.tutorial.survival;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class MakingAFire implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Making a fire", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());

		// if (Inventory.getCount("Logs") < 3 &&
		// AdditionalTraining.trainWoodCutting()) { // logs
		// return;
		// }

		if (!Inventory.contains(2511)) { // logs
			new CutDownATree().execute();
			return;
		}

		int tries = 0;

		while (!canMakeFire(Client.getLocalPlayer().getTile()) && tries++ < 3) {
			Tile t = getClosestEmptyTile();
			if (t == null)
				return;

			if (Calculations.onViewport(t.toScreen())) {
				Clicking.interact(t, "Walk here");
			} else {
				Walking.walk(t);
			}
		}

		if (canMakeFire(Client.getLocalPlayer().getTile())) {

			// select TinderBox/Logs

			int slot1 = Inventory.getFirst(590).getIndex();
			int slot2 = Inventory.getFirst(2511).getIndex();

			Inventory.open();

			if (Inventory.isItemSelected()) {
				Menu.interact("Cancel");
			}

			Clicking.hover(Inventory.getBounds(slot1));

			if (Menu.interact("Use")) {
				Inventory.open();
				Clicking.hover(Inventory.getBounds(slot2));
				if (Inventory.isItemSelected()) {
					Menu.interact("Use");
					Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != -1, 1200, 2000);
					Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == -1, 1200, 2000);
				}
			}

		}

	}

	public Tile getClosestEmptyTile() {
		List<Tile> tiles = new ArrayList<Tile>();
		int plane = Client.getPlane();
		for (int x = 3101; x <= 3104; x++) {
			for (int y = 3094; y <= 3103; y++) {
				if (canMakeFire(new Tile(x, y, plane))) {
					tiles.add(new Tile(x, y, plane));
				}
			}
		}

		Tile loc = Client.getLocalPlayer().getTile();
		return tiles.parallelStream().sorted((a, b) -> {
			double d = loc.distanceTo(a) - loc.distanceTo(b);
			if (d > 0)
				return 1;
			else if (d < 0)
				return -1;
			else
				return 0;
		}).findFirst().orElse(null);
	}

	public boolean canMakeFire(Tile t) {
		if (t.getX() < 3101 || t.getX() > 3104 || t.getY() < 3094 || t.getY() > 3103)
			return false;
		return RSObjects.getGameObjects(t).isEmpty();
	}

}
