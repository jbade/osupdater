package org.iinc.osbot.scripts.tutorial.survival;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.RSItem;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class CookingBurningYourShrimp implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Cooking your shrimp.", 372, 0)
				|| TutorialHelper.checkWidgetText("Burning your shrimp.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());

		// if (AdditionalTraining.trainFishing()) {
		// return;
		// }

		if (!Inventory.contains(2514)) { // Raw shrimps
			new CatchSomeShrimp().execute();
			return;
		}
		
		if (RSObjects.getGameObjects(15).parallelStream().filter((go) -> go.getObjectId() == 26185).findAny().isPresent()) {
			if (Interacting.useItem(
					() -> RSObjects.getGameObjects(15).parallelStream().filter((go) -> go.getObjectId() == 26185).sorted()
							.findFirst().orElse(null),
					new Tile(3083, 9498, 0), new RSItem("Raw shrimps", 2514), "Fire")) {
				Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != -1 || Walking.isMoving(), 1200, 2000);
				Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == -1 && !Walking.isMoving(), 6000, 8000);
			}
		} else {
			new MakingAFire().execute();
		}

	}

}
