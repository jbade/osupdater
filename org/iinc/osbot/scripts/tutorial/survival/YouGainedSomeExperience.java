package org.iinc.osbot.scripts.tutorial.survival;

import java.util.logging.Level;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class YouGainedSomeExperience implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("You gained some experience.", 372, 1);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		if (Clicking.interact(Widgets.getWidget(548, 48))){
			Time.sleepUntil(() -> Widgets.getWidget(548, 48).getTextureId() != -1);
		}
	}

}
