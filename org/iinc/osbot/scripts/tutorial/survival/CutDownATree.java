package org.iinc.osbot.scripts.tutorial.survival;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class CutDownATree implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Cut down a tree", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		if (Interacting
				.interact(() -> RSObjects.getGameObjects(15).parallelStream().filter((go) -> go.getObjectId() == 9730)
						.sorted().findFirst().orElse(null), new Tile(3103, 3095, 0), "Chop down", "Tree")
				&& Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != -1)) {
			Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == -1, 3000, 5000);
		}
	}

}
