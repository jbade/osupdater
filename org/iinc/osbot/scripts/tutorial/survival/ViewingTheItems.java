package org.iinc.osbot.scripts.tutorial.survival;

import java.util.logging.Level;

import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class ViewingTheItems implements Node
{

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Viewing the items that you were given.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		Widget widget = Widgets.getWidget(548, 50);
		if (widget != null && !widget.isHidden()) {	
			if (Clicking.interact(widget, "Inventory"))
				Time.sleepUntil(() -> Inventory.isOpen(), 1000, 1400);
		}

	}

}
