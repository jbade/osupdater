package org.iinc.osbot.scripts.tutorial.survival;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class WellDone implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Well done, you've just cooked your first RuneScape meal.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		// if (AdditionalTraining.trainCooking()){
		// return;
		// }
		
		if (Interacting.interact(new Tile(3089, 3092, 0, 64, -64, 65), "Open")
				&& Time.sleepUntil(() -> Walking.isMoving())) {
			Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
		}
	}

}
