package org.iinc.osbot.scripts.tutorial.mining;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class YouveMadeABarOfBronze implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("You've made a bronze bar!", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		if (Interacting.interact(() -> Npcs.getClosest("Mining Instructor"), new Tile(3081, 9504, 0), "Talk-to")){
			Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting() && !Walking.isMoving());
		}
	}

}