package org.iinc.osbot.scripts.tutorial.mining;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.RSItem;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class Smelting implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Smelting.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());

		// if (AdditionalTraining.trainMining()) {
		// return;
		// }

		if (!Inventory.contains(438) || !Inventory.contains(436)) {
			new Mining().execute();
			return;
		}

		if (Interacting.useItem(() -> RSObjects.getGameObjects(15).parallelStream().filter((go) -> go.getObjectId() == 10082)
				.sorted().findFirst().orElse(null), new Tile(3079, 9497, 0), new RSItem("Copper ore", 436),
				"Furnace")) {
			Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != -1, 1200, 2000);
			Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == -1, 1200, 2000);
		}
	}

}