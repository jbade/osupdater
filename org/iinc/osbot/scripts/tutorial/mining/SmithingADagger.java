package org.iinc.osbot.scripts.tutorial.mining;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.RSItem;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class SmithingADagger implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Smithing a dagger.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());

		if (!Inventory.contains(2349)) { // bronze bar
			new Smelting().execute();
			return;
		}

		if (Widgets.exists(312, 2)) {
			if (Clicking.interact(Widgets.getWidget(312, 2))) {
				Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != -1, 1200, 2000);
				Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == -1, 5000, 7000);
			}
			return;
		}

		if (Interacting
				.useItem(
						() -> RSObjects.getGameObjects(15).parallelStream().filter((go) -> go.getObjectId() == 2097).sorted()
								.findFirst().orElse(null),
						new Tile(3083, 9498, 0), new RSItem("Bronze bar", 2349), "Anvil")) {
			Time.sleepUntil(() -> Walking.isMoving(), 1200, 2000);
			Time.sleepUntil(() -> Widgets.exists(312, 2) || !Walking.isMoving(), 6000, 8000);

			if (Widgets.exists(312, 2)) {
				if (Clicking.interact(Widgets.getWidget(312, 2))) {
					Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != -1, 1200, 2000);
					Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == -1, 5000, 7000);
				}
				return;
			}
		}

	}

}