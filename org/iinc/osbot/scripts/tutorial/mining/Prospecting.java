package org.iinc.osbot.scripts.tutorial.mining;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class Prospecting implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Prospecting.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		if (Interacting.interact(
				() -> RSObjects.getGameObjects(15).parallelStream()
				.filter((go) -> go.getObjectId() == 10080).sorted().findFirst().orElse(null),
		new Tile(3077, 9505, 0), "Prospect")) {
			Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
		}
	}

}
