package org.iinc.osbot.scripts.tutorial.mining;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class YouveFinishedInThisArea implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("You've finished in this area.", 372, 0);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		//if (AdditionalTraining.trainSmithing()){
		//	return;
		//}
		
		
		if (Interacting.interact(new Tile(3094, 9502, 0, 50, 64, 100), "Open")) {
			Time.sleepUntil(() -> !Walking.isMoving(), 5000, 7000);
		}
	}

}
