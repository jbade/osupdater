package org.iinc.osbot.scripts.tutorial;

import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class InventoryHandler implements Node {

	int[] drop = new int[] { 2511, 2514 };

	@Override
	public boolean activate() {
		return Inventory.getCount() >= Random.modifiedRandomGuassianRange(25, 26, ConfidenceIntervals.CI_70, 2,
				Modifiers.get("TutorialIsland.drop")) && Inventory.contains(drop);
	}

	@Override
	public void execute() {
		if (Inventory.contains(drop)) {
			Clicking.interact(Inventory.getFirst(drop), "Drop");
		}
	}

}
