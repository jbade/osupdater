package org.iinc.osbot.scripts.tutorial.quest;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.TutorialHelper;

public class MovingOn implements Node {

	@Override
	public boolean activate() {
		return TutorialHelper.checkWidgetText("Moving on.", 372, 1);
	}

	@Override
	public void execute() {
		Bot.LOGGER.log(Level.FINEST, this.getClass().getName());
		
		Tile loc = Client.getLocalPlayer().getTile();

		if (Interacting.interact(new Tile(3088, 3119, 0, 0, -45, 0), "Climb-down") && Time.sleepUntil(() -> Walking.isMoving()
				|| Client.getLocalPlayer().getTile().distanceTo(new Tile(3088, 3119, 0, 0, -45, 0)) <= 1)) {
			Time.sleepUntil(() -> Client.getLocalPlayer().getTile().distanceTo(loc) > 10, 4000, 7000);
		}
	}

}
