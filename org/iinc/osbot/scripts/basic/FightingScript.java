package org.iinc.osbot.scripts.basic;

import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.NpcDefinition;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.renderable.Players;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.ArrayUtil;

import java.util.List;
import java.util.stream.Collectors;

public class FightingScript extends Script {
    String[] ATTACK = {"seagull"};

    @Override
    public int loop() {
        if (!Login.isLoggedIn()){
            Login.login();
            Time.sleep(600, 1200);
            Camera.setAngle(Camera.getCameraAngle(), true);
            return 300;
        }

        Interfaces.closeAll();

        if (!Walking.isRunEnabled() && Walking.getRunEnergy() > 20){
            Walking.enableRun();
        }

        Widget widget = Widgets.getAllWidgets().filter((w) -> !w.isHidden() && "Click here to continue".equals(w.getText())).findFirst().orElse(null);
        if (widget != null){
            Clicking.interact(widget, "Continue");
        }

        if (!Client.getLocalPlayer().isInteracting()){
            Time.sleep(300, 600);
            if (Interacting.interact(() -> {

                List<Npc> filtered = Npcs.getAll().parallelStream().filter((npc) -> {
                    NpcDefinition def = npc.getNpcDefinition();
                     if (def == null)
                        return false;
                    String name = def.getName();
                    if (name == null)
                        return false;

                    return ArrayUtil.contains(ATTACK, name.toLowerCase())
                            && npc.getAnimation() == -1
                            && !npc.isBeingInteractedWith(Players.getAll());
                }).sorted().collect(Collectors.toList());

                return filtered.parallelStream().filter((npc) -> {
                    if (npc == null || npc.getInteractingReference() != Client.getLocalPlayer().getReference())
                        return false;
                    NpcDefinition def = npc.getNpcDefinition();
                    if (def == null)
                        return false;
                    String name = def.getName();
                    return name != null && (name.toLowerCase().contains("frog") || name.toLowerCase().contains("rat"));
                }).sorted().findFirst().orElse(filtered.parallelStream().findFirst().orElse(null));

            }, null, new String[]{"Attack", "Dismiss"}, null)) {

                if (Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting(), 600, 800))
                    Time.sleepUntil(() -> !Client.getLocalPlayer().isInteracting(), 3000, 4000);

                return 300;

            }
        }

        return 300;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }
}
