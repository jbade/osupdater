package org.iinc.osbot.scripts.thieving.watchtower;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.script.Node;

public class UnstuckNode implements Node {

	@Override
	public boolean activate() {
		return Client.getPlane() != 1;
	}

	@Override
	public void execute() {
		int plane = Client.getPlane();
		if (plane == 0) {
			if (Interacting.interact(new Tile(2548, 3119, 0, 0, -54, 120), "Climb-up"))
				Time.sleep(600, 1200);
		} else if (plane == 2) {
			if (Interacting.interact(() -> RSObjects.getGameObjects(10).parallelStream()
					.filter((o) -> o.getObjectId() == 2797).findAny().orElse(null), new Tile(2549, 3112, 2),
					"Climb-down"))
				Time.sleep(600, 1200);
		}
	}

}
