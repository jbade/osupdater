package org.iinc.osbot.scripts.thieving.watchtower;

import java.util.function.Supplier;

import org.iinc.osbot.api.base.Clickable;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Random;

public class StealNode implements Node {
	Supplier<Clickable> warrior = () -> Npcs.getClosestStream("Watchman").findFirst().orElse(null);

	@Override
	public boolean activate() {
		return Levels.getCurrentHitpoints() > 6;
	}

	@Override
	public void execute() {
		if (Random.roll(.95) && Menu.contains("Pickpocket", "Watchman")) {
			Time.sleep(Random.randomGuassianRange(200, 600, ConfidenceIntervals.CI_70));
			Menu.interact("Pickpocket", "Watchman");
		} else {
			Time.sleep(Random.randomGuassianRange(100, 1000, ConfidenceIntervals.CI_70));
			Interacting.interact(warrior, null, "Pickpocket");
		}
	}

}
