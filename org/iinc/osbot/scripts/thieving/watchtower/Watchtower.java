package org.iinc.osbot.scripts.thieving.watchtower;

import org.iinc.osbot.script.Node;

public class Watchtower implements Node {

	static Node[] nodes = new Node[] {new UnstuckNode(), new EatNode(), new StealNode()};
	
	@Override
	public boolean activate() {
		return true;
	}

	@Override
	public void execute() {
		for (Node n : nodes){
			if (n.activate()){
				n.execute();
				return;
			}
		}
	}

}
