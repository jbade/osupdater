package org.iinc.osbot.scripts.thieving.watchtower;

import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.Memory;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class WatchtowerScript extends Script {

	Watchtower rf = new Watchtower();

	@Override
	public int loop() {
		if (!Login.isLoggedIn()) {
			Login.login();
			Camera.setAngle(Camera.getCameraAngle(), true);
			return 0;
		}

		Interfaces.closeAll();
		Memory.checkMemory(1000000L);

		if (BreakHandler.shouldBreak())
			System.exit(0);

		if (rf.activate())
			rf.execute();

		return Random.randomRange(0, 50);
	}

	@Override
	public void onStop() {

	}

	@Override
	public void onStart() {
		Modifiers.init(Bot.username.hashCode());
		BreakHandler.init(Bot.username.hashCode());
	}

}
