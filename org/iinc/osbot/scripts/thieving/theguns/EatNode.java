package org.iinc.osbot.scripts.thieving.theguns;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.Random;

public class EatNode implements Node {
	static RSItemConstant[] food = new RSItemConstant[] { Items.BREAD, Items.CHOCOLATE_CAKE1, Items.CAKE1, Items.CAKE2,
			Items.CAKE1, Items.CAKE, Items.TRIANGLE_SANDWICH };

	@Override
	public boolean activate() {
		return Levels.getCurrentHitpoints() <= Levels.getHitpoints() - 3 || Inventory.isFull();
	}

	@Override
	public void execute() {
		if (Inventory.isFull()) {

			int max = Random.randomRange(5, 15);
			int i = 0;
			for (Item item : Inventory.getItems()) {
				if (i >= max)
					break;
				
				for (RSItemConstant i2 : food) {
					if (item.getId() == i2.getId()) {
						i++;
						Clicking.interact(item, "Drop");
						Time.sleep(0, 300);
						break;
					}
				}
			}
		} else {

		}

	}

}
