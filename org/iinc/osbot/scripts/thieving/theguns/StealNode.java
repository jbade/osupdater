package org.iinc.osbot.scripts.thieving.theguns;

import java.util.function.Supplier;

import org.iinc.osbot.api.base.Clickable;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Random;

public class StealNode implements Node {
	static Tile[] toWarriors = new Tile[] { new Tile(2643, 3275, 0), new Tile(2639, 3262, 0), new Tile(2636, 3243, 0),
			new Tile(2643, 3227, 0) };

	Supplier<Clickable> farmer = () -> Npcs.getClosestStream("'The Guns'").findFirst().orElse(null);

	@Override
	public boolean activate() {
		return Levels.getHitpoints() > 3;
	}

	@Override
	public void execute() {
		if (Inventory.isItemSelected()) {
			Menu.interact("Cancel");
		}

		if (Walking.getRunEnergy() > Random.randomRange(30, 60)) {
			Walking.enableRun();
		}

		if (farmer.get() != null) {
			if (Random.roll(.95) && Menu.contains("Pickpocket", "'The Guns'")) {
				Time.sleep(Random.randomGuassianRange(200, 600, ConfidenceIntervals.CI_70));
				Menu.interact("Pickpocket", "'The Guns'");
			} else {
				Time.sleep(Random.randomGuassianRange(100, 1000, ConfidenceIntervals.CI_70));
				Interacting.interact(farmer, null, "Pickpocket");
			}
		} else {
			Walking.walkPath(Walking.generatePath(toWarriors, 3, true), () -> farmer.get() != null, true);
		}
	}

}
