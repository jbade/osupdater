package org.iinc.osbot.scripts.thieving.castleknights;

import org.iinc.osbot.script.Node;

public class CastleKnights implements Node {

	static Node[] nodes = new Node[] {new BankNode(), new EatNode(), new StealNode()};
	
	@Override
	public boolean activate() {
		return true;
	}

	@Override
	public void execute() {
		for (Node n : nodes){
			if (n.activate()){
				n.execute();
				return;
			}
		}
	}

}
