package org.iinc.osbot.scripts.thieving.castleknights;

import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.scripts.thieving.cakestall.CakeStall;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class BankNode implements Node {
	static Tile[] toBank = { new Tile(2579, 3297, 0), new Tile(2606, 3296, 0), new Tile(2630, 3296, 0),
			new Tile(2647, 3307, 0), new Tile(2641, 3288, 0), new Tile(2646, 3283, 0), new Tile(2655, 3283, 0) };
	boolean opened = false;

	@Override
	public boolean activate() {
		int q = 0;
		for (RSItemConstant i : EatNode.food) {
			q += Inventory.getCount(i.getId());
		}
		return q == 0;
	}

	@Override
	public void execute() {
		if (Client.getLocalPlayer().getTile().distanceTo(toBank[toBank.length - 1]) > 3) {
			Walking.walkPath(Walking.generatePath(toBank, 3, false), true);
			return;
		}

		if (!Bank.openBooth(new Tile(2656, 3283, 0)))
			return;

		if (!Bank.depositInventory())
			return;

		for (RSItemConstant item : EatNode.food) {
			if (Bank.getQuantity(item.getId()) >= 27) {
				if (Bank.withdraw(item.getId(), 27))
					return;
			}
		}

		// out of food
		Timer t = new Timer(Random.randomRange(20 * 60 * 1000, 30 * 60 * 1000));
		CakeStall cs = new CakeStall();
		while (t.isRunning()) {
			if (!Login.isLoggedIn()) {
				Login.login();
				Camera.setAngle(Camera.getCameraAngle(), true);
				continue;
			}

			Interfaces.closeAll();
			
			cs.execute();
		}
	}

}
