package org.iinc.osbot.scripts.thieving.castleknights;

import java.util.function.Supplier;

import org.iinc.osbot.api.base.Clickable;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.base.worlds.World;
import org.iinc.osbot.api.base.worlds.Worlds;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Random;

public class StealNode implements Node {
	Tile[] toGuards = { new Tile(2644, 3283, 0), new Tile(2629, 3297, 0), new Tile(2603, 3296, 0),
			new Tile(2580, 3297, 0) };
	Area castle = new Area(new Tile(2572, 3306, 0), new Tile(2575, 3305, 0), new Tile(2579, 3304, 0),
			new Tile(2579, 3301, 0), new Tile(2575, 3301, 0), new Tile(2576, 3295, 0), new Tile(2575, 3294, 0),
			new Tile(2574, 3287, 0), new Tile(2579, 3287, 0), new Tile(2581, 3287, 0), new Tile(2586, 3292, 0),
			new Tile(2586, 3294, 0), new Tile(2592, 3294, 0), new Tile(2592, 3290, 0), new Tile(2583, 3281, 0),
			new Tile(2565, 3281, 0), new Tile(2565, 3308, 0), new Tile(2570, 3312, 0), new Tile(2583, 3312, 0),
			new Tile(2592, 3304, 0), new Tile(2592, 3299, 0), new Tile(2586, 3299, 0), new Tile(2585, 3300, 0),
			new Tile(2585, 3301, 0), new Tile(2581, 3305, 0), new Tile(2579, 3305, 0));
	Area room1 = new Area(new Tile[] { new Tile(2576, 3302, 0), new Tile(2576, 3294, 0), new Tile(2568, 3294, 0),
			new Tile(2568, 3301, 0), });
	Area room2 = new Area(new Tile[] { new Tile(2570, 3295, 0), new Tile(2575, 3295, 0), new Tile(2575, 3287, 0),
			new Tile(2570, 3287, 0) });

	Supplier<Clickable> warrior = () -> Npcs.getClosestStream("Knight of Ardougne")
			.filter((npc) -> npc.getTile().distanceTo(new Tile(2590, 3296, 0)) < 30 && !castle.contains(npc.getTile()))
			.findFirst().orElse(null);

	@Override
	public boolean activate() {
		return Levels.getHitpoints() > 3;
	}

	@Override
	public void execute() {
		if (Inventory.isItemSelected()) {
			Menu.interact("Cancel");
		}

		if (Walking.getRunEnergy() > Random.randomRange(30, 60)) {
			Walking.enableRun();
		}

		if (room1.contains(Client.getLocalPlayer().getTile())) {
			if (RSObjects.getBoundaryObject(new Tile(2576, 3299, 0)) != null) {
				Interacting.interact(new Tile(2576, 3299, 0, -64, 0, 0), "Open");
			} else {
				Walking.walk(new Tile(2580, 3299, 0));
			}
			return;
		}
		if (room2.contains(Client.getLocalPlayer().getTile())){
			if (RSObjects.getBoundaryObject(new Tile(2574, 3293, 0)) != null) {
				Interacting.interact(new Tile(2574, 3293, 0, 64, 0, 0), "Open");
			} else {
				Walking.walk(new Tile(2580, 3299, 0));
			}
			return;
		}

		if (warrior.get() != null) {
			if (Client.getLocalPlayer().getY() == 3315 && Client.getLocalPlayer().getModelHeight() != 1000) {
				Interacting.interact(Client.getLocalPlayer().getTile().modify(0, 1), "Walk here");
			}
			Time.sleep(Random.randomGuassianRange(80, 200, ConfidenceIntervals.CI_70));
			if (Menu.contains("Pickpocket", "Knight of Ardougne")) {
				Menu.interact("Pickpocket", "Knight of Ardougne");
			} else {
				Interacting.interact(warrior, toGuards[toGuards.length - 1], "Pickpocket");
			}
		} else {
			if (Client.getLocalPlayer().getTile().distanceTo(toGuards[toGuards.length - 1]) > 5) {
				Walking.walkPath(Walking.generatePath(toGuards, 3, true), () -> warrior.get() != null, true);
			} else {
				System.exit(0);
			}
		}
	}

}
