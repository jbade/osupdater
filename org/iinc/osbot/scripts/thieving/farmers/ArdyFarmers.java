package org.iinc.osbot.scripts.thieving.farmers;

import org.iinc.osbot.script.Node;

public class ArdyFarmers implements Node {

	static Node[] nodes = new Node[] {new BankNode(), new EatNode(), new DropNode(), new StealNode()};
	
	@Override
	public boolean activate() {
		return true;
	}

	@Override
	public void execute() {
		for (Node n : nodes){
			if (n.activate()){
				n.execute();
				return;
			}
		}
	}

}
