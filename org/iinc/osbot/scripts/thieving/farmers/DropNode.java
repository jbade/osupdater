package org.iinc.osbot.scripts.thieving.farmers;

import java.awt.Point;

import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.Shop;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class DropNode implements Node {

    RSItemConstant[] drop = {
            Items.POTATO_SEED, Items.ONION_SEED, Items.CABBAGE_SEED, Items.TOMATO_SEED,
            Items.BARLEY_SEED, Items.HAMMERSTONE_SEED,
            Items.ASGARNIAN_SEED, Items.JUTE_SEED, Items.YANNILLIAN_SEED, Items.KRANDORIAN_SEED, Items.WILDBLOOD_SEED,
            Items.MARIGOLD_SEED, Items.NASTURTIUM_SEED, Items.ROSEMARY_SEED, Items.WOAD_SEED, Items.REDBERRY_SEED,
            Items.CADAVABERRY_SEED, Items.DWELLBERRY_SEED, Items.JANGERBERRY_SEED, Items.WHITEBERRY_SEED,
            Items.POISON_IVY_SEED, Items.MUSHROOM_SPORE, Items.SWEETCORN_SEED, Items.GUAM_SEED, Items.MARRENTILL_SEED,
            Items.TARROMIN_SEED, Items.HARRALANDER_SEED, Items.STRAWBERRY_SEED, Items.BELLADONNA_SEED
    };
    //

    @Override
    public boolean activate() {
        return Inventory.isFull() || Client.getLocalPlayer().getModelHeight() == 1000;
    }

    @Override
    public void execute() {
        boolean did = false;

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 6; j++) {
                Inventory.open();

                int slot = i + j * 4;
                Item item = Inventory.getSlot(slot);
                if (item != null) {

                    boolean cont = false;
                    for (RSItemConstant i2 : drop) {
                        if (i2.getId() == item.getId()) {
                            cont = true;
                            break;
                        }
                    }

                    if (!cont)
                        continue;

                    did = true;

                    if (Inventory.isItemSelected()) {
                        Menu.interact("Cancel");
                    }

                    if (!Inventory.getBounds(slot).contains(Mouse.getLocation())) {
                        Clicking.hover(Inventory.getBounds(slot));
                        Time.sleep(Random.modifiedRandomGuassianRange(100, 300, ConfidenceIntervals.CI_70, 0,
                                Modifiers.get("RatFrog.drop1")));
                    }

                    Time.sleep(Random.modifiedRandomGuassianRange(20, 40, ConfidenceIntervals.CI_60, 0,
                            Modifiers.get("RatFrog.drop2")));

                    Mouse.click(Mouse.RIGHT_BUTTON);
                    Time.sleep(Random.modifiedRandomGuassianRange(20, 40, ConfidenceIntervals.CI_60, 0,
                            Modifiers.get("RatFrog.drop2")));

                    Point pos = Mouse.getLocation();
                    Mouse.setLocation(pos.x, pos.y + 36);

                    Time.sleep(Random.modifiedRandomGuassianRange(20, 40, ConfidenceIntervals.CI_60, 0,
                            Modifiers.get("RatFrog.drop2")));

                    Mouse.click(Mouse.LEFT_BUTTON);

                    Time.sleep(Random.modifiedRandomGuassianRange(20, 40, ConfidenceIntervals.CI_60, 0,
                            Modifiers.get("RatFrog.drop3")));

                }
            }
        }

        for (int i = 24; i < 28; i++) {
            Item item = Inventory.getSlot(i);
            if (item != null) {

                boolean cont = false;
                for (RSItemConstant i2 : drop) {
                    if (i2.getId() == item.getId()) {
                        cont = true;
                        break;
                    }
                }

                if (!cont)
                    continue;

                did = true;

                if (Inventory.isItemSelected()) {
                    Menu.interact("Cancel");
                }

                Clicking.interact(item, "Drop");
            }
        }

        if (Inventory.isFull()) {
            new EatNode().execute();
        }

        // for (RSItemConstant i : drop) {
        // if (Inventory.contains(i.getId())) {
        // Clicking.interact(Inventory.getFirst(i.getId()), "Drop");
        // }
        // }
    }

}
