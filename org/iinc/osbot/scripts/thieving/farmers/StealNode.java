package org.iinc.osbot.scripts.thieving.farmers;

import java.util.function.Supplier;

import org.iinc.osbot.api.base.Clickable;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.base.worlds.World;
import org.iinc.osbot.api.base.worlds.Worlds;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Random;

public class StealNode implements Node {
	static Tile[] toWarriors = new Tile[] { new Tile(2617, 3333, 0), new Tile(2630, 3336, 0), new Tile(2635, 3347, 0),
			new Tile(2637, 3362, 0), };

	static Area house = new Area(    new Tile(2650, 3366, 0),
		    new Tile(2640, 3366, 0),
		    new Tile(2640, 3354, 0),
		    new Tile(2650, 3354, 0));
	static Area field = new Area(new Tile(2635, 3370, 0), new Tile(2635, 3351, 0), new Tile(2630, 3349, 0),
			new Tile(2626, 3349, 0), new Tile(2623, 3351, 0), new Tile(2623, 3370, 0), new Tile(2627, 3371,0), new Tile(2628, 3372,0));


	Supplier<Clickable> farmer = () -> Npcs.getClosestStream("Master Farmer")
			.filter((npc) -> !house.contains(npc.getTile()) && !field.contains(npc.getTile())).findFirst().orElse(null);

	@Override
	public boolean activate() {
		return Levels.getHitpoints() > 3;
	}

	@Override
	public void execute() {
		if (Inventory.isItemSelected()){
			Menu.interact("Cancel");
		}
		
		if (Walking.getRunEnergy() > Random.randomRange(30, 60)){
			Walking.enableRun();
		}
		
		if (field.contains(Client.getLocalPlayer().getTile())){
			if (RSObjects.getBoundaryObject(new Tile(2634, 3361, 0)) == null){
				Walking.walk(toWarriors[toWarriors.length - 1].randomized());
			} else {
				Clicking.interact(new Tile(2634, 3361, 0, 55, -64, 64), "Open");
			}
			Time.sleep(600, 1200);
			return;
		} 
		
	
		
		if (farmer.get() != null) {
			if (Random.roll(.95) && Menu.contains("Pickpocket", "Master Farmer")) {
				Time.sleep(Random.randomGuassianRange(200, 600, ConfidenceIntervals.CI_70));
				Menu.interact("Pickpocket", "Master Farmer");
			} else {
				Time.sleep(Random.randomGuassianRange(100, 1000, ConfidenceIntervals.CI_70));
				Interacting.interact(farmer, null, "Pickpocket");
			}
		} else {
			if (Client.getLocalPlayer().getTile().distanceTo(toWarriors[toWarriors.length - 1]) > 5) {
				Walking.walkPath(Walking.generatePath(toWarriors, 3, true), () -> farmer.get() != null, true);
			} else {
				World w = Worlds.getRandomNormalWorld(true);
				if (w != null) {
					Logout.switchWorld(w.getId());
					Time.sleep(1200, 1800);
				}
			}
			
		}
	}

}
