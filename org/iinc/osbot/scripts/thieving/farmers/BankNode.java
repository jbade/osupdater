package org.iinc.osbot.scripts.thieving.farmers;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;

public class BankNode implements Node {
	static Tile[] toBank = new Tile[] {     new Tile(2637, 3362, 0),
		    new Tile(2635, 3347, 0),
		    new Tile(2630, 3336, 0),
		    new Tile(2617, 3333, 0) };

	@Override
	public boolean activate() {
		int q = 0;
		for (RSItemConstant i : EatNode.food){
			q += Inventory.getCount(i.getId());
		}
		return q == 0;
	}

	@Override
	public void execute() {
		if (StealNode.field.contains(Client.getLocalPlayer().getTile())){
			if (RSObjects.getBoundaryObject(new Tile(2634, 3361, 0)) == null){
				Walking.walk(StealNode.toWarriors[StealNode.toWarriors.length - 1].randomized());
			} else {
				Clicking.interact(new Tile(2634, 3361, 0, 55, -64, 64), "Open");
			}
			Time.sleep(600, 1200);
			return;
		} 
		
		
		if (Client.getLocalPlayer().getTile().distanceTo(toBank[toBank.length - 1]) > 3) {
			Walking.walkPath(Walking.generatePath(toBank, 3, false), true);
			return;
		}

		if (!Bank.openBooth(new Tile(2615, 3331, 0), new Tile(2618, 3331, 0), new Tile(2619, 3331, 0)))
			return;

		if (!Bank.depositInventory())
			return;

		for (RSItemConstant item : EatNode.food) {
			if (Bank.getQuantity(item.getId()) >= 16) {
				if (Bank.withdraw(item.getId(), 16))
					return;
			}
		}
		
		System.exit(Settings.DISABLED_EXIT_CODE);
	}

}
