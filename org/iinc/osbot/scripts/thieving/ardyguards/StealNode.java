package org.iinc.osbot.scripts.thieving.ardyguards;

import java.util.function.Supplier;

import org.iinc.osbot.api.base.Clickable;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Random;

public class StealNode implements Node {
	Tile[] toGuards = new Tile[] { new Tile(2618, 3333, 0), new Tile(2625, 3337, 0), new Tile(2636, 3339, 0), };

	Supplier<Clickable> warrior = () -> Npcs.getClosestStream("Guard").findFirst().orElse(null);

	@Override
	public boolean activate() {
		return Levels.getHitpoints() > 2;
	}

	@Override
	public void execute() {
		if (Walking.getRunEnergy() > 30)
			Walking.enableRun();

		if (Client.getLocalPlayer().getModelHeight() == 1000) { // stunned
			return;
		}

		if (warrior.get() != null) {
			Time.sleep(Random.randomGuassianRange(50, 250, ConfidenceIntervals.CI_60));
			Interacting.interact(warrior, toGuards[toGuards.length - 1], "Pickpocket");
		} else {
			Walking.walkPath(Walking.generatePath(toGuards, 3, true), () -> Npcs.getClosestStream("Guard").count() > 0,
					true);
		}
	}

}
