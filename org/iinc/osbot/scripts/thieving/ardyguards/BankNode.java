package org.iinc.osbot.scripts.thieving.ardyguards;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.Node;

public class BankNode implements Node {
	static Tile[] toBank = new Tile[]{
		    new Tile(2636, 3339, 0),
		    new Tile(2625, 3337, 0),
		    new Tile(2618, 3333, 0),
		};

	@Override
	public boolean activate() {
		return Inventory.getCount() <= 1;
	}

	@Override
	public void execute() {
		if (Client.getLocalPlayer().getTile().distanceTo(toBank[toBank.length - 1]) > 3) {
			Walking.walkPath(Walking.generatePath(toBank, 3, false), true);
			return;
		}

		if (!Bank.openBooth(new Tile(2618, 3331, 0)))
			return;

		if (!Bank.depositInventory())
			return;

		for (RSItemConstant item : EatNode.food) {
			if (Bank.getQuantity(item.getId()) >= 27) {
				if (Bank.withdraw(item.getId(), 27))
					return;
			}
		}
		
		System.exit(Settings.DISABLED_EXIT_CODE);

	}

}
