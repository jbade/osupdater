package org.iinc.osbot.scripts.thieving.ardyguards;

import org.iinc.osbot.script.Node;

public class ArdyGuards implements Node {

	static Node[] nodes = new Node[] {new EatNode(), new BankNode(), new StealNode()};
	
	@Override
	public boolean activate() {
		return true;
	}

	@Override
	public void execute() {
		for (Node n : nodes){
			if (n.activate()){
				n.execute();
				return;
			}
		}
	}

}
