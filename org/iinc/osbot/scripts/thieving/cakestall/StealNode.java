package org.iinc.osbot.scripts.thieving.cakestall;

import java.util.Optional;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.base.worlds.Worlds;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.GameObject;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class StealNode implements Node {
	public static boolean DMM = false;
	static Tile stand = new Tile(2668, 3312, 0);

	// includes path from The Guns and Castle Knights
	static Tile[] toStall = new Tile[] { new Tile(2584, 3296, 0), new Tile(2598, 3296, 0), new Tile(2609, 3296, 0),
			new Tile(2629, 3296, 0), new Tile(2643, 3229, 0), new Tile(2638, 3244, 0), new Tile(2638, 3258, 0),
			new Tile(2642, 3267, 0), new Tile(2655, 3283, 0), new Tile(2644, 3284, 0), new Tile(2643, 3303, 0), stand };

	static int objectId = 11730;

	public static Timer t = new Timer(15000);

	@Override
	public boolean activate() {
		return Levels.getCurrentHitpoints() > 3;
	}

	@Override
	public void execute() {
		if (Client.getLocalPlayer().getTile().distanceTo(toStall[toStall.length - 1]) > 4) {
			if (DMM){
				Walking.disableRun();
			}
			Walking.walkPath(Walking.generatePath(toStall, 3, false), true);
			return;
		}

		if (Client.getLocalPlayer().getTile().distanceTo(stand) > 1) {
			if (Clicking.interact(stand, "Walk here")
					&& Time.sleepUntil(() -> Walking.getDestinationTile() != null, 600, 1000))
				Time.sleepUntil(() -> !Walking.isMoving(), 600, 1000);
			return;
		}
		
		if (DMM){
			Walking.enableRun();
		}

		if (Npcs.getAll().parallelStream().filter((npc) -> {
			if (DMM)
				return false;

			if (!npc.getNpcDefinition().getName().equals("Guard"))
				return false;
			if (npc.getTile().distanceTo(Client.getLocalPlayer().getTile()) <= 2)
				return true;

			if (Math.abs(npc.getTile().getX() - Client.getLocalPlayer().getTile().getX()) <= 4
					&& (npc.getTile().getY() - Client.getLocalPlayer().getTile().getY()) <= 2
					&& npc.getTile().getY() - Client.getLocalPlayer().getTile().getY() >= -1) {
				return true;
			}

			return false;

		}).findAny().isPresent()) {
			if (!t.isRunning()) {
				Logout.switchWorld(Worlds.getRandomNormalWorld(true).getId());
				return;
			} else {
				Time.sleep(600);
				return;
			}
		} else {
			t.reset();
		}

		Optional<GameObject> stall = RSObjects.getGameObjects(5).parallelStream()
				.filter((obj) -> obj.getObjectId() == objectId).findAny();
		if (stall.isPresent()) {
			if (!DMM) {
				Time.sleep(Random.randomGuassianRange(150, 250, ConfidenceIntervals.CI_70));
			}

			if (Random.roll(.95) && Menu.contains("Steal-from", "Baker's stall")) {
				if (Menu.interact("Steal-from", "Baker's stall")
						&& Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != -1, 600, 1000))
					Time.sleep(800, 1000);

			} else {
				if (Clicking.interact(stall.get(), "Steal-from")
						&& Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != -1, 600, 1000))
					Time.sleep(800, 1000);

			}

		}
	}

}
