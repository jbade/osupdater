package org.iinc.osbot.scripts.thieving.cakestall;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.Node;

public class BankNode implements Node {
	static Tile[] toBank = new Tile[] { new Tile(2668, 3312, 0), new Tile(2643, 3303, 0), new Tile(2644, 3284, 0),
			new Tile(2655, 3283, 0) };
	static EatNode eat = new EatNode();
	
	@Override
	public boolean activate() {
		return Inventory.isFull() || Inventory.getCount() > 0 && Client.getLocalPlayer().getTile().distanceTo(toBank[toBank.length -1]) < 5;
	}

	@Override
	public void execute() {
		if (StealNode.DMM){
			Walking.disableRun();
		}
		
		if (Client.getLocalPlayer().getTile().distanceTo(toBank[toBank.length -1]) > 3){
			Walking.walkPath(Walking.generatePath(toBank, 3, false), true);
			return;
		}
		
		if (!Bank.openBooth(new Tile(2656, 3283, 0)))
			return;
		
		if (!eat.activate()){
			Bank.depositInventory();
		} else {
			for (RSItemConstant item : eat.food){
				if (Bank.contains(item.getId())){
					Bank.withdraw(item.getId(), -1);
					return;
				}
			}
			
			
			// no food in bank
			// low hp
			
			Time.sleepUntil(() -> Levels.getCurrentHitpoints() > Levels.getHitpoints() - 3, 4 * 60 * 1000, 5 * 60 * 1000);
		}
		
	}

}
