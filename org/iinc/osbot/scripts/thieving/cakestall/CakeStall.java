package org.iinc.osbot.scripts.thieving.cakestall;

import org.iinc.osbot.script.Node;

public class CakeStall implements Node {

	static Node[] nodes = new Node[] {new EatNode(), new EscapeNode(), new BankNode(), new StealNode()};
	
	@Override
	public boolean activate() {
		return true;
	}

	@Override
	public void execute() {
		for (Node n : nodes){
			if (n.activate()){
				n.execute();
				return;
			}
		}
	}

}
