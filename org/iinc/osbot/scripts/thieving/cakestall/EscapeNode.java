package org.iinc.osbot.scripts.thieving.cakestall;

import java.util.function.BooleanSupplier;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ArrayUtil;

public class EscapeNode implements Node {

	static Tile runTo = new Tile(2672, 3312, 0);
	static String[] runFrom = new String[] { "Guard" };
	static EatNode eat = new EatNode();

	static BooleanSupplier run = () -> {
		Object player = Client.getLocalPlayer().getReference();
		return Npcs.getAll().parallelStream().filter((npc) -> {
			if (npc.getInteractingReference() != player)
				return false;
			String npcName = npc.getNpcDefinition().getName();
			return npcName != null && ArrayUtil.contains(runFrom, npcName) && npc.getTile().distanceTo(Client.getLocalPlayer().getTile()) <= 5;
		}).count() > 0;
	};

	@Override
	public boolean activate() {
		return run.getAsBoolean();
	}

	@Override
	public void execute() {
		Walking.enableRun();
		
		if(Calculations.onViewport(runTo.toScreen())){
			Clicking.interact(runTo.randomized(1, 1), "Walk here");
		} else {
			Walking.blindWalk(runTo.modify(5, 0).randomized(2, 2),
					() -> eat.activate() || !run.getAsBoolean());
		}
			
	}

}
