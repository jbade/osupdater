package org.iinc.osbot.scripts.thieving.ardywarriors;

import java.util.function.Supplier;

import org.iinc.osbot.api.base.Clickable;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Random;

public class StealNode implements Node {
	Tile[] toWarriors = new Tile[] { new Tile(2655, 3283, 0), new Tile(2636, 3289, 0), new Tile(2629, 3298, 0), };

	Area petShop = new Area(new Tile(2691, 3295, 0), new Tile(2619, 3290, 0), new Tile(2624, 3290, 0),
			new Tile(2624, 3295, 0));

	Supplier<Clickable> warrior = () -> Npcs.getClosestStream("Warrior woman").filter((npc) -> !petShop.contains(npc.getTile())).findFirst().orElse(null);
	
	@Override
	public boolean activate() {
		return Levels.getHitpoints() > 2;
	}

	@Override
	public void execute() {
	//	if (Client.getLocalPlayer().getModelHeight() == 1000){ // stunned
	//		return;
	//	}
			
		
		if (warrior.get() != null) {
			Time.sleep(Random.randomGuassianRange(50, 250, ConfidenceIntervals.CI_60));
			
			
			if (Random.roll(.95) && Menu.contains("Pickpocket", "Warrior woman")) {
				Time.sleep(Random.randomGuassianRange(200, 600, ConfidenceIntervals.CI_70));
				Menu.interact("Pickpocket", "Warrior woman");
			} else {
				Time.sleep(Random.randomGuassianRange(100, 1000, ConfidenceIntervals.CI_70));
				Interacting.interact(warrior, toWarriors[toWarriors.length - 1], "Pickpocket");
			}
		} else {
			Walking.walkPath(Walking.generatePath(toWarriors, 3, true), () -> Npcs.getClosestStream("Warrior woman").count() > 0,
					true);
		}
	}

}
