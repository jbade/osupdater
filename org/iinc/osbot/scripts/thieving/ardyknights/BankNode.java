package org.iinc.osbot.scripts.thieving.ardyknights;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.Node;

public class BankNode implements Node {
	static Tile[] toBank = new Tile[]{
		    new Tile(2668, 3315, 0),
		    new Tile(2650, 3307, 0),
		    new Tile(2642, 3299, 0),
		    new Tile(2645, 3284, 0),
		    new Tile(2655, 3283, 0),
		};
	
	boolean opened = false;

	@Override
	public boolean activate() {
		int q = 0;
		for (RSItemConstant i : EatNode.food){
			q += Inventory.getCount(i.getId());
		}
		return q == 0;
	}

	@Override
	public void execute() {
		if (!StealNode.house.contains(Client.getLocalPlayer().getTile())) {
			if (opened) {
				if (StealNode.openedDoor.get() != null) {
					if (Interacting.interact(StealNode.openedDoor, null, "Close")) {
						opened = false;
						Time.sleep(3600, 4000);
					}
				}
			}
			
			if (Client.getLocalPlayer().getTile().distanceTo(toBank[toBank.length - 1]) > 3) {
				Walking.walkPath(Walking.generatePath(toBank, 3, false), true);
				return;
			}

			if (!Bank.openBooth(new Tile(2656, 3283, 0)))
				return;

			if (!Bank.depositInventory())
				return;

			for (RSItemConstant item : EatNode.food) {
				if (Bank.getQuantity(item.getId()) >= 27) {
					if (Bank.withdraw(item.getId(), 27))
						return;
				}
			}
			
			System.exit(Settings.DISABLED_EXIT_CODE);
		} else {
			if (StealNode.closedDoor.get() != null) {
				if (Interacting.interact(StealNode.closedDoor, null, "Open")) {
					opened = true;
					if (Time.sleepUntil(() -> Walking.isMoving(), 600, 800))
						Time.sleepUntil(() -> !Walking.isMoving(), 1800, 2000);
				}
			} else {
				if (Walking.walk(new Tile(2668, 3314, 0)) && Time.sleepUntil(() -> Walking.isMoving(), 600, 800))
					Time.sleepUntil(() -> !Walking.isMoving(), 1800, 2000);
			}
		}
		

	}

}
