package org.iinc.osbot.scripts.thieving.ardyknights;

import java.util.Optional;
import java.util.function.Supplier;

import org.iinc.osbot.api.base.Clickable;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.base.worlds.World;
import org.iinc.osbot.api.base.worlds.Worlds;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.GameObject;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Random;

public class StealNode implements Node {
	static Tile[] toGuards = new Tile[] { new Tile(2655, 3283, 0), new Tile(2645, 3284, 0), new Tile(2642, 3299, 0),
			new Tile(2650, 3307, 0), new Tile(2668, 3315, 0), };
	static Area house = new Area(new Tile(2676, 3314, 0), new Tile(2670, 3314, 0), new Tile(2669, 3315, 0),
			new Tile(2668, 3316, 0), new Tile(2667, 3317, 0), new Tile(2667, 3320, 0), new Tile(2676, 3320, 0));
	static Supplier<Clickable> warrior = () -> Npcs.getClosestStream("Knight of Ardougne")
			.filter((npc) -> house.contains(npc.getTile())).findFirst().orElse(null);
	static Supplier<Clickable> closedDoor = () -> RSObjects.getGameObjects(new Tile(2669, 3316, 0)).parallelStream()
			.filter((obj) -> obj.getObjectId() == 1535).findAny().orElse(null);
	static Supplier<Clickable> openedDoor = () -> RSObjects.getGameObjects(new Tile(2670, 3316, 0)).parallelStream()
			.filter((obj) -> obj.getObjectId() == 1536).findAny().orElse(null);
	
	
	boolean opened = false;

	@Override
	public boolean activate() {
		return Levels.getHitpoints() > 6;
	}

	@Override
	public void execute() {
		

		if (house.contains(Client.getLocalPlayer().getTile())) {
			if (opened) {
				if (openedDoor.get() != null) {
					if (Interacting.interact(openedDoor, null, "Close")) {
						opened = false;
						Time.sleep(3600, 4000);
					}
				}
			}
			if (warrior.get() != null) {
				if (Client.getLocalPlayer().getY() == 3315 && Client.getLocalPlayer().getModelHeight() != 1000){
					Interacting.interact(Client.getLocalPlayer().getTile().modify(0, 1), "Walk here");
				}
				Time.sleep(Random.randomGuassianRange(80, 200, ConfidenceIntervals.CI_70));
				if (Menu.contains("Pickpocket", "Knight of Ardougne")){
					Menu.interact("Pickpocket", "Knight of Ardougne");
				} else {
					Interacting.interact(warrior, toGuards[toGuards.length - 1], "Pickpocket");
				}
				
			} else {
				World w = Worlds.getRandomNormalWorld(true);
				if (w != null) {
					Logout.switchWorld(w.getId());
					Time.sleep(1200, 1800);
				}
			}
		} else {
			if (Client.getLocalPlayer().getTile().distanceTo(toGuards[toGuards.length - 1]) > 5) {
				Walking.walkPath(Walking.generatePath(toGuards, 3, true), true);
			} else {
				if (closedDoor.get() != null) {
					if (Interacting.interact(closedDoor, null, "Open")) {
						opened = true;
						if (Time.sleepUntil(() -> Walking.isMoving(), 600, 800))
							Time.sleepUntil(() -> !Walking.isMoving(), 1800, 2000);
					}
				} else {
					if (Walking.walk(new Tile(2670, 3318, 0)) && Time.sleepUntil(() -> Walking.isMoving(), 600, 800))
						Time.sleepUntil(() -> !Walking.isMoving(), 1800, 2000);
				}
			}
		}
	}

}
