package org.iinc.osbot.scripts.callisto;

import java.util.logging.Level;

import org.iinc.osbot.api.base.interfaces.tabs.Equipment;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.MouseMovementHandler;
import org.iinc.osbot.util.ArrayUtil;

public class ExitWildernessNode implements CallistoNode {

	@Override
	public boolean activate(Npc callisto, int wildernessLevel, Tile playerLocation) {
		boolean result = false;

		if (wildernessLevel == -1)
			return false;
		
		// looted
		result = result || (Callisto.looted > 0);
		Bot.LOGGER.fine("ExitWilderness a " + result);
		
		// run energy
		if (!result && Inventory.getCount(Callisto.STAMINA_POTS_IDS) == 0) {
			boolean isStaminaActive = Walking.isStaminaActive();
			int runEnergy = Walking.getRunEnergy();
			result = result || (isStaminaActive && runEnergy <= 30) || (!isStaminaActive && runEnergy <= 60);
		}
		Bot.LOGGER.fine("ExitWilderness b " + result);
		
		// poison
		result = result || Client.isPoisoned() && Inventory.getCount(Callisto.ANTIPOISON_POTS_IDS) == 0;
		Bot.LOGGER.fine("ExitWilderness c " + result);
		
		// combat pots
		result = result || Inventory.getCount(Callisto.ATTACK_POT_IDS) == 0
				|| Inventory.getCount(Callisto.STRENGTH_POT_IDS) == 0;
		Bot.LOGGER.fine("ExitWilderness d " + result);
		
		// food
		result = result || Inventory.getCount(Callisto.FOOD_IDS) < 4;
		Bot.LOGGER.fine("ExitWilderness e " + result);
		
		return result;
	}

	private static int teleportAttempts = 0;

	private static int incrementTeleportAttempts() {
		return ++teleportAttempts;
	}

	@Override
	public void execute(Npc callisto, int wildernessLevel, Tile playerLocation) {
		MouseMovementHandler.DEFAULT_MOUSE_SPEED = 7;
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		teleportAttempts = 0;
		Walking.blindWalk(Callisto.WILDERNESS_DITCH_TILE, () -> {
			if (WalkingCondition.get().check())
				return true;

			int level = Client.getWildernessLevel();
			if (level <= 30 && incrementTeleportAttempts() <= 3) {
				if (!ArrayUtil.contains(Callisto.CHARGED_GLORY_IDS, Equipment.getAmulet())) {
					Clicking.interact(Inventory.getFirst(Callisto.CHARGED_GLORY_IDS), "Wear");
					int invCount = Inventory.getCount();
					Time.sleepUntil(() -> invCount != Inventory.getCount(), 600, 800);
				}

				if (Clicking.interact(Equipment.getAmuletWidget(), "Edgeville")) {
					Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != -1, 600, 800);
					Time.sleepUntil(() -> Client.getWildernessLevel() < 1, 4000, 5000);
				}
			}

			return level == -1;
		});

	}
}
