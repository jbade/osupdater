package org.iinc.osbot.scripts.callisto;

import java.util.logging.Level;

import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.MouseMovementHandler;

public class WalkToCallistoNode implements CallistoNode {

	private static final Tile[] PATH_TO_CALLISTO = new Tile[] { new Tile(3206, 3681, 0), new Tile(3209, 3684, 0),
			new Tile(3210, 3687, 0), new Tile(3210, 3690, 0), new Tile(3213, 3693, 0), new Tile(3216, 3695, 0),
			new Tile(3219, 3695, 0), new Tile(3222, 3698, 0), new Tile(3225, 3699, 0), new Tile(3228, 3701, 0),
			new Tile(3231, 3704, 0), new Tile(3234, 3704, 0), new Tile(3236, 3707, 0), new Tile(3239, 3708, 0),
			new Tile(3242, 3710, 0), new Tile(3245, 3713, 0), new Tile(3247, 3716, 0), new Tile(3250, 3717, 0),
			new Tile(3252, 3720, 0), new Tile(3252, 3723, 0), new Tile(3255, 3725, 0), new Tile(3258, 3726, 0),
			new Tile(3261, 3729, 0), new Tile(3263, 3732, 0), new Tile(3263, 3735, 0), new Tile(3265, 3738, 0),
			new Tile(3268, 3741, 0), new Tile(3270, 3744, 0), new Tile(3270, 3747, 0), new Tile(3270, 3750, 0),
			new Tile(3271, 3753, 0), new Tile(3272, 3756, 0), new Tile(3275, 3756, 0), new Tile(3278, 3756, 0),
			new Tile(3278, 3759, 0), new Tile(3279, 3762, 0), new Tile(3279, 3765, 0), new Tile(3279, 3768, 0),
			new Tile(3279, 3771, 0), new Tile(3279, 3774, 0), new Tile(3279, 3777, 0), new Tile(3282, 3780, 0),
			new Tile(3281, 3783, 0), new Tile(3281, 3786, 0), new Tile(3281, 3789, 0), new Tile(3284, 3792, 0),
			new Tile(3285, 3795, 0), new Tile(3287, 3798, 0), new Tile(3287, 3801, 0), new Tile(3287, 3804, 0),
			new Tile(3287, 3807, 0), new Tile(3289, 3810, 0), new Tile(3292, 3813, 0), new Tile(3294, 3816, 0),
			new Tile(3294, 3819, 0), new Tile(3294, 3822, 0), new Tile(3296, 3825, 0), new Tile(3297, 3828, 0),
			new Tile(3300, 3831, 0), new Tile(3301, 3834, 0), };

	@Override
	public boolean activate(Npc callisto, int wildernessLevel, Tile playerLocation) {
		return wildernessLevel > 0 && playerLocation.distanceTo(Callisto.CALLISTO_TILE) > 70;
	}

	@Override
	public void execute(Npc callisto, int wildernessLevel, Tile playerLocation) {
		MouseMovementHandler.DEFAULT_MOUSE_SPEED = 7;
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		Walking.walkPath(PATH_TO_CALLISTO, WalkingCondition.get(), false);
	}
}
