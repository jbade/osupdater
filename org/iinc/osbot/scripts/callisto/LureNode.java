package org.iinc.osbot.scripts.callisto;

import java.awt.Point;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Filters;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Condition;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.NpcDefinition;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.input.MouseMovementHandler;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class LureNode implements CallistoNode {

	private final LureStage[] LURE_STAGES = new LureStage[] {
			new LureStage(new Tile(3326, 3837, 0), new Tile(3326, 3837, 0), new Tile(3316, 3839, 0)),
			new LureStage(new Tile(3330, 3809, 0), new Tile(3332, 3807, 0), new Tile(3333, 3818, 0)), // new
																										// Tile(3333,
																										// 3818,
																										// 0)),
			new LureStage(new Tile(3332, 3806, 0), new Tile(3332, 3806, 0), null),
			new LureStage(new Tile(3337, 3816, 0), new Tile(3337, 3816, 0), null) };

	class LureStage {
		Tile ms, mm, callisto;

		public LureStage(Tile mm, Tile ms, Tile callisto) {
			this.mm = mm;
			this.ms = ms;
			this.callisto = callisto;
		}
	}

	@Override
	public boolean activate(Npc callisto, int wildernessLevel, Tile playerLocation) {
		return wildernessLevel > 0 && playerLocation.distanceTo(Callisto.CALLISTO_TILE) <= 70;
	}

	Tile goal;

	@Override
	public void execute(Npc callisto, int wildernessLevel, Tile playerLocation) {
		MouseMovementHandler.DEFAULT_MOUSE_SPEED = 5;
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		goal = Callisto.CALLISTO_TILE.randomized();

		Condition aggro = () -> {
			if (WalkingCondition.get().check())
				return true;

			List<Npc> n = Npcs.getAll().stream().filter(Filters.byName("Callisto")).collect(Collectors.toList());

			if (n.size() > 0) {
				Npc temp = n.get(0);
				goal = temp.getTile();
				if (temp.getTile().distanceTo(Callisto.CALLISTO_TILE) > 10 || temp.isInteracting()) {
					// TODO check if interacting index is us
					// we have aggro
					return true;
				}
			}

			return false;
		};

		if (!aggro.check()) {
			Tile loc = Walking.getClosestTileOnMiniMap(goal);
			Walking.walk(loc, aggro, Walking.getDistance());
			return;
		}

		if (WalkingCondition.get().check())
			return;

		List<Npc> npcs = Npcs.getAll().stream()
				.filter((npc) -> npc != null && npc.getNpcDefinition().getName().equals("Callisto"))
				.collect(Collectors.toList());
		if (npcs.size() == 0)
			return; // callisto is missing, we should switch worlds

		callisto = npcs.get(0);

		if (callisto.getTile().distanceTo(Callisto.CALLISTO_TILE) > 10)
			return; // callisto is out of place. get new world

		if (callisto.isInteracting()) { // we have aggro and can lure
			int i = 0;
			for (LureStage stage : LURE_STAGES) {
				Bot.LOGGER.log(Level.INFO, "Stage " + ++i);
				Timer t = new Timer(Random.randomRange(30000, 40000));
				while (t.isRunning()) {
					if (WalkingCondition.get().check())
						return;

					if (!Client.getLocalPlayer().getTile().equals(stage.ms)) {
						blindWalkExact(stage.mm, stage.ms, WalkingCondition.get());
						continue;
					}
					if (Walking.isMoving())
						continue;

					if (stage.callisto == null)
						break;

					npcs = Npcs.getAll().stream().filter((npc) -> {
						if (npc == null)
							return false;
						NpcDefinition def = npc.getNpcDefinition();
						if (def == null)
							return false;
						String name = def.getName();
						return name != null && name.equals("Callisto");
					}).collect(Collectors.toList());
					if (npcs.size() != 0) {
						callisto = npcs.get(0);
						if (callisto.getTile().distanceTo(stage.callisto) <= 1) {
							break;
						}
					}
					// Time.sleep(100, 150);
				}

				if (!t.isRunning())
					break;
			}
		}
	}

	public static boolean blindWalkExact(Tile mm, Tile ms, Condition until) {
		double prevDistance = Double.MAX_VALUE;
		double distance = Client.getLocalPlayer().getTile().distanceTo(ms);
		while (distance >= 0 && distance < prevDistance && !until.check()) {
			prevDistance = distance;

			Tile loc = Walking.getClosestTileOnMiniMap(mm);
			if (walkExact(loc, ms, until))
				return false;
			distance = Client.getLocalPlayer().getTile().distanceTo(ms);
		}
		return true;
	}

	public static boolean walkExact(Tile mm, Tile ms, Condition until) {
		if (Client.getLocalPlayer().getTile().distanceTo(ms) == 0 || until.check())
			return true;

		Point p = ms.toScreen();
		if (Calculations.onViewport(p)) {
			if (Walking.isMoving())
				return false;

			p = ms.toScreen();
			Mouse.moveMouse(p, 3, 3);
			Mouse.click(Mouse.LEFT_BUTTON);

			Time.sleepUntil(() -> until != null && until.check()
					|| Client.getLocalPlayer().getTile().distanceTo(ms) < Random.randomRange(4, 6)
					|| Walking.isMoving(), 600, 700);
			Time.sleepUntil(() -> {
				return until != null && until.check() || !Walking.isMoving()
						|| Client.getLocalPlayer().getTile().distanceTo(ms) < Random.randomRange(4, 6);
			}, 20000, 25000);
			return Client.getLocalPlayer().getTile().distanceTo(ms) == 0;

		} else {
			p = mm.toMiniMap();
			if (Calculations.onMiniMap(p)) {
				int stops = Random.randomRange(0, 2);

				for (int i = 0; i < stops; i++) {
					Mouse.moveGeneral(p);
				}

				p = mm.toMiniMap();
				if (Calculations.onMiniMap(p)) {
					Mouse.moveMouse(p, 0, 0);
					Mouse.click(Mouse.LEFT_BUTTON);

					Time.sleepUntil(() -> Walking.isMoving(), 600, 700);
					Time.sleepUntil(() -> {
						return until != null && until.check() || !Walking.isMoving()
								|| Client.getLocalPlayer().getTile().distanceTo(mm) < Random.randomRange(4, 6);
					}, 20000, 25000);

				}
				return Client.getLocalPlayer().getTile().distanceTo(mm) >= Random.randomRange(4, 6);
			}
		}
		return true;
	}
}
