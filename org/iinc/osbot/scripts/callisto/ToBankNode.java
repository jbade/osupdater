package org.iinc.osbot.scripts.callisto;

import java.util.logging.Level;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.interfaces.tabs.Equipment;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.MouseMovementHandler;
import org.iinc.osbot.scripts.common.Navigation;
import org.iinc.osbot.scripts.gdk.GDK;
import org.iinc.osbot.util.ArrayUtil;

public class ToBankNode implements CallistoNode {

	@Override
	public boolean activate(Npc callisto, int wildernessLevel, Tile playerLocation) {
		return wildernessLevel < 1 
				&& !Navigation.atLumbridgeBank() && !Navigation.atGrandExchange()
				&& playerLocation.distanceTo(Callisto.EDGEVILLE_WALK_TILE) >= 3;
	}

	@Override
	public void execute(Npc callisto, int wildernessLevel, Tile playerLocation) {
		MouseMovementHandler.DEFAULT_MOUSE_SPEED = 7;
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		if (playerLocation.distanceTo(Callisto.EDGEVILLE_WALK_TILE) < 11
				&& playerLocation.distanceTo(Callisto.EDGEVILLE_WALK_TILE) >= 3) {
			Walking.blindWalk(Callisto.EDGEVILLE_WALK_TILE);
			return;
		}

		if (!Equipment.open())
			return;
		Time.sleep(600, 1200);

		int amulet = Equipment.getAmulet();
		boolean gamesEquipped = ArrayUtil.contains(Callisto.GAMES_NECKLACES_IDS, amulet);

		if (gamesEquipped) {
			if (Clicking.interact(Equipment.getAmuletWidget(), "Barbarian Outpost")
					&& Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 714, 600, 700)) {
				Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != 714, 3000, 4000);
				return;
			}
		} else {
			if (Inventory.contains(Callisto.GAMES_NECKLACES_IDS)) {
				Clicking.interact(Inventory.getFirst(Callisto.GAMES_NECKLACES_IDS), "Wear");
				int invCount = Inventory.getCount();
				Time.sleepUntil(() -> invCount != Inventory.getCount(), 600, 800);
				return;
			}

			// use glory
			boolean gloryEquipped = ArrayUtil.contains(Callisto.CHARGED_GLORY_IDS, amulet);
			boolean hasGlory = Inventory.contains(Callisto.CHARGED_GLORY_IDS);

			if (gloryEquipped) {
				if (Clicking.interact(Equipment.getAmuletWidget(), "Edgeville")
						&& Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 714, 600, 700)) {
					Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != 714, 3000, 4000);
					return;
				}
			} else if (hasGlory) {
				Clicking.interact(Inventory.getFirst(Callisto.CHARGED_GLORY_IDS), "Wear");
				int invCount = Inventory.getCount();
				Time.sleepUntil(() -> invCount != Inventory.getCount(), 600, 800);
				return;
			} else {
				Navigation.toLumbridgeBank();
			}
		}

	}

}
