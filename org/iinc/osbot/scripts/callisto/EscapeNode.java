package org.iinc.osbot.scripts.callisto;

import java.util.Arrays;
import java.util.logging.Level;

import org.iinc.osbot.api.base.interfaces.tabs.Equipment;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.Player;
import org.iinc.osbot.api.injection.accessors.renderable.Players;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.MouseMovementHandler;
import org.iinc.osbot.scripts.common.Navigation;

public class EscapeNode implements CallistoNode {
	
	@Override
	public boolean activate(Npc callisto, int wildernessLevel, Tile playerLocation) {
		if (wildernessLevel < 1)
			return false;
		
		if (Callisto.playerSpotted)
			return true;
		
		int cb = Client.getLocalPlayer().getCombatLevel();
		int minLevel = cb - wildernessLevel; // TODO are these correct?
		int maxLevel = cb + wildernessLevel;

		Object localPlayerReference = Client.getLocalPlayer().getReference();
		for (Player p : Players.getAll()) {
			int c = p.getCombatLevel();
			if (c >= minLevel && c <= maxLevel && p.getReference() != localPlayerReference) {
				Callisto.playerSpotted = true;
				return true;
			}
		}
		
		return false;
	}

	private static int teleportAttempts = 0;

	private  static int incrementTeleportAttempts() {
		return ++teleportAttempts;
	}

	@Override
	public void execute(Npc callisto, int wildernessLevel, Tile playerLocation) {
		MouseMovementHandler.DEFAULT_MOUSE_SPEED = 2;
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		//System.out.println(callisto == null);
		//System.out.println(wildernessLevel);
		//System.out.println(playerLocation);
		teleportAttempts = 0;
		while ( Client.isLoggedIn() && Client.getWildernessLevel() > -1) {
			Logout.clickLogout();
			Walking.blindWalk(Callisto.WILDERNESS_DITCH_TILE, () -> {
				int level = Client.getWildernessLevel();
				if (level != -1 && level <= 30 && incrementTeleportAttempts() <= 3) {
					if (!Arrays.asList(Callisto.CHARGED_GLORY_IDS).contains(Equipment.getAmulet())) {
						Clicking.interact(Inventory.getFirst(Callisto.CHARGED_GLORY_IDS), "Wear");
						int invCount = Inventory.getCount();
						Time.sleepUntil(() -> invCount != Inventory.getCount(), 600, 800);
					}
					
					Clicking.interact(Equipment.getAmuletWidget(), "Edgeville");
				}

				Logout.clickLogout();
				WalkingCondition.get().check();
				
				return !Client.isLoggedIn() || Client.getLocalPlayer().getTile().distanceTo(Navigation.LUMBRIDGE_STAIRS_GROUND_FLOOR_TILE) < 70;
			});
		}
		
		Bot.LOGGER.log(Level.FINE, "Switching worlds");
		System.exit(0);
	}

}
