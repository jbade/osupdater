package org.iinc.osbot.scripts.callisto;

import java.util.logging.Level;
import java.util.stream.Collectors;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.CombatOptions;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.NpcDefinition;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.MouseMovementHandler;
import org.iinc.osbot.scripts.common.Antiban;
import org.iinc.osbot.util.Email;

public class AttackCallistoNode implements CallistoNode {

	@Override
	public boolean activate(Npc callisto, int wildernessLevel, Tile playerLocation) {
		// just putting this in after escape node
		if (Npcs.getAll().stream().filter((npc) -> {
			if (npc == null)
				return false;
			NpcDefinition def = npc.getNpcDefinition();
			if (def == null)
				return false;
			String name = def.getName();
			return name != null && name.equals("Callisto cub");
		}).collect(Collectors.toList()).size() > 0){
			Bot.LOGGER.log(Level.FINE, "Cub?");
			Email.sendEmail(Email.TYPE.SEVERE, "Cub?");
			System.exit(Settings.DISABLED_EXIT_CODE);
		}
		
		
		return wildernessLevel > 1 && callisto != null && callisto.getTile().getX() >= 3333
				&& playerLocation.distanceTo(Callisto.CALLISTO_LURE_TILE) < 10
				&& (Client.getLocalPlayer().getInteractingIndex() == callisto.getIndex()
						|| !callisto.isBeingInteractedWith());
	}

	@Override
	public void execute(Npc callisto, int wildernessLevel, Tile playerLocation) {
		MouseMovementHandler.DEFAULT_MOUSE_SPEED = 7;
		

		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		if (Levels.getCurrentAttack() - Levels.getAttack() <= 10 && Inventory.contains(Callisto.ATTACK_POT_IDS)) {
			Clicking.interact(Inventory.getFirst(Callisto.ATTACK_POT_IDS), "Drink");
			Time.sleep(200, 300);
			Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != -1, 500, 600);
			return;
		}
		if (Levels.getCurrentStrength() - Levels.getStrength() <= 9 && Inventory.contains(Callisto.STRENGTH_POT_IDS)) {
			Clicking.interact(Inventory.getFirst(Callisto.STRENGTH_POT_IDS), "Drink");
			Time.sleep(200, 300);
			Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != -1, 500, 600);
			return;
		}

		if (!Client.getLocalPlayer().isInteracting() && callisto.getAnimation() != 4929) { // dying animation
			// TODO handle players attacking callisto that we dont run from
			System.out.println("Attacking callisto");
			Clicking.interact(callisto, "Attack", "Callisto  (level-470)");
			Time.sleep(300, 500);
			return;
		}

		if (CombatOptions.getSpecialAttackPercent() >= Callisto.WEAPON_SPEC_PERCENT
				&& !CombatOptions.isSpecialAttackEnabled()) {
			CombatOptions.clickSpecialAttack();
			Time.sleepUntil(() -> CombatOptions.isSpecialAttackEnabled(), 1200, 1600);
			return;
		}

		// TODO improve
		//Antiban.disableRandomMouseMovement();
		//Antiban.disableRandomTinyMouseMovement();
		//Antiban.tick();
		//Antiban.enableRandomMouseMovement();
		//Antiban.enableRandomTinyMouseMovement();
		
		Logout.open();
		Clicking.hover(Logout.getLogoutWidget());
		Time.sleep(100, 200);
	}

}
