package org.iinc.osbot.scripts.callisto;

import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.NpcDefinition;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Keyboard;
import org.iinc.osbot.input.MouseMovementHandler;
import org.iinc.osbot.util.Email;

public class ExitCorpCaveNode implements CallistoNode {

	static final int[] EXIT_CORP_WIDGET = new int[] { 219, 0 };

	@Override
	public boolean activate(Npc callisto, int wildernessLevel, Tile playerLocation) {
		return playerLocation.distanceTo(Callisto.EXIT_CORP_TILE) < 20;
	}

	@Override
	public void execute(Npc callisto, int wildernessLevel, Tile playerLocation) {
		MouseMovementHandler.DEFAULT_MOUSE_SPEED = 7;
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		if (Widgets.exists(EXIT_CORP_WIDGET)) {
			Keyboard.sendKey(KeyEvent.VK_1);
			Time.sleepUntil(() -> Client.getWildernessLevel() != -1, 3000, 4000);
			return;
		}
		

		// turn camera so clickbox is larger
		if (Camera.getCameraAngle() > Math.PI * 3 / 4.0 || Camera.getCameraAngle() < Math.PI / 4.0)
			Camera.setAngle(Math.random() * Math.PI / 2.0 + Math.PI / 4.0, true);

		if (!Calculations.onViewport(Callisto.EXIT_CORP_TILE.toScreen())) {
			Walking.walk(Callisto.EXIT_CORP_TILE);
		} else if (Clicking.interact(Callisto.EXIT_CORP_TILE, "Exit", "Cave exit")) {
			Time.sleepUntil(() -> Widgets.exists(EXIT_CORP_WIDGET), 3000, 6000, 600);
			if (Widgets.exists(EXIT_CORP_WIDGET)) {
				Keyboard.sendKey(KeyEvent.VK_1);
				Time.sleepUntil(() -> Client.getWildernessLevel() != -1, 3000, 4000);
			}
		}
	}
}
