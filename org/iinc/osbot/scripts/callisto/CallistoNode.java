package org.iinc.osbot.scripts.callisto;

import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;

public interface CallistoNode {
	public boolean activate(Npc callisto, int wildernessLevel, Tile playerLocation);

	public void execute(Npc callisto, int wildernessLevel, Tile playerLocation);
}
