package org.iinc.osbot.scripts.callisto;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.NpcDefinition;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.renderable.Player;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.MouseMovementHandler;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Antiban;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.Random;

public class Callisto extends Script implements PaintListener {

	private ArrayList<CallistoNode> nodes = new ArrayList<>();

	static boolean playerSpotted = false;
	static int looted = 0;

	static final int[] FOOD_IDS = new int[] { Items.SHARK.getId(), Items.TUNA.getId() };
	static final int[] FOOD_HEAL_AMOUNTS = new int[] { 20, 10 };
	static final int[] STAMINA_POTS_IDS = new int[] { Items.STAMINA_POTION_4.getId(), Items.STAMINA_POTION_3.getId(),
			Items.STAMINA_POTION_2.getId(), Items.STAMINA_POTION_1.getId() };
	static final int[] ANTIPOISON_POTS_IDS = new int[] { Items.ANTIDOTE_PLUS_PLUS_4.getId(),
			Items.ANTIDOTE_PLUS_PLUS_3.getId(), Items.ANTIDOTE_PLUS_PLUS_2.getId(),
			Items.ANTIDOTE_PLUS_PLUS_1.getId() };
	static final int[] ATTACK_POT_IDS = new int[] { Items.SUPER_ATTACK_POTION_4.getId(),
			Items.SUPER_ATTACK_POTION_3.getId(), Items.SUPER_ATTACK_POTION_2.getId(),
			Items.SUPER_ATTACK_POTION_1.getId() };
	static final int[] STRENGTH_POT_IDS = new int[] { Items.SUPER_STRENGTH_POTION_4.getId(),
			Items.SUPER_STRENGTH_POTION_3.getId(), Items.SUPER_STRENGTH_POTION_2.getId(),
			Items.SUPER_STRENGTH_POTION_1.getId() };
	static final int[] CHARGED_GLORY_IDS = new int[] { Items.AMULET_OF_GLORY_1.getId(), Items.AMULET_OF_GLORY_2.getId(),
			Items.AMULET_OF_GLORY_3.getId(), Items.AMULET_OF_GLORY_4.getId(), Items.AMULET_OF_GLORY_5.getId(),
			Items.AMULET_OF_GLORY_6.getId() };
	static final int[] GAMES_NECKLACES_IDS = new int[] { Items.GAMES_NECKLACE_1.getId(), Items.GAMES_NECKLACE_2.getId(),
			Items.GAMES_NECKLACE_3.getId(), Items.GAMES_NECKLACE_4.getId(), Items.GAMES_NECKLACE_5.getId(),
			Items.GAMES_NECKLACE_6.getId(), Items.GAMES_NECKLACE_7.getId(), Items.GAMES_NECKLACE_8.getId() };

	static final Tile CALLISTO_TILE = new Tile(3296, 3849, 0);
	static final Tile EXIT_CORP_TILE = new Tile(2963, 4382, 2);
	static final Tile CALLISTO_LURE_TILE = new Tile(3336, 3816, 0);
	static final Tile WILDERNESS_DITCH_TILE = new Tile(3306, 3519, 0);
	static final Tile LOGOUT_TILE = new Tile(3347, 3844, 0);
	static final Tile LUMBRIDGE_BANK_TILE = new Tile(3209, 3220, 2);
	static final Tile EDGEVILLE_BANK_TILE = new Tile(3094, 3490, 0);
	static final Tile EDGEVILLE_WALK_TILE = new Tile(3093, 3490, 0);

	static final Tile LUMBRIDGE_BANK_TILE_1 = new Tile(3209, 3221, 2);
	static final Tile LUMBRIDGE_BANK_TILE_2 = new Tile(3208, 3221, 2);
	static final Tile BARBARIAN_OUTPOST_BANK_TILE = new Tile(2537, 3573, 0);
	static final Tile EDGEVILLE_BANK_TILE_1 = new Tile(3095, 3491, 0);
	static final Tile EDGEVILLE_BANK_TILE_2 = new Tile(3095, 3489, 0);

	public static final int WEAPON_SPEC_PERCENT = 65;

	@Override
	public int loop() {

		Interfaces.closeAll();

		if (Inventory.isItemSelected()) {
			Menu.interact("Cancel");
		}

		List<Npc> npcs = Npcs.getAll().stream().filter((npc) -> {
			if (npc == null)
				return false;
			NpcDefinition def = npc.getNpcDefinition();
			if (def == null)
				return false;
			String name = def.getName();
			return name != null && name.equals("Callisto");
		}).collect(Collectors.toList());
		Npc callisto = npcs.size() > 0 ? npcs.get(0) : null;
		int wildernessLevel = Client.getWildernessLevel();
		Player p = Client.getLocalPlayer();
		Tile loc = p != null ? p.getTile() : new Tile(-1, -1, -1);
		// System.out.println("lvl " + wildernessLevel);
		for (CallistoNode node : nodes) {
			if (node.activate(callisto, wildernessLevel, loc)) {
				node.execute(callisto, wildernessLevel, loc);
				return 0;
			}
		}

		return Random.randomRange(50, 100);
	}

	@Override
	public void onStart() {
		Bot.LOGGER.log(Level.INFO, "Starting callisto");
		Collections.addAll(nodes, new LoginNode(), new EscapeNode(), new AttackCallistoNode(), new LootNode(),
				new ExitWildernessNode(), new WalkToCallistoNode(), new ExitCorpCaveNode(), new HopWorldsNode(),
				new LureNode(), new ToBankNode(), new BankingNode());

		// Antiban.disableAltTab();
		// Antiban.disableExamineItem();
		// Antiban.disableHoverSkill();
		// Antiban.disableRandomSleep();
		Settings.MAKE_CLICKING_ERRORS = false;
	}

	@Override
	public void onStop() {

	}

	@Override
	public void onDraw(Graphics g) {

	}

}
