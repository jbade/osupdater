package org.iinc.osbot.scripts.callisto;

import java.awt.event.KeyEvent;
import java.util.logging.Level;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.tabs.Equipment;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Keyboard;
import org.iinc.osbot.input.MouseMovementHandler;
import org.iinc.osbot.scripts.common.Navigation;
import org.iinc.osbot.scripts.gdk.GrandExchangeNode;
import org.iinc.osbot.util.ArrayUtil;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Email;
import org.iinc.osbot.util.Email.TYPE;
import org.iinc.osbot.util.Memory;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class BankingNode implements CallistoNode {

	private static final InvSupply[] INV_SUPPLIES = new InvSupply[] {
			new InvSupply(1, Items.GAMES_NECKLACE_1, Items.GAMES_NECKLACE_2, Items.GAMES_NECKLACE_3,
					Items.GAMES_NECKLACE_4, Items.GAMES_NECKLACE_5, Items.GAMES_NECKLACE_6, Items.GAMES_NECKLACE_7,
					Items.GAMES_NECKLACE_8),
			new InvSupply(1, Items.STAMINA_POTION_4, Items.STAMINA_POTION_3, Items.STAMINA_POTION_2,
					Items.STAMINA_POTION_1),
			new InvSupply(1, Items.SUPER_ATTACK_POTION_4, Items.SUPER_ATTACK_POTION_3, Items.SUPER_ATTACK_POTION_2,
					Items.SUPER_ATTACK_POTION_1),
			new InvSupply(1, Items.SUPER_STRENGTH_POTION_4, Items.SUPER_STRENGTH_POTION_3,
					Items.SUPER_STRENGTH_POTION_2, Items.SUPER_STRENGTH_POTION_1),
			new InvSupply(1, Items.ANTIDOTE_PLUS_PLUS_1, Items.ANTIDOTE_PLUS_PLUS_2, Items.ANTIDOTE_PLUS_PLUS_3,
					Items.ANTIDOTE_PLUS_PLUS_4),
			new InvSupply(15, Items.TUNA), new InvSupply(8, Items.SHARK) };

	private static Equip GLOVES = new Equip(Items.COMBAT_BRACLET_4);
	private static Equip WEAPON = new Equip(Items.BANDOS_GODSWORD);
	private static Equip AMULET = new Equip(Items.AMULET_OF_GLORY_1, Items.AMULET_OF_GLORY_2, Items.AMULET_OF_GLORY_3,
			Items.AMULET_OF_GLORY_4, Items.AMULET_OF_GLORY_5, Items.AMULET_OF_GLORY_6);

	static final int[] GAMES_NECK_WIDGET = new int[] { 219, 0 };

	@Override
	public boolean activate(Npc callisto, int wildernessLevel, Tile playerLocation) {
		return wildernessLevel < 1 && (Navigation.atLumbridgeBank()
				|| playerLocation.distanceTo(Callisto.EDGEVILLE_WALK_TILE) < 3 || Navigation.atGrandExchange());
	}

	boolean outOfGloves = false;
	boolean outOfWeapon = false;
	boolean outOfAmulet = false;
	int out = 0;

	@Override
	public void execute(Npc callisto, int wildernessLevel, Tile playerLocation) {
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		MouseMovementHandler.DEFAULT_MOUSE_SPEED = 7;

		Memory.checkMemory(1000000); // 1 MB

		if (BreakHandler.shouldBreak())
			System.exit(0);

		Callisto.looted = 0;

		boolean acted = false;
		boolean needGloves = !outOfGloves && !ArrayUtil.contains(GLOVES.ids, Equipment.getGloves());
		boolean needWeapon = !outOfWeapon && !ArrayUtil.contains(WEAPON.ids, Equipment.getWeapon());
		boolean needAmulet = !outOfAmulet && !ArrayUtil.contains(AMULET.ids, Equipment.getAmulet());

		if (needGloves || needWeapon || needAmulet) {
			Time.sleep(800, 1200);
			needGloves = !ArrayUtil.contains(GLOVES.ids, Equipment.getGloves());
			needWeapon = !ArrayUtil.contains(WEAPON.ids, Equipment.getWeapon());
			needAmulet = !ArrayUtil.contains(AMULET.ids, Equipment.getAmulet());
		}

		Timer t = new Timer(Random.randomRange(15000, 20000));

		boolean equipped = false;
		if (needGloves && Inventory.contains(GLOVES.ids)) {
			Clicking.interact(Inventory.getFirst(GLOVES.ids), "Wear");
			acted = true;
			equipped = true;
		}
		if (needWeapon && Inventory.contains(WEAPON.ids)) {
			Clicking.interact(Inventory.getFirst(WEAPON.ids), "Wield");
			acted = true;
			equipped = true;
		}
		if (needAmulet && Inventory.contains(AMULET.ids)) {
			Clicking.interact(Inventory.getFirst(AMULET.ids), "Wear");
			acted = true;
			equipped = true;
		}

		if (equipped) {
			Time.sleep(Random.modifiedRandomGuassianRange(600, 900, ConfidenceIntervals.CI_70, 400,
					Modifiers.get("Callisto.sleepEquipped")));
		}

		boolean needSupplies = false;
		for (InvSupply inv : INV_SUPPLIES) {
			int quantity = 0;
			for (int i = 0; i < inv.ids.length; i++) {
				quantity += Inventory.getCount(inv.ids[i]);
			}
			if (quantity != inv.quantity)
				needSupplies = true;
		}

		Bot.LOGGER.log(Level.FINEST, String.format("Need [gloves: %B, weapon: %B, amulet: %B] [supplies: %B] acted: %B",
				needGloves, needWeapon, needAmulet, needSupplies, acted));

		if (acted)
			return;

		if (needGloves || needWeapon || needAmulet || needSupplies) {
			Bot.LOGGER.log(Level.FINEST, "Opening bank");

			if (Bank.openBooth("Bank", Callisto.LUMBRIDGE_BANK_TILE_1, Callisto.LUMBRIDGE_BANK_TILE_2)
					|| Bank.openBooth("Bank", Callisto.EDGEVILLE_BANK_TILE_1, Callisto.EDGEVILLE_BANK_TILE_2)
					|| Bank.openBanker()) {
				if (!Bank.isOpen())
					return;

				// deposit items or wait for bank to load
				if (Inventory.getCount() == 0) {
					Time.sleep(600, 1200);
				} else if ((!Bank.depositInventory() || !Time.sleepUntil(() -> Inventory.getCount() == 0, 1200, 1700)))
					return;

				boolean outOfSupplies = false;

				if (needGloves || needWeapon || needAmulet) {
					if (needGloves) {
						if (Bank.contains(GLOVES.ids)) {
							if (!Bank.withdraw(GLOVES.ids, 1))
								return;
						} else {
							needGloves = true;
							Bot.LOGGER.log(Level.FINEST, "Out of GLOVES");
						}
					}
					if (needWeapon) {
						if (Bank.contains(WEAPON.ids)) {
							if (!Bank.withdraw(WEAPON.ids, 1))
								return;
						} else {
							outOfSupplies = true;
							Bot.LOGGER.log(Level.FINEST, "Out of WEAPON");
						}
					}
					if (needAmulet) {
						if (Bank.contains(AMULET.ids)) {
							if (!Bank.withdraw(AMULET.ids, 1))
								return;
						} else {
							outOfAmulet = true;
							Bot.LOGGER.log(Level.FINEST, "Out of AMULET");
						}
					}
				} else { // supplies
					for (InvSupply inv : INV_SUPPLIES) {
						if (Inventory.isFull())
							return;
						if (Bank.getQuantity(inv.ids) >= inv.quantity) {
							if (!Bank.withdraw(inv.ids, inv.quantity))
								return;
						} else {
							outOfSupplies = true;
						}
					}
					Time.sleep(500, 800);
				}

				if (outOfSupplies) {
					if (out++ < 3) {
						return;
					}

					Email.sendEmail(TYPE.SEVERE, "Out of supplies callisto");
					System.exit(Settings.DISABLED_EXIT_CODE);
					if (!Navigation.atGrandExchange() && Bank.contains(Items.VARROCK_TELEPORT.getId())) {
						if (Inventory.getCount() == 0) {
							Time.sleep(600, 1200);
						} else if ((!Bank.depositInventory()
								|| !Time.sleepUntil(() -> Inventory.getCount() == 0, 1200, 1700)))
							return;

						if (!Bank.withdraw(Items.VARROCK_TELEPORT.getId(), 1))
							return;

						if (!Bank.close())
							return;

						Time.sleep(Random.randomRange(400, 500));

						Item item = Inventory.getFirst(Items.VARROCK_TELEPORT.getId());
						if (item != null) {
							if (Clicking.interact(item, "Break")) {
								Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == 4069
										|| Client.getLocalPlayer().getAnimation() == 4071, 600, 1200);
								Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() == -1, 3000, 4000);
								GrandExchangeNode.start();
								return;
							}
						}
					} else {
						GrandExchangeNode.start();
						return;
					}
				}
				out = 0;
			}
		} else {
			// done banking
			if (Clicking.interact(Inventory.getFirst(Callisto.GAMES_NECKLACES_IDS), "Rub")) {
				Time.sleepUntil(() -> Widgets.exists(GAMES_NECK_WIDGET), 3000, 6000, 600);
				if (Widgets.exists(GAMES_NECK_WIDGET)) {
					Time.sleepReactionTime();
					Keyboard.sendKey(KeyEvent.VK_3);
					Time.sleepUntil(() -> Client.getLocalPlayer().getTile().distanceTo(Callisto.EXIT_CORP_TILE) < 20,
							5000, 7000);
					return;
				}
			}
		}

	}

	private static final class InvSupply {
		int[] ids;
		int quantity;

		public InvSupply(int quantity, RSItemConstant... items) {
			this.quantity = quantity;

			ids = new int[items.length];
			for (int i = 0; i < items.length; i++)
				ids[i] = items[i].getId();
		}
	}

	private static final class Equip {
		int[] ids;

		public Equip(RSItemConstant... items) {
			ids = new int[items.length];
			for (int i = 0; i < items.length; i++)
				ids[i] = items[i].getId();
		}
	}

}
