package org.iinc.osbot.scripts.callisto;

import java.util.List;
import java.util.logging.Level;

import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.grounditem.GroundItem;
import org.iinc.osbot.api.injection.accessors.grounditem.GroundItems;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.MouseMovementHandler;

public class LootNode implements CallistoNode {

	@Override
	public boolean activate(Npc callisto, int wildernessLevel, Tile playerLocation) {
		return wildernessLevel > 0 && callisto == null && playerLocation.distanceTo(Callisto.CALLISTO_LURE_TILE) < 10
				&& GroundItems.getAll(6).size() > 0 && (!Inventory.isFull() || Inventory.contains(Callisto.FOOD_IDS));
	}

	@Override
	public void execute(Npc callisto, int wildernessLevel, Tile playerLocation) {
		MouseMovementHandler.DEFAULT_MOUSE_SPEED = 7;
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());
		List<GroundItem> items = GroundItems.getAll(6);
		if (items.size() > 0) {
			if (Inventory.isFull()) {
				if (Inventory.contains(Callisto.FOOD_IDS)) {
					Inventory.open();
					Clicking.interact(Inventory.getFirst(Callisto.FOOD_IDS), "Eat");
					Time.sleep(600, 1100);
				}
				return;
			}

			if (Clicking.interact(items.get(0), "Take", null)) {
				Callisto.looted++; // TODO handle loot tracking
				int invCount = Inventory.getCount();
				Time.sleepUntil(() -> Inventory.getCount() != invCount, 2000, 3000);
			} else {
				Bot.LOGGER.log(Level.FINEST, "Walk camera");
				Walking.walk(items.get(0).getTile());
				Camera.turnTo(items.get(0).getTile());
			}
		}
	}
}
