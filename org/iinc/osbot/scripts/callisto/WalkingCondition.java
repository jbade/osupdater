package org.iinc.osbot.scripts.callisto;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Condition;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Player;
import org.iinc.osbot.api.injection.accessors.renderable.Players;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.MouseMovementHandler;
import org.iinc.osbot.scripts.common.Navigation;

public class WalkingCondition implements Condition {
	private static WalkingCondition condition = new WalkingCondition();

	public static WalkingCondition get() {
		return condition;
	}

	// return true to stop walking
	@Override
	public boolean check() {
		int tmp = MouseMovementHandler.DEFAULT_MOUSE_SPEED;
		MouseMovementHandler.DEFAULT_MOUSE_SPEED = Math.min(7, tmp);
		
		// we died TODO check animation
		if (Client.getLocalPlayer().getTile().distanceTo(Navigation.LUMBRIDGE_STAIRS_GROUND_FLOOR_TILE) < 70)
			return true;

		// check for players
		if (!Callisto.playerSpotted) {
			int cb = Client.getLocalPlayer().getCombatLevel();
			int wild = Client.getWildernessLevel();

			int minLevel = cb - wild;
			int maxLevel = cb - wild;

			for (Player p : Players.getAll()) {
				int c = p.getCombatLevel();
				if (c >= minLevel && c <= maxLevel) {
					Callisto.playerSpotted = true;
					return true;
				}
			}
		}

		int diff = Levels.getHitpoints() - Levels.getCurrentHitpoints();
		for (int i = 0; i < Callisto.FOOD_HEAL_AMOUNTS.length; i++) {
			if (diff >= Callisto.FOOD_HEAL_AMOUNTS[i] && Inventory.contains(Callisto.FOOD_IDS[i])) {
				Clicking.interact(Inventory.getFirst(Callisto.FOOD_IDS[i]), "Eat");
				break;
			}
		}
	
		if (!Walking.isStaminaActive() && Inventory.contains(Callisto.STAMINA_POTS_IDS)) {
			Clicking.interact(Inventory.getFirst(Callisto.STAMINA_POTS_IDS), "Drink");
		} else if (Walking.getRunEnergy() > 10 && !Walking.isRunEnabled()) {
			Walking.enableRun();
		} else if (Client.isPoisoned() && Inventory.contains(Callisto.ANTIPOISON_POTS_IDS)) {
			Clicking.interact(Inventory.getFirst(Callisto.ANTIPOISON_POTS_IDS), "Drink");
		}
		


		MouseMovementHandler.DEFAULT_MOUSE_SPEED = tmp;
		return Callisto.playerSpotted;
	}

}
