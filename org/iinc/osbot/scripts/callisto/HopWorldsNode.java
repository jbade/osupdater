package org.iinc.osbot.scripts.callisto;

import java.util.logging.Level;

import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Logout;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.MouseMovementHandler;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class HopWorldsNode implements CallistoNode {

	@Override
	public boolean activate(Npc callisto, int wildernessLevel, Tile playerLocation) {
		return wildernessLevel > 0 && ((callisto != null && callisto.getTile().distanceTo(Callisto.CALLISTO_TILE) > 10)
				|| (callisto == null && playerLocation.distanceTo(Callisto.CALLISTO_TILE) < 5));
	}

	@Override
	public void execute(Npc callisto, int wildernessLevel, Tile playerLocation) {
		MouseMovementHandler.DEFAULT_MOUSE_SPEED = 7;
		Bot.LOGGER.log(Level.INFO, this.getClass().getName());

		while (Client.getLocalPlayer().getTile().distanceTo(Callisto.LOGOUT_TILE) > 5 && Login.isLoggedIn()) {
			Walking.blindWalk(Callisto.LOGOUT_TILE.randomized(3, 3), () -> {
				if (!Login.isLoggedIn() || WalkingCondition.get().check()) {
					return true;
				}

				Logout.clickLogout();

				return Client.getLocalPlayer().getTile().distanceTo(Callisto.LOGOUT_TILE) <= 5;
			});

			if (Callisto.playerSpotted)
				return;
		}

		if (Client.getLocalPlayer().getTile().distanceTo(Callisto.LOGOUT_TILE) <= 5 || !Login.isLoggedIn()) {
			Bot.LOGGER.log(Level.FINE, "Logging out and getting new world");
			Timer t = new Timer(Random.randomRange(15 * 1000, 20 * 1000));
			while (Login.isLoggedIn() && t.isRunning()) {
				if (WalkingCondition.get().check())
					return;

				Logout.clickLogout();
				Time.sleep(600, 1200);
			}

			if (!Login.isLoggedIn())
				System.exit(0);
			else
				this.execute(callisto, wildernessLevel, playerLocation);
		}
	}

}
