package org.iinc.osbot.scripts.dmm;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.tutorial.ClickContinue;
import org.iinc.osbot.scripts.tutorial.start.PlayerDesign;

public class TutorialIslandNode implements Node {

	String location;
	PlayerDesign pd = new PlayerDesign();
	ClickContinue cc = new ClickContinue();

	public TutorialIslandNode(String location) {
		this.location = location;
	}

	@Override
	public boolean activate() {
		return Widgets.exists(371, 0);
	}

	@Override
	public void execute() {
		Settings.MAKE_CLICKING_ERRORS = false;
		
		if (pd.activate()) {
			pd.execute();
			return;
		}

		Widget w = Widgets.getWidget(219, 0);
		if (w != null && !w.isHidden()) {
			for (Widget child : w.getChildren()) {
				if (child != null) {
					String text = child.getText();
					if (text != null) {
						if (location.equals(text) || "I am brand new! This is my first time here.".equals(text)
								|| "Send me to the mainland now.".equals(text)) {
							Clicking.interact(child);
							break;
						}
					}

				}
			}

			Time.sleep(600, 1200);
			return;
		}

		if (cc.activate()) {
			cc.execute();
			return;
		}

		if (Interacting.interact(() -> Npcs.getClosest("Skippy"), null, "Talk-to")) {
			Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting() && !Walking.isMoving());
			return;
		}

		System.out.println("ti2");
	}

}
