package org.iinc.osbot.scripts.dmm.silkstall;

import java.util.concurrent.ThreadLocalRandom;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.interfaces.Trading;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.scripts.dmm.silkseller.DMMSilkSeller;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class TradeNode implements Node {

	public static String[] names = new String[] { "Cke Runs De", "HUGH MUNGASS", "Ijufj", "Poker Tets", "Margins Alt",
			"Lamblionmax", "Eweswordbat", "FearElf" };
	public static String[] interact_names = new String[] { "Cke Runs De  (", "HUGH MUNGASS  (", "Ijufj  (",
			"Poker Tets  (", "Margins Alt  (", "Lamblionmax  (", "Eweswordbat  (", "FearElf  (" };

	static Tile[] toStall = new Tile[] { new Tile(2584, 3296, 0), new Tile(2598, 3296, 0), new Tile(2609, 3296, 0),
			new Tile(2629, 3296, 0), new Tile(2643, 3229, 0), new Tile(2638, 3244, 0), new Tile(2638, 3258, 0),
			new Tile(2642, 3267, 0), new Tile(2655, 3283, 0), new Tile(2644, 3284, 0), new Tile(2643, 3303, 0),
			DMMSilkSeller.loc };

	@Override
	public boolean activate() {
		return Inventory.isFull() && Inventory.contains(Items.SILK.getId())
				|| Inventory.getCount(Items.SILK.getId()) >= 27;
	}

	@Override
	public void execute() {
		Walking.disableRun();

		if (Client.getLocalPlayer().getTile().distanceTo(toStall[toStall.length - 1]) > 4) {
			Walking.walkPath(Walking.generatePath(toStall, 3, false), true);
			return;
		}

		if (!Calculations.onViewport(DMMSilkSeller.loc.toScreen())) {
			Walking.walk(DMMSilkSeller.loc);
			return;
		}

		if (Client.getLocalPlayer().getTile().equals(DMMSilkSeller.loc)) {
			Walking.walk(DMMSilkSeller.loc.randomized());
			return;
		}
		
		shuffleArray(interact_names);
		
		for (String name : interact_names) {
			if (Interacting.interact(DMMSilkSeller.loc, new String[] { "Trade with" }, new String[] { name }) && Time
					.sleepUntil(() -> Trading.isOpen() && Trading.getName() != null && !Trading.getName().equals(""),
							5000, 7000))
				break;
		}

		if (!(Trading.isOpen() && Trading.getName() != null && !Trading.getName().equals("")))
			return;

		boolean found = false;
		for (String name : names) {
			if (name.equalsIgnoreCase(Trading.getName())) {
				found = true;
				break;
			}
		}
		if (!found) {
			Interfaces.closeAll();
			return;
		}

		if (!Trading.offerAll(Items.SILK.getName())) {
			Interfaces.closeAll();
			return;
		}

		Timer t2 = new Timer(Random.randomRange(12000, 15000));
		while (Trading.isOpen() && t2.isRunning()) {
			Trading.accept();
			Time.sleep(600, 1200);
		}
	}

	static void shuffleArray(String[] ar) {
		// If running on Java 6 or older, use `new Random()` on RHS here
		java.util.Random rnd = ThreadLocalRandom.current();
		for (int i = ar.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);
			// Simple swap
			String a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}

}
