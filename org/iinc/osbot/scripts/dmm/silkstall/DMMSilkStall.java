package org.iinc.osbot.scripts.dmm.silkstall;

import org.iinc.osbot.script.Node;

public class DMMSilkStall implements Node {

	static Node[] nodes = new Node[] { new EatNode(), new EscapeNode(), new TradeNode(), new BankNode(),
			new StealNode() };

	@Override
	public boolean activate() {
		return true;
	}

	@Override
	public void execute() {
		for (Node n : nodes) {
			if (n.activate()) {
				n.execute();
				return;
			}
		}
	}

}
