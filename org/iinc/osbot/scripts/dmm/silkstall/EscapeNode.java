package org.iinc.osbot.scripts.dmm.silkstall;

import java.util.function.BooleanSupplier;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ArrayUtil;

public class EscapeNode implements Node {

	static Tile runTo = new Tile(2649, 3321, 0);
	static String[] runFrom = new String[] { "Guard", "Knight of Ardougne" };
	static EatNode eat = new EatNode();

	static BooleanSupplier run = () -> {
		Object player = Client.getLocalPlayer().getReference();
		return Npcs.getAll().parallelStream().filter((npc) -> {
			if (npc.getInteractingReference() != player)
				return false;
			String npcName = npc.getNpcDefinition().getName();
			return npcName != null && ArrayUtil.contains(runFrom, npcName);
		}).count() > 0;
	};

	@Override
	public boolean activate() {
		return run.getAsBoolean();
	}

	@Override
	public void execute() {
		Walking.enableRun();

		if (Calculations.onViewport(runTo.toScreen())) {
			Clicking.interact(runTo.randomized(1, 1), "Walk here");
		} else {
			// there is a knight that can follow you to the bank
			// fix this
			Walking.walkPath(Walking.generatePath(BankNode.toBank, 3, false), () -> !run.getAsBoolean(), true);
		}
	}

}
