package org.iinc.osbot.scripts.dmm.silkstall;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;

public class EatNode implements Node {
	static RSItemConstant[] food = new RSItemConstant[] { Items.BREAD, Items.CHOCOLATE_CAKE1, Items.CAKE1, Items.CAKE2,
			Items.CAKE1, Items.CAKE};
	static BankNode bank = new BankNode();

	@Override
	public boolean activate() {
		return Levels.getCurrentHitpoints() < Levels.getHitpoints() || Inventory.getCount(food) == 0;
	}

	@Override
	public void execute() {
		for (RSItemConstant item : food) {
			if (Inventory.contains(item.getId())) {
				Clicking.interact(Inventory.getFirst(item.getId()), "Eat");
				return;
			}
		}

		// no food
		bank.execute();
	}

}
