package org.iinc.osbot.scripts.dmm.silkstall;

import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.Bank;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.scripts.thieving.cakestall.CakeStall;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class BankNode implements Node {
	static Tile[] toBank = new Tile[] { new Tile(2668, 3312, 0), new Tile(2643, 3303, 0), new Tile(2644, 3284, 0),
			new Tile(2655, 3283, 0) };
	static EatNode eat = new EatNode();
	static RSItemConstant[] items = new RSItemConstant[] { Items.BREAD, Items.CHOCOLATE_CAKE1, Items.CAKE1, Items.CAKE2,
			Items.CAKE1, Items.CAKE, Items.SILK };

	@Override
	public boolean activate() {
		return !Inventory.containsOnly(items) || Inventory.getCount(EatNode.food) > 4
				|| (Client.getLocalPlayer().getTile().distanceTo(toBank[toBank.length - 1]) < 5
						&& (Inventory.isFull() || Inventory.getCount(EatNode.food) == 0));
	}

	@Override
	public void execute() {
		Walking.disableRun();

		if (Client.getLocalPlayer().getTile().distanceTo(toBank[toBank.length - 1]) > 3) {
			Walking.walkPath(Walking.generatePath(toBank, 3, false), true);
			return;
		}

		if (!Bank.openBooth(new Tile(2656, 3283, 0)))
			return;

		if (!Bank.depositInventory())
			return;

		for (RSItemConstant item : EatNode.food) {
			if (Bank.getQuantity(item.getId()) >= 4) {
				if (Bank.withdraw(item.getId(), 4))
					return;
			}
		}

		// out of food
		Timer t = new Timer(Random.randomRange(10 * 60 * 1000, 20 * 60 * 1000));
		CakeStall cs = new CakeStall();
		while (t.isRunning()) {
			if (!Login.isLoggedIn()) {
				Login.login();
				Camera.setAngle(Camera.getCameraAngle(), true);
				continue;
			}

			Interfaces.closeAll();

			cs.execute();
		}

	}

}
