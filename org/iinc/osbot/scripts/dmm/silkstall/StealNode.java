package org.iinc.osbot.scripts.dmm.silkstall;

import java.util.Optional;

import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.object.GameObject;
import org.iinc.osbot.api.injection.accessors.object.RSObjects;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class StealNode implements Node {
	static Tile stand = new Tile(2663, 3316, 0);

	// includes path from The Guns and Castle Knights
	static Tile[] toStall = new Tile[] { new Tile(2584, 3296, 0), new Tile(2598, 3296, 0), new Tile(2609, 3296, 0),
			new Tile(2629, 3296, 0), new Tile(2643, 3229, 0), new Tile(2638, 3244, 0), new Tile(2638, 3258, 0),
			new Tile(2642, 3267, 0), new Tile(2655, 3283, 0), new Tile(2644, 3284, 0), new Tile(2643, 3303, 0), stand };

	static int objectId = 11729;

	public static Timer t = new Timer(15000);

	@Override
	public boolean activate() {
		return Levels.getCurrentHitpoints() > 3;
	}

	@Override
	public void execute() {
		if (Client.getLocalPlayer().getTile().distanceTo(toStall[toStall.length - 1]) > 4) {
			Walking.disableRun();
			Walking.walkPath(Walking.generatePath(toStall, 3, false), true);
			return;
		}

		if (Client.getLocalPlayer().getTile().distanceTo(stand) > 1) {
			if (Clicking.interact(stand, "Walk here")
					&& Time.sleepUntil(() -> Walking.getDestinationTile() != null, 600, 1000))
				Time.sleepUntil(() -> !Walking.isMoving(), 600, 1000);
			return;
		}

		Walking.enableRun();

		Node[] nodes = new Node[] { new EatNode(), new EscapeNode(), new TradeNode(), new BankNode()};
		
		boolean run = true;
		
		while(run && Levels.getCurrentHitpoints() > 3 && Login.isLoggedIn()){
			for (Node n : nodes){
				if (n.activate())
					return;
			}
			
			
			Optional<GameObject> stall = RSObjects.getGameObjects(5).parallelStream()
					.filter((obj) -> obj.getObjectId() == objectId).findAny();
			if (stall.isPresent()) {
				// Time.sleep(25, 100);

				if (Menu.contains("Steal-from", "Silk stall")) {
					if (Menu.interact("Steal-from", "Silk stall")
							&& Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != -1, 600, 1000))
						Time.sleep(800, 1000);

				} else {
					if (Clicking.interact(stall.get(), "Steal-from")
							&& Time.sleepUntil(() -> Client.getLocalPlayer().getAnimation() != -1, 600, 1000))
						Time.sleep(800, 1000);

				}
			}
			Time.sleep(50);
		}
		
		
	}

}
