package org.iinc.osbot.scripts.dmm.silkseller;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.Memory;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class DMMSilkSellerScript extends Script {

	DMMSilkSeller rf = new DMMSilkSeller();

	@Override
	public int loop() {
		if (!Login.isLoggedIn()) {
			Login.login();
			Time.sleep(600, 1200);
			Camera.setAngle(Camera.getCameraAngle(), true);
			return 0;
		}

		//Interfaces.closeAll();

		if (rf.activate())
			rf.execute();

		return 100;
	}

	@Override
	public void onStop() {

	}

	@Override
	public void onStart() {
		Modifiers.init(Bot.username.hashCode());
		BreakHandler.init(Bot.username.hashCode());
		Settings.MAKE_CLICKING_ERRORS = false;
	}

}
