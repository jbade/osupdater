package org.iinc.osbot.scripts.dmm.silkseller;

import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;

public class DropNode implements Node {

	@Override
	public boolean activate() {
		return !Inventory.containsOnly(Items.COINS.getId(), Items.SILK.getId());
	}

	@Override
	public void execute() {
		for (Item i : Inventory.getItems()) {
			if (i.getId() != Items.COINS.getId() && i.getId() != Items.SILK.getId()) {
				Clicking.interact(i, "Drop");
			}
		}
	}

}
