package org.iinc.osbot.scripts.dmm.silkseller;

import java.util.ArrayList;
import java.util.List;

import org.iinc.osbot.api.base.interfaces.Trading;
import org.iinc.osbot.api.base.interfaces.tabs.ChatBox;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Keyboard;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class TradeNode implements Node {

	String[] names = new String[] { "Fairyace2203", "53holy2254", "chaosplx", "Magicarmarm", "Robarcher", "Deathsueput",
			"Knifelosspk", "Earlyflydig", "Debtlich", "9kuduranger", "Blastcast562", "28bumpy1130", "93oftenholy",
			"Evokejade532", "84minephrin", "99spell1940", "Coldlyre2786", "5tainted1468", "83fiyr1609", "45heated1109",
			"26walk2879", "5ragnardoom", "58silver1790", "1flyjatizso", "Umbrafoam13", "55slaying331", "Walkrule2160",
			"76towercake", "61uzer1392", "Pkpotion1586" };
	int[][] spaceWidgets = new int[][] { { 217, 2 }, { 231, 2 }, { 193, 2 } };

	boolean closed = false;
	
	@Override
	public boolean activate() {
		return true;
	}

	@Override
	public void execute() {
		for (int[] w : spaceWidgets) {
			if (Widgets.exists(w)) {
				Keyboard.sendKeys(" ");
				Time.sleep(600, 800);
				return;
			}
		}

		boolean traded = false;

		if (!Trading.isOpen()) {
			// find last trade widget from accounts
			Widget[] arr = Widgets.getWidgets()[162];
			for (int i = 44; i <= 45; i++) {
				Widget w = arr[i];

				if (w != null) {
					String name = w.getName();
					if (name != null && w.getActions().contains("Accept trade")) {
						// boolean cont = false;
						// for (String n : names) {
						// if (name.equals(n)) {
						// cont = true;
						// break;
						// }
						// }
						//
						// if (cont) {
						if (!Clicking.interact(w, "Accept trade")) {
							return;
						}

						traded = true;
						break;
						// }
					}
				}
			}

			if (!traded)
				return;
		}

		if (!Time.sleepUntil(
				() -> Walking.isMoving() || (Trading.isOpen() && Trading.getName() != null && !Trading.getName().trim().equals("")), 5000,
				6000))
			return;
		
		if (Walking.isMoving())
			return;

		boolean found = false;
		for (String name : names) {
			if (name.equalsIgnoreCase(Trading.getName().trim())) {
				found = true;
				break;
			}
		}
		if (!found) {
			System.out.println("\"" + Trading.getName() + "\"");
			Interfaces.closeAll();
			return;
		}

		Time.sleep(1200, 1600);

		Timer t2 = new Timer(Random.randomRange(12000, 15000));
		while (Trading.isOpen() && t2.isRunning()) {
			Trading.accept();
			Time.sleep(600, 1200);
		}
	}

}
