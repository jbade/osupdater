package org.iinc.osbot.scripts.dmm.silkseller;

import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Keyboard;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.script.Node;

public class SellNode implements Node {
	int[][] spaceWidgets = new int[][] { { 217, 2 }, { 231, 2 } };
	int[][] threeWidgets = new int[][] { { 219, 0, 3 } };
	int[][] twoWidgets = new int[][] { { 219, 0, 2 } };

	@Override
	public boolean activate() {
		return Inventory.contains(Items.SILK.getId());
	}

	@Override
	public void execute() {
		
		for (int[] w : twoWidgets) {
			Widget w2 = Widgets.getWidget(w);
			if (w2 != null && "I'll give it to you for 60.".equals(w2.getText())) {
				Keyboard.sendKeys("2");
				Time.sleep(1200, 1400);
				return;
			}
		}

		for (int[] w : threeWidgets) {
			Widget w2 = Widgets.getWidget(w);
			if (w2 != null && "120 coins.".equals(w2.getText())) {
				Keyboard.sendKeys("3");
				Time.sleep(1200, 1400);
				return;
			}
		}

		for (int[] w : spaceWidgets) {
			if (Widgets.exists(w)) {
				Keyboard.sendKeys(" ");
				Time.sleep(600, 800);
				return;
			}
		}

		Npc npc = Npcs.getClosest("Silk merchant");
		if (npc == null)
			return;

		Mouse.move(npc.toScreen());
		Mouse.click(Mouse.RIGHT_BUTTON);
		Time.sleep(20, 35);
		if (Menu.isOpen()) {
			// check again to make sure we are clicking the correct thing
			int i = Menu.getIndex(new String[]{"Talk-to"}, new String[]{"Silk merchant"});
			int count = Client.getMenuCount();

			if (i != -1) {
				Mouse.move(Menu.getBounds(count - i - 1));
				Mouse.sleepBeforeClick();
				Mouse.click(Mouse.LEFT_BUTTON);
				Menu.getBounds(count - i - 1).contains(Mouse.getLocation());
			} else {
				Menu.close();
			}
		}
		Time.sleep(600, 800);
	}

}
