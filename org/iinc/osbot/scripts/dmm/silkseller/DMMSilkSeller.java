package org.iinc.osbot.scripts.dmm.silkseller;

import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.script.Node;
import org.iinc.osbot.scripts.dmm.TutorialIslandNode;

public class DMMSilkSeller implements Node {
	public static Tile loc = new Tile(2655, 3301, 0);
	
	static Node[] nodes = new Node[] {new TutorialIslandNode("East Ardougne"), new DropNode(), new RepositionNode(), new SellNode(), new TradeNode() };

	@Override
	public boolean activate() {
		return true;
	}

	@Override
	public void execute() {
		for (Node n : nodes) {
			if (n.activate()) {
				n.execute();
				return;
			}
		}
	}

}
