package org.iinc.osbot.scripts.dmm.silkseller;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Node;

public class RepositionNode implements Node {

	@Override
	public boolean activate() {
		return !Client.getLocalPlayer().getTile().equals(DMMSilkSeller.loc);
	}

	@Override
	public void execute() {
		if (Calculations.onViewport(DMMSilkSeller.loc.toScreen())) {
			Clicking.interact(DMMSilkSeller.loc, "Walk here");
			Time.sleep(3000, 5000);
		} else {
			Walking.blindWalk(DMMSilkSeller.loc);
		}
	}

}
