package org.iinc.osbot.scripts.dmm;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.Levels;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.positioning.Area;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.scripts.common.Interfaces;
import org.iinc.osbot.scripts.dmm.silkstall.DMMSilkStall;
import org.iinc.osbot.scripts.thieving.cakestall.CakeStall;
import org.iinc.osbot.scripts.thieving.theguns.TheGuns;
import org.iinc.osbot.util.Email;
import org.iinc.osbot.util.Random;

/*
 * things for next season
 * 	prices crash hard after first day. sell all before sleep
 * 	only sell to highly trusted
 * 	fix escape knight bug in silk stall
 * 
 */
public class DMMArdyThievingScript extends Script {

	TutorialIslandNode ti = new TutorialIslandNode("East Ardougne");
	TheGuns tg = new TheGuns();
	CakeStall cs = new CakeStall();
	DMMSilkStall dss = new DMMSilkStall();
	Area area = new Area(new Tile(2556, 3349, 0), new Tile(2551, 3221, 0), new Tile(2671, 3189, 0),
			new Tile(2718, 3354, 0));

	@Override
	public int loop() {
		if (!Login.isLoggedIn()) {
			Login.login();
			Time.sleep(600, 1200);
			Camera.setAngle(Camera.getCameraAngle(), true);
			return 0;
		}

		Interfaces.closeAll();

		if (ti.activate()) {
			ti.execute();
		} else {
			if (!area.contains(Client.getLocalPlayer().getTile())) {
				Email.sendEmail(Email.TYPE.SEVERE, "DMM Ardy Thieving: Player not in area");
				System.exit(Settings.DISABLED_EXIT_CODE);
			}

			// hide attack player option
			if (Client.getSetting(1107) != 3) {
				if (Clicking.interact(Widgets.getWidget(548, 34))) {
					Time.sleep(600, 800);
					if (Clicking.interact(Widgets.getWidget(261, 1, 6))) {
						Time.sleep(600, 800);
						if (Clicking.interact(Widgets.getWidget(261, 67, 2))) {
							Time.sleep(600, 800);
							if (Clicking.interact(Widgets.getWidget(261, 83, 4))) {
								Time.sleep(600, 800);
							}
						}
					}
				}
				return 0;
			}

			int thieving = Levels.getCurrentThieving();

			if (thieving >= 25) {
				dss.execute();
			} else if (thieving >= 5) {
				cs.execute();
			} else {
				tg.execute();
			}

		}

		return Random.randomRange(50, 100);
	}

	@Override
	public void onStop() {

	}

	@Override
	public void onStart() {
		org.iinc.osbot.scripts.thieving.cakestall.StealNode.DMM = true;
		Settings.MAKE_CLICKING_ERRORS = false;
		// org.iinc.osbot.scripts.thieving.cakestall.StealNode.t = new Timer(8000);
	}

}
