package org.iinc.osbot;

import java.util.logging.Level;

public class Settings {
	public static boolean SHOW_GUI = false;
	public static String DIRECTORY = "/home/jd64234/osbot/bot";
	public static String TEMPORARY_DIRECTORY = DIRECTORY + "/tmp";
	public static String BOTWATCH_DIRECTORY = DIRECTORY + "/botwatch";
	public static boolean LOG_TO_CONSOLE = false;
	public static Level LOG_LEVEL = Level.FINE;
	public static boolean MAKE_CLICKING_ERRORS = true;
	public static boolean PRINT_ANTIBAN = true;
	public static int DISABLED_EXIT_CODE = 166;
	public static boolean BOTWATCH_ENABLED = false;
}
