package org.iinc.osbot;

import java.applet.Applet;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.base.worlds.World;
import org.iinc.osbot.api.base.worlds.Worlds;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.gui.GUI;
import org.iinc.osbot.hooks.Hooks;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.script.ScriptHandler;
import org.iinc.osbot.scripts.LesserDemon;
import org.iinc.osbot.scripts.LumbridgeOffloader;
import org.iinc.osbot.scripts.Multi;
import org.iinc.osbot.scripts.NewAccountScript;
import org.iinc.osbot.scripts.bond.RecieveCreateBondOfferScript;
import org.iinc.osbot.scripts.bond.RedeemBondOfferScript;
import org.iinc.osbot.scripts.callisto.Callisto;
import org.iinc.osbot.scripts.common.Antiban;
import org.iinc.osbot.scripts.dmm.DMMArdyOffloaderScript;
import org.iinc.osbot.scripts.dmm.DMMArdyThievingScript;
import org.iinc.osbot.scripts.fishing.barbarian.BarbarianFishingScript;
import org.iinc.osbot.scripts.fishing.catherby.CatherbyFishingScript;
import org.iinc.osbot.scripts.fishing.fishing_guild.FishingGuildFishingScript;
import org.iinc.osbot.scripts.fishing.lumbridge_river.LumbridgeRiverFishingScript;
import org.iinc.osbot.scripts.fishing.lumbridge_swamp.LumbridgeSwampFishingScript;
import org.iinc.osbot.scripts.gdk.GDKScript;
import org.iinc.osbot.scripts.hunter.Hunter;
import org.iinc.osbot.scripts.ratfrog.RatFrogScript;
import org.iinc.osbot.scripts.ratfrog.RatFrogStrScript;
import org.iinc.osbot.scripts.ratfrog.RatFrogTzhScript;
import org.iinc.osbot.scripts.shop.zaff.ZaffScript;
import org.iinc.osbot.scripts.thieving.ardyguards.ArdyGuardsScript;
import org.iinc.osbot.scripts.thieving.ardyknights.ArdyKnightsScript;
import org.iinc.osbot.scripts.thieving.ardywarriors.ArdyWarriorsScript;
import org.iinc.osbot.scripts.thieving.cakestall.CakeStallScript;
import org.iinc.osbot.scripts.thieving.watchtower.WatchtowerScript;
import org.iinc.osbot.util.ClassLoader;
import org.iinc.osbot.util.Crawler;
import org.iinc.osbot.util.Email;
import org.iinc.osbot.util.Email.TYPE;
import org.iinc.osbot.util.Injector;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class AppHeadless {

	public static void main(String[] args) {
		try {
			System.out.println(Arrays.toString(args));

			// init settings
			Settings.LOG_TO_CONSOLE = true;
			Settings.LOG_LEVEL = Level.FINEST;
			Settings.SHOW_GUI = false;

			try {
				Files.createDirectories(Paths.get(Settings.TEMPORARY_DIRECTORY).toAbsolutePath());
				Files.createDirectories(Paths.get(Settings.BOTWATCH_DIRECTORY).toAbsolutePath());
			} catch (IOException e) {
				System.out.println("Unable to create directories");
				System.exit(0);
			}

			List<Script> scripts = new ArrayList<Script>();
			int world = -1;

			for (int i = 0; i < args.length; i++) {
				// System.out.println(args[i]);
				switch (args[i]) {
				case "--account":
					Bot.username = args[i + 1];
					Bot.password = args[i + 2];
					i += 2;
					break;
				case "--script":
					switch (args[i + 1]) {
					case "CatherbyFisher":
					case "CatherbyFishing":
						scripts.add(new CatherbyFishingScript());
						break;
					case "RatFrog":
						scripts.add(new RatFrogScript());
						break;
					case "RatFrogStr":
						scripts.add(new RatFrogStrScript());
						break;
					case "Callisto":
						scripts.add(new Callisto());
						break;
					case "GDK":
						scripts.add(new GDKScript());
						break;
					case "LesserDemon":
						scripts.add(new LesserDemon());
						break;
					case "Hunter":
						scripts.add(new Hunter());
						break;
					case "Multi":
						scripts.add(new Multi());
						break;
					case "LumbridgeOffloader":
						scripts.add(new LumbridgeOffloader());
						break;
					case "Antiban":
						scripts.add(new Antiban());
						break;
					case "Zaff":
						scripts.add(new ZaffScript());
						break;
					case "NewAccount":
						scripts.add(new NewAccountScript());
						break;
					case "NewAccountFishing":
					case "LumbridgeSwampFishing":
						scripts.add(new LumbridgeSwampFishingScript());
						break;
					case "LumbridgeRiverFishing":
						scripts.add(new LumbridgeRiverFishingScript());
						break;
					case "CakeStallScript":
						scripts.add(new CakeStallScript());
						break;
					case "ArdyWarriorsScript":
						scripts.add(new ArdyWarriorsScript());
						break;
					case "ArdyGuardsScript":
						scripts.add(new ArdyGuardsScript());
						break;
					case "ArdyKnightsScript":
						scripts.add(new ArdyKnightsScript());
						break;
					case "WatchtowerScript":
						scripts.add(new WatchtowerScript());
						break;
					case "BarbarianFishing":
						scripts.add(new BarbarianFishingScript());
						break;
					case "FishingGuildFishing":
						scripts.add(new FishingGuildFishingScript());
						break;
					case "RedeemBondOffer":
						scripts.add(new RedeemBondOfferScript());
						break;
					case "RecieveCreateBondOffer":
						scripts.add(new RecieveCreateBondOfferScript());
						break;
					case "DMMArdyThieving":
						scripts.add(new DMMArdyThievingScript());
						break;
					case "DMMArdyOffloader":
						scripts.add(new DMMArdyOffloaderScript());
						break;
					case "RatFrogTzh":
						scripts.add(new RatFrogTzhScript());
						break;
					}
					i += 1;
					break;
				case "--break":
					BreakHandler.breaking = true;
					if (args.length >= i + 3 && args[i + 1].matches("\\d+") && args[i + 2].matches("\\d+")) {
						BreakHandler.minHours = Integer.parseInt(args[i + 1]);
						BreakHandler.maxHours = Integer.parseInt(args[i + 2]);
					}
					break;
				case "--gui":
					Settings.SHOW_GUI = true;
					break;
				case "--world": // -1 = random members, -2 = random f2p
					if (args.length >= i + 2 && args[i + 1].matches("-?\\d+")) {
						world = Integer.parseInt(args[i + 1]);
					}
					break;
				}
			}
			Random.random = new java.util.Random(Bot.username.hashCode());

			Bot.LOGGER.log(Level.INFO, "Launched bot");

			Bot.LOGGER.log(Level.INFO, "Loading hooks");
			// Hooks.loadFromURL("http://pastebin.com/raw/5ABytwZM");
			Hooks.loadFromFile(Paths.get(Settings.DIRECTORY, "hooks.json").toString());

			Bot.LOGGER.log(Level.INFO, "Getting world");
			World w = null;
			if (world == -1) {
				w = Worlds.getRandomNormalWorld(true);
			} else if (world == -2) {
				w = Worlds.getRandomNormalWorld(false);
			} else if (world == -3) {
				List<World> worlds = Worlds.getWorlds().stream()
						.filter((ww) -> ww.getActivity().equals("Deadman Seasonal")).collect(Collectors.toList());
				w = worlds.get(Random.randomRange(0, worlds.size() - 1));
			} else {
				for (World ww : Worlds.getWorlds())
					if (ww.getId() == world)
						w = ww;
			}

			Bot.LOGGER.log(Level.INFO, "Crawling website");
			Crawler crawler = new Crawler(w);
			if (!crawler.crawl() || crawler.remoteHash() == -1) {
				Bot.LOGGER.log(Level.FINE, "Crawler error");
				Time.sleep(20 * 1000); // sleep 20 seconds
				System.exit(0);
			}

			if (crawler.outdated()) {
				Bot.LOGGER.log(Level.INFO, "Crawling outdated. Downloading new gamepack");
				crawler.download();
			}

			Bot.LOGGER.log(Level.INFO, "Checking hook revision");
			int rev = crawler.revision();
			if (Hooks.getRevision() != rev) {
				// corrupt gamepack
				if (crawler.revision() == -1) {
					new File(crawler.getFileURL().getPath()).delete();
					System.exit(1);
				}

				int hash = crawler.remoteHash();
				Bot.LOGGER.log(Level.FINE,
						"Hooks not updated hooks: " + Hooks.getRevision() + " game: " + rev + " remote hash: " + hash);
				Email.sendEmail(Email.TYPE.SEVERE, "Hooks not updated hooks\nBot: " + Hooks.getRevision() + "\nGame: "
						+ rev + "\nRemote hash: " + hash);
				System.exit(Settings.DISABLED_EXIT_CODE);
			}

			Bot.LOGGER.log(Level.INFO, "Injecting");
			Injector inj = new Injector(crawler.getFileURL());
			inj.inject();

			Bot.LOGGER.log(Level.INFO, "Creating class loader");
			ClassLoader cl = new ClassLoader(inj.getFileURL());

			// fake system properties
			Bot.LOGGER.log(Level.INFO, "Faking system properties");
			System.setProperty("os.name", "Mac OS X");
			System.setProperty("os.version", "10." + Random.randomRange(7, 14) + "." + Random.randomRange(0, 10));
			// windows causes crash https://hastebin.com/yazitanoxe.css
			// System.setProperty("os.name", "Windows 10");
			// System.setProperty("os.version", "10.0");
			// System.setProperty("java.vendor", "Oracle Corporation");
			// System.setProperty("java.version", "1.8.0_" + Random.randomRange(80, 100));

			Bot.LOGGER.log(Level.INFO, "Collecting garbage");
			Runtime.getRuntime().gc();

			Bot.LOGGER.log(Level.INFO, "Starting game");
			Applet game = crawler.start(cl);
			Bot.LOGGER.log(Level.INFO, "Starting bot");
			Bot.start(game);
			if (Settings.SHOW_GUI)
				GUI.start();

			Modifiers.init(Bot.username.hashCode());
			BreakHandler.init(Bot.username.hashCode());
			// if (BreakHandler.shouldBreak())
			// BreakHandler.doBreak();

			Bot.LOGGER.log(Level.INFO, "Started scripts");
			scripts.forEach(ScriptHandler::addScript);

		} catch (Exception e) {
			Email.sendEmail(TYPE.SEVERE, "Something crashed " + Bot.username, e);
			System.exit(0);
		}
	}
}
