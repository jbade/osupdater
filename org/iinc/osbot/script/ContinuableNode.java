package org.iinc.osbot.script;

public interface ContinuableNode {
	/**
	 * @return if this node should be executed
	 */
	public boolean activate();

	/**
	 * @return if script should continue checking/executing nodes
	 */
	public boolean execute();
}
