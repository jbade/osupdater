package org.iinc.osbot.script;

import java.util.HashSet;

public class ScriptHandler {
	// private static final ThreadGroup scripts = new ThreadGroup("scripts");
	public static final HashSet<Script> scripts = new HashSet<Script>();
	public static final HashSet<PaintListener> painters = new HashSet<PaintListener>();

	public static void stopScripts() {
		synchronized (scripts) {
			for (Script t : scripts) {
				t.stop();
				if (t instanceof PaintListener) {
					painters.remove(t);
				}
			}

			scripts.clear();
		}

	}

	public static void stopScript(Script script) {
		synchronized (scripts) {
			if (script instanceof PaintListener) {
				painters.remove((PaintListener) script);
			}

			script.stop();
			scripts.remove(script);
		}
	}

	public static void addScript(Script script) {
		synchronized (scripts) {
			if (script instanceof PaintListener) {
				painters.add((PaintListener) script);
			}

			Thread t = new Thread(script);
			t.start();
			scripts.add(script);
		}
	}

}
