package org.iinc.osbot.script;
public interface Node {
	public boolean activate();

	public void execute();
}
