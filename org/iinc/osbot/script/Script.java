package org.iinc.osbot.script;

import java.util.logging.Level;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.util.Email;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public abstract class Script implements Runnable {
	protected boolean running = true;

	public void run() {
		onStart();
		while (running && !Thread.currentThread().isInterrupted()) {
			try {
				int length = loop();
				Thread.sleep(Math.max(10, Random.modifiedRandomRange((int) (length * 0.9), (int) (length * 1.1),
						Modifiers.get("scriptSleepTime"))));
			} catch (Exception e) {
				e.printStackTrace();
				Bot.LOGGER.log(Level.SEVERE, "Script exception", e);
				Email.sendEmail(Email.TYPE.SEVERE, "Script exception: " + Bot.username, e);
				Time.sleep(100);
			}
		}

		onStop();
	}

	public final void stop() {
		running = false;
	}

	public abstract int loop();

	public abstract void onStop();

	public abstract void onStart();

}
