package org.iinc.osbot.script;

import java.awt.Graphics;

public interface PaintListener {
	public void onDraw(Graphics g);
}
