package org.iinc.osbot.bot;

import java.applet.Applet;
import java.awt.image.BufferedImage;
import java.nio.file.Paths;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.injection.interfaces.IClient;

public class Bot {
	public static IClient game;
	public static Applet applet;
	public static BufferedImage gameImage;
	public static String username = "chtsroogafo@gmail.com";
	public static String password = "youtube1";
	public static Logger LOGGER;
	public static ReentrantLock mouseLock = new ReentrantLock();
	public static ReentrantLock keyboardLock = new ReentrantLock();

	static {
		LOGGER = Logger.getLogger("Bot");
		LOGGER.setUseParentHandlers(false);
		LOGGER.setLevel(Level.ALL);

		// console
		if (Settings.LOG_TO_CONSOLE) {
			ConsoleHandler ch = new ConsoleHandler();
			ch.setFormatter(new SimpleFormatter());
			ch.setLevel(Settings.LOG_LEVEL);
			LOGGER.addHandler(ch);
		}

		// file
		try {
			FileHandler fh = new FileHandler(
					Paths.get(Settings.TEMPORARY_DIRECTORY).toAbsolutePath().toString() + "/log%g.txt", 1073741824, 1,
					true);
			fh.setFormatter(new SimpleFormatter());
			fh.setLevel(Level.ALL);
			LOGGER.addHandler(fh);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void start(Applet game) {
		Bot.game = (IClient) game;
		Bot.applet = game;
	}

	/* screenshot 
	 * 		
		try {
			File outputfile = new File("image" + System.currentTimeMillis() + ".jpg");
			ImageIO.write(Bot.gameImage, "png", outputfile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	 */
}
