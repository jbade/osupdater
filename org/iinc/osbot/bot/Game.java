package org.iinc.osbot.bot;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JPanel;

import org.iinc.osbot.input.InputListeners;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.ScriptHandler;

public class Game extends JPanel implements ComponentListener {
	private static final long serialVersionUID = 1L;
	private final InputListeners inputListeners;

	public Game() {
		setPreferredSize(Bot.applet.getPreferredSize());
		setFocusable(true);
		setBackground(Color.black);
		inputListeners = new InputListeners(Bot.applet);
		addMouseListener(inputListeners);
		addMouseMotionListener(inputListeners);
		addMouseWheelListener(inputListeners);
		addKeyListener(inputListeners);
		addFocusListener(inputListeners);
		addComponentListener(this);
		if (!hasFocus())
			requestFocusInWindow();
	}

	@Override
	public void paint(Graphics g) {
		if (!isVisible())
			return;
		super.paint(g);
		g.drawImage(Bot.gameImage, 0, 0, null);

		g.setColor(Color.green);
		for (PaintListener p : ScriptHandler.painters)
			p.onDraw(g);

	}

	@Override
	public void componentResized(ComponentEvent e) {
		Bot.applet.resize(e.getComponent().getSize());
	}

	@Override
	public void componentHidden(ComponentEvent e) {
	}

	@Override
	public void componentMoved(ComponentEvent e) {
	}

	@Override
	public void componentShown(ComponentEvent e) {
	}

}
