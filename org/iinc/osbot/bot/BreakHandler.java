package org.iinc.osbot.bot;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;

import org.iinc.osbot.util.Formatter;

public class BreakHandler {
	private static final int DAY = 24 * 60 * 60 * 1000;
	private static final int HOUR = 60 * 60 * 1000;
	private static final int MINUTE = 60 * 1000;

	private static int hash;
	public static boolean breaking = false;
	public static int minHours = 26;// 10; // minimum hours to play each day
	public static int maxHours = 26;// 11; // maximum hours to play each day

	// +1 is London time zone, 17 is 6:00pm utc
	public static int startDayOffsetMin = 0;// 17; // 5:00pm London
	public static int startDayOffsetMax = 0;// 18; // 6:00pm London

	public static int minSessionDuration = 30 * MINUTE;
	public static int maxSessionDuration = 2 * HOUR;
	public static int minBreakDuration = MINUTE;
	public static int maxBreakDuration = 5 * MINUTE;

	private static long breakFor() {
		SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("UTC"));

		Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getTimeZone("UTC"));
		c.setTime(new Date(c.getTimeInMillis() - startDayOffsetMin * HOUR));
		long curr = c.getTimeInMillis();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		long day = c.getTimeInMillis();
		long elapsed = curr - day; // the ms that have elapsed during the current day

		java.util.Random random = new java.util.Random(day + hash);

		double goalHours = randomRange(random, minHours, maxHours);
		double hours = 0;

		long t = randomRange(random, 0, (startDayOffsetMax - startDayOffsetMin) * HOUR);

		if (t > elapsed) {
			// Bot.LOGGER.log(Level.FINE, format("break", elapsed, t - elapsed));
			return t - elapsed; // time start of first session
		}

		while (t < 24 * 60 * 60 * 1000) {
			long sessionDuration = randomRange(random, minSessionDuration, maxSessionDuration);
			// FIXME
			if (sessionDuration / ((double) HOUR) + hours > goalHours) // dont go over goal hours
				sessionDuration = (long) ((goalHours - hours) * HOUR + 1); // +1 to account for
																			// rounding errors?
			hours += sessionDuration / ((double) HOUR);
			if (t + sessionDuration > DAY)
				break;
			t += sessionDuration;
			if (t > elapsed) {
				// Bot.LOGGER.log(Level.FINE, format("session", t, sessionDuration));
				return -1; // we should be playing now
			}

			if (hours >= goalHours)
				break;

			long breakDuration = randomRange(random, minBreakDuration, maxBreakDuration);
			if (t + sessionDuration > DAY || hours + breakDuration / ((double) HOUR) >= goalHours)
				break;
			hours += breakDuration / ((double) HOUR);
			t += breakDuration;
			if (t > elapsed) {
				// Bot.LOGGER.log(Level.FINE, format("break", t, breakDuration));
				return t - elapsed; // time until end of break
			}

		}

		Bot.LOGGER.log(Level.FINE, format("break", t, DAY - t));
		return DAY - elapsed; // time until next day
	}

	private static String format(String label, long t, long duration) {
		StringBuilder sb = new StringBuilder();
		sb.append(label).append("\t");
		sb.append(Formatter.timeToString(t)).append(" - ").append(Formatter.timeToString(t + duration)).append("\t");
		sb.append(Formatter.timeToString(duration));
		return sb.toString();
	}

	public static boolean shouldBreak() {
		if (!breaking)
			return false;
		return breakFor() != -1;
	}

	/**
	 * 
	 * @return time to break or -1 if not breaking
	 */
	public static long old() {
		long now = new Date().getTime() - 18 * 60 * 60 * 1000; // day starts at
																// 5-6pm uk time
		long day = now / (24 * 60 * 60 * 1000); // current 'day'

		java.util.Random random = new java.util.Random(day + hash);

		long startTime = randomRange(random, 0, 2 * 60 * 60 * 1000);
		int totalSessions = randomRange(random, 8, 14);

		long currTime = day * (24 * 60 * 60 * 1000) + startTime;
		if (currTime > now)
			return currTime - now; // return time until we start

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("UTC"));

		// System.out.println(df.format(new Date(now)));

		for (int i = 0; i < totalSessions; i++) {
			long sessionDuration = randomRange(random, 10 * 60 * 1000, 120 * 60 * 1000);
			// Bot.LOGGER.log(Level.INFO, "session: " + df.format(new
			// Date(currTime)) + " duration: "
			// + Formatter.timeToString(sessionDuration));
			currTime += sessionDuration;
			if (currTime > now)
				return -1; // we should be playing now

			long breakDuration = randomRange(random, 1 * 60 * 1000, 20 * 60 * 1000);
			// Bot.LOGGER.log(Level.INFO,
			// "break: " + df.format(new Date(currTime)) + " duration: " +
			// Formatter.timeToString(breakDuration));
			currTime += breakDuration;
			if (currTime > now)
				return currTime - now; // time until end of break
		}

		return (day + 1) * 24 * 60 * 60 * 1000 - now; // time until next day
	}

	private static int randomRange(java.util.Random random, int min, int max) {
		int range = (max - min) + 1;
		return (int) (random.nextDouble() * range + min);
	}

	public static void doBreak() {
		long time = breakFor();

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("EST"));
		Bot.LOGGER.log(Level.INFO, "Breaking at " + df.format(new Date()) + " for " + Formatter.timeToString(time));

		if (time > 0) {
			try {
				if (Bot.applet != null) {
					Bot.applet.stop();
					Bot.applet.destroy();
				}
				Thread.sleep(time);
				System.exit(0);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void init(int hash) {
		BreakHandler.hash = hash;
	}
}
