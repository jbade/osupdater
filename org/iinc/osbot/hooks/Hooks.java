package org.iinc.osbot.hooks;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.iinc.osbot.Settings;
import org.iinc.osbot.hooks.log.ClassInfo;
import org.iinc.osbot.hooks.log.FieldInfo;
import org.iinc.osbot.hooks.log.HooksLog;
import org.iinc.osbot.hooks.log.Log;
import org.iinc.osbot.util.internet.Internet;

import com.google.gson.Gson;

public class Hooks {
	private static Log log = null;
	
	public static boolean loadFromURL(String url) {
		try {
			Internet.download(url, Paths.get(Settings.TEMPORARY_DIRECTORY, "hooks.json"));
			String json = new String(Files.readAllBytes(Paths.get(Settings.TEMPORARY_DIRECTORY, "hooks.json")), Charset.defaultCharset());
			Gson gson = new Gson();
			log = gson.fromJson(json, Log.class);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Attempts to load hooks from the provided file.
	 * 
	 * @param path
	 *            The path of the hooks file.
	 * @return If the hooks were loaded successfully
	 */
	public static boolean loadFromFile(String path) {
		try {
			String json = new String(Files.readAllBytes(Paths.get(path)), Charset.defaultCharset());
			Gson gson = new Gson();
			log = gson.fromJson(json, Log.class);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static void buildHooks(org.iinc.osbot.util.ClassLoader cl) {
		try {
			for (ClassInfo ci : log.getHooks().getClasses().values()) {
				for (FieldInfo fi : ci.getFields().values()) {
					try{
					Class<?> c = cl.loadClass(fi.getClassName());
					fi.setField(c.getDeclaredField(fi.getName()));
					} catch(Exception e){
						System.err.println("error finding hook " + ci.getName() + "." + fi.getName());
					}
				}
			}

			for (FieldInfo fi : log.getHooks().getStatic().getFields().values()) {
				Class<?> c = cl.loadClass(fi.getClassName());
				fi.setField(c.getDeclaredField(fi.getName()));
			}
		} catch (ClassNotFoundException | NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
	}

	public static boolean hasField(String classId, String fieldId) {
		return log == null ? false : log.getHooks().hasFieldInfo(classId, fieldId);
	}

	public static FieldInfo getField(String classId, String fieldId) {
		return log == null ? null : log.getHooks().getFieldInfo(classId, fieldId);
	}

	public static HooksLog getHooks(){
		return log == null ? null : log.getHooks();
	}
	
	/**
	 * Gets the revision the hooks are for. -1 if the hooks have not been
	 * loaded.
	 * 
	 * @return
	 */
	public static int getRevision() {
		return log == null ? -1 : log.getRevision();
	}
}
