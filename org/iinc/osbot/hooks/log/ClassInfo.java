package org.iinc.osbot.hooks.log;

import java.util.HashMap;

public class ClassInfo {
	private String name;
	private HashMap<String, FieldInfo> fields;

	public String getName() {
		return name;
	}

	public FieldInfo getField(String id) {
		return fields.get(id);
	}

	public boolean hasField(String id) {
		return fields.containsKey(id);
	}

	public HashMap<String, FieldInfo> getFields() {
		return fields;
	}
}
