package org.iinc.osbot.hooks.log;

import java.lang.reflect.Field;

public class FieldInfo {
	private String className;
	private String name;
	private String desc;
	private Integer multiplier;
	private Field field;

	public String getClassName() {
		return className;
	}

	public String getName() {
		return name;
	}

	public String getDesc() {
		return desc;
	}

	public Integer getMultiplier() {
		return multiplier;
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}

	@Override
	public String toString() {
		return "FieldInfo [className=" + className + ", name=" + name + ", desc=" + desc + ", multiplier=" + multiplier
				+ ", field=" + field + "]";
	}

}
