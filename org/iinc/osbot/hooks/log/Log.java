package org.iinc.osbot.hooks.log;

public class Log {
	private int revision;
	private HooksLog hooks;

	public HooksLog getHooks() {
		return hooks;
	}

	public int getRevision() {
		return revision;
	}
}
