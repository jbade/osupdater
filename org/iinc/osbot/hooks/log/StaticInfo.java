package org.iinc.osbot.hooks.log;

import java.util.HashMap;

public class StaticInfo {
	private HashMap<String, FieldInfo> fields = new HashMap<String, FieldInfo>();

	public FieldInfo getField(String fieldId) {
		return fields.get(fieldId);
	}

	public boolean hasField(String fieldId) {
		return fields.containsKey(fieldId);
	}

	public HashMap<String, FieldInfo> getFields() {
		return fields;
	}
}
