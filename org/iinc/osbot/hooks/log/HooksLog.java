package org.iinc.osbot.hooks.log;

import java.util.HashMap;

import com.google.gson.annotations.SerializedName;

public class HooksLog {
	private HashMap<String, ClassInfo> classes;
	@SerializedName("static")
	private StaticInfo global;

	public FieldInfo getFieldInfo(String classId, String fieldId) {
		if (classId == null) {
			return global.getField(fieldId);
		} else {
			ClassInfo cil = classes.get(classId);
			return cil == null ? null : cil.getField(fieldId);
		}
	}

	public boolean hasFieldInfo(String classId, String fieldId) {
		if (classId == null) {
			return global.hasField(fieldId);
		} else {
			ClassInfo cil = classes.get(classId);
			return cil == null ? false : cil.hasField(fieldId);
		}
	}

	public HashMap<String, ClassInfo> getClasses() {
		return classes;
	}

	public StaticInfo getStatic() {
		return global;
	}

}
