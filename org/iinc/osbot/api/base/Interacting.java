package org.iinc.osbot.api.base;

import java.util.function.Predicate;
import java.util.function.Supplier;

import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.api.base.item.RSItem;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.positioning.Walking;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class Interacting {

	private static boolean interactClickable(Supplier<Clickable> query, Tile expected, Predicate<Clickable> interact) {
		int tries = 0;

		while (tries++ < 1) {
			Clickable entity = query.get();

			if (entity != null) {
				if (Random.modifiedRandomRange(0, 100, 10, Modifiers.get("interactingModifiers[0]")) > 97
						|| !Calculations.onViewport(entity.toScreen())
								&& Client.getLocalPlayer().getTile().distanceTo(entity.getTile()) <= Random
										.modifiedRandomGuassianRange(4, 5, ConfidenceIntervals.CI_95, 1.5,
												Modifiers.get("interactingModifiers[2]"))
								&& Random.modifiedRandomRange(0, 100, 30,
										Modifiers.get("interactingModifiers[1]")) > 70) {
					Camera.turnTo(entity.getTile());
				}

				Tile t = entity.getTile();
				t = Walking
						.getClosestTile(t,
								Client.getLocalPlayer().getTile().distanceTo(t) - Random.random.nextDouble() + 2.0)
						.randomized(1, 1); // 2.0 - 3.0

				if (Calculations.onViewport(entity.toScreen())
						|| Walking.walkPath(Walking.generatePath(Client.getLocalPlayer().getTile(), t, 3), true)) {

					if (interact.test(entity)) {
						return true;
					}

					Time.sleepUntil(() -> !Walking.hasDestination(), 1200, 1800);
					if (interact.test(entity)) {
						return true;
					}

				}
			} else {
				if (expected == null) {
					return false;
				} else {
					if (Client.getLocalPlayer().getTile().distanceTo(expected) > 2)
						Walking.walkPath(Walking.generatePath(Client.getLocalPlayer().getTile(), expected, 3),
								() -> query.get() != null, false);
				}
			}
		}

		return false;
	}

	public static boolean interact(Supplier<Clickable> query, Tile expected, String[] actions, String[] options) {
		return interactClickable(query, expected, (e) -> Clicking.interact(e, actions, options));
	}

	public static boolean interact(Supplier<Clickable> query, Tile expected, String action, String option) {
		return interactClickable(query, expected, (e) -> Clicking.interact(e, strToArr(action), strToArr(option)));
	}

	public static boolean interact(Supplier<Clickable> query, Tile expected, String action) {
		return interactClickable(query, expected, (e) -> Clicking.interact(e, strToArr(action), null));
	}

	public static boolean interact(Supplier<Clickable> query, Tile expected) {
		return interactClickable(query, expected, (e) -> Clicking.interact(e));
	}

	public static boolean interact(Tile tile, String[] actions, String[] options) {
		return interactClickable(() -> tile, tile, (e) -> Clicking.interact(e, actions, options));
	}

	public static boolean interact(Tile tile, String action, String option) {
		return interactClickable(() -> tile, tile, (e) -> Clicking.interact(e, strToArr(action), strToArr(option)));
	}

	public static boolean interact(Tile tile, String action) {
		return interactClickable(() -> tile, tile, (e) -> Clicking.interact(e, strToArr(action), null));
	}

	public static boolean interact(Tile tile) {
		return interactClickable(() -> tile, tile, (e) -> Clicking.interact(e));
	}

	public static boolean useItem(Supplier<Clickable> query, Tile expected, RSItem item, String clickableName) {
		return interactClickable(query, expected, (e) -> {
			Bot.mouseLock.lock();
			try {
				Inventory.open();
				if (Inventory.isItemSelected()) {
					Menu.interact("Cancel");
				}

				Item i = Inventory.getFirst(item.getId());
				if (i != null) {
					Clicking.hover(Inventory.getBounds(i.getIndex()));
					if (Menu.interact("Use")) {
						Mouse.move(e.toScreen());
						if (Inventory.isItemSelected()) {
							return Menu.interact("Use", item.getName() + " -> " + clickableName);
						}
					}
				}
				return false;
			} finally {
				Bot.mouseLock.unlock();
			}
		});
	}

	private static String[] strToArr(String str) {
		if (str == null) {
			return null;
		} else {
			return new String[] { str };
		}
	}
}
