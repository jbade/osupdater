package org.iinc.osbot.api.base.interfaces;

import java.awt.Rectangle;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class Shop {
	private static final int[] SHOP_CONTAINER = { 300, 0 };
	private static final int[] SHOP_CLOSE = { 300, 1, 11 };
	private static final int[] SHOP_ITEMS_PARENT = { 300, 2 };

	public static boolean isOpen() {
		Widget w = Widgets.getWidget(SHOP_CONTAINER);
		return w != null && !w.isHidden();
	}

	public static boolean close() {
		Timer timer = new Timer(Random.randomRange(30000, 60000));
		boolean success = !Bank.isOpen();

		while (!success && timer.isRunning() && Shop.isOpen() && Login.isLoggedIn()) {
			if (Clicking.interact(Widgets.getWidget(SHOP_CLOSE), "Close")) {
				success = Time.sleepUntil(() -> !Shop.isOpen(), 600, 1000);
			}
		}

		return success;
	}

	public static int getQuantity(int id) {
		Widget parent = Widgets.getWidget(SHOP_ITEMS_PARENT);
		if (parent == null)
			return 0;

		for (Widget child : parent.getChildren()) {
			if (child != null && child.getItemId() == id) {
				return child.getItemQuantity();
			}
		}

		return 0;
	}

	public static Widget getWidget(int id) {
		Widget parent = Widgets.getWidget(SHOP_ITEMS_PARENT);
		if (parent == null)
			return null;

		for (Widget child : parent.getChildren()) {
			if (child != null && child.getItemId() == id) {
				return child;
			}
		}

		return null;
	}
}
