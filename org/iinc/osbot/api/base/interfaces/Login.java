package org.iinc.osbot.api.base.interfaces;

import java.awt.event.KeyEvent;
import java.util.logging.Level;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Keyboard;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.util.Email;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;
import org.iinc.osbot.util.Email.TYPE;

public class Login {

	private static int membershipDaysLeft = -1;

	public static boolean isLoggedIn() {
		if (Client.getGameState() <= 20)
			return false;

		Widget w = Widgets.getWidget(378, 6);
		return w == null || w.isHidden();
	}

	public static void login() {
		login(Bot.username, Bot.password);
	}

	public static void login(String user, String password) {
		if (isLoggedIn())
			return;

		Bot.LOGGER.log(Level.FINEST, "Logging in " + user);
		Bot.LOGGER.log(Level.FINEST, "Waiting for game state 10");

		if (!Time.sleepUntil(() -> Client.getGameState() >= 10, 4 * 60 * 1000, 5 * 60 * 1000)) {
			Bot.LOGGER.log(Level.FINE, "Login error: game state 10");
			System.exit(0);
		}

		Timer t = new Timer(30 * 60 * 1000); //new Timer(17 * 60 * 1000);
		boolean failedLogin = false;

		while (t.isRunning() && Client.getGameState() <= 20) {
			// existing user
			if (Client.getLoginState() == 0) {
				Bot.LOGGER.log(Level.FINEST, "Clicking existing user");
				failedLogin = false;
				Mouse.click(410, 285, 515, 300, Mouse.LEFT_BUTTON);
				Time.sleep(300, 500);
				continue;
			}

			// enter username/password
			if (Client.getLoginState() == 2 && !failedLogin) {
				Bot.LOGGER.log(Level.FINEST, "Entering details");
				Keyboard.sendKeys(user);
				Keyboard.sendKey(KeyEvent.VK_ENTER);
				Keyboard.sendKeys(password);
				Keyboard.sendKey(KeyEvent.VK_ENTER);

				Time.sleepUntil(() -> Client.getGameState() == 20, 5000, 8000);
				Time.sleepUntil(() -> Client.getGameState() != 20, 30000, 40000);

				if (Client.getGameState() <= 20) {
					Bot.LOGGER.log(Level.FINE, "Failed to login. Sleeping...");
					failedLogin = true;
					Time.sleep(5 * 60 * 1000, (int) (5.1 * 60 * 1000));
					// sleep because too many login attempts error
				} else {
					Time.sleepUntil(() -> Client.getGameState() == 30, 10000, 15000);
				}

				continue;
			}

			// banned? already logged in?
			if (Client.getLoginState() == 2 && failedLogin) {
				Mouse.click(400, 310, 510, 330, Mouse.LEFT_BUTTON);
				Time.sleep(300, 500);
				continue;
			}

			// invalid username/password
			if (Client.getLoginState() == 3) {
				failedLogin = true;
				Mouse.click(330, 270, 420, 280, Mouse.LEFT_BUTTON);
				Time.sleep(300, 500);
				continue;
			}
		}

		if (!t.isRunning()) {
			Bot.LOGGER.log(Level.FINE, "Login error");
			Email.sendEmail(Email.TYPE.SEVERE, "Login error  " + Bot.username);
			System.exit(Settings.DISABLED_EXIT_CODE);
		}

		Time.sleepUntil(() -> Client.isInWilderness() || Widgets.exists(371, 0) || Widgets.exists(378, 6) || Widgets.exists(90), 5000, 6000);
		Time.sleep(300);

		Timer t2 = new Timer(Random.randomRange(60000, 70000));
		Widget w = Widgets.getWidget(378, 18);
		while (t2.isRunning() && !Client.isInWilderness() && !Widgets.exists(90)) {
			if (w != null) {
				String s = w.getText();
				if (s != null && s.length() > 0) {
					//Email.sendEmail(TYPE.INFO, s);
					s = "0" + s.replaceAll("[^\\d]", "");
					membershipDaysLeft = Integer.valueOf(s);
					break;
				}
			}
			w = Widgets.getWidget(378, 18);
		}

		t2.reset();
		w = Widgets.getWidget(378, 6);
		while (t2.isRunning() && w != null && !w.isHidden() && !Client.isInWilderness()) { //  && !Widgets.exists(90)
			if (Clicking.interact(w, "Play RuneScape"))
				Time.sleep(600, 800);
			w = Widgets.getWidget(378, 6);
		}

		//
		// if (Client.isInWilderness()) {
		// Bot.LOGGER.log(Level.FINEST, "Player in wilderness");
		// Bot.LOGGER.log(Level.FINEST, "Done login");
		// return;
		// }
		//
		// if (Widgets.exists(371, 0)) {
		// Bot.LOGGER.log(Level.FINEST, "Player on tutorial island");
		// Bot.LOGGER.log(Level.FINEST, "Done login");
		// return;
		// }
		//
		// Time.sleepUntil(() -> {
		// Widget widget = Widgets.getWidget(378, 18);
		// if (widget == null || widget.isHidden())
		// return false;
		// String text = widget.getText();
		// return text != null && !text.replaceAll("[^\\d]", "").equals("");
		// }, 4000, 5000);

		Bot.LOGGER.log(Level.FINEST, "Done login");

	}

	public static int getMembershipDaysLeft() {
		return membershipDaysLeft;
	}

	public static void incrementMembershipDaysLeft(int i) {
		membershipDaysLeft += i;
	}
}
