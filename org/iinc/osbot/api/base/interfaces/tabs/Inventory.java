package org.iinc.osbot.api.base.interfaces.tabs;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.item.RSItem;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;

public class Inventory {
	// TODO fix id by -1

	private static final int[] INVENTORY_WIDGET = { 548, 50 };

	public static boolean isOpen() {
		Widget widget = Widgets.getWidget(INVENTORY_WIDGET);
		return widget != null && widget.getTextureId() == 1030;
	}

	public static boolean isItemSelected() {
		return Client.getItemSelectionState() != 0;
	}

	public static boolean open() {
		if (isOpen())
			return true;

		Widget widget = Widgets.getWidget(INVENTORY_WIDGET);
		if (widget != null && !widget.isHidden()) {
			if (Clicking.interact(widget, "Inventory"))
				return Time.sleepUntil(() -> isOpen(), 1000, 1400);
		}

		return false;
	}

	public static boolean isFull() {
		return getCount() == 28;
	}

	public static boolean contains(List<RSItem> items) {
		int[] ids = new int[items.size()];
		for (int i = 0; i < items.size(); i++)
			ids[i] = items.get(i).getId();
		return contains(ids);
	}

	// true if contains any of the ids
	public static boolean contains(int... ids) {
		Widget inv = Widgets.getWidget(149, 0);
		if (inv == null)
			return false;

		int[] itemIds = inv.getItemIds();

		for (int i = 0; i < itemIds.length; i++) {
			if (itemIds[i] > 0 && contains(ids, itemIds[i] - 1)) {
				return true;
			}
		}

		return false;
	}

	public static boolean containsOnly(int... ids) {
		Widget inv = Widgets.getWidget(149, 0);
		if (inv == null)
			return false;

		int[] itemIds = inv.getItemIds();

		for (int i = 0; i < itemIds.length; i++) {
			if (itemIds[i] > 0 && !contains(ids, itemIds[i] - 1)) {
				return false;
			}
		}

		return true;
	}

	public static int getTotalQuantities() {
		Widget inv = Widgets.getWidget(149, 0);
		if (inv == null)
			return -1;

		int count = 0;
		int[] quantities = inv.getItemQuantities();

		for (int i = 0; i < quantities.length; i++) {
			if (quantities[i] > 0) {
				count += quantities[i];
			}
		}

		return count;
	}

	public static Item getSlot(int index) {
		if (index < 0 || index > 27)
			return null;

		Widget inv = Widgets.getWidget(149, 0);
		if (inv == null)
			return null;

		int id = inv.getItemIds()[index];
		if (id > 0)
			return new Item(index, id - 1, inv.getItemQuantities()[index]);

		return null;
	}

	public static int getCount(int... ids) {
		int count = 0;
		for (int i = 0; i < ids.length; i++)
			count += getCount(ids[i]);

		return count;
	}

	public static int getCount(RSItemConstant... items) {
		int count = 0;
		for (int i = 0; i < items.length; i++)
			count += getCount(items[i].getId());

		return count;
	}

	public static int getCount(int id) {
		Widget inv = Widgets.getWidget(149, 0);
		if (inv == null)
			return -1;

		int count = 0;
		int[] ids = inv.getItemIds();
		int[] quantities = inv.getItemQuantities();

		for (int i = 0; i < ids.length; i++) {
			if (ids[i] - 1 == id) {
				count += quantities[i];
			}
		}

		return count;
	}

	public static int getCount() {
		Widget inv = Widgets.getWidget(149, 0);
		if (inv == null)
			return -1;

		int count = 0;
		int[] ids = inv.getItemIds();

		for (int i = 0; i < ids.length; i++) {
			if (ids[i] > 0) {
				count++;
			}
		}

		return count;
	}

	public static ArrayList<Item> getItems() {
		ArrayList<Item> items = new ArrayList<Item>();

		Widget inv = Widgets.getWidget(149, 0);
		if (inv == null)
			return items;

		int[] ids = inv.getItemIds();
		int[] quantities = inv.getItemQuantities();

		for (int i = 0; i < ids.length; i++) {
			if (ids[i] > 0) {
				items.add(new Item(i, ids[i] - 1, quantities[i]));
			}
		}

		return items;
	}

	// public static ArrayList<Item> getAll() {
	// ArrayList<Item> items = new ArrayList<>();
	//
	// Widget inv = Widgets.getWidget(149, 0);
	// if (inv == null)
	// return items;
	//
	// int[] itemIds = inv.getItemIds();
	// int[] quantities = inv.getItemQuantities();
	//
	// for (int i = 0; i < itemIds.length; i++) {
	// items.add(new Item(i, itemIds[i] - 1, quantities[i]));
	// }
	//
	// return items;
	// }
	//
	public static ArrayList<Item> getAllWithIds(int... ids) {
		ArrayList<Item> items = new ArrayList<>();

		Widget inv = Widgets.getWidget(149, 0);
		if (inv == null)
			return items;

		int[] itemIds = inv.getItemIds();
		int[] quantities = inv.getItemQuantities();

		for (int i = 0; i < itemIds.length; i++) {
			if (itemIds[i] > 0 && contains(ids, itemIds[i] - 1)) {
				items.add(new Item(i, itemIds[i] - 1, quantities[i]));
			}
		}

		return items;
	}

	public static Item getFirst(int... ids) {
		Widget inv = Widgets.getWidget(149, 0);
		if (inv == null)
			return null;

		int[] itemIds = inv.getItemIds();
		int[] quantities = inv.getItemQuantities();

		for (int i = 0; i < itemIds.length; i++) {
			if (itemIds[i] > 0 && contains(ids, itemIds[i] - 1)) {
				return new Item(i, itemIds[i] - 1, quantities[i]);
			}
		}

		return null;
	}

	public static Item getFirstNotIn(int... ids) {
		Widget inv = Widgets.getWidget(149, 0);
		if (inv == null)
			return null;

		int[] itemIds = inv.getItemIds();
		int[] quantities = inv.getItemQuantities();

		for (int i = 0; i < itemIds.length; i++) {
			if (itemIds[i] > 0 && !contains(ids, itemIds[i] - 1)) {
				return new Item(i, itemIds[i] - 1, quantities[i]);
			}
		}

		return null;
	}

	private static boolean contains(int[] stack, int needle) {
		if (stack == null || stack.length == 0)
			return false;

		for (int i = 0; i < stack.length; i++)
			if (stack[i] == needle)
				return true;

		return false;
	}

	public static Rectangle getBounds(int index) {
		if (index < 0 || index >= 28)
			return new Rectangle(-1, -1, 0, 0);

		Widget inv = Widgets.getWidget(149, 0);
		if (inv == null)
			return new Rectangle(-1, -1, 0, 0);

		int row = index / 4;
		int col = index % 4;
		int paddingX = 10;
		int paddingY = 4;
		int baseX = inv.getX();
		int baseY = inv.getY();
		int x = baseX + ((32 + paddingX) * col);
		int y = baseY + ((32 + paddingY) * row);
		// return new Rectangle(x - 1, y - 1, 32, 32);
		return new Rectangle(x + 4, y + 4, 24, 24);

	}

	public static boolean containsOnly(RSItemConstant[] food) {
		int[] ids = new int[food.length * 2];
		for (int i = 0; i < food.length; i++){
			ids[i] = food[i].getId();
			ids[i + 1] = food[i].getNotedId();
		}
			
		return contains(ids);
	}
}
