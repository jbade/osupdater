package org.iinc.osbot.api.base.interfaces.tabs;

import java.awt.Point;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Camera;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.base.worlds.World;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Keyboard;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class Logout {
	private static final int[] LOGOUT_TAB_WIDGET = new int[] { 548, 33 };

	private static final int[] LOGOUT_BUTTON = new int[] { 182, 6 };
	private static final int[] WORLD_SWITCHER_BUTTON = new int[] { 182, 1 };

	private static final int[] WORLD_HOP_VIEWPORT = new int[] { 69, 4 };
	private static final int[] WORLD_HOP_SCROLL_UP = new int[] { 69, 15, 4 };
	private static final int[] WORLD_HOP_SCROLL_DOWN = new int[] { 69, 15, 5 };
	private static final int[] WORLD_HOP_WORLDS_PARENT = new int[] { 69, 14 };
	private static final int[] WORLD_HOP_LOGOUTBUTTON_WIDGET = new int[] { 69, 0 };
	private static final int[] WORLD_HOP_CURRENT_WORLD = new int[] { 69, 2 };
	private static final int[] WORLD_HOP_DONT_ASK_AGAIN = new int[] { 219, 0, 2 };

	public static boolean isOpen() {
		Widget widget = Widgets.getWidget(LOGOUT_TAB_WIDGET);
		return widget != null && widget.getTextureId() != -1;
	}

	public static boolean open() {
		if (isOpen())
			return true;

		Widget widget = Widgets.getWidget(LOGOUT_TAB_WIDGET);
		if (widget != null && !widget.isHidden()) {
			if (Clicking.interact(widget, "Logout"))
				return Time.sleepUntil(() -> isOpen(), 1000, 1400);
		}

		return false;
	}

	public static boolean isSwitchWorldMenu() {
		Widget widget = Widgets.getWidget(WORLD_HOP_VIEWPORT);
		return widget != null && !widget.isHidden();
	}

	public static boolean openSwitchWorldMenu() {
		if (!open())
			return false;

		if (isSwitchWorldMenu())
			return true;

		Widget widget = Widgets.getWidget(WORLD_SWITCHER_BUTTON);
		if (widget != null && !widget.isHidden()) {
			if (Clicking.interact(widget, "World Switcher"))
				return Time.sleepUntil(() -> isSwitchWorldMenu(), 2000, 3000);
		}

		return false;
	}

	public static boolean doneLoadingWorlds() {
		Widget w = Widgets.getWidget(WORLD_HOP_CURRENT_WORLD);
		if (w == null || w.isHidden())
			return false;

		String text = w.getText();
		return text != null && text.startsWith("Current world - ");
	}

	public static Widget getLogoutWidget() {
		if (isSwitchWorldMenu()) {
			return Widgets.getWidget(WORLD_HOP_LOGOUTBUTTON_WIDGET);
		} else {
			return Widgets.getWidget(LOGOUT_BUTTON);
		}
	}

	public static void clickLogout() {
		open();
		Widget widget = Widgets.getWidget(LOGOUT_BUTTON);
		if (widget != null && !widget.isHidden())
			Clicking.interact(widget, "Logout");
	}

	public static int getCurrentWorld() {
		if (!openSwitchWorldMenu())
			return -1;
		Time.sleepUntil(() -> doneLoadingWorlds(), 5000, 7000);
		
		Widget w = Widgets.getWidget(WORLD_HOP_CURRENT_WORLD);
		if (w == null || w.isHidden())
			return -1;

		Timer t = new Timer(Random.randomRange(5000, 10000));
		String s = w.getText();
		while (t.isRunning() && (s == null || !s.startsWith("Current world - "))) {
			Time.sleep(50, 100);
			s = w.getText();
		}

		if (s != null && s.startsWith("Current world - ")) {
			try {
				return Integer.valueOf(s.replaceAll("[^\\d]", ""));
			} catch (Exception e) {
				return -1;
			}
		}

		return -1;
	}

	
	public static boolean switchWorld(int world) {
		if (world < 300)
			world += 300;
		
		if (!openSwitchWorldMenu())
			return false;

		if (getCurrentWorld() == world)
			return true;

		boolean tmp = Settings.MAKE_CLICKING_ERRORS;
		Settings.MAKE_CLICKING_ERRORS = false;
		try {
			Widget parent = Widgets.getWidget(WORLD_HOP_WORLDS_PARENT);
			if (parent == null || parent.isHidden())
				return false;

			Widget w = parent.getChild(world);
			if (parent == null || parent.isHidden())
				return false;

			Widget viewportWidget = Widgets.getWidget(WORLD_HOP_VIEWPORT);
			if (viewportWidget == null || viewportWidget.isHidden())
				return false;

			Timer t = new Timer(Random.randomRange(10000, 15000));
			while (t.isRunning() && !viewportWidget.toScreen().contains(w.toScreen())) {
				Bot.mouseLock.lock();
				try {
					if (!viewportWidget.toScreen().contains(Mouse.getLocation()))
						Mouse.move(viewportWidget.toScreen());
					Point p = Mouse.getLocation();
					int lines = 0;
					if (w.toScreen().getMinY() < viewportWidget.toScreen().getMinY()) {
						lines = -1; // scroll up
					} else {
						lines = 1; // scroll down
					}
					Mouse.scrollMouse(p.x, p.y, lines);
					Time.sleep(80, 120);
				} finally {
					Bot.mouseLock.unlock();
				}
			}

			if (viewportWidget.toScreen().contains(w.toScreen())) {
				Bot.mouseLock.lock();
				try {
					if (Clicking.interact(w, "Switch", String.valueOf(world - 300))) {
						Mouse.click(Mouse.LEFT_BUTTON);

						if (Time.sleepUntil(
								() -> Client.getGameState() == 45 || Widgets.exists(WORLD_HOP_DONT_ASK_AGAIN), 3000,
								4000)) {
							Widget askw = Widgets.getWidget(WORLD_HOP_DONT_ASK_AGAIN);
							if (askw != null && !askw.isHidden()) {
								String text = askw.getText();
								if (text != null && text.equals("Yes. In future, only warn about dangerous worlds.")) {
									if (!Clicking.interact(askw, "Continue"))
										return false;
									Time.sleepUntil(() -> Client.getGameState() == 45, 3000, 4000);
								}
							}
							Time.sleepUntil(() -> Client.getGameState() != 45 && Client.getGameState() != 25, 30000, 35000); 
							// we can sleep a long time because we wont be able to login anyway
							if (!Login.isLoggedIn()){
								Login.login();
								Time.sleep(1200, 2000);
								Camera.setAngle(Camera.getCameraAngle(), true);
							}
							
							Time.sleepUntil(() -> isOpen(), 600, 800);
							return getCurrentWorld() == world;
						}
					}
				} finally {
					Bot.mouseLock.unlock();
				}

			}
		} finally {
			Settings.MAKE_CLICKING_ERRORS = tmp;
		}
		return false;
	}

}
