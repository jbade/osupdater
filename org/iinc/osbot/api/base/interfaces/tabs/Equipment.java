package org.iinc.osbot.api.base.interfaces.tabs;

import java.util.logging.Level;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class Equipment {

	private static final int[] EQUIPMENT_TAB_WIDGET = new int[] { 548, 51 };

	private static final int[] EQUIPMENT_BASE = new int[] { 387, 0 };

	private static final int[] EQUIPMENT_AMULET_HIDDEN_WIDGET = new int[] { 387, 8 };
	private static final int[] EQUIPMENT_ARROW_HIDDEN_WIDGET = new int[] { 387, 16 };
	private static final int[] EQUIPMENT_BODY_HIDDEN_WIDGET = new int[] { 387, 10 };
	private static final int[] EQUIPMENT_BOOTS_HIDDEN_WIDGET = new int[] { 387, 14 };
	private static final int[] EQUIPMENT_CAPE_HIDDEN_WIDGET = new int[] { 387, 7 };
	private static final int[] EQUIPMENT_GLOVES_HIDDEN_WIDGET = new int[] { 387, 13 };
	private static final int[] EQUIPMENT_HELMET_HIDDEN_WIDGET = new int[] { 387, 6 };
	private static final int[] EQUIPMENT_LEGS_HIDDEN_WIDGET = new int[] { 387, 12 };
	private static final int[] EQUIPMENT_RING_HIDDEN_WIDGET = new int[] { 387, 15 };
	private static final int[] EQUIPMENT_SHIELD_HIDDEN_WIDGET = new int[] { 387, 11 };
	private static final int[] EQUIPMENT_WEAPON_HIDDEN_WIDGET = new int[] { 387, 9 };

	private static final int[] EQUIPMENT_AMULET_WIDGET = new int[] { 387, 8, 1 };
	private static final int[] EQUIPMENT_ARROW_WIDGET = new int[] { 387, 16, 1 };
	private static final int[] EQUIPMENT_BODY_WIDGET = new int[] { 387, 10, 1 };
	private static final int[] EQUIPMENT_BOOTS_WIDGET = new int[] { 387, 14, 1 };
	private static final int[] EQUIPMENT_CAPE_WIDGET = new int[] { 387, 7, 1 };
	private static final int[] EQUIPMENT_GLOVES_WIDGET = new int[] { 387, 13, 1 };
	private static final int[] EQUIPMENT_HELMET_WIDGET = new int[] { 387, 6, 1 };
	private static final int[] EQUIPMENT_LEGS_WIDGET = new int[] { 387, 12, 1 };
	private static final int[] EQUIPMENT_RING_WIDGET = new int[] { 387, 15, 1 };
	private static final int[] EQUIPMENT_SHIELD_WIDGET = new int[] { 387, 11, 1 };
	private static final int[] EQUIPMENT_WEAPON_WIDGET = new int[] { 387, 9, 1 };

	public static boolean isOpen() {
		Widget widget = Widgets.getWidget(EQUIPMENT_BASE);
		return widget != null && !widget.isHidden();
	}

	public static boolean open() {
		if (isOpen())
			return true;

		Widget widget = Widgets.getWidget(EQUIPMENT_TAB_WIDGET);
		if (widget != null && !widget.isHidden()) {
			if (Clicking.interact(widget, "Worn Equipment")) {
				return Time.sleepUntil(() -> {
					return isOpen();
				}, 300, 400);
			}
		}

		return false;
	}

	private static int get(int[] widget, int[] hidden_widget) {
		Timer t = new Timer(Random.randomRange(4000, 5000));
		while (t.isRunning()) {
			if (open()) {
				Widget w = Widgets.getWidget(widget);
				if (w == null) {
					Time.sleep(20, 40);
					continue;
				}

				Widget parent = w.getParent();
				while (parent != null) {
					if (parent.isHidden()) {
						Time.sleep(20, 40);
						continue;
					}
					parent = parent.getParent();
				}

				if (w.isHidden())
					return -1;

				int id = w.getItemId();
				Bot.LOGGER.log(Level.INFO, "id : " + id);
				return id;
			}
		}
		return -1;
	}

	private static Widget getWidget(int[] widget, int[] hidden_widget) {
		Timer t = new Timer(Random.randomRange(4000, 5000));
		while (t.isRunning()) {
			if (open()) {
				Widget hw = Widgets.getWidget(hidden_widget);
				if (hw == null || hw.isHidden()) {
					Time.sleep(50, 100);
					continue;
				}

				Widget w = Widgets.getWidget(widget);
				if (w == null) {
					Time.sleep(50, 100);
					continue;
				}

				return w;
			}
		}
		return null;
	}

	public static int getAmulet() {
		return get(EQUIPMENT_AMULET_WIDGET, EQUIPMENT_AMULET_HIDDEN_WIDGET);
	}

	public static int getArrow() {
		return get(EQUIPMENT_ARROW_WIDGET, EQUIPMENT_ARROW_HIDDEN_WIDGET);
	}

	public static int getBody() {
		return get(EQUIPMENT_BODY_WIDGET, EQUIPMENT_BODY_HIDDEN_WIDGET);
	}

	public static int getBoots() {
		return get(EQUIPMENT_BOOTS_WIDGET, EQUIPMENT_BOOTS_HIDDEN_WIDGET);
	}

	public static int getCape() {
		return get(EQUIPMENT_CAPE_WIDGET, EQUIPMENT_CAPE_HIDDEN_WIDGET);
	}

	public static int getGloves() {
		return get(EQUIPMENT_GLOVES_WIDGET, EQUIPMENT_GLOVES_HIDDEN_WIDGET);
	}

	public static int getHelmet() {
		return get(EQUIPMENT_HELMET_WIDGET, EQUIPMENT_HELMET_HIDDEN_WIDGET);
	}

	public static int getLegs() {
		return get(EQUIPMENT_LEGS_WIDGET, EQUIPMENT_LEGS_HIDDEN_WIDGET);
	}

	public static int getRing() {
		return get(EQUIPMENT_RING_WIDGET, EQUIPMENT_RING_HIDDEN_WIDGET);
	}

	public static int getShield() {
		return get(EQUIPMENT_SHIELD_WIDGET, EQUIPMENT_SHIELD_HIDDEN_WIDGET);
	}

	public static int getWeapon() {
		return get(EQUIPMENT_WEAPON_WIDGET, EQUIPMENT_WEAPON_HIDDEN_WIDGET);
	}

	public static Widget getAmuletWidget() {
		return getWidget(EQUIPMENT_AMULET_WIDGET, EQUIPMENT_AMULET_HIDDEN_WIDGET);
	}

	public static Widget getRingWidget() {
		return getWidget(EQUIPMENT_RING_WIDGET, EQUIPMENT_RING_HIDDEN_WIDGET);
	}
}
