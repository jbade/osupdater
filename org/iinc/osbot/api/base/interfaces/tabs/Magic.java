package org.iinc.osbot.api.base.interfaces.tabs;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;

public class Magic {
	private static final int[] MAGIC_WIDGET = { 548, 53 };

	public static enum SPELL {
		LUMBRIDGE_HOME_TELEPORT("Lumbridge Home Teleport", 356, new int[] { 218, 1 }), WIND_STRIKE("Wind Strike", 15,
				new int[] { 218, 2 });

		String name;
		int enabledTextureID;
		int[] widget;

		private SPELL(String name, int enabledTextureID, int[] widget) {
			this.name = name;
			this.enabledTextureID = enabledTextureID;
			this.widget = widget;
		}
	}

	public static boolean isOpen() {
		Widget widget = Widgets.getWidget(MAGIC_WIDGET);
		return widget != null && widget.getTextureId() != -1;
	}

	public static boolean open() {
		if (isOpen())
			return true;

		Widget widget = Widgets.getWidget(MAGIC_WIDGET);
		if (widget != null && !widget.isHidden()) {
			if (Clicking.interact(widget, new String[] { "Magic" }, null))
				return Time.sleepUntil(() -> isOpen(), 1000, 1400);
		}

		return false;
	}

	public static boolean canCast(SPELL spell) {
		if (!open())
			return false;

		Widget w = Widgets.getWidget(spell.widget);
		return w != null && spell.name.equals(w.getName()) && w.getTextureId() == spell.enabledTextureID;
	}

	public static boolean cast(SPELL spell) {
		if (!open())
			return false;

		Widget w = Widgets.getWidget(spell.widget);
		if (w != null && spell.name.equals(w.getName()) && w.getTextureId() == spell.enabledTextureID) {
			return Clicking.interact(w, new String[] { "Cast" }, new String[] { spell.name });
		}
		return false;
	}
}
