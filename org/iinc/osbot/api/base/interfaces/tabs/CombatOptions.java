package org.iinc.osbot.api.base.interfaces.tabs;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;

public class CombatOptions {
	private static final int[] COMBAT_OPTIONS_WIDGET = { 548, 47 };

	// enabled textureId = 654
	private static final int[][] COMBAT_OPTIONS_STYLES = { { 593, 4 }, { 593, 8 }, { 593, 12 }, { 593, 16 } };

	private static final int[] COMBAT_OPTIONS_SPECIAL_ATTACK = { 593, 34 };

	public static boolean isOpen() {
		Widget widget = Widgets.getWidget(COMBAT_OPTIONS_WIDGET);
		return widget != null && widget.getTextureId() != -1;
	}

	public static boolean open() {
		if (isOpen())
			return true;

		Widget widget = Widgets.getWidget(COMBAT_OPTIONS_WIDGET);
		if (widget != null && !widget.isHidden()) {
			if (Clicking.interact(widget, "Combat Options"))
				return Time.sleepUntil(() -> isOpen(), 1000, 1400);
		}

		return false;
	}

	public static int getStyle() {
		for (int i = 0; i < COMBAT_OPTIONS_STYLES.length; i++) {
			Widget widget = Widgets.getWidget(COMBAT_OPTIONS_STYLES[i]);
			if (widget != null && widget.getTextureId() == 654) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * 
	 * @param style
	 *            3 may appear as 2 if there are only 3 styles </br>
	 *            0,1 </br>
	 *            2,3
	 * @return
	 */
	public static boolean setStyle(int style) {
		if (style < 0 || style > 3)
			return false;

		if (getStyle() == style)
			return true;

		if (open()) {
			Widget widget = Widgets.getWidget(COMBAT_OPTIONS_STYLES[style]);
			if (widget != null && !widget.isHidden()) {
				if (Clicking.interact(widget)) {
					return Time.sleepUntil(() -> getStyle() == style, 1000, 1400);
				}
			}
		}

		return false;
	}

	public static int getSpecialAttackPercent() {
		return Client.getSetting(300) / 10;
	}

	public static boolean isSpecialAttackEnabled() {
		return Client.getSetting(301) == 1;
	}

	public static void clickSpecialAttack() {
		if (open()) {
			Widget w = Widgets.getWidget(COMBAT_OPTIONS_SPECIAL_ATTACK);
			if (w == null || w.isHidden())
				return;

			Clicking.interact(w);
		}
	}

}
