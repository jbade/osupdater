package org.iinc.osbot.api.base.interfaces.tabs;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;

public class Stats {
	private static final int[] STATS_WIDGET = { 548, 48 };

	public static boolean isOpen() {
		Widget widget = Widgets.getWidget(STATS_WIDGET);
		return widget != null && widget.getTextureId() == 1030;
	}

	public static boolean open() {
		if (isOpen())
			return true;

		Widget widget = Widgets.getWidget(STATS_WIDGET);
		if (widget != null && !widget.isHidden()) {
			if (Clicking.interact(widget, new String[] { "Stats" }, null))
				return Time.sleepUntil(() -> isOpen(), 1000, 1400);
		}

		return false;
	}
}
