package org.iinc.osbot.api.base.interfaces.tabs;

import java.util.ArrayList;
import java.util.List;

import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;

public class ChatBox {

	private static final int[] CHATBOX_MESSAGE_CONTAINER_WIDGET = new int[] { 162, 44 };

	public static List<String> getMessages() {
		return getMessages(-1);
	}

	public static List<String> getMessages(int limit) {
		ArrayList<String> result = new ArrayList<String>();
		Widget parent = Widgets.getWidget(CHATBOX_MESSAGE_CONTAINER_WIDGET);
		if (parent == null || parent.isHidden())
			return result;

		Widget[] children = parent.getChildren();
		if (children.length == 0)
			return result;

		String msg = "";
		int y = children[0].getY();
		int hiddenStreak = 0;

		for (int i = 0; i < children.length; i++) {
			if (y != children[i].getY()) {
				result.add(msg);
				if (limit != -1 && result.size() >= limit)
					break;
				y = children[i].getY();
				msg = "";
			}
			if (!children[i].isHidden()) {
				hiddenStreak = 0;
				msg += transform(children[i].getText());
			} else {
				hiddenStreak++;
			}

			if (hiddenStreak >= 6) { // this could be less but 6 is safe
				break;
			}
		}

		return result;
	}

	public static List<String> getNewMessages(List<String> previous) {
		List<String> current = getMessages(previous.size());

		int psize = previous.size();
		int csize = current.size();
		for (int i = 0; i < csize; i++) {
			if (previous.subList(0, Math.min(psize, csize - i)).equals(current.subList(i, csize))) {
				return current.subList(0, i);
			}
		}

		return current;
	}

	private static String transform(String text) {
		if (text == null)
			return null;

		return text.replaceAll("<col=[0-9a-z]{1,6}>", "").replaceAll("<\\/col>", ""); // .trim();
	}
}
