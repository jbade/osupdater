package org.iinc.osbot.api.base.interfaces.grandexchange;

import java.util.List;
import java.util.stream.Collectors;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.grandexchange.GrandExchange;
import org.iinc.osbot.api.injection.accessors.grandexchange.GrandExchangeOffer;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class GrandExchangeInterface {
	static final int[] PARENT = { 465, 0 };
	static final int[][] SLOT_WIDGETS = { { 465, 7 }, { 465, 8 }, { 465, 9 }, { 465, 10 }, { 465, 11 }, { 465, 12 },
			{ 465, 13 }, { 465, 14 } };
	static final int[] COLLECT_BUTTON_WIDGET = { 465, 6, 1 };
	static final int[] CLOSE_BUTTON_WIDGET = { 465, 2, 11 };

	public static boolean isOpen() {
		Widget widget = Widgets.getWidget(PARENT);
		return widget != null && !widget.isHidden();
	}

	public static boolean close() {
		boolean success = !isOpen();
		for (int tries = 0; tries < 5 && !success; tries++) {

			Widget widget = Widgets.getWidget(CLOSE_BUTTON_WIDGET);
			if (widget != null && !widget.isHidden() && Clicking.interact(widget)) {
				success = Time.sleepUntil(() -> !isOpen(), 1000, 1200);
			} else {
				success = !isOpen();
				if (!success)
					Time.sleep(200, 300);
			}
		}

		return success;
	}

	public static String getItemName(GrandExchangeOffer offer) {
		if (!isOpen() || offer == null)
			return null;

		for (int tries = 0; tries < 5; tries++) {
			Widget w = Widgets.getWidget(SLOT_WIDGETS[offer.getIndex()]);
			if (w != null) {
				Widget[] children = w.getChildren();
				if (children != null && children.length >= 20) {
					Widget w2 = children[19];
					if (w2 != null){
						String text = w2.getText();
						if (text != null)
							return text;
					}
				}
			}
			Time.sleep(200, 300);
		}

		return null;
	}

	public static boolean canCollect(){
		if (!isOpen())
			return false;

		if (OfferMenu.isOpen())
			if (!OfferMenu.cancel())
				return false;
		Widget widget = Widgets.getWidget(COLLECT_BUTTON_WIDGET);
		return widget != null && !widget.isHidden();
	}
	
	public static boolean collect(boolean inventory) {
		if (!isOpen())
			return false;

		if (OfferMenu.isOpen())
			if (!OfferMenu.cancel())
				return false;
		
		Widget widget = Widgets.getWidget(COLLECT_BUTTON_WIDGET);
		boolean success = widget == null || widget.isHidden();
		for (int tries = 0; tries < 5 && !success; tries++) {
			if (Clicking.interact(widget, inventory ? "Collect to inventory" : "Collect to bank")) {
				Time.sleepUntil(() -> {
					Widget w = Widgets.getWidget(COLLECT_BUTTON_WIDGET);
					return w == null || w.isHidden();
				}, 1500, 2000);
			} else {
				Time.sleep(200, 300);
			}
			
			widget = Widgets.getWidget(COLLECT_BUTTON_WIDGET);
			success = widget == null || widget.isHidden();
		}

		return success;
	}

	public static boolean collect(GrandExchangeOffer offer, boolean inventory) {
		if (!isOpen())
			return false;

		if (OfferMenu.isOpen())
			if (!OfferMenu.cancel())
				return false;

		boolean old = Settings.MAKE_CLICKING_ERRORS;
		Settings.MAKE_CLICKING_ERRORS = false;

		boolean success = false;
		for (int tries = 0; tries < 5 && !success; tries++) {
			Widget widget = Widgets.getWidget(SLOT_WIDGETS[offer.getIndex()]);

			if (widget != null && !widget.isHidden() && Clicking.interact(widget, "View offer")) {
				success = Time.sleepUntil(() -> OfferStatus.isOpen(), 3000, 4000);
			} else {
				success = Time.sleepUntil(() -> OfferStatus.isOpen(), 200, 300);
			}
		}
		
		if (success) {
			success = OfferStatus.collect(inventory);
		}
		
		Settings.MAKE_CLICKING_ERRORS = old;
		
		return success;
	}

	public static boolean abortOffer(GrandExchangeOffer offer) {
		if (!isOpen())
			return false;

		if (OfferMenu.isOpen())
			if (!OfferMenu.cancel())
				return false;

		boolean old = Settings.MAKE_CLICKING_ERRORS;
		Settings.MAKE_CLICKING_ERRORS = false;

		boolean success = offer.getState() == GrandExchangeOffer.STATES.COMPLETE;
		for (int tries = 0; tries < 5 && !success; tries++) {
			Widget widget = Widgets.getWidget(SLOT_WIDGETS[offer.getIndex()]);

			if (widget != null && !widget.isHidden() && Clicking.interact(widget, "Abort offer")) {
				success = Time.sleepUntil(() -> {
					GrandExchangeOffer o = GrandExchange.getOffer(offer.getIndex());
					return o != null && o.getState() == GrandExchangeOffer.STATES.COMPLETE;
				}, 3000, 4000);
			} else {
				Time.sleep(200, 300);
			}
		}

		Settings.MAKE_CLICKING_ERRORS = old;
		return success;
	}

	public static boolean createBuyOfferPercent(String item, double percent, int quantity) {
		System.out.println("Creating buy offer item: " + item + ", percent: " + percent + ", quantity: " + quantity);

		if (GrandExchange.getEmptySlots() == 0)
			return false;

		if (OfferMenu.isOpen() && OfferMenu.isSellOffer())
			if (!OfferMenu.cancel())
				return false;

		if (!OfferMenu.openBuyOffer())
			return false;

		if (OfferMenu.setBuyItem(item)) {
			OfferMenu.setPricePercent(percent);
			OfferMenu.setQuantity(quantity == -1 ? GrandExchangeInventory.getCount(item) : quantity, quantity == -1);
			if (OfferMenu.getItemName().equals(item)
					&& (quantity == -1 && OfferMenu.getQuantity() == GrandExchangeInventory.getCount(item)
							|| OfferMenu.getQuantity() == quantity)) {
				if (OfferMenu.confirm())
					return true;
			}
		}

		if (OfferMenu.isOpen())
			OfferMenu.cancel();

		return false;
	}

	public static boolean createSellOfferPercent(String item, double percent, int quantity) {
		System.out.println("Creating sell offer item: " + item + ", percent: " + percent + ", quantity: " + quantity);

		if (GrandExchange.getEmptySlots() == 0)
			return false;

		if (OfferMenu.isOpen() && OfferMenu.isBuyOffer())
			if (!OfferMenu.cancel())
				return false;

		if (OfferMenu.setSellItem(item)) {
			OfferMenu.setPricePercent(percent);
			OfferMenu.setQuantity(quantity == -1 ? GrandExchangeInventory.getCount(item) : quantity, quantity == -1);

			if (OfferMenu.getItemName().equals(item)
					&& (quantity == -1 && OfferMenu.getQuantity() == GrandExchangeInventory.getCount(item)
							|| OfferMenu.getQuantity() == quantity)) {
				if (OfferMenu.confirm())
					return true;
			}
		}

		if (OfferMenu.isOpen())
			OfferMenu.cancel();

		return false;
	}

	// -1 = all
	public static boolean createBuyOffer(String item, int price, int quantity) {
		System.out.println("Creating buy offer item: " + item + ", price: " + price + ", quantity: " + quantity);

		if (GrandExchange.getEmptySlots() == 0)
			return false;

		if (OfferMenu.isOpen() && OfferMenu.isSellOffer())
			if (!OfferMenu.cancel())
				return false;

		if (!OfferMenu.openBuyOffer())
			return false;

		if (OfferMenu.setBuyItem(item)) {
			OfferMenu.setPrice(price);
			OfferMenu.setQuantity(quantity == -1 ? GrandExchangeInventory.getCount(item) : quantity, quantity == -1);
			if (OfferMenu.getItemName().equals(item) && OfferMenu.getPrice() == price
					&& (quantity == -1 && OfferMenu.getQuantity() == GrandExchangeInventory.getCount(item)
							|| OfferMenu.getQuantity() == quantity)) {
				if (OfferMenu.confirm())
					return true;
			}
		}

		if (OfferMenu.isOpen())
			OfferMenu.cancel();

		return false;
	}

	// -1 = all
	public static boolean createSellOffer(String item, int price, int quantity) {
		System.out.println("Creating sell offer item: " + item + ", price: " + price + ", quantity: " + quantity);

		if (GrandExchange.getEmptySlots() == 0)
			return false;

		if (OfferMenu.isOpen() && OfferMenu.isBuyOffer())
			if (!OfferMenu.cancel())
				return false;

		if (OfferMenu.setSellItem(item)) {
			OfferMenu.setPrice(price);
			OfferMenu.setQuantity(quantity == -1 ? GrandExchangeInventory.getCount(item) : quantity, quantity == -1);

			if (OfferMenu.getItemName().equals(item) && OfferMenu.getPrice() == price
					&& (quantity == -1 && OfferMenu.getQuantity() == GrandExchangeInventory.getCount(item)
							|| OfferMenu.getQuantity() == quantity)) {
				if (OfferMenu.confirm())
					return true;
			}
		}

		if (OfferMenu.isOpen())
			OfferMenu.cancel();

		return false;
	}

	public static boolean openClerk() {
		if (isOpen())
			return true;

		if (Interacting.interact(() -> Npcs.getAll().parallelStream().filter((npc) -> {
			String name = npc.getNpcDefinition().getName();
			return name != null && (name.equals("Grand Exchange Clerk"));
		}).sorted().findFirst().orElse(null), new Tile(3164, 3489, 0), "Exchange")) {
			Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting(), 600, 800);
			boolean success = Time.sleepUntil(() -> isOpen(), 3000, 4000);
			if (success) // need to wait for interface to load
				Time.sleep(100, 200);
			return success;
		}

		return false;
	}

	public static boolean abortAndCollect(boolean inventory) {
		if (!isOpen())
			return false;
		
		Timer t = new Timer(Random.randomRange(20 * 1000, 30 * 1000));
		while (GrandExchange.getOffers().size() != 0 && t.isRunning()) {
			if (GrandExchangeInterface.collect(inventory))
				Time.sleep(600, 1200);
			for (GrandExchangeOffer offer : GrandExchange.getOffers()) {
				GrandExchangeInterface.abortOffer(offer);
			}
			Time.sleep(600, 1200);
		}

		return GrandExchange.getOffers().size() == 0;
	}

	public static boolean sellInventory() {
		if (!isOpen())
			return false;
		Timer t = new Timer(Random.randomRange(60 * 1000, 90 * 1000));
		List<String> items = GrandExchangeInventory.getInventoryItems().stream().filter((s) -> !s.equals("Coins"))
				.distinct().collect(Collectors.toList());
		while (GrandExchangeInterface.isOpen() && items.size() > 0 && t.isRunning()) {
			if (GrandExchange.getEmptySlots() == 0)
				GrandExchangeInterface.abortAndCollect(true);

			for (int i = 0; i < items.size() && GrandExchange.getEmptySlots() > 0; i++) {
				GrandExchangeInterface.createSellOffer(items.get(i), 1, -1);
				Time.sleep(600, 1200);
			}

			items = GrandExchangeInventory.getInventoryItems().stream().filter((s) -> !s.equals("Coins")).distinct()
					.collect(Collectors.toList());
		}

		GrandExchangeInterface.abortAndCollect(true);

		return GrandExchangeInventory.getInventoryItems().stream().filter((s) -> !s.equals("Coins")).count() == 0;
	}

	
	// None of the methods below this line work
	
	
	public static int checkInstantPrice(boolean buy, String item, int id) {
		System.out.println("checkInstantPrice " + buy + " " + item + " " + id);
		
		boolean success = false;
		for (int tries = 0; tries < 5 && !success; tries++) {
			if (buy) {
				success = createBuyOfferPercent(item, 2, 1);
			} else {
				success = createSellOfferPercent(item, 0.5, 1);
			}
		}
		
		System.out.println("checkInstantPrice " + success);
		
		if (!success)
			return -1;

		if (!Time.sleepUntil(() -> {
			for (GrandExchangeOffer offer : GrandExchange.getOffers()) {
				if ((offer.getItemId() == id || offer.getItemId() == id - 1) && offer.getState() == GrandExchangeOffer.STATES.COMPLETE)
					return true;
			}
			return false;
		}, 8000, 10000)) {
			System.out.println("checkInstantPrice if");
			boolean done = false;
			while (!done && Login.isLoggedIn() && GrandExchangeInterface.isOpen()) {
				done = true;
				for (GrandExchangeOffer offer : GrandExchange.getOffers()) {
					if (offer.getItemId() == id || offer.getItemId() == id - 1) {
						done = done && GrandExchangeInterface.abortOffer(offer)
								&& GrandExchangeInterface.collect(offer, true);
					}
				}
			}
			return -1;
		}

		System.out.println("checkInstantPrice else");
		for (GrandExchangeOffer offer : GrandExchange.getOffers()) {
			if ((offer.getItemId() == id || offer.getItemId() == id - 1) && offer.getState() == GrandExchangeOffer.STATES.COMPLETE) {
				System.out.println("checkInstantPrice in");
				while (GrandExchangeInterface.isOpen() && !GrandExchangeInterface.collect(offer, true))
					;
				System.out.println("checkInstantPrice collected");
				return offer.getSpent();
			}
		}

		return -1;
	}

	public static Margin checkMargin(String item, int id) {
		if (!isOpen())
			return null;

		Margin result = new Margin();

		int i = checkInstantPrice(true, item, id);
		if (i == -1)
			return null;
		result.instantBuy = i;

		i = checkInstantPrice(false, item, id);
		if (i == -1)
			return null;
		result.instantSell = i;

		return result;
	}

	public static boolean createSlowBuyOffer(String item, int id, int maxQuantity) {
		Margin margin = checkMargin(item, id);
		if (margin == null)
			return false;

		int price = margin.instantSell + 1;
		maxQuantity = Math.min(maxQuantity, Inventory.getCount(995) / price);

		return createBuyOffer(item, price, maxQuantity);
	}

	public static boolean createSlowSellOffer(String item, int id, int quantity) {
		Margin margin = checkMargin(item, id);
		if (margin == null)
			return false;

		return createSellOffer(item, margin.instantBuy - 1, quantity);
	}

}
