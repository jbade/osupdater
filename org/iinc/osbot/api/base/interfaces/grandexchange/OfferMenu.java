package org.iinc.osbot.api.base.interfaces.grandexchange;

import java.awt.event.KeyEvent;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.grandexchange.GrandExchange;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Keyboard;
import org.iinc.osbot.util.Random;

public class OfferMenu {
	static final int BUY_BUTTON_CHILD = 0;
	static final int[] BACK_WIDGET = { 465, 4 };
	static final int[] OFFER_WIDGET = { 465, 24 };
	static final int[] OFFER_TYPE_LABEL_WIDGET = { 465, 24, 18 };
	static final int[] OFFER_SEARCH_ITEM_BUTTON_WIDGET = { 465, 24, 0 };
	static final int[] OFFER_QUANTITY_BUTTON_WIDGET = { 465, 24, 7 };
	static final int[] OFFER_QUANTITY_WIDGET = { 465, 24, 32 };
	static final int[] OFFER_PRICE_BUTTON_WIDGET = { 465, 24, 12 };
	static final int[] OFFER_PRICE_WIDGET = { 465, 24, 39 };
	static final int[] OFFER_ITEM_NAME_WIDGET = { 465, 24, 25 };
	static final int[] OFFER_COST_WIDGET = { 465, 24, 43 };
	static final int[] OFFER_CONFIRM_BUTTON_WIDGET = { 465, 24, 54 };
	static final int[] OFFER_ALL_BUTTON_WIDGET = { 465, 24, 6 };
	static final int[] SEARCH_TEXT_WIDGET = { 162, 34 };
	static final int[] SEARCH_RESULTS_BASE_WIDGET = { 162, 39 };
	static final int[] QUANTITY_TEXT_WIDGET = { 162, 34 };
	static final int[] QUANTITY_ENTER_LABEL_WIDGET = { 162, 33 };
	static final int[] PRICE_TEXT_WIDGET = { 162, 34 };
	static final int[] PRICE_ENTER_LABEL_WIDGET = { 162, 33 };
	

	public static boolean isOpen() {
		Widget widget = Widgets.getWidget(OFFER_WIDGET);
		return widget != null && !widget.isHidden();
	}

	public static boolean isBuyOffer() {
		Widget widget = Widgets.getWidget(OFFER_TYPE_LABEL_WIDGET);
		if (widget == null)
			return false;

		String text = widget.getText();
		return text != null && text.equals("Buy offer");
	}

	public static boolean isSellOffer() {
		Widget widget = Widgets.getWidget(OFFER_TYPE_LABEL_WIDGET);
		if (widget == null)
			return false;

		String text = widget.getText();
		return text != null && text.equals("Sell offer");
	}

	/**
	 * Gets the name of the item selected in the offer. "" if no item is selected
	 * 
	 * @return
	 */
	public static String getItemName() {
		Widget widget = Widgets.getWidget(OFFER_ITEM_NAME_WIDGET);
		if (widget == null)
			return "";

		String text = widget.getText();

		if (text.equals("Choose an item..."))
			return "";

		return text;
	}

	public static boolean isItemSearchBoxOpen() {
		Widget widget = Widgets.getWidget(SEARCH_TEXT_WIDGET);
		return widget != null && !widget.isHidden();
	}

	public static String getSearchInput() {
		Widget widget = Widgets.getWidget(SEARCH_TEXT_WIDGET);
		if (widget == null)
			return "";

		String text = widget.getText();
		text = text.replaceAll("What would you like to buy\\? ", "");
		if (text.length() > 0)
			text = text.substring(0, text.length() - 1); // remove ending *
		return text;
	}

	public static int getQuantityInput() {
		Widget widget = Widgets.getWidget(QUANTITY_TEXT_WIDGET);
		if (widget == null)
			return -1;

		String text = widget.getText();
		if (text.length() > 1) {
			try {
				return Integer.parseInt(text.substring(0, text.length() - 1)); // remove ending *
			} catch (Exception e){
				e.printStackTrace();
			}
		}

		return -1;
	}

	public static int getPriceInput() {
		Widget widget = Widgets.getWidget(PRICE_TEXT_WIDGET);
		if (widget == null)
			return -1;

		String text = widget.getText();
		if (text.length() > 1) {
			try {
				return Integer.parseInt(text.substring(0, text.length() - 1)); // remove ending *
			} catch (Exception e){
				e.printStackTrace();
			}
		}

		return -1;
	}

	public static int getQuantity() {
		Widget widget = Widgets.getWidget(OFFER_QUANTITY_WIDGET);
		if (widget == null || widget.isHidden())
			return -1;

		try {
			return Integer.parseInt(widget.getText().replaceAll(",", ""));
		} catch (NumberFormatException e) {
			return -1;
		}
	}

	public static int getPrice() {
		Widget widget = Widgets.getWidget(OFFER_PRICE_WIDGET);
		if (widget == null || widget.isHidden())
			return -1;

		try {
			return Integer.parseInt(widget.getText().replaceAll(" coins?", "").replaceAll(",", ""));
		} catch (NumberFormatException e) {
			return -1;
		}
	}

	public static boolean isQuantityInputOpen() {
		Widget widget = Widgets.getWidget(QUANTITY_ENTER_LABEL_WIDGET);
		return widget != null && !widget.isHidden() && widget.getText().startsWith("How many do you wish to");
	}

	public static boolean isPriceInputOpen() {
		Widget widget = Widgets.getWidget(PRICE_ENTER_LABEL_WIDGET);
		return widget != null && !widget.isHidden() && "Set a price for each item:".equals(widget.getText());
	}

	public static boolean openItemSearchBox() {
		if (!isOpen())
			return false;

		boolean success = isItemSearchBoxOpen();
		for (int tries = 0; !success && tries < 5; tries++) {
			if (Clicking.interact(Widgets.getWidget(OFFER_SEARCH_ITEM_BUTTON_WIDGET), "Choose item")) {
				success = Time.sleepUntil(() -> isItemSearchBoxOpen(), 800, 1000);
			}
		}

		return success;
	}

	public static boolean openQuantityInput() {
		if (!isOpen())
			return false;

		boolean success = isQuantityInputOpen();
		for (int tries = 0; !success && tries < 5; tries++) {
			if (Clicking.interact(Widgets.getWidget(OFFER_QUANTITY_BUTTON_WIDGET))) {
				success = Time.sleepUntil(() -> isQuantityInputOpen(), 800, 1000);
			}
		}

		return success;
	}

	public static boolean openPriceInput() {
		if (!isOpen())
			return false;

		boolean success = isPriceInputOpen();
		for (int tries = 0; !success && tries < 5; tries++) {
			if (Clicking.interact(Widgets.getWidget(OFFER_PRICE_BUTTON_WIDGET))) {
				success = Time.sleepUntil(() -> isPriceInputOpen(), 800, 1000);
			}
		}

		return success;
	}

	public static boolean setQuantity(int quantity, boolean all) {
		if (!isOpen() || getItemName() == "")
			return false;

		boolean success = getQuantity() == quantity;
		if (all) {
			for (int tries = 0; tries < 5 && !success; tries++) {
				Widget w = Widgets.getWidget(OFFER_ALL_BUTTON_WIDGET);
				if (w != null && !w.isHidden() && Clicking.interact(w)) {
					success = Time.sleepUntil(() -> getQuantity() == quantity, 1000, 1500);
				}
			}
		} else {
			if (!success && openQuantityInput()) {
				for (int tries = 0; tries < 5 && !success; tries++) {
					if (getQuantityInput() != -1) {
						long end = System.currentTimeMillis() + Random.randomRange(4000, 6000);
						while (System.currentTimeMillis() < end && getQuantityInput() != -1) {
							Keyboard.sendKey(KeyEvent.VK_BACK_SPACE);
						}
					}

					Keyboard.sendKeys(quantity + "");
					success = Time.sleepUntil(() -> getQuantityInput() == quantity, 1000, 1500);
				}

				Keyboard.sendKey(KeyEvent.VK_ENTER);
				success = Time.sleepUntil(() -> getQuantity() == quantity, 1000, 1500);
			}
		}

		return success;
	}

	public static boolean setPricePercent(double percent) {
		if (!isOpen() || getItemName() == "")
			return false;

		int price = (int) (getPrice() * percent);
		if (price <= 0)
			return false;
		
		boolean success = getPrice() == price;
		if (openPriceInput()) {
			for (int tries = 0; tries < 5 && !success; tries++) {
				if (getPriceInput() != -1) {
					long end = System.currentTimeMillis() + Random.randomRange(4000, 6000);
					while (System.currentTimeMillis() < end && getQuantityInput() != -1) {
						Keyboard.sendKey(KeyEvent.VK_BACK_SPACE);
					}
				}

				Keyboard.sendKeys(price + "");
				success = Time.sleepUntil(() -> getPriceInput() == price, 1000, 1500);
			}

			Keyboard.sendKey(KeyEvent.VK_ENTER);
		}

		return Time.sleepUntil(() -> getPrice() == price, 1000, 1500);
	}

	public static boolean setPrice(int price) {
		if (!isOpen() || getItemName() == "")
			return false;

		boolean success = getPrice() == price;
		if (!success && openPriceInput()) {
			for (int tries = 0; tries < 5 && !success; tries++) {
				if (getPriceInput() != -1) {
					long end = System.currentTimeMillis() + Random.randomRange(4000, 6000);
					while (System.currentTimeMillis() < end && getQuantityInput() != -1) {
						Keyboard.sendKey(KeyEvent.VK_BACK_SPACE);
					}
				}

				Keyboard.sendKeys(price + "");
				success = Time.sleepUntil(() -> getPriceInput() == price, 1000, 1500);
			}

			Keyboard.sendKey(KeyEvent.VK_ENTER);
			success = Time.sleepUntil(() -> getPrice() == price, 1000, 1500);
		}

		return success;
	}

	public static boolean setBuyItem(String item) {
		if (!isOpen())
			return false;

		// wait for search box to load
		if (!Time.sleepUntil(() -> isItemSearchBoxOpen(), 600, 800) && !openItemSearchBox())
			return false;

		boolean complete = getSearchInput().toLowerCase().equals(item.toLowerCase());
		for (int tries = 0; !complete && tries < 5; tries++) {
			if (getSearchInput().length() > 0) {
				long end = System.currentTimeMillis() + Random.randomRange(4000, 6000);
				while (System.currentTimeMillis() < end && getSearchInput().length() > 0) {
					Keyboard.sendKey(KeyEvent.VK_BACK_SPACE);
				}
			}
			Keyboard.sendKeys(item.toLowerCase());
			complete = getSearchInput().toLowerCase().equals(item.toLowerCase());
		}

		if (!complete)
			return false;

		return selectSearchBoxItem(item);
	}

	public static boolean selectSearchBoxItem(String item) {
		boolean success = false;
		for (int tries = 0; tries < 5; tries++) {
			if (!isItemSearchBoxOpen())
				return getItemName().equals(item);

			Widget results = Widgets.getWidget(SEARCH_RESULTS_BASE_WIDGET);
			if (results == null) {
				Time.sleep(200, 300);
				continue;
			}

			Widget[] children = results.getChildren();

			for (int i = 1; i < children.length; i += 3) {
				if (children[i] != null && !children[i].isHidden() && children[i].getText().equals(item)) {
					Widget selector = children[i - 1];
					if (Clicking.interact(selector)) {
						success = Time.sleepUntil(() -> getItemName().equals(item), 1000, 1500);
						if (success)
							break;
					}
				}
			}

			if (!success)
				Time.sleep(200, 300);
		}
		return false;
	}

	public static boolean openBuyOffer() {
		if (isOpen()) {
			if (isBuyOffer())
				return true;
			if (isSellOffer())
				if (!cancel())
					return false;
		}

		boolean success = false;
		for (int tries = 0; tries < 5 && !success; tries++) {
			int index = GrandExchange.getEmptySlot();
			if (index == -1) {
				Time.sleep(200, 300);
				continue;
			}

			Widget widget = Widgets.getWidget(GrandExchangeInterface.SLOT_WIDGETS[index]);
			if (widget != null) {
				widget = widget.getChild(BUY_BUTTON_CHILD);

				if (widget != null && !widget.isHidden() && Clicking.interact(widget)) {
					success = Time.sleepUntil(() -> isOpen() && isBuyOffer(), 1000, 1500);
					continue;
				}

			}

			Time.sleep(200, 300);
		}
		return success;
	}

	public static boolean setSellItem(String item) {
		if (isOpen()) {
			if (isSellOffer() && getItemName().equals(item))
				return true;
			if (isBuyOffer())
				if (!cancel())
					return false;
		}

		boolean success = false;
		for (int tries = 0; tries < 5 && !success; tries++) {

			Widget parent = Widgets.getWidget(GrandExchangeInventory.INVENTORY_WIDGET);
			if (parent == null) {
				Time.sleep(200, 300);
				continue;
			}

			Widget[] children = parent.getChildren();
			for (int i = 0; i < children.length; i++) {
				if (children[i] != null && !children[i].isHidden() && children[i].getName().equals(item)) {
					if (Clicking.interact(children[i])) {
						success = Time.sleepUntil(() -> isSellOffer() && getItemName().equals(item), 1000, 1500);
						if (success)
							break;
					}
				}
			}
			if (!success) {
				Time.sleep(200, 300);
				success = Time.sleepUntil(() -> isSellOffer() && getItemName().equals(item), 1000, 1500);
			}

		}
		return success;
	}

	public static boolean confirm() {
		if (!isOpen())
			return true;

		boolean success = false;
		for (int tries = 0; tries < 5 && !success; tries++) {
			Widget widget = Widgets.getWidget(OFFER_CONFIRM_BUTTON_WIDGET);

			if (widget != null && !widget.isHidden() && Clicking.interact(widget)) {
				success = Time.sleepUntil(() -> !isOpen(), 1000, 1500);
			} else {
				Time.sleep(200, 300);
			}
		}

		return success;
	}

	public static boolean cancel() {
		if (!isOpen())
			return true;

		boolean success = false;
		for (int tries = 0; tries < 5 && !success; tries++) {
			Widget widget = Widgets.getWidget(BACK_WIDGET);

			if (widget != null && !widget.isHidden() && Clicking.interact(widget)) {
				success = Time.sleepUntil(() -> !isOpen(), 1000, 1500);
			} else {
				Time.sleep(200, 300);
			}
		}

		return success;
	}

}