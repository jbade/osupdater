package org.iinc.osbot.api.base.interfaces.grandexchange;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;

public class OfferStatus {
	static final int[] BACK_WIDGET = { 465, 4 };
	static final int[] COLLECT_BOXES_PARENT_WIDGET = { 465, 23 };
	
	public static boolean isOpen() {
		Widget widget = Widgets.getWidget(COLLECT_BOXES_PARENT_WIDGET);
		return widget != null && !widget.isHidden();
	}
	

	public static boolean collect(boolean inventory) {
		if (!isOpen())
			return false;

		boolean success = false;
		for (int tries = 0; tries < 5 && !success; tries++) {
			Widget widget = Widgets.getWidget(COLLECT_BOXES_PARENT_WIDGET);

			if (widget != null && !widget.isHidden()) {
				Widget[] children = widget.getChildren();
				success = children.length > 0;
				for (Widget child : children) {
					if (child.getActions().contains("Collect-note")) {
						success = success && Clicking.interact(child, inventory ? "Collect-note" : "Bank");
					} else if (child.getActions().contains("Collect-notes")) {
						success = success && Clicking.interact(child, inventory ? "Collect-notes" : "Bank");
					} else if (child.getActions().contains("Collect")) {
						success = success && Clicking.interact(child, inventory ? "Collect" : "Bank");
					}
				}
				if (success){
					Time.sleepUntil(() -> !isOpen(), 1200, 1500);
				}
			} else {
				Time.sleep(200, 300);
			}
		}

		if (isOpen()) {
			close();
		}

		return success;
	}
	
	public static boolean close() {
		if (!isOpen())
			return true;

		boolean success = false;
		for (int tries = 0; tries < 5 && !success; tries++) {
			Widget widget = Widgets.getWidget(BACK_WIDGET);

			if (widget != null && !widget.isHidden() && Clicking.interact(widget)) {
				success = Time.sleepUntil(() -> !isOpen(), 1000, 1500);
			} else {
				Time.sleep(200, 300);
			}
		}

		return success;
	}
}
