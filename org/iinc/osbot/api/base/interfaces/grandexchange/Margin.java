package org.iinc.osbot.api.base.interfaces.grandexchange;

/**
 * Helper class for tracking the margins that items are buying and selling at
 */
public class Margin {
	public int instantBuy, instantSell;

	public Margin() {
		this(-1, -1);
	}

	public Margin(int instantBuy, int instantSell) {
		this.instantBuy = instantBuy;
		this.instantSell = instantSell;
	}

}