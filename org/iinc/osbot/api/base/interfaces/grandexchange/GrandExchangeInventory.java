package org.iinc.osbot.api.base.interfaces.grandexchange;

import java.util.HashMap;
import java.util.HashSet;

import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.RSItem;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;

public class GrandExchangeInventory {
	static final int[] INVENTORY_WIDGET = { 467, 0 };

	public static int getCount(String itemName) {
		if (!GrandExchangeInterface.isOpen())
			return 0;

		int count = 0;

		Widget parent = Widgets.getWidget(INVENTORY_WIDGET);
		Widget[] children = parent.getChildren();
		for (int i = 0; i < children.length; i++) {
			if (!children[i].isHidden() && children[i].getName().equals(itemName)) {
				count += Inventory.getSlot(i).getQuantity();
			}
		}

		return count;
	}

	/**
	 * Returns a hashmap of the inventory contents. </br>
	 * keys: Item names </br>
	 * values: Total item quantity</br>
	 * 
	 * @return
	 */
	public static HashMap<RSItem, Integer> getInventory() {
		HashMap<RSItem, Integer> inv = new HashMap<RSItem, Integer>();

		if (!GrandExchangeInterface.isOpen())
			return inv;

		Widget parent = Widgets.getWidget(INVENTORY_WIDGET);
		Widget[] children = parent.getChildren();
		for (int i = 0; i < children.length; i++) {
			if (!children[i].isHidden() && children[i].getName() != "") {
				int count = children[i].getItemQuantity();
				RSItem item = new RSItem(children[i].getName(), children[i].getItemId());
				if (inv.containsKey(item)) {
					inv.put(item, inv.get(children[i].getName()) + count);
				} else {
					inv.put(item, count);
				}
			}
		}

		return inv;
	}

	public static HashSet<String> getInventoryItems() {
		HashSet<String> inv = new HashSet<String>();

		if (!GrandExchangeInterface.isOpen())
			return inv;

		Widget parent = Widgets.getWidget(INVENTORY_WIDGET);
		if (parent != null) {
			Widget[] children = parent.getChildren();
			for (int i = 0; i < children.length; i++) {
				if (!children[i].isHidden() && children[i].getName() != "") {
					inv.add(children[i].getName());
				}
			}
		}
		
		return inv;
	}
}
