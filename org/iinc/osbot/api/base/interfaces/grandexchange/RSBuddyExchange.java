package org.iinc.osbot.api.base.interfaces.grandexchange;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import org.iinc.osbot.util.internet.Internet;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class RSBuddyExchange {
	private static long nextSummaryUpdate;
	private static Map<String, SummaryPrice> summary;

	public static void main(String[] args){
		updateSummary();
		System.out.println(summary.get("2"));
	}
	
	public static void updateSummary() {
		List<String> lines = Internet.read("https://rsbuddy.com/exchange/summary.json");
		String body = "";
		for (String line : lines)
			body += line;
		System.out.println(body);
		Gson gson = new Gson();
		Type type = new TypeToken<Map<String, SummaryPrice>>() {
		}.getType();

		summary = gson.fromJson(body, type);
		nextSummaryUpdate = System.currentTimeMillis() + 5 * 60 * 1000;
	}

	public static Margin getPriceMarginSummary(int id) {
		if (nextSummaryUpdate < System.currentTimeMillis() || summary == null) {
			updateSummary();
		}

		SummaryPrice gp = summary.get(id + "");
		if (gp == null || gp.buy_average <= 0 || gp.sell_average <= 0)
			return null;

		return new Margin(Math.max(gp.sell_average, gp.buy_average), Math.min(gp.sell_average, gp.buy_average));
	}

	public static Margin getPriceMargin(int id) {
		List<String> lines = Internet.read("https://api.rsbuddy.com/grandExchange?a=guidePrice&i=" + id);
		String body = "";
		for (String line : lines)
			body += line;

		Gson gson = new Gson();
		GuidePrice gp = gson.fromJson(body, GuidePrice.class);

		if (gp == null || gp.buying <= 0 || gp.selling <= 0)
			return null;

		return new Margin(Math.max(gp.selling, gp.buying), Math.min(gp.selling, gp.buying));
	}

	private class SummaryPrice {
		private int buy_average, sp, id, sell_average, overall_average;
		private boolean members;
		private String name;
		
		@Override
		public String toString() {
			return "SummaryPrice [buy_average=" + buy_average + ", sp=" + sp + ", id=" + id + ", sell_average="
					+ sell_average + ", overall_average=" + overall_average + ", members=" + members + ", name=" + name
					+ "]";
		}
	}
	
	private class GuidePrice {
		private int overall;
		private int buying;
		private int buyingQuantity;
		private int selling;
		private int sellingQuantity;
		@Override
		public String toString() {
			return "GuidePrice [overall=" + overall + ", buying=" + buying + ", buyingQuantity=" + buyingQuantity
					+ ", selling=" + selling + ", sellingQuantity=" + sellingQuantity + "]";
		}
		
		
	}
}
