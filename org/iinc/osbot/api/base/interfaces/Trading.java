package org.iinc.osbot.api.base.interfaces;

import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;

public class Trading {
	private static final int[] TRADING_SCREEN_1_WIDGET = new int[] { 335, 0 };
	private static final int[] TRADING_SCREEN_2_WIDGET = new int[] { 334, 0 };

	private static final int[] TRADING_SCREEN_1_NAME_WIDGET = new int[] { 335, 31 };
	private static final int[] TRADING_SCREEN_2_NAME_WIDGET = new int[] { 334, 30 };

	private static final int[] TRADING_SCREEN_1_ACCEPT_WIDGET = new int[] { 335, 11 };
	private static final int[] TRADING_SCREEN_2_ACCEPT_WIDGET = new int[] { 334, 13 };

	private static final int[] TRADING_INVENTORY_WIDGET = new int[] { 336, 0 };

	public static boolean isOpen() {
		Widget w1 = Widgets.getWidget(TRADING_SCREEN_1_NAME_WIDGET);
		Widget w2 = Widgets.getWidget(TRADING_SCREEN_2_NAME_WIDGET);
		
		
		
		return (w1 != null && !w1.isHidden() && w1.getWidth() != 0) || (w2 != null && !w2.isHidden() && w2.getWidth() != 0);
	}

	public static boolean isScreen1() {
		return Widgets.exists(TRADING_SCREEN_1_WIDGET);
	}

	public static boolean accept() {
		Widget w = Widgets.getWidget(TRADING_SCREEN_1_ACCEPT_WIDGET);
		if (w != null) {
			return Clicking.interact(w);
			//if (Clicking.interact(w)) {
				//return Time.sleepUntil(() -> !Widgets.exists(TRADING_SCREEN_1_ACCEPT_WIDGET), 10000, 15000);
			//}
			//return false;
		}

		w = Widgets.getWidget(TRADING_SCREEN_2_ACCEPT_WIDGET);
		if (w != null) {
			return Clicking.interact(w);
			//if (Clicking.interact(w)) {
				//return Time.sleepUntil(() -> !Widgets.exists(TRADING_SCREEN_2_ACCEPT_WIDGET), 10000, 15000);
			//}
			//return false;
		}
		return false;
	}

	public static boolean offerAll(String item) {
		// TODO make this not so awful
		
		if (!Trading.isScreen1())
			return false;

		Widget parent = Widgets.getWidget(TRADING_INVENTORY_WIDGET);
		if (parent == null)
			return false;

		Widget[] children = parent.getChildren();
		for (int i = 0; i < children.length; i++) {
			if (children[i] != null && !children[i].isHidden() && children[i].getName().equals(item)) {
				return Clicking.interact(children[i], "Offer-All");
			}
		}
		
		return true;
	}

	public static String getName() {
		if (Widgets.exists(TRADING_SCREEN_2_WIDGET)) {
			Widget w = Widgets.getWidget(TRADING_SCREEN_2_NAME_WIDGET);
			if (w == null)
				return null;
			String text = w.getText();
			if (text == null)
				return null;

			return text.replace("Trading with:<br>", "").trim();
		} else if (Widgets.exists(TRADING_SCREEN_1_WIDGET)) {
			Widget w = Widgets.getWidget(TRADING_SCREEN_1_NAME_WIDGET);
			if (w == null)
				return null;
			String text = w.getText();
			if (text == null)
				return null;

			return text.replace("Trading With: ", "").trim();
		}
		return null;
	}

}
