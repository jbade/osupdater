package org.iinc.osbot.api.base.interfaces;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Interacting;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.ChatBox;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Items.RSItemConstant;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.NpcDefinition;
import org.iinc.osbot.api.injection.accessors.renderable.Npcs;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Keyboard;
import org.iinc.osbot.util.Random;
import org.iinc.osbot.util.Timer;

public class Bank {
	private static final int[] BANK_CONTAINER = { 12, 0 };
	private static final int[] BANK_CLOSE = { 12, 3, 11 };
	private static final int[] BANK_ITEM_PARENT = { 12, 12 };
	private static final int[] BANK_SEARCH_INPUT_LABEL = { 162, 33 };
	private static final int[] BANK_SEARCH = { 12, 27 };
	private static final int[] BANK_DEPOSIT_INVENTORY = { 12, 29 };
	private static final int[] BANK_DEPOSIT_EQUIPMENT = { 12, 31 };
	private static final int[] BANK_TOP_LABEL = { 12, 4 };
	private static final int[] BANK_QUANTITY_INPUT_LABEL = { 162, 33 };
	private static final int[] BANK_INVENTORY_PARENT = { 15, 3 };
	private static final int[] BANK_WITHDRAW_AS_ITEM = { 12, 21 };
	private static final int[] BANK_WITHDRAW_AS_NOTE = { 12, 23 };
	private static final int[] BANK_WITHDRAW_AS_NOTE_TEXTURED = { 12, 23, 0 };
	private static final int BANK_WITHDRAW_AS_NOTE_TEXTURED_ENABLED = 1150;

	public static final int[] COLLECT_CLOSE = { 402, 2, 11 };

	public static boolean isOpen() {
		Widget w = Widgets.getWidget(BANK_CONTAINER);
		return w != null && !w.isHidden();
	}

	public static boolean isSearchInputOpen() {
		Widget w = Widgets.getWidget(BANK_SEARCH_INPUT_LABEL);
		return w != null && !w.isHidden()
				&& w.getText().startsWith("Show items whose names contain the following text:");
	}

	public static boolean isSearching() {
		Widget w = Widgets.getWidget(BANK_TOP_LABEL);
		return w != null && !w.isHidden() && w.getText().startsWith("Showing items:");
	}

	public static boolean isQuantityInputOpen() {
		Widget w = Widgets.getWidget(BANK_QUANTITY_INPUT_LABEL);
		return w != null && !w.isHidden() && w.getText().startsWith("Enter amount:");
	}

	public static String getSearchedText() {
		Widget w = Widgets.getWidget(BANK_TOP_LABEL);
		if (w == null || w.isHidden())
			return null;

		String text = w.getText();
		if (text == null)
			return null;

		return text.replace("Showing items: ", "");
	}

	private static Widget getItemWidget(int itemId) {
		if (itemId < 0 || !isOpen())
			return null;

		Widget parent = Widgets.getWidget(BANK_ITEM_PARENT);
		for (Widget child : parent.getChildren()) {
			if (child != null && child.getItemId() == itemId)
				return child;
		}

		return null;
	}

	public static boolean contains(int itemId) {
		return getItemWidget(itemId) != null;
	}

	public static boolean contains(int... itemIds) {
		for (Integer itemId : itemIds) {
			if (Bank.contains(itemId))
				return true;
		}
		return false;
	}

	public static boolean contains(RSItemConstant[] sell) {
		for (RSItemConstant item : sell) {
			if (Bank.contains(item.getId()))
				return true;
		}
		return false;
	}
	
	public static int getQuantity(int itemId) {
		Widget w = getItemWidget(itemId);
		if (w != null)
			return w.getItemQuantity();
		return 0;
	}

	public static int getQuantity(int... itemIds) {
		int n = 0;
		for (Integer itemId : itemIds) {
			n += getQuantity(itemId);
		}
		return n;
	}

	public static boolean close() {
		Timer timer = new Timer(Random.randomRange(30000, 60000));
		boolean success = !Bank.isOpen();
		
		boolean temp = Settings.MAKE_CLICKING_ERRORS;
		Settings.MAKE_CLICKING_ERRORS = false;

		while (!success && timer.isRunning() && Bank.isOpen() && Login.isLoggedIn()) {
			if (Clicking.interact(Widgets.getWidget(BANK_CLOSE), "Close")) {
				success = Time.sleepUntil(() -> !Bank.isOpen(), 600, 1000);
			}
		}

		Settings.MAKE_CLICKING_ERRORS = temp;

		return success;
	}

	public static boolean depositInventory() {
		Timer timer = new Timer(Random.randomRange(30000, 60000));
		boolean success = Inventory.getCount() == 0;

		while (!success && timer.isRunning() && Bank.isOpen() && Login.isLoggedIn()) {
			if (Clicking.interact(Widgets.getWidget(BANK_DEPOSIT_INVENTORY), "Deposit inventory")) {
				success = Time.sleepUntil(() -> Inventory.getCount() == 0, 600, 1000);
			}
		}

		return success;
	}

	public static boolean depositEquipment() {
		List<String> previous = ChatBox.getMessages(50);
		Timer timer = new Timer(Random.randomRange(30000, 60000));
		boolean success = false;

		while (!success && timer.isRunning() && Bank.isOpen() && Login.isLoggedIn()) {
			if (Clicking.interact(Widgets.getWidget(BANK_DEPOSIT_EQUIPMENT), "Deposit worn items")) {
				success = Time.sleepUntil(
						() -> ChatBox.getNewMessages(previous).contains("You have nothing to deposit."), 300, 800);
			}
		}

		return success;
	}

	public static boolean withdrawAsItem() {
		Timer timer = new Timer(Random.randomRange(30000, 60000));
		Widget w = Widgets.getWidget(BANK_WITHDRAW_AS_NOTE_TEXTURED);
		boolean success = w != null && w.getTextureId() != BANK_WITHDRAW_AS_NOTE_TEXTURED_ENABLED;

		while (!success && timer.isRunning() && Bank.isOpen() && Login.isLoggedIn()) {
			if (Clicking.interact(Widgets.getWidget(BANK_WITHDRAW_AS_ITEM), "Item")) {
				success = Time.sleepUntil(() -> {
					Widget temp = Widgets.getWidget(BANK_WITHDRAW_AS_NOTE_TEXTURED);
					return temp != null && temp.getTextureId() != BANK_WITHDRAW_AS_NOTE_TEXTURED_ENABLED;
				}, 600, 1000);
			}
		}

		return success;
	}

	public static boolean withdrawAsNote() {
		Timer timer = new Timer(Random.randomRange(30000, 60000));
		Widget w = Widgets.getWidget(BANK_WITHDRAW_AS_NOTE_TEXTURED);
		boolean success = w != null && w.getTextureId() == BANK_WITHDRAW_AS_NOTE_TEXTURED_ENABLED;

		while (!success && timer.isRunning() && Bank.isOpen() && Login.isLoggedIn()) {
			if (Clicking.interact(Widgets.getWidget(BANK_WITHDRAW_AS_NOTE), "Note")) {
				success = Time.sleepUntil(() -> {
					Widget temp = Widgets.getWidget(BANK_WITHDRAW_AS_NOTE_TEXTURED);
					return temp != null && temp.getTextureId() == BANK_WITHDRAW_AS_NOTE_TEXTURED_ENABLED;
				}, 600, 1000);
			}
		}

		return success;
	}

	private static boolean cancelSearch() {
		Timer timer = new Timer(Random.randomRange(30000, 60000));
		boolean success = !isSearching();

		while (!success && timer.isRunning() && isSearching()) {
			if (Clicking.interact(Widgets.getWidget(BANK_SEARCH), "Search")) {
				success = Time.sleepUntil(() -> !isSearching(), 600, 1000);
			}
		}

		return success;
	}

	public static boolean withdraw(int[] itemIds, int quantity) {
		if (quantity != -1 && Bank.getQuantity(itemIds) < quantity)
			return false;

		int total = 0;
		for (Integer item : itemIds) {
			int ct = Bank.getQuantity(item);
			if (ct != 0) {
				if (quantity == -1 || ct + total < quantity) {
					if (!Bank.withdraw(item, -1))
						return false;
					total += ct;
				} else {
					return Bank.withdraw(item, quantity - total);
				}
			}
		}

		return quantity == -1;
	}

	// -1 = all
	public static boolean withdraw(int itemId, int quantity) {
		if (!isOpen() || !cancelSearch())
			return false;

		Widget widget = getItemWidget(itemId);
		if (widget == null)
			return false;

		boolean temp = Settings.MAKE_CLICKING_ERRORS;
		Settings.MAKE_CLICKING_ERRORS = false;

		if (widget.getY() < 83 || widget.getY() > 263) { // need to search
			Widget search = Widgets.getWidget(BANK_SEARCH);

			if (!Clicking.interact(search, "Search") || !Time.sleepUntil(() -> isSearchInputOpen(), 1000, 1400)) {
				cancelSearch();
				Settings.MAKE_CLICKING_ERRORS = temp;
				return false;
			}

			String itemName = widget.getName().toLowerCase();

			Keyboard.sendKeys(itemName);
			Keyboard.sendKey(KeyEvent.VK_ENTER);

			if (!Time.sleepUntil(() -> {
				String text = getSearchedText();
				if (text == null) {
					Settings.MAKE_CLICKING_ERRORS = temp;
					return false;
				}

				// TODO changed this from 15 to 16. its funny because you can
				// type 16 but once you type 17 it gets shortened to 15. lets
				// stick with 13
				if (itemName.length() > 13) {
					Settings.MAKE_CLICKING_ERRORS = temp;
					return (text.length() > 13 && itemName.startsWith(text));
				} else {
					Settings.MAKE_CLICKING_ERRORS = temp;
					return text.equals(itemName);
				}
			}, 1000, 1400)) {
				cancelSearch();
				Settings.MAKE_CLICKING_ERRORS = temp;
				return false;
			}

			// make sure widget bounds are updated
			widget = getItemWidget(itemId);
		}

		boolean success = false;
		switch (quantity) {
		case 1:
			success = Clicking.interact(widget, "Withdraw-1");
			break;
		case 5:
			success = Clicking.interact(widget, "Withdraw-5");
			break;
		case 10:
			success = Clicking.interact(widget, "Withdraw-10");
			break;
		case -1:
			success = Clicking.interact(widget, "Withdraw-All");
			break;
		default:
			if (Clicking.hover(widget)) {
				if (Menu.contains("Withdraw-" + quantity, null)) {
					success = Menu.interact("Withdraw-" + quantity);
					break;
				}
			}

			if (Clicking.interact(widget, "Withdraw-X")) {
				if (Time.sleepUntil(() -> isQuantityInputOpen(), 1400, 2000)) {
					Keyboard.sendKeys(quantity + "");
					Keyboard.sendKey(KeyEvent.VK_ENTER);
					success = true;
				}
			}
		}

		Settings.MAKE_CLICKING_ERRORS = temp;
		return success;
	}

	public static boolean depositSlot(int slot, int quantity) {
		if (slot < 0 || slot > 27 || !isOpen())
			return false;

		Widget parent = Widgets.getWidget(BANK_INVENTORY_PARENT);
		if (parent == null)
			return false;

		Widget[] children = parent.getChildren();
		if (children.length <= slot)
			return false;

		boolean temp = Settings.MAKE_CLICKING_ERRORS;
		Settings.MAKE_CLICKING_ERRORS = false;

		Widget widget = children[slot];
		if (widget != null && !widget.isHidden()) {
			boolean success = false;
			switch (quantity) {
			case 1:
				success = Clicking.interact(widget, "Deposit-1");
				break;
			case 5:
				success = Clicking.interact(widget, "Deposit-5");
				break;
			case 10:
				success = Clicking.interact(widget, "Deposit-10");
				break;
			case -1:
				success = Clicking.interact(widget, "Deposit-All");
				break;
			default:
				if (Clicking.hover(widget)) {
					if (Menu.contains("Deposit-" + quantity, null)) {
						success = Menu.interact("Deposit-" + quantity);
						break;
					}
				}

				if (Clicking.interact(widget, "Deposit-X")) {
					if (Time.sleepUntil(() -> isQuantityInputOpen(), 1400, 2000)) {
						Keyboard.sendKeys(quantity + "");
						Keyboard.sendKey(KeyEvent.VK_ENTER);
						success = true;
					}
				}
			}

			Settings.MAKE_CLICKING_ERRORS = temp;
			return success;
		}

		Settings.MAKE_CLICKING_ERRORS = temp;
		return false;
	}

	public static boolean deposit(String item, int quantity) {
		if (!isOpen())
			return false;

		Widget parent = Widgets.getWidget(BANK_INVENTORY_PARENT);
		if (parent == null)
			return false;

		boolean temp = Settings.MAKE_CLICKING_ERRORS;
		Settings.MAKE_CLICKING_ERRORS = false;

		ArrayList<Widget> widgets = new ArrayList<Widget>();

		for (Widget widget : parent.getChildren()) {
			if (widget != null && !widget.isHidden() && widget.getName().equals(item)) {
				widgets.add(widget);
			}
		}

		if (widgets.size() > 0) {
			Widget widget = widgets.get(Random.randomRange(0, widgets.size() - 1));
			boolean success = false;
			switch (quantity) {
			case 1:
				success = Clicking.interact(widget, "Deposit-1", item);
				break;
			case 5:
				success = Clicking.interact(widget, "Deposit-5", item);
				break;
			case 10:
				success = Clicking.interact(widget, "Deposit-10", item);
				break;
			case -1:
				success = Clicking.interact(widget, "Deposit-All", item);
				break;
			default:
				if (Clicking.interact(widget, "Deposit-X", item)) {
					if (Time.sleepUntil(() -> isQuantityInputOpen(), 1400, 2000)) {
						Keyboard.sendKeys(quantity + "");
						Keyboard.sendKey(KeyEvent.VK_ENTER);
						success = true;
					}
				}
			}

			Settings.MAKE_CLICKING_ERRORS = temp;
			return success;
		}

		Settings.MAKE_CLICKING_ERRORS = temp;
		return false;
	}

	public static boolean openBooth(Tile... tiles) {
		return openBooth("Bank", tiles);
	}

	public static boolean openBankLumbridge() {
		return openBooth("Bank", new Tile(3209, 3221, 2), new Tile(3208, 3221, 2));
	}

	public static boolean openBooth(String action, Tile... tiles) {
		if (isOpen())
			return true;

		if (Interacting.interact(() -> Arrays.asList(tiles).stream()
				.filter((t) -> Client.getLocalPlayer().getTile().distanceTo(t) < 20).sorted().findFirst().orElse(null),
				null, action)) {
			Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting(), 600, 800);
			boolean success = Time.sleepUntil(() -> isOpen(), 3000, 4000);
			if (success) // need to wait for bank to load
				Time.sleep(100, 200);
			return success;
		}

		return false;
	}

	public static boolean openBanker() {
		if (isOpen())
			return true;

		if (Interacting.interact(() -> Npcs.getAll().parallelStream().filter((npc) -> {
			NpcDefinition def = npc.getNpcDefinition();
			if (def == null)
				return false;
			
			String name = def.getName();
			int id = npc.getNpcDefinition().getNpcId();
			Tile loc = npc.getTile();
			
			// fishing guild bank broken bankers
			if (loc.equals(new Tile(2584, 3418, 0)) || loc.equals(new Tile(2584, 3422, 0)))
				return false;
			
			return id == 6520 || name != null
					&& (name.equals("Banker") || name.equals("Banker tutor") || name.equals("Emerald Benedict"));
		}).sorted().findFirst().orElse(null), null, "Bank")) {
			Time.sleepUntil(() -> Client.getLocalPlayer().isInteracting(), 600, 800);
			boolean success = Time.sleepUntil(() -> isOpen(), 3000, 4000);
			if (success) // need to wait for bank to load
				Time.sleep(100, 200);
			return success;
		}

		return false;
	}

}
