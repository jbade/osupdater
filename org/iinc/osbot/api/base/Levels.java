package org.iinc.osbot.api.base;

import org.iinc.osbot.api.injection.accessors.Client;

public class Levels {

	/* 
	Attack
	Defense
	Strength
	Hitpoints
	Ranged
	Prayer
	Magic
	Cooking
	Woodcutting
	Fletching
	Fishing
	Firemaking
	Crafting
	Smithing
	Mining
	Herblore
	Agility
	Thieving
	Slayer
	Farming
	Runecrafting
	Hunter
	Construction ???
	Construction ???
	Construction ???
	 */

	public static double getCurrentHitpointsPercent() {
		int a = getCurrentHitpoints();
		int b = getHitpoints();
		if (a == -1 || b == -1)
			return -1;

		return ((double) a) / b;
	}

	private static int getCurrentLevel(int index) {
		if (index < 0)
			return -1;
		int[] arr = Client.getCurrentLevels();
		if (arr == null || arr.length == 0 || arr.length <= index)
			return -1;
		return arr[index];
	}

	private static int getLevel(int index) {
		if (index < 0)
			return -1;
		int[] arr = Client.getLevels();
		if (arr == null || arr.length == 0 || arr.length <= index)
			return -1;
		return arr[index];
	}

	public static int getCurrentHitpoints() {
		return getCurrentLevel(3);
	}

	public static int getHitpoints() {
		return getLevel(3);
	}

	public static int getCurrentAttack(){
		return getCurrentLevel(0);
	}
	
	public static int getCurrentStrength(){
		return getCurrentLevel(2);
	}
	
	public static int getCurrentDefence(){
		return getCurrentLevel(1);
	}
	
	public static int getCurrentHerblore(){
		return getCurrentLevel(15);
	}
	
	public static int getAttack() {
		return getLevel(0);
	}

	public static int getStrength() {
		return getLevel(2);
	}

	public static int getDefence() {
		return getLevel(1);
	}
	
	public static int getCurrentFishing() {
		return getCurrentLevel(10);
	}
	
	public static int getCurrentCooking() {
		return getCurrentLevel(7);
	}
	
	public static int getCurrentHunter(){
		return getCurrentLevel(21);
	}
	
	public static int getCurrentThieving(){
		return getCurrentLevel(17);
	}
	
	
	
}
