package org.iinc.osbot.api.base.positioning;

import java.awt.Point;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.timing.Condition;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Character;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Clicking;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.input.MouseMovementHandler;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class Walking {

	private static final double GOAL_DISTANCE = 4;
	private static final int[] RUN_WIDGET = { 160, 22 };
	private static final int[] RUN_ENERGY_VALUE = { 160, 23 };

	public static boolean isRunEnabled() {
		return (Client.getSetting(173) & 0x1) != 0;
	}

	public static boolean isStaminaActive() {
		return (Client.getSetting(638) & 0x100000) != 0;
	}

	public static int getRunEnergy() {
		Widget widget = Widgets.getWidget(RUN_ENERGY_VALUE);
		if (widget != null) {
			try {
				return Integer.parseInt(widget.getText());
			} catch (Exception e) {
			}
		}

		return -1;
	}

	public static boolean disableRun() {
		if (!isRunEnabled())
			return true;

		Widget widget = Widgets.getWidget(RUN_WIDGET);
		if (widget != null && !widget.isHidden()) {
			if (Clicking.interact(widget, "Toggle Run"))
				return Time.sleepUntil(() -> !isRunEnabled(), 1000, 1400);
		}
		return false;
	}
	
	public static boolean enableRun() {
		if (getRunEnergy() <= 0 || isRunEnabled())
			return true;

		Widget widget = Widgets.getWidget(RUN_WIDGET);
		if (widget != null && !widget.isHidden()) {
			if (Clicking.interact(widget, "Toggle Run"))
				return Time.sleepUntil(() -> isRunEnabled(), 1000, 1400);
		}
		return false;
	}

	public static boolean isMoving() {
		int lx = Client.getLocalPlayer().getLocalX();
		int ly = Client.getLocalPlayer().getLocalY();
		return Time.sleepUntil(
				() -> lx != Client.getLocalPlayer().getLocalX() || ly != Client.getLocalPlayer().getLocalY(), 100, 200);
	}

	public static double getDistance() {
		return Random.modifiedRandomGuassianRange(200, 700, ConfidenceIntervals.CI_60, 100, Modifiers.get("walkingDistance"))
				/ 100.0;
	}

	public static Tile getDestinationTile() {
		int x = Client.getDestinationX();
		int y = Client.getDestinationY();
		if (x == 0 || y == 0) {
			return null;
		}

		return new Tile(Client.getBaseX() + x, Client.getBaseY() + y, Client.getPlane());
	}

	public static boolean hasDestination() {
		return Client.getDestinationX() != 0 && Client.getDestinationY() != 0;
	}

	public static boolean walkTo(Character c) {
		return walkTo(c, null);
	}

	public static boolean walkTo(Character c, Condition until) {
		if (until != null && until.check())
			return true;

		boolean success = false; // Client.getLocalPlayer().getTile().distanceTo(c.getTile())
									// < GOAL_DISTANCE;
		for (int tries = 0; tries < 5 && !success; tries++) {
			Point p = c.toMiniMap();
			if (Calculations.onMiniMap(p)) {
				int stops = Random.randomRange(0, 2);

				for (int i = 0; i < stops; i++) {
					Mouse.moveGeneral(p);
				}

				p = c.toMiniMap();
				Tile dest = null;
				if (Calculations.onMiniMap(p)) {
					try {
						do
							Mouse.moveMouse(p, 7, 7);
						while (!Calculations.onMiniMap(Mouse.getLocation()));

						Mouse.click(Mouse.LEFT_BUTTON);
					} finally {
						Bot.mouseLock.unlock();
					}
					Time.sleepUntil(() -> Walking.hasDestination(), 800, 1200);
					dest = Walking.getDestinationTile();
					Time.sleepUntil(() -> {
						return until != null && until.check() || !Walking.hasDestination()
								|| Client.getLocalPlayer().getTile().distanceTo(c.getTile()) <= GOAL_DISTANCE;
					}, 20000, 25000);
				}
				success = Client.getLocalPlayer().getTile().distanceTo(c.getTile()) <= GOAL_DISTANCE
						|| c.getTile().distanceTo(dest) <= GOAL_DISTANCE;
			} else {
				blindWalk(c.getTile(), () -> Calculations.onMiniMap(c.toMiniMap()));
			}
		}

		return success;
	}

	public static boolean walk(Tile tile) {
		return walk(tile, null, GOAL_DISTANCE);
	}

	public static boolean walk(Tile tile, Condition until, double dist) {
		if (until != null && until.check())
			return true;
			
		int tries = 1;
		
		while (tries++ < 3 && Calculations.onMiniMap(tile.toMiniMap())){
			if (until != null && until.check())
				return true;
			
			int stops = Random.randomRange(0, 2);

			for (int i = 0; i < stops; i++) {
				Mouse.moveGeneral(tile.toMiniMap());
			}

			Point p = tile.toMiniMap();
			if (Calculations.onMiniMap(p)) {
				Bot.mouseLock.lock();
				try {
					do
						Mouse.moveMouse(p, 4, 4);
					while (!Calculations.onMiniMap(Mouse.getLocation()));

					Mouse.click(Mouse.LEFT_BUTTON);
				} finally {
					Bot.mouseLock.unlock();
				}
				
				Time.sleepUntil(() -> hasDestination(), 800, 1200);
				Tile dest  = getDestinationTile();
				
				if (dest != null  && dest.distanceTo(tile) <= dist){
					Time.sleepUntil(() -> {
						return until != null && until.check() || !hasDestination()
								|| Client.getLocalPlayer().getTile().distanceTo(tile) <= dist;
					}, 20000, 25000);
					
					return Client.getLocalPlayer().getTile().distanceTo(tile) <= dist;
				}
			}
			
		}

		return false;
	}

	public static boolean blindWalk(Tile tile) {
		return blindWalk(tile, null);
	}

	/**
	 * Attempts to blindly walk to the specified tile in a straight line.
	 * 
	 * @param tile
	 * @return
	 */
	public static boolean blindWalk(Tile tile, Condition until) {
		double prevDistance = Double.MAX_VALUE;
		double distance = Double.MAX_VALUE / 2;// Client.getLocalPlayer().getTile().distanceTo(tile);
		while (distance >= GOAL_DISTANCE && distance < prevDistance) {
			prevDistance = distance;

			Tile loc = getClosestTileOnMiniMap(tile);
			if (!walk(loc, until, GOAL_DISTANCE))
				return false;
			distance = Client.getLocalPlayer().getTile().distanceTo(tile);
		}
		return true;
	}

	/**
	 * Gets the closest tile on the minimap to the tile provided.
	 * 
	 * @param tile
	 * @return
	 */
	public static Tile getClosestTileOnMiniMap(Tile tile) {
		return getClosestTileOnMiniMap(tile, 12);
	}

	public static Tile getClosestTile(Tile tile, double dist) {
		Tile loc = Client.getLocalPlayer().getTile();
		double x1 = tile.getX() - loc.getX();
		double y1 = tile.getY() - loc.getY();
		double c1 = Math.sqrt(x1 * x1 + y1 * y1);

		int x2 = loc.getX() + (int) (x1 * dist / c1) + Random.randomRange(-1, 1);
		int y2 = loc.getY() + (int) (y1 * dist / c1) + Random.randomRange(-1, 1);
		return new Tile(x2, y2, loc.getPlane());
	}

	public static Tile getClosestTileOnMiniMap(Tile tile, double dist) {
		if (Calculations.onMiniMap(tile.toMiniMap()))
			return tile;

		return getClosestTile(tile, dist);
	}

	public static boolean walkPath(Tile[] path, boolean exact) {
		return walkPath(path, null, exact);
	}

	public static boolean walkPath(Tile[] path, Condition until, boolean exact) {
		int prevIndex = -1;
		int index = 0;
		boolean success = false;
		while (prevIndex < index && !success) {
			boolean walked = false;

			for (int i = path.length - 1; i >= index; i--) {
				Tile t = exact ? path[i] : path[i].randomized();
				if (Calculations.onMiniMap(t.toMiniMap())) {
					prevIndex = index;
					index = i;
					walked = true;
					// only use GOAL_DISTANCE on last tile
					walk(t, until, i == path.length - 1 ? GOAL_DISTANCE : getDistance());
					if (until != null && until.check())
						return false;
					break;
				}
			}

			// if no tile on minimap then blindwalk to closest
			if (!walked) {
				Tile loc = Client.getLocalPlayer().getTile();
				Tile closest = null;
				double dist = Double.MAX_VALUE;

				prevIndex = index;
				for (int i = 0; i < path.length; i++) {
					double td = Calculations.distance(loc, path[i]);
					if (td <= dist) {
						closest = path[i];
						dist = td;
						index = i;
					}
				}

				if (closest != null) {
					blindWalk(exact ? closest : closest.randomized(), until);
					if (until != null && until.check())
						return false;
				}
			}

			success = Client.getLocalPlayer().getTile().distanceTo(path[path.length - 1]) <= GOAL_DISTANCE;
		}
		return success;
	}

	public static Tile[] randomizePath(Tile[] path) {
		List<Tile> l = Arrays.asList(path).stream().map(t -> t.randomized()).collect(Collectors.toList());
		Tile[] nPath = new Tile[l.size()];
		nPath = l.toArray(nPath);
		return nPath;
	}

	public static Tile[] generatePath(Tile[] points, int randomized, boolean randomizeFinish) {
		List<Tile> lpoints = Arrays.asList(points).stream().map((tile) -> tile.randomized(randomized, randomized))
				.collect(Collectors.toList());
		if (!randomizeFinish)
			lpoints.set(points.length - 1, points[points.length - 1]);

		ArrayList<Tile> path = new ArrayList<Tile>();
		Tile prev = null;
		for (Tile tile : lpoints) {
			if (prev != null) {
				Tile[] subpath = generatePath(prev, tile, Random.modifiedRandomGuassianRange(3, 4,
						ConfidenceIntervals.CI_90, 2, Modifiers.get("walkingDistanceBetweenTiles")));
				for (int i = 1; i < subpath.length; i++) {
					path.add(subpath[i]);
				}
			} else {
				path.add(tile);
			}
			prev = tile;
		}

		Tile[] result = new Tile[path.size()];
		return path.toArray(result);
	}

	public static Tile[] generatePath(Tile start, Tile end, int distanceBetweenTiles) {
		if (start.getPlane() != end.getPlane())
			throw new InvalidParameterException("Plane of start and end tile not the same");

		final Point[] controls = MouseMovementHandler.generateControls(start.getX(), start.getY(), end.getX(),
				end.getY(), controlSpacing(), controlVariance());
		final Point[] spline = MouseMovementHandler.generateSpline(controls);

		ArrayList<Point> path = new ArrayList<Point>();
		path.add(spline[0]);
		Point prev = spline[0];
		for (int i = 1; i < spline.length - 1; i++) {
			if (prev.distance(spline[i]) >= distanceBetweenTiles) {
				prev = spline[i];
				path.add(spline[i]);
			}
		}
		path.add(spline[spline.length - 1]);
		Tile[] tpath = new Tile[path.size()];
		int plane = start.getPlane();
		for (int i = 0; i < path.size(); i++) {
			Point p = path.get(i);
			tpath[i] = new Tile(p.x, p.y, plane);
		}

		return tpath;
	}

	private static int controlSpacing() {
		return Random.modifiedRandomGuassianRange(13, 17, ConfidenceIntervals.CI_99p5, 2,
				Modifiers.get("walkingControlSpacing")); // 15
	}

	private static int controlVariance() {
		return Random.modifiedRandomGuassianRange(4, 6, ConfidenceIntervals.CI_99p5, 1,
				Modifiers.get("walkingControlVariance")); // 5
	}

}
