package org.iinc.osbot.api.base.positioning;

import org.iinc.osbot.api.base.Calculations;

public class PreciseTile {
	private final double x, y;
	private final int plane;
	
	public PreciseTile(double x, double y, int plane) {
		this.x = x;
		this.y = y;
		this.plane = plane;
	}
	
	public double distanceTo(PreciseTile t) {
		if (t == null || this.plane != t.plane)
			return Double.MAX_VALUE;

		return Calculations.distance(x, y, t.x, t.y);
	}
	
	public double distanceTo(Tile t) {
		if (t == null || this.plane != t.getPlane())
			return Double.MAX_VALUE;

		return Calculations.distance(x, y, t.getX() + .5, t.getY() + .5);
	}

}
