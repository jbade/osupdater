package org.iinc.osbot.api.base.positioning;

import java.awt.Polygon;

/**
 * 
 * @author powerbot
 *
 */
public class Area {
	private final Polygon polygon;
	private final int plane;

	public Area(final Tile... tiles) {
		if (tiles.length < 0)
			throw new IllegalArgumentException("Area tiles.length < 0");
		this.plane = tiles[0].getPlane();
		
		Polygon temp = new Polygon();
		for (Tile t : tiles){
			if (t.getPlane() != this.plane)
				throw new IllegalArgumentException("Area mismatched planes " + plane + " != " + t.getPlane());
			temp.addPoint(t.getX(), t.getY());
		}
		
		// should be more consistent on not including edges in contain
		this.polygon = new Polygon();
		for (final Tile tile : tiles) {
			if (temp.contains(tile.getX(), tile.getY())){
				polygon.addPoint(tile.getX() + 1, tile.getY() + 1);
			} else {
				polygon.addPoint(tile.getX(), tile.getY());
			}
			
		}
	}

	public boolean contains(final Tile... tiles) {
		for (final Tile tile : tiles) {
			if (tile.getPlane() != plane || !polygon.contains(tile.getX(), tile.getY()))
				return false;
		}
		return true;
	}
}
