package org.iinc.osbot.api.base.positioning;

import java.awt.Point;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Clickable;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.util.Random;

public class Tile implements Clickable {
	
	private final int x, y, plane;
	private int localXOffset, localYOffset, height;

	public Tile(int x, int y, int plane) {
		this.x = x;
		this.y = y;
		this.plane = plane;
	}
	
	public Tile(int x, int y, int plane, int localXOffset, int localYOffset, int height) {
		this.x = x;
		this.y = y;
		this.plane = plane;
		this.localXOffset = localXOffset;
		this.localYOffset = localYOffset;
		this.height = height;
	}

	public void setClickingOffsets(int localXOffset, int localYOffset, int height) {
		this.localXOffset = localXOffset;
		this.localYOffset = localYOffset;
		this.height = height;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getLocalX() {
		return ((x - Client.getBaseX()) << 7) + 64;
	}

	public int getLocalY() {
		return ((y - Client.getBaseY()) << 7) + 64;
	}

	public int getPlane() {
		return plane;
	}

	public Tile randomized() {
		return randomized(2, 2);
	}

	public Tile randomized(int x, int y) {
		return new Tile(this.x + Random.randomRange(-x, x), this.y + Random.randomRange(-y, y), plane);
	}

	public Tile modify(int x, int y){
		return new Tile(this.x + x, this.y + y, plane);
	}
	
	public Point toMiniMap() {
		return Calculations.worldToMiniMap(getLocalX() + localXOffset, getLocalY() + localYOffset);
	}
	
	public Point toScreen() {
		return toScreen(height);
	}

	public Point toScreen(int height) {
		return Calculations.worldToScreen(getLocalX() + localXOffset, getLocalY() + localYOffset, Client.getPlane(),
				height);
	}

	@Override
	public Tile getTile() {
		return this;
	}

	/**
	 * Compared based on distance from local player
	 */
	public int compareTo(Tile b) {
		Tile loc = Client.getLocalPlayer().getTile();
		
		double dist = loc.distanceTo(this) - loc.distanceTo(b);
		
		if (dist > 0) {
			return 1;
		} else if (dist < 0) {
			return -1;
		} else {
			// adjacent tiles are closer than diagonal tiles
			int d = 0;
			if (loc.getX() != this.getX())
				d ++;
			if (loc.getY() != this.getY())
				d ++;
			if (loc.getX() != b.getX())
				d --;
			if (loc.getY() != b.getY())
				d --;
			 
			if (d > 0)
				return 1;
			if  (d < 0)
				return -1;
			
			return 0;
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + plane;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tile other = (Tile) obj;
		if (plane != other.plane)
			return false;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	public double distanceTo(Tile t) {
		if (t == null || this.plane != t.plane)
			return Double.MAX_VALUE;

		return Calculations.distance(x, y, t.x, t.y);
	}

	@Override
	public String toString() {
		return "Tile(" + x + ", " + y + ", " + plane + ")";
	}

}
