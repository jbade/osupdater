package org.iinc.osbot.api.base;

import java.awt.Point;

import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.util.Random;

public class Camera {

	public static void turnTo(Tile tile) {
		// calculates angle to tile
		Tile loc = Client.getLocalPlayer().getTile();
		double angle = Math.atan2(tile.getY() - loc.getY(), tile.getX() - loc.getX()) - Math.PI / 2
				+ (Math.random() * Math.PI / 4 - Math.PI / 8);

		setAngle(angle, true);
	}

	/**
	 * Set the camera angle.
	 * 
	 * @param angle
	 *            radians between 0 and 2 pi
	 * @param high
	 */
	public static void setAngle(double angle, boolean high) {
		double deltaAngle = getCameraAngle() - angle; // angle change needed to reach desired angle
		while (deltaAngle < Math.PI)
			deltaAngle += Math.PI * 2.0;
		while (deltaAngle > Math.PI)
			deltaAngle -= Math.PI * 2.0;

		//System.out.println(deltaAngle);

		int toMovePitch = 0;
		if (high) {
			toMovePitch = (int) (Random.randomRange(5, 7) / 10.0 * (383 - Client.getCameraPitch())); // max pitch is 383
		} else {
			toMovePitch = (int) (Random.randomRange(5, 7) / 10.0 * (Client.getCameraPitch() - 128)); // min pitch is 128
		}

		int toMoveAngle = (int) (Random.randomRange(170, 185) * deltaAngle); // randomize number of pixels per radian

		Point pt = Mouse.getLocation();
		if (!Client.getCanvas().getBounds().contains(pt)) {
			Mouse.move(Client.getCanvas().getBounds());
		}
		Mouse.holdMouse(pt.x, pt.y, Mouse.MIDDLE_BUTTON);
		Mouse.move(new Point(pt.x + toMoveAngle, pt.y + toMovePitch));
		pt = Mouse.getLocation();
		Mouse.releaseMouse(pt.x, pt.y, Mouse.MIDDLE_BUTTON);
	}

	public static double getCameraAngle() {
		return Client.getCameraYaw() * 2.0 * Math.PI / 2047.0;
	}
	
	public static double getCameraPitch(){
		return Client.getCameraPitch();
	}

}
