package org.iinc.osbot.api.base.worlds;

public class World {
	private final String activity, domain;
	private final int id, location, flags, playerCount;

	//bit index of flags
	private static final int MEMBERS_BIT = 32;

	public World(int id, int flags, String domain, String activity, int location, int playerCount) {
		this.activity = activity;
		this.domain = domain;
		this.id = id;
		this.location = location;
		this.flags = flags;
		this.playerCount = playerCount;
	}

	public String getDomain() {
		return domain;
	}

	public int getLocation() {
		return location;
	}

	public boolean isMembers() {
		return ((flags >> MEMBERS_BIT) & 1) == 1;
	}

	public int getPlayerCount() {
		return playerCount;
	}

	public String getActivity() {
		return activity;
	}

	public boolean isNormalWorld() {
		return (1 >> MEMBERS_BIT) == (flags) || (0 >> MEMBERS_BIT) == (flags);
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "World [activity=" + activity + ", domain=" + domain + ", id=" + id + ", location=" + location
				+ ", flags=" + flags + ", playerCount=" + playerCount + "]";
	}

}