package org.iinc.osbot.api.base.worlds;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.util.Random;

public class Worlds {

	public static List<World> getWorlds() {
		WorldParser wp = new WorldParser();
		World[] worlds = wp.getWorlds();
		if (worlds == null || worlds.length == 0)
			return new ArrayList<World>();
		return Arrays.asList(worlds);
	}

	public static List<World> getNormalWorlds(boolean members) {
		return getWorlds().stream().filter((w) -> w.isNormalWorld() && w.isMembers() == members)
				.collect(Collectors.toList());
	}

	public static World getRandomNormalWorld(boolean members) {
		List<World> worlds = getNormalWorlds(members);
		if (worlds.size() == 0)
			return null;
		return worlds.get(Random.randomRange(0, worlds.size() - 1));
	}

	private static class WorldParser {
		private byte[] bytes = new byte[5000];
		private int offset;

		public World[] getWorlds() {
			for (int tries = 0; tries < 30; tries++) {
				offset = 0;
				try {
					URL url = new URL("http://www.runescape.com/slr.ws");
					InputStream is = url.openStream();
					int success = is.read(bytes);
					if (success != 5000) {
						Bot.LOGGER.log(Level.FINER, "Invalid size");
						Time.sleep(tries * 200);
						continue;
					}
				} catch (IOException e) {
					Bot.LOGGER.log(Level.FINER, "IOException", e);
					Time.sleep(tries * 200);
					continue;
				}

				int length = bytes[5];
				World[] worlds = new World[length];
				offset = 6;
				for (int i = 0; i < length; i++) {
					// ordering matters
					worlds[i] = new World(readShort(), readInt(), readString(), readString(), readByte(), readShort());
				}
				return worlds;
			}

			Bot.LOGGER.log(Level.SEVERE, "Unable to load worlds");
			return null;
		}

		private int readByte() {
			offset += 2;
			return bytes[offset - 1] & 0xF;
		}

		private int readShort() {
			offset += 2;
			return ((bytes[offset - 2] & 0xFF) << 8) + (bytes[offset - 1] & 0xFF);
		}

		private int readInt() {
			offset += 4;
			return ((bytes[offset - 4] & 0xFF) << 24) + ((bytes[offset - 3] & 0xFF) << 16)
					+ ((bytes[offset - 2] & 0xFF) << 8) + (bytes[offset - 1] & 0xFF);
		}

		private String readString() {
			String s = "";
			if (bytes[offset] == 0) {
				offset++;
			}
			while (bytes[offset] != 0) {
				s += (char) bytes[offset];
				offset++;
			}
			return s;
		}

	}
}
