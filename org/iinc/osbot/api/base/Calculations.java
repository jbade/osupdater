package org.iinc.osbot.api.base;

import java.awt.Point;

import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Player;

public class Calculations {
	private static int[] CURVE_SIN = new int[2048];
	private static int[] CURVE_COS = new int[2048];

	static {
		for (int i = 0; i < 2048; i++) {
			CURVE_SIN[i] = (int) Math.round(65536.0 * Math.sin(i * 0.0030679615));
			CURVE_COS[i] = (int) Math.round(65536.0 * Math.cos(i * 0.0030679615));
		}
	}

	/**
	 * Use local coordinates.
	 * 
	 * @param x
	 * @param y
	 * @param plane
	 * @return
	 */
	public static int getHeight(int x, int y, int plane) {
		int sx = x >> 7;
		int sy = y >> 7;

		if (sx < 0 || sx > 103 || sy < 0 || sy > 103)
			return 0;

		// TODO if we hook ground settings
		if (plane < 3 && (Client.getTileSettings()[1][sx][sy] & 0x2) == 0x2)
			plane++;

		int ax = x & 0x7F;
		int ay = y & 0x7F;
		int[][][] heights = Client.getTileHeights();
		return (128 - ay) * (heights[plane][sx][sy] * (128 - ax) + heights[plane][1 + sx][sy] * ax >> 7)
				+ ay * (heights[plane][sx][sy + 1] * (128 - ax) + heights[plane][1 + sx][sy + 1] * ax >> 7) >> 7;
	}

	/**
	 * Use local coordinates.
	 * 
	 * @param x
	 * @param y
	 * @param plane
	 * @param height
	 * @return
	 */
	public static Point worldToScreen(int x, int y, int plane, int height) {
		if (x < 128 || y < 128 || x > 13056 || y > 13056) {
			return new Point(-1, -1);
		}

		try {
			int z = getHeight(x, y, plane) - height;
			x -= Client.getCameraX();
			y -= Client.getCameraY();
			z -= Client.getCameraZ();

			int curvexsin = CURVE_SIN[Client.getCameraYaw()];
			int curvexcos = CURVE_COS[Client.getCameraYaw()];
			int curveysin = CURVE_SIN[Client.getCameraPitch()];
			int curveycos = CURVE_COS[Client.getCameraPitch()];

			int calculation = curvexsin * y + x * curvexcos >> 16;
			y = -(curvexsin * x) + y * curvexcos >> 16;
			x = calculation;
			calculation = curveycos * z - curveysin * y >> 16;
			y = curveysin * z + y * curveycos >> 16;
			z = calculation;

			int screenX = (x * Client.getViewportScale()) / y + Client.getViewportWidth() / 2;
			int screenY = (z * Client.getViewportScale()) / y + Client.getViewportHeight() / 2;
			Point p = new Point(screenX, screenY);
			if (!onViewport(p))
				new Point(-1, -1);
			return p;
		} catch (Exception e) {
			return new Point(-1, -1);
		}
	}

	public static boolean onViewport(Point p) {
		return p.x >= 5 && p.y >=5 && p.x < Client.getViewportWidth() - 5 && p.y < Client.getViewportHeight() - 5;
	}
	
	public static double distance(Tile a, Tile b) {
		return distance(a.getX(), a.getY(), b.getX(), b.getY());
	}

	public static double distance(double x1, double y1, double x2, double y2) {
		return Math.max(Math.abs(x1 - x2), Math.abs(y1 - y2));
	}

	public static final Point MINIMAP_CENTER_POINT = new Point(642 + 1, 84 - 6);

	public static boolean onMiniMap(Point p) {
		if (p.x <= 0 || p.y <= 0)
			return false;

		return distance(p.x, p.y, MINIMAP_CENTER_POINT.x, MINIMAP_CENTER_POINT.y) <= 68;
	}

	public static Point tileToMiniMap(int x, int y) {
		return worldToMiniMap(x - Client.getBaseX() << 7, y - Client.getBaseY() << 7);
	}

	public static Point worldToMiniMap(int x, int y) {
		Player localPlayer = Client.getLocalPlayer();

		/* rev 119 az.cl
		 Npc var64 = client.npcs[client.npcIndicies[var24]]; 
		 var27 = var64.localX * 781832805 / 32 - localPlayer.localY * 781832805 / 32;
		 var52 = var64.localY * -2057538877 / 32 - localPlayer.localX * -2057538877 / 32;
		 
		 bs.da(var13, var14, var27, var52, n.ez[1], var56, '\u8000');       
		 */
		int var27 = x / 32 - localPlayer.getLocalX() / 32;
		int var52 = y / 32 - localPlayer.getLocalY() / 32;

		/* rev 119 bs.da
			final int n5 = client.mapOffset * 1413032325 + client.mapAngle * -485763403 & 0x7FF;
		    final int n6 = var27 * var27 + var52 * var52;
		    if (n6 > 6400) {
		        return;
		    }
		    final int n7 = cy.ab[n5]; // sin
		    final int n8 = cy.ai[n5]; // cos
		    final int n9 = n7 * 256 / (client.mapScale * 1387517637 + 256);
		    final int n10 = n8 * 256 / (client.mapScale * 1387517637 + 256);
		    final int n11 = n10 * n3 + n9 * n4 >> 16;
		    final int n12 = n10 * n4 - n3 * n9 >> 16;
		    if (n6 > 2500) {
		        cz.n(fh.m * 1498912197 / 2 + n11 - cz.l / 2, fh.w * -1313887825 / 2 - n12 - cz.j / 2, n, n2, fh.m * 1498912197, fh.w * -1313887825, fh.o, fh.e);
		    }
		    else {
		        cz.u(fh.m * 1498912197 / 2 + n + n11 - cz.l / 2, fh.w * -1313887825 / 2 + n2 - n12 - cz.j / 2);
		    }
		*/

		int n5 = Client.getMapAngle() & 0x7FF;
		int n6 = var27 * var27 + var52 * var52;
		if (n6 > 6400)
			return new Point(-1, -1);

		int n7 = CURVE_SIN[n5];
		int n8 = CURVE_COS[n5];
		int n9 = n7;// * 256 / (Client.getMapScale() + 256);
		int n10 = n8;// * 256 / (Client.getMapScale() + 256);
		final int n11 = n10 * var27 + n9 * var52 >> 16;
		final int n12 = n10 * var52 - var27 * n9 >> 16;

		return new Point(MINIMAP_CENTER_POINT.x + n11, MINIMAP_CENTER_POINT.y - n12);
	}
}
