package org.iinc.osbot.api.base.item;

import java.awt.Point;
import java.awt.Rectangle;

import org.iinc.osbot.api.base.interfaces.tabs.Inventory;

public class Item {
	private final int index;
	private final int id;
	private final int quantity;

	public Item(int index, int id, int quantity) {
		this.index = index;
		this.id = id;
		this.quantity = quantity;
	}

	public int getIndex() {
		return index;
	}

	public int getId() {
		return id;
	}

	public int getQuantity() {
		return quantity;
	}

	public Point toScreen() {
		Rectangle rect = Inventory.getBounds(index);
		return new Point(rect.x, rect.y);
	}
}
