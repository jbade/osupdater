package org.iinc.osbot.api.base.item;

public class Items {
	public static RSItemConstant COINS = new RSItemConstant(995, 995, "Coins");
	public static RSItemConstant BOND_UNTRADABLE = new RSItemConstant(13192, 13192, "Old school bond");

	public static RSItemConstant IRON_SCIMITAR = new RSItemConstant(1323, 1324, "Iron scimitar");
	public static RSItemConstant STEEL_SCIMITAR = new RSItemConstant(1325, 1326, "Steel scimitar");
	public static RSItemConstant MITHRIL_SCIMITAR = new RSItemConstant(1329, 1330, "Mithril scimitar");
	public static RSItemConstant ADAMANT_SCIMITAR = new RSItemConstant(1331, 1332, "Adamant scimitar");
	public static RSItemConstant RUNE_SCIMITAR = new RSItemConstant(1333, 1334, "Rune scimitar");
	public static RSItemConstant ABYSSAL_WHIP = new RSItemConstant(4151, 4152, "Abyssal whip");
	public static RSItemConstant BANDOS_GODSWORD = new RSItemConstant(11804, 11805, "Bandos godsword");

	public static RSItemConstant VARROCK_TELEPORT = new RSItemConstant(8007, 8007, "Varrock teleport");
	public static RSItemConstant CAMELOT_TELEPORT = new RSItemConstant(8010, 8010, "Camelot teleport");

	public static RSItemConstant ENERGY_POTION_1 = new RSItemConstant(3014, 3015, "Energy potion(1)");
	public static RSItemConstant ENERGY_POTION_2 = new RSItemConstant(3012, 3013, "Energy potion(2)");
	public static RSItemConstant ENERGY_POTION_3 = new RSItemConstant(3010, 3011, "Energy potion(3)");
	public static RSItemConstant ENERGY_POTION_4 = new RSItemConstant(3008, 3009, "Energy potion(4)");

	public static RSItemConstant SUPER_ATTACK_POTION_1 = new RSItemConstant(149, 150, "Super attack(1)");
	public static RSItemConstant SUPER_ATTACK_POTION_2 = new RSItemConstant(147, 148, "Super attack(2)");
	public static RSItemConstant SUPER_ATTACK_POTION_3 = new RSItemConstant(145, 146, "Super attack(3)");
	public static RSItemConstant SUPER_ATTACK_POTION_4 = new RSItemConstant(2436, 2437, "Super attack(4)");

	public static RSItemConstant SUPER_STRENGTH_POTION_1 = new RSItemConstant(161, 162, "Super strength(1)");
	public static RSItemConstant SUPER_STRENGTH_POTION_2 = new RSItemConstant(159, 160, "Super strength(2)");
	public static RSItemConstant SUPER_STRENGTH_POTION_3 = new RSItemConstant(157, 158, "Super strength(3)");
	public static RSItemConstant SUPER_STRENGTH_POTION_4 = new RSItemConstant(2440, 2441, "Super strength(4)");

	public static RSItemConstant SUPER_DEFENCE_POTION_1 = new RSItemConstant(167, 168, "Super defence(1)");
	public static RSItemConstant SUPER_DEFENCE_POTION_2 = new RSItemConstant(165, 166, "Super defence(2)");
	public static RSItemConstant SUPER_DEFENCE_POTION_3 = new RSItemConstant(163, 164, "Super defence(3)");
	public static RSItemConstant SUPER_DEFENCE_POTION_4 = new RSItemConstant(2442, 2441, "Super defence(4)");

	public static RSItemConstant STAMINA_POTION_1 = new RSItemConstant(12631, 12632, "Stamina potion(1)");
	public static RSItemConstant STAMINA_POTION_2 = new RSItemConstant(12629, 12630, "Stamina potion(2)");
	public static RSItemConstant STAMINA_POTION_3 = new RSItemConstant(12627, 12628, "Stamina potion(3)");
	public static RSItemConstant STAMINA_POTION_4 = new RSItemConstant(12625, 12626, "Stamina potion(4)");

	public static RSItemConstant SUPER_ANTIPOISON_1 = new RSItemConstant(185, 186, "Superantipoison(1)");

	public static RSItemConstant ANTIDOTE_PLUS_PLUS_1 = new RSItemConstant(5958, 5959, "Antidote++(1)");
	public static RSItemConstant ANTIDOTE_PLUS_PLUS_2 = new RSItemConstant(5956, 5957, "Antidote++(2)");
	public static RSItemConstant ANTIDOTE_PLUS_PLUS_3 = new RSItemConstant(5954, 5955, "Antidote++(3)");
	public static RSItemConstant ANTIDOTE_PLUS_PLUS_4 = new RSItemConstant(5952, 5953, "Antidote++(4)");

	public static RSItemConstant GAMES_NECKLACE_1 = new RSItemConstant(3867, 3868, "Games necklace(1)"); // check
																											// id
	public static RSItemConstant GAMES_NECKLACE_2 = new RSItemConstant(3865, 3866, "Games necklace(2)");
	public static RSItemConstant GAMES_NECKLACE_3 = new RSItemConstant(3863, 3864, "Games necklace(3)");
	public static RSItemConstant GAMES_NECKLACE_4 = new RSItemConstant(3861, 3862, "Games necklace(4)");
	public static RSItemConstant GAMES_NECKLACE_5 = new RSItemConstant(3859, 3860, "Games necklace(5)");
	public static RSItemConstant GAMES_NECKLACE_6 = new RSItemConstant(3857, 3858, "Games necklace(6)");
	public static RSItemConstant GAMES_NECKLACE_7 = new RSItemConstant(3855, 3856, "Games necklace(7)");
	public static RSItemConstant GAMES_NECKLACE_8 = new RSItemConstant(3853, 3854, "Games necklace(8)");

	public static RSItemConstant AMULET_OF_GLORY = new RSItemConstant(1704, 1705, "Amulet of glory");
	public static RSItemConstant AMULET_OF_GLORY_1 = new RSItemConstant(1706, 1707, "Amulet of glory(1)");
	public static RSItemConstant AMULET_OF_GLORY_2 = new RSItemConstant(1708, 1709, "Amulet of glory(2)");
	public static RSItemConstant AMULET_OF_GLORY_3 = new RSItemConstant(1710, 1711, "Amulet of glory(3)");
	public static RSItemConstant AMULET_OF_GLORY_4 = new RSItemConstant(1712, 1713, "Amulet of glory(4)");
	public static RSItemConstant AMULET_OF_GLORY_5 = new RSItemConstant(11976, 11977, "Amulet of glory(5)");
	public static RSItemConstant AMULET_OF_GLORY_6 = new RSItemConstant(11978, 11979, "Amulet of glory(6)");

	public static RSItemConstant SKILLS_NECKLACE = new RSItemConstant(11113, 11114, "Skills necklace");
	public static RSItemConstant SKILLS_NECKLACE_1 = new RSItemConstant(11111, 11112, "Skills necklace(1)");
	public static RSItemConstant SKILLS_NECKLACE_2 = new RSItemConstant(11109, 11110, "Skills necklace(2)");
	public static RSItemConstant SKILLS_NECKLACE_3 = new RSItemConstant(11107, 11108, "Skills necklace(3)");
	public static RSItemConstant SKILLS_NECKLACE_4 = new RSItemConstant(11105, 11106, "Skills necklace(4)");

	public static RSItemConstant AMULET_OF_POWER = new RSItemConstant(1731, 1732, "Amulet of power");
	public static RSItemConstant AMULET_OF_STRENGTH = new RSItemConstant(1725, 1726, "Amulet of strength");

	public static RSItemConstant ADAMANT_FULL_HELM = new RSItemConstant(1161, 1162, "Adamant full helm");
	public static RSItemConstant RUNE_FULL_HELM = new RSItemConstant(1163, 1164, "Rune full helm");

	public static RSItemConstant ANTI_DRAGON_SHIELD = new RSItemConstant(1540, 1541, "Anti-dragon shield");

	public static RSItemConstant RUNE_CHAINBODY = new RSItemConstant(1113, 1114, "Rune chainbody");
	public static RSItemConstant ADAMANT_PLATEBODY = new RSItemConstant(1123, 1124, "Adamant platebody");

	public static RSItemConstant RUNE_PLATESKIRT = new RSItemConstant(1093, 1094, "Rune plateskirt");
	public static RSItemConstant RUNE_PLATELEGS = new RSItemConstant(1079, 1080, "Rune platelegs");
	public static RSItemConstant DRAGON_PLATELEGS = new RSItemConstant(4087, 4088, "Dragon platelegs");
	public static RSItemConstant DRAGON_PLATESKIRT = new RSItemConstant(4585, 4586, "Dragon plateskirt");
	public static RSItemConstant DRAGON_CHAINBODY = new RSItemConstant(3140, 3141, "Dragon chainbody");

	public static RSItemConstant BARROWS_GLOVES = new RSItemConstant(7462, 7462, "Barrows gloves");
	public static RSItemConstant COMBAT_BRACE = new RSItemConstant(11126, 11127, "Combat brace");
	public static RSItemConstant COMBAT_BRACLET_4 = new RSItemConstant(11118, 11119, "Combat bracelet(4)");

	public static RSItemConstant CAKE = new RSItemConstant(1891, 1892, "Cake");
	public static RSItemConstant CAKE2 = new RSItemConstant(1893, 1894, "2/3 cake");
	public static RSItemConstant CAKE1 = new RSItemConstant(1895, 1896, "Slice of cake");
	public static RSItemConstant CHOCOLATE_CAKE1 = new RSItemConstant(1901, 1902, "Chocolate slice");
	public static RSItemConstant BREAD = new RSItemConstant(2309, 2310, "Bread");
	public static RSItemConstant LOBSTER = new RSItemConstant(379, 380, "Lobster");
	public static RSItemConstant TUNA = new RSItemConstant(361, 362, "Tuna");
	public static RSItemConstant SHARK = new RSItemConstant(385, 386, "Shark");

	public static RSItemConstant GREEN_DRAGONHIDE = new RSItemConstant(1753, 1754, "Green dragonhide");

	public static RSItemConstant DRAGON_BONES = new RSItemConstant(536, 537, "Dragon bones");
	public static RSItemConstant NATURE_RUNE = new RSItemConstant(561, 561, "Nature rune");
	public static RSItemConstant MITHRIL_KITESHIELD = new RSItemConstant(1197, 1198, "Mithril kiteshield");
	public static RSItemConstant RUNE_DAGGER = new RSItemConstant(1213, 1214, "Rune dagger");
	public static RSItemConstant ADAMANTITE_ORE = new RSItemConstant(449, 450, "Adamantite ore");
	public static RSItemConstant ENSOULED_DRAGON_HEAD_DROP = new RSItemConstant(13510, -1, "Ensouled dragon head");
	public static RSItemConstant ENSOULED_DRAGON_HEAD = new RSItemConstant(13511, 13512, "Ensouled dragon head");
	public static RSItemConstant GRIMY_RANARR_WEED = new RSItemConstant(207, 208, "Grimy ranarr weed");
	public static RSItemConstant GRIMY_CADANTINE = new RSItemConstant(215, 216, "Grimy cadantine");
	public static RSItemConstant GRIMY_LANTADYME = new RSItemConstant(2485, 2486, "Grimy lantadyme");
	public static RSItemConstant GRIMY_DWARF_WEED = new RSItemConstant(217, 218, "Grimy dwarf weed");
	public static RSItemConstant GRIMY_KWUARM = new RSItemConstant(213, 214, "Grimy kwuarm");
	public static RSItemConstant GRIMY_IRIT_LEAF = new RSItemConstant(209, 210, "Grimy irit leaf");
	public static RSItemConstant GRIMY_AVANTOE = new RSItemConstant(211, 212, "Grimy avantoe");

	public static RSItemConstant SMALL_FISHING_NET = new RSItemConstant(303, 304, "Small fishing net");
	public static RSItemConstant HARPOON = new RSItemConstant(311, 312, "Harpoon");
	public static RSItemConstant LOBSTER_POT = new RSItemConstant(301, 302, "Lobster pot");
	public static RSItemConstant FLY_FISHING_ROD = new RSItemConstant(309, 310, "Fly fishing rod");
	public static RSItemConstant FEATHER = new RSItemConstant(314, 314, "Feather");

	public static RSItemConstant RAW_TUNA = new RSItemConstant(359, 360, "Raw tuna");
	public static RSItemConstant RAW_ANCHOVIES = new RSItemConstant(321, 322, "Raw anchovies");
	public static RSItemConstant RAW_SHRIMPS = new RSItemConstant(317, 318, "Raw shrimps");
	public static RSItemConstant RAW_LOBSTER = new RSItemConstant(377, 378, "Raw lobster");
	public static RSItemConstant RAW_SWORDFISH = new RSItemConstant(371, 372, "Raw swordfish");
	public static RSItemConstant RAW_SHARK = new RSItemConstant(383, 384, "Raw shark");
	public static RSItemConstant RAW_TROUT = new RSItemConstant(335, 336, "Raw trout");
	public static RSItemConstant RAW_SALMON = new RSItemConstant(331, 332, "Raw salmon");

	public static RSItemConstant LEAPING_TROUT = new RSItemConstant(11328, 11329, "Leaping trout");
	public static RSItemConstant LEAPING_SALMON = new RSItemConstant(11330, 11331, "Leaping salmon");
	public static RSItemConstant LEAPING_STURGEON = new RSItemConstant(11332, 11333, "Leaping sturgeon");

	public static RSItemConstant BATTLESTAFF = new RSItemConstant(1391, 1392, "Battlestaff");

	public static RSItemConstant IRON_ARROWTIPS = new RSItemConstant(40, 40, "Iron arrowtips");
	public static RSItemConstant STEEL_ARROWTIPS = new RSItemConstant(41, 41, "Steel arrowtips");
	public static RSItemConstant MITHRIL_ARROWTIPS = new RSItemConstant(42, 42, "Mithril arrowtips");

	public static RSItemConstant POTATO_SEED = new RSItemConstant(5318, 5318, "Potato seed");
	public static RSItemConstant ONION_SEED = new RSItemConstant(5319, 5319, "Onion seed");
	public static RSItemConstant CABBAGE_SEED = new RSItemConstant(5324, 5324, "Cabbage seed");
	public static RSItemConstant TOMATO_SEED = new RSItemConstant(5322, 5322, "Tomato seed");
	public static RSItemConstant SWEETCORN_SEED = new RSItemConstant(5320, 5320, "Sweetcorn seed");
	public static RSItemConstant STRAWBERRY_SEED = new RSItemConstant(5323, 5323, "Strawberry seed");
	public static RSItemConstant WATERMELON_SEED = new RSItemConstant(5321, 5321, "Watermelon seed");

	public static RSItemConstant BARLEY_SEED = new RSItemConstant(5305, 5305, "Barley seed");
	public static RSItemConstant HAMMERSTONE_SEED = new RSItemConstant(5307, 5307, "Hammerstone seed");
	public static RSItemConstant ASGARNIAN_SEED = new RSItemConstant(5308, 5308, "Asgarnian seed");
	public static RSItemConstant JUTE_SEED = new RSItemConstant(5306, 5306, "Jute seed");
	public static RSItemConstant YANNILLIAN_SEED = new RSItemConstant(5309, 5309, "Yanillian seed");
	public static RSItemConstant KRANDORIAN_SEED = new RSItemConstant(5310, 5310, "Krandorian seed");
	public static RSItemConstant WILDBLOOD_SEED = new RSItemConstant(5311, 5311, "Wildblood seed");

	public static RSItemConstant MARIGOLD_SEED = new RSItemConstant(5096, 5096, "Marigold seed");
	public static RSItemConstant NASTURTIUM_SEED = new RSItemConstant(5098, 5098, "Nasturtium seed");
	public static RSItemConstant ROSEMARY_SEED = new RSItemConstant(5097, 5097, "Rosemary seed");
	public static RSItemConstant WOAD_SEED = new RSItemConstant(5099, 5099, "Woad seed");
	public static RSItemConstant LIMPWURT_SEED = new RSItemConstant(5100, 5100, "Limpwurt seed");

	public static RSItemConstant REDBERRY_SEED = new RSItemConstant(5101, 5101, "Redberry seed");
	public static RSItemConstant CADAVABERRY_SEED = new RSItemConstant(5102, 5102, "Cadavaberry seed");
	public static RSItemConstant DWELLBERRY_SEED = new RSItemConstant(5103, 5103, "Dwellberry seed");
	public static RSItemConstant JANGERBERRY_SEED = new RSItemConstant(5104, 5104, "Jangerberry seed");
	public static RSItemConstant WHITEBERRY_SEED = new RSItemConstant(5105, 5105, "Whiteberry seed");
	public static RSItemConstant POISON_IVY_SEED = new RSItemConstant(5106, 5106, "Poison ivy seed");

	public static RSItemConstant GUAM_SEED = new RSItemConstant(5291, 5291, "Guam seed");
	public static RSItemConstant MARRENTILL_SEED = new RSItemConstant(5292, 5292, "Marrentill seed");
	public static RSItemConstant TARROMIN_SEED = new RSItemConstant(5293, 5293, "Tarromin seed");
	public static RSItemConstant HARRALANDER_SEED = new RSItemConstant(5294, 5294, "Harralander seed");
	public static RSItemConstant RANARR_SEED = new RSItemConstant(5295, 5295, "Ranarr seed");
	public static RSItemConstant TOADFLAX_SEED = new RSItemConstant(5296, 5296, "Toadflax seed");
	public static RSItemConstant IRIT_SEED = new RSItemConstant(5297, 5297, "Irit seed");
	public static RSItemConstant AVANTOE_SEED = new RSItemConstant(5298, 5298, "Avantoe seed");
	public static RSItemConstant KWUARM_SEED = new RSItemConstant(5299, 5299, "Kwuarm seed");
	public static RSItemConstant SNAPDRAGON_SEED = new RSItemConstant(5300, 5300, "Snapdragon seed");
	public static RSItemConstant CADANTINE_SEED = new RSItemConstant(5301, 5301, "Cadantine seed");
	public static RSItemConstant LANTADYME_SEED = new RSItemConstant(5302, 5302, "Lantadyme seed");
	public static RSItemConstant DWARF_WEED_SEED = new RSItemConstant(5303, 5303, "Dwarf weed seed");
	public static RSItemConstant TORSTOL_SEED = new RSItemConstant(5304, 5304, "Torstol seed");

	public static RSItemConstant MUSHROOM_SPORE = new RSItemConstant(5282, 5282, "Mushroom spore");
	public static RSItemConstant BELLADONNA_SEED = new RSItemConstant(5281, 5281, "Belladonna seed");
	public static RSItemConstant CACTUS_SEED = new RSItemConstant(5280, 5280, "Cactus seed");

	public static RSItemConstant BONES = new RSItemConstant(526, 527, "Bones");
	public static RSItemConstant RAW_BIRD_MEAT = new RSItemConstant(9978, 9979, "Raw bird meat");

	public static RSItemConstant TRIANGLE_SANDWICH = new RSItemConstant(6962, 6963, "Triangle sandwich");
	public static RSItemConstant SILK = new RSItemConstant(950, 951, "Silk");

	public static class RSItemConstant {
		int id, notedId;
		String name;

		public RSItemConstant(int id, int notedId, String name) {
			this.id = id;
			this.notedId = notedId;
			this.name = name;
		}

		public int getId() {
			return id;
		}

		public int getNotedId() {
			return notedId;
		}

		public String getName() {
			return name;
		}
	}
}
