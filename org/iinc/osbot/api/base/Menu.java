package org.iinc.osbot.api.base;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.input.Mouse;
import org.iinc.osbot.util.ArrayUtil;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class Menu {

	public static void close() {
		if (Menu.isOpen())
			Mouse.moveMouse(new Point(Menu.getLocation().x, Menu.getLocation().y - 30), 40, 15);
	}

	/**
	 * Checks to see if the menu contains an action that is equal to the one provided and an option
	 * that starts with the one provided
	 * 
	 * @param action
	 * @param option
	 * @return
	 */
	public static boolean contains(String action, String option) {
		int count = Client.getMenuCount();
		String[] actions = Client.getMenuActions();
		String[] options = Client.getMenuOptions();

		for (int i = 0; i < count; i++) {
			String str = transform(options[i]);
			if (actions[i].equals(action) && (option == null || str != null && str.startsWith(option))) {
				return true;
			}
		}

		return false;
	}

	public static boolean isOpen() {
		return Client.isMenuVisible();
	}

	public static boolean interact(String action, String option) {
		if (action != null) {
			if (option != null) {
				return interact(new String[] { action }, new String[] { option });
			} else {
				return interact(new String[] { action }, null);
			}
		} else {
			if (option != null) {
				return interact(null, new String[] { option });
			} else {
				String[] arr = null;
				return interact(arr, null);
			}
		}
	}

	public static boolean interact(String[] action, String[] option) {
		Bot.mouseLock.lock();
		try {
			Time.sleep(Random.randomGuassianRange(20, 30, ConfidenceIntervals.CI_70));

			if (Settings.MAKE_CLICKING_ERRORS
					&& Random.modifiedRandomRange(0, 100, 3, Modifiers.get("mouseAccidentalClick2")) < 5) {
				Mouse.click(Mouse.LEFT_BUTTON);
				return false;
			}

			int i = getIndex(action, option);
			int count = Client.getMenuCount();

			if (i == -1) {
				return false;
			} else if (i == count - 1) {
				// can left click
				Mouse.click(Mouse.LEFT_BUTTON);
				return true;
			} else {

				Mouse.click(Mouse.RIGHT_BUTTON);
				Time.sleep(Random.randomGuassianRange(10, 20, ConfidenceIntervals.CI_60));

				if (Menu.isOpen()) {
					// check again to make sure we are clicking the correct thing
					i = getIndex(action, option);
					count = Client.getMenuCount();

					if (i != -1) {
						Mouse.move(Menu.getBounds(count - i - 1));
						Time.sleep(Random.randomGuassianRange(10, 20, ConfidenceIntervals.CI_60));
						Mouse.click(Mouse.LEFT_BUTTON);
						return Menu.getTrueBounds(count - i - 1).contains(Mouse.getLocation());
					} else {
						Menu.close();
					}
				}
			}

			return false;
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	/**
	 * 
	 * @param action
	 *            null == accept anything. array == accept anything in array
	 * @param option
	 *            null == accept anything. array == accept anything in array
	 * @return index of action or -1 if not found
	 */
	public static int getIndex(String[] action, String[] option) {
		int count = Client.getMenuCount();
		String[] actions = Client.getMenuActions();
		String[] options = Client.getMenuOptions();
		int i = 0;
		boolean found = false;

		for (i = count - 1; i >= 0; i--) {
			if ((action == null || ArrayUtil.contains(action, transform(actions[i])))
					&& (option == null || ArrayUtil.startsWith(transform(options[i]), option))) {
				found = true;
				break;
			}
		}

		return found ? i : -1;
	}

	public static boolean contains(String[] action, String[] option) {
		return getIndex(action, option) != -1;
	}

	public static Rectangle getTrueBounds(int index) {
		// actual menu box
		Point location = Menu.getLocation();
		
		return new Rectangle(location.x, location.y + 19 + 15 * index, Menu.getSize().width, 14);
	}

	public static Rectangle getBounds(int index) {
		Point location = Menu.getLocation();
		return new Rectangle(location.x + 10, location.y + 21 + 15 * index, Menu.getSize().width - 25, 10);

		// actual menu box
		// return new Rectangle(location.x, location.y + 19 + 15 * index, Menu.getSize().width, 14);
	}

	public static boolean interact(String action) {
		return interact(action, null);
	}

	public static Dimension getSize() {
		return new Dimension(Client.getMenuWidth(), Client.getMenuHeight());
	}

	public static Point getLocation() {
		return new Point(Client.getMenuX(), Client.getMenuY());
	}

	public static ArrayList<String> getMenuActions() {
		ArrayList<String> result = new ArrayList<String>();
		int count = Client.getMenuCount();
		String[] actions = Client.getMenuActions();
		for (int i = 0; i < count; i++)
			result.add(transform(actions[i]));

		return result;
	}

	public static ArrayList<String> getMenuOptions() {
		ArrayList<String> result = new ArrayList<String>();
		int count = Client.getMenuCount();
		String[] options = Client.getMenuOptions();
		for (int i = 0; i < count; i++)
			result.add(transform(options[i]));

		return result;
	}

	private static String transform(String text) {
		return text.replaceAll("<col=[0-9a-z]{1,6}>", "").replaceAll("<\\/col>", "").trim();
	}
}
