package org.iinc.osbot.api.base.timing;

public interface Condition {
	public boolean check();
}
