package org.iinc.osbot.api.base.timing;

import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

public class Time {

	public static boolean sleep(long ms) {
		if (ms <= 0)
			return false;

		try {
			Thread.sleep(ms);
		} catch (Exception e) {
		}
		return true;
	}

	public static void sleepReactionTime() {
		sleep(Random.modifiedRandomGuassianRange(100, 400, ConfidenceIntervals.CI_70, 200,
				Modifiers.get("reactionTime")));
	}

	public static void sleep(int min, int max) {
		sleep(Random.modifiedRandomGuassianRange(min, max, ConfidenceIntervals.CI_60, (min + max) / 2 - min,
				Modifiers.get("sleepTime")));
	}

	public static boolean sleepUntil(Condition c, int val) {
		return sleepUntil(c, val, val, Random.randomRange(50, 100));
	}

	public static boolean sleepUntil(Condition c, int min, int max) {
		return sleepUntil(c, min, max, Random.randomRange(50, 100));
	}

	public static boolean sleepUntil(Condition c, int min, int max, int interval) {
		long startTime = System.currentTimeMillis();
		int sleepTime = Random.modifiedRandomGuassianRange(min, max, ConfidenceIntervals.CI_60, (min + max) / 2 - min,
				Modifiers.get("sleepUntilTime"));
		boolean complete = c.check();
		while (!complete && System.currentTimeMillis() < startTime + sleepTime) {
			Time.sleep(interval);
			complete = c.check();
		}
		return complete;
	}

	// sleeps using modifiers reaction time
	public static boolean sleepUntil(Condition c) {
		int sleepTimeMin = Random.modifiedRandomGuassianRange(400, 1200, ConfidenceIntervals.CI_80,
				(400 + 1200) / 2 - 400, Modifiers.get("sleepTimeMin"));
		int sleepTimeMax = Random.modifiedRandomGuassianRange(1200, 3200, ConfidenceIntervals.CI_80,
				(1200 + 3200) / 2 - 1200, Modifiers.get("sleepTimeMax"));
		return sleepUntil(c, sleepTimeMin, sleepTimeMax, 100);
	}

}
