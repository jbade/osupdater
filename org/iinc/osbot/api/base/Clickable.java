package org.iinc.osbot.api.base;

import java.awt.Point;

import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.injection.accessors.Client;

public interface Clickable extends Comparable<Clickable> {

	public Tile getTile();

	public Point toScreen();

	public Point toMiniMap();
	
	/**
	 * Compared based on distance from local player
	 */
	public default int compareTo(Clickable other) {
		Tile loc = Client.getLocalPlayer().getTile();
		Tile a = getTile();
		Tile b = other.getTile();
		
		double dist = loc.distanceTo(a) - loc.distanceTo(b);
		
		if (dist > 0) {
			return 1;
		} else if (dist < 0) {
			return -1;
		} else {
			// adjacent tiles are closer than diagonal tiles
			int d = 0;
			if (loc.getX() != a.getX())
				d ++;
			if (loc.getY() != a.getY())
				d ++;
			if (loc.getX() != b.getX())
				d --;
			if (loc.getY() != b.getY())
				d --;
			 
			if (d > 0)
				return 1;
			if  (d < 0)
				return -1;
			
			return 0;
		}
	}
}
