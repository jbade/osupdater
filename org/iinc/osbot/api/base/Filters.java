package org.iinc.osbot.api.base;

import java.util.function.Predicate;

import org.iinc.osbot.api.injection.accessors.renderable.Npc;
import org.iinc.osbot.api.injection.accessors.renderable.NpcDefinition;

public class Filters {

	public static Predicate<Npc> byName(String... names) {
		return (npc) -> {
			if (npc == null)
				return false;

			NpcDefinition def = npc.getNpcDefinition();
			if (def == null)
				return false;

			String name = def.getName();
			if (name == null)
				return false;
			
			for (String nm : names){
				if (name.equals(nm))
					return true;
			}
			
			return false;
		};
	}
}
