package org.iinc.osbot.api.injection.accessors.object;

import java.awt.Point;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Clickable;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.injection.interfaces.object.IGameObject;
import org.iinc.osbot.api.injection.accessors.Client;

public class GameObject implements Clickable {

	private IGameObject ref;

	public GameObject(IGameObject ref) {
		this.ref = ref;
	}

	public int getObjectId() {
		return ref.getObjectId() >> 14 & 32767;
	}

	public int getLocalX() {
		return ref.getLocalX();
	}

	public int getLocalY() {
		return ref.getLocalY();
	}

	public int getOffsetX() {
		return ref.getOffsetX();
	}

	public int getOffsetY() {
		return ref.getOffsetY();
	}

	public int getX() {
		return Client.getBaseX() + (getLocalX() + getOffsetX()) / 2;
	}

	public int getY() {
		return Client.getBaseY() + (getLocalY() + getOffsetY()) / 2;
	}

	public Tile getTile() {
		return new Tile(getX(), getY(), Client.getPlane());
	}

	public Point toScreen() {
		int X = ((getLocalX() << 7) + (getOffsetX() << 7)) / 2 + 64;
		int Y = ((getLocalY() << 7) + (getOffsetY() << 7)) / 2 + 64;

		return Calculations.worldToScreen(X, Y, Client.getPlane(), 0);
	}

	public Point toMiniMap() {
		return Calculations.worldToMiniMap(((getLocalX() << 7) + (getOffsetX() << 7)) / 2,
				((getLocalY() << 7) + (getOffsetY() << 7)) / 2);
	}

}
