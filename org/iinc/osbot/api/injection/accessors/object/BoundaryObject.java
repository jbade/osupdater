package org.iinc.osbot.api.injection.accessors.object;

import java.awt.Point;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Clickable;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.injection.interfaces.object.IBoundaryObject;
import org.iinc.osbot.api.injection.accessors.Client;

public class BoundaryObject implements Clickable {
	
	private IBoundaryObject ref;
	private int localXOffset, localYOffset, height;
	
	public BoundaryObject(IBoundaryObject ref) {
		this.ref = ref;
	}

	public void setClickingOffsets(int localXOffset, int localYOffset, int height){
		this.localXOffset = localXOffset;
		this.localYOffset = localYOffset;
		this.height = height;
	}
	
	public int getObjectId() {
		return ref.getObjectId() >> 14 & 32767;
	}

	public int getLocalX() {
		return ref.getLocalX();
	}

	public int getLocalY() {
		return ref.getLocalY();
	}

	public int getX() {
		return Client.getBaseX() + (getLocalX() >> 7);
	}

	public int getY() {
		return Client.getBaseY() + (getLocalY() >> 7);
	}

	public Tile getTile() {
		return new Tile(getX(), getY(), Client.getPlane(), localXOffset, localYOffset, height);
	}

	public Point toScreen() {
		return Calculations.worldToScreen(getLocalX() + localXOffset, getLocalY() + localYOffset, Client.getPlane(), height);
	}

	public Point toMiniMap() {
		return Calculations.worldToMiniMap(getLocalX() + localXOffset, getLocalY() + localYOffset);
	}

}
