package org.iinc.osbot.api.injection.accessors.object;

import java.util.ArrayList;
import java.util.List;

import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Player;

public class RSObjects {

	public static List<BoundaryObject> getBoundaryObjects(int radius) {
		List<BoundaryObject> list = new ArrayList<BoundaryObject>();

		Player p = Client.getLocalPlayer();
		if (p == null)
			return list;

		Tile tile = p.getTile();
		if (tile == null)
			return list;

		for (int x = tile.getX() - radius; x <= tile.getX() + radius; x++) {
			for (int y = tile.getY() - radius; y <= tile.getY() + radius; y++) {
				BoundaryObject bo = getBoundaryObject(new Tile(x, y, Client.getPlane())); // TODO support other planes
				if (bo != null)
					list.add(bo);
			}
		}

		return list;

	}

	public static BoundaryObject getBoundaryObject(Tile tile) {
		if (tile == null)
			return null;

		Region region = Client.getRegion();
		if (region == null)
			return null;

		SceneTile st = region.getSceneTile(tile);
		if (st == null)
			return null;

		return st.getBoundaryObject();
	}

	public static List<GameObject> getGameObjects(int radius) {
		ArrayList<GameObject> list = new ArrayList<GameObject>();

		Player p = Client.getLocalPlayer();
		if (p == null)
			return list;

		Tile tile = p.getTile();
		if (tile == null)
			return list;

		for (int x = tile.getX() - radius; x <= tile.getX() + radius; x++) {
			for (int y = tile.getY() - radius; y <= tile.getY() + radius; y++) {
				list.addAll(getGameObjects(new Tile(x, y, Client.getPlane())));
			}
		}

		return list;

	}

	public static List<GameObject> getGameObjects(Tile tile) {
		if (tile == null)
			return new ArrayList<GameObject>(0);

		Region region = Client.getRegion();
		if (region == null)
			return new ArrayList<GameObject>(0);

		SceneTile st = region.getSceneTile(tile);
		if (st == null)
			return new ArrayList<GameObject>(0);

		return st.getGameObjects();
	}
}
