package org.iinc.osbot.api.injection.accessors.object;

import java.util.ArrayList;
import java.util.List;

import org.iinc.osbot.api.injection.interfaces.object.IBoundaryObject;
import org.iinc.osbot.api.injection.interfaces.object.IGameObject;
import org.iinc.osbot.api.injection.interfaces.object.ISceneTile;

public class SceneTile {

	private ISceneTile ref;

	public SceneTile(ISceneTile ref) {
		this.ref = ref;
	}

	public List<GameObject> getGameObjects() {
		IGameObject[] arr = ref.getGameObjects();

		if (arr == null)
			return new ArrayList<GameObject>(0);

		ArrayList<GameObject> objects = new ArrayList<GameObject>(arr.length);

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != null) {
				objects.add(new GameObject(arr[i]));
			}
		}

		return objects;
	}

	public BoundaryObject getBoundaryObject() {
		IBoundaryObject o = ref.getBoundaryObject();
		if (o == null)
			return null;
		return new BoundaryObject(o);
	}
}
