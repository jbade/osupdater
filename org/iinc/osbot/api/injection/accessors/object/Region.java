package org.iinc.osbot.api.injection.accessors.object;

import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.injection.interfaces.object.IRegion;
import org.iinc.osbot.api.injection.interfaces.object.ISceneTile;
import org.iinc.osbot.api.injection.accessors.Client;

public class Region {

	private IRegion ref;
	
	public Region(IRegion ref) {
		this.ref = ref;
	}

	public SceneTile getSceneTile(Tile tile) {
		ISceneTile[][][] arr3d = ref.getSceneTiles();
		if (arr3d == null || arr3d.length <= tile.getPlane())
			return null;

		ISceneTile[][] arr2d = arr3d[tile.getPlane()];
		if (arr2d == null || arr2d.length <= tile.getX() - Client.getBaseX())
			return null;

		ISceneTile[] arr = arr2d[tile.getX() - Client.getBaseX()];
		if (arr == null || arr.length <= tile.getY() - Client.getBaseY())
			return null;

		ISceneTile o = arr[tile.getY() - Client.getBaseY()];
		if (o == null)
			return null;

		return new SceneTile(o);
	}
}
