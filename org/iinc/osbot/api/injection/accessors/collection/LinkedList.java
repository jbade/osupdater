package org.iinc.osbot.api.injection.accessors.collection;

import org.iinc.osbot.api.injection.interfaces.collection.ILinkedList;
import org.iinc.osbot.api.injection.interfaces.collection.INode;

public class LinkedList {

	private ILinkedList ref;

	public LinkedList(ILinkedList ref) {
		this.ref = ref;
	}

	// TODO improve o.getClass().getInterfaces()
	public INode getHeadObject() {
		return ref.getHead();
	}

}
