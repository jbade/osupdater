package org.iinc.osbot.api.injection.accessors.collection;

import org.iinc.osbot.api.injection.interfaces.collection.INode;

public class Node {

	private INode ref;

	public Node(INode ref) {
		this.ref = ref;
	}

	public Node getNext() {
		INode o = ref.getNext();
		return o == null ? null : new Node(o);
	}

	public Node getPrev() {
		INode o = ref.getPrev();
		return o == null ? null : new Node(o);
	}

	public long getId() {
		return ref.getId();
	}
	
}
