package org.iinc.osbot.api.injection.accessors.collection;

import org.iinc.osbot.api.injection.interfaces.collection.IHashTable;
import org.iinc.osbot.api.injection.interfaces.collection.INode;

public class HashTable {

	private IHashTable ref;

	public HashTable(IHashTable ref) {
		this.ref = ref;
	}

	public int getSize() {
		return ref.getSize();
	}

	public int getIndex() {
		return ref.getIndex();
	}

	public INode[] getBuckets() {
		return ref.getBuckets();
	}
	
}
