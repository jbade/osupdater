package org.iinc.osbot.api.injection.accessors.collection;

import org.iinc.osbot.api.injection.interfaces.collection.INode;

public class DualNode extends Node {

	private INode ref;

	public DualNode(INode ref) {
		super(ref);
		this.ref = ref;
	}

}
