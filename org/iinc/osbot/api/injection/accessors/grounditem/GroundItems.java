package org.iinc.osbot.api.injection.accessors.grounditem;

import java.util.ArrayList;

import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.collection.LinkedList;
import org.iinc.osbot.api.injection.accessors.renderable.Player;
import org.iinc.osbot.api.injection.interfaces.IGroundItem;
import org.iinc.osbot.api.injection.interfaces.collection.ILinkedList;
import org.iinc.osbot.api.injection.interfaces.collection.INode;
import org.iinc.osbot.bot.Bot;

public class GroundItems {

	public static ArrayList<GroundItem> getAt(int x, int y, int plane) {
		ArrayList<GroundItem> result = new ArrayList<GroundItem>();
		
		ILinkedList[][][] arr3d = Bot.game.getGroundItems();
		if (arr3d == null || arr3d.length <= plane)
			return result;

		int xi = x - Client.getBaseX();
		ILinkedList[][] arr2d = arr3d[plane];
		if (arr2d == null || arr2d.length <= xi)
			return result;

		int yi = y - Client.getBaseY();
		ILinkedList[] arr = arr2d[xi];
		if (arr == null || arr.length <= yi)
			return result;

		ILinkedList o = arr[yi];
		if (o == null)
			return result;

		LinkedList list = new LinkedList(o);
		INode headObject = list.getHeadObject();
		if (headObject == null)
			return result;
		
		// we skip the head because it is not a ground item
		GroundItem head = new GroundItem((IGroundItem) headObject.getNext(), x, y, plane);
		for (GroundItem curr = head; curr != null; curr = curr.getNext()) { //  && !head.equals(curr)
			result.add(curr);
		}

		return result;
	}

	public static ArrayList<GroundItem> getAt(Tile tile) {
		return getAt(tile.getX(), tile.getY(), tile.getPlane());
	}

	public static ArrayList<GroundItem> getAll() {
		return getAll(9);
	}
	
	public static ArrayList<GroundItem> getAll(Tile center, int distance) {
		ArrayList<GroundItem> result = new ArrayList<GroundItem>();
				
		for (int i = distance * -1; i <= distance; i++) {
			for (int j = distance * -1; j <= distance; j++) {
				result.addAll(getAt(center.getX() + i, center.getY() + j, center.getPlane()));
			}
		}

		return result;
	}


	public static ArrayList<GroundItem> getAll(int distance) {
		ArrayList<GroundItem> result = new ArrayList<GroundItem>();
		Player localPlayer = Client.getLocalPlayer();
		if (localPlayer == null)
			return result;
		
		int x = localPlayer.getX();
		int y = localPlayer.getY();
		int plane = Client.getPlane();

		for (int i = distance * -1; i <= distance; i++) {
			for (int j = distance * -1; j <= distance; j++) {
				result.addAll(getAt(x + i, y + j, plane));
			}
		}

		return result;
	}

	public static ArrayList<GroundItem> getAllTrue() {
		ArrayList<GroundItem> result = new ArrayList<GroundItem>();
		int x = Client.getBaseX();
		int y = Client.getBaseY();
		int plane = Client.getPlane();

		for (int i = 0; i < 104; i++) {
			for (int j = 0; j < 104; j++) {
				result.addAll(getAt(x + i, y + j, plane));
			}
		}

		return result;
	}
}
