package org.iinc.osbot.api.injection.accessors.grounditem;

import java.awt.Point;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Clickable;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.injection.interfaces.IGroundItem;
import org.iinc.osbot.api.injection.interfaces.collection.INode;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.api.injection.accessors.renderable.Renderable;

public class GroundItem extends Renderable implements Clickable {

	private IGroundItem ref;
	private final int x, y, plane;

	public GroundItem(IGroundItem ref, int x, int y, int plane) {
		super(ref);
		this.ref = ref;
		this.x = x;
		this.y = y;
		this.plane = plane;
	}

	public GroundItem getNext() {
		INode o = ref.getNext();
		if (o == null || !(o instanceof IGroundItem))
			return null;
		return new GroundItem((IGroundItem) o, x, y, plane);
	}

	public int getItemId() {
		return ref.getItemId();
	}

	public Tile getTile() {
		return new Tile(x, y, plane);
	}

	public Point toScreen() {
		return Calculations.worldToScreen((x - Client.getBaseX() << 7) + 64, (y - Client.getBaseY() << 7) + 64,
				Client.getPlane(), 0);
	}

	public Point toMiniMap() {
		return Calculations.tileToMiniMap(x, y);
	}

}
