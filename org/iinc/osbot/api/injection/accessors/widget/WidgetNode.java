package org.iinc.osbot.api.injection.accessors.widget;

import org.iinc.osbot.api.injection.accessors.collection.Node;
import org.iinc.osbot.api.injection.interfaces.widget.IWidgetNode;

public class WidgetNode extends Node {

	private IWidgetNode ref;

	public WidgetNode(IWidgetNode ref) {
		super(ref);
		this.ref = ref;
	}

	public WidgetNode getNext() {
		Object o = ref.getNext();
		if (o == null || !(o instanceof IWidgetNode))
			return null;
		return new WidgetNode((IWidgetNode) o);
	}

	public WidgetNode getPrev() {
		Object o = ref.getPrev();
		if (o == null || !(o instanceof IWidgetNode))
			return null;
		return new WidgetNode((IWidgetNode) o);
	}

	public int getWidgetNodeId() {
		return ref.getWidgetNodeId();
	}

}
