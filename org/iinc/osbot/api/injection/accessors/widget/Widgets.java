package org.iinc.osbot.api.injection.accessors.widget;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.stream.Stream;

import org.iinc.osbot.api.injection.accessors.collection.HashTable;
import org.iinc.osbot.api.injection.interfaces.collection.IHashTable;
import org.iinc.osbot.api.injection.interfaces.widget.IWidget;
import org.iinc.osbot.bot.Bot;

public class Widgets {
	
	public static Stream<Widget> getAllWidgets() {
		ArrayList<Widget> widgets = new ArrayList<Widget>();

		Widget[][] w = getWidgets();

		LinkedList<Widget> queue = new LinkedList<Widget>();
		
		if (w != null) {
			for (Widget[] arr : w) {
				for (Widget widget : arr){		
					if (widget != null){
						queue.add(widget);
					}
				}
			}
		}
		
		while (!queue.isEmpty()){
			Widget widget = queue.remove();
			widgets.add(widget);
			for (Widget w2 : widget.getChildren()){
				if (w2 != null)
					queue.add(w2);
			}
		}
		
		return widgets.parallelStream();
	}

	public static Widget[][] getWidgets() {
		IWidget[][] arr2d = Bot.game.getWidgets();
		if (arr2d == null)
			return new Widget[0][];

		Widget[][] array = new Widget[arr2d.length][];
		for (int i = 0; i < arr2d.length; i++) {
			int length = 0;
			if (arr2d[i] != null)
				length = arr2d[i].length;
			array[i] = new Widget[length];
			for (int j = 0; j < length; j++) {
				if (arr2d[i][j] != null) {
					array[i][j] = new Widget(arr2d[i][j]);
				}

			}
		}

		return array;
	}

	public static boolean exists(int... indices) {
		// check length
		if (indices.length == 0)
			return false;

		// check for invalid indices
		for (int i = 0; i < indices.length; i++)
			if (indices[i] < 0)
				return false;

		IWidget[][] arr2d = Bot.game.getWidgets();

		if (arr2d == null || arr2d.length <= indices[0])
			return false;

		IWidget[] arr = arr2d[indices[0]];
		if (indices.length == 1)
			return arr != null;

		if (arr == null || arr.length <= indices[1])
			return false;

		IWidget o = arr[indices[1]];
		if (indices.length == 2)
			return o != null;

		if (o == null)
			return false;

		Widget widget = new Widget(o);
		for (int i = 2; i < indices.length; i++) {
			widget = widget.getChild(indices[i]);
			if (widget == null)
				return false;
		}

		return widget != null;
	}

	public static Widget getWidget(int... indices) {
		if (indices.length < 2)
			return null;

		IWidget[][] arr2d = Bot.game.getWidgets();

		if (arr2d == null || arr2d.length <= indices[0])
			return null;

		IWidget[] arr = arr2d[indices[0]];
		if (arr == null || arr.length <= indices[1])
			return null;

		IWidget o = arr[indices[1]];
		if (o == null)
			return null;

		Widget widget = new Widget(o);
		for (int i = 2; i < indices.length; i++) {
			widget = widget.getChild(indices[i]);
			if (widget == null)
				return null;
		}

		return widget;
	}

	public static int getBoundsX(int index) {
		if (index < 0)
			return -1;

		int[] arr = Bot.game.getWidgetBoundsX();

		if (arr.length <= index)
			return -1;

		return arr[index];
	}

	public static int getBoundsY(int index) {
		if (index < 0)
			return -1;

		int[] arr = Bot.game.getWidgetBoundsY();

		if (arr.length <= index)
			return -1;

		return arr[index];
	}

	public static HashTable getWidgetNodeCache() {
		IHashTable o = Bot.game.getWidgetNodeCache();
		if (o == null)
			return null;
		
		return new HashTable(o);
	}
}
