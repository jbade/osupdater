package org.iinc.osbot.api.injection.accessors.widget;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.iinc.osbot.api.injection.accessors.collection.HashTable;
import org.iinc.osbot.api.injection.accessors.collection.Node;
import org.iinc.osbot.api.injection.interfaces.collection.INode;
import org.iinc.osbot.api.injection.interfaces.widget.IWidget;
import org.iinc.osbot.api.injection.interfaces.widget.IWidgetNode;

public class Widget extends Node {

	private IWidget ref;

	public Widget(IWidget ref) {
		super(ref);
		this.ref = ref;
	}

	public int getWidgetId() {
		return ref.getWidgetId();
	}

	public int getWidth() {
		return ref.getWidth();
	}

	public int getHeight() {
		return ref.getHeight();
	}

	public int getAbsoluteX() {
		return ref.getAbsoluteX();
	}

	public int getAbsoluteY() {
		return ref.getAbsoluteY();
	}

	public int getBoundsIndex() {
		return ref.getBoundsIndex();
	}

	public int getItemId() {
		return ref.getItemId();
	}

	public int getItemQuantity() {
		return ref.getItemQuantity();
	}

	public String getName() {
		// TODO replace with one regex
		return (ref.getName()).replaceAll("<col=[0-9a-z]{1,6}>", "").replaceAll("<\\/col>", "");
	}

	public int getTextureId() {
		return ref.getTextureId();
	}

	private int getParentWidgetId() {
		Object result = ref.getParentId();

		if (result == null || (int) result == -1) {
			Integer id = getWidgetId();
			if (id < 0)
				return id;

			int shifted = id >> 16;

			HashTable table = Widgets.getWidgetNodeCache();

			// TODO reverse for way to find correct bucket instead of blindly searching
			INode[] buckets = table.getBuckets();
			for (int i = 0; i < buckets.length; i++) {
				if (buckets[i] == null)
					continue;

				Long headId = buckets[i].getId();
				INode next = buckets[i].getNext();
				if (next.getId() != headId) {
					WidgetNode curr = new WidgetNode((IWidgetNode) next);
					while (curr != null && curr.getId() != headId) {
						if (curr.getWidgetNodeId() == shifted)
							return (int) curr.getId();
						curr = curr.getNext();
					}
				}
			}
		}

		return (int) result;
	}

	private int getRelativeX() {
		return ref.getRelativeX();
	}

	private int getRelativeY() {
		return ref.getRelativeY();
	}

	public String getText() {
		return (ref.getText()).replaceAll("<col=[0-9a-z]{1,6}>", "").replaceAll("<\\/col>", "");
	}

	public boolean isHidden() {
		Widget parent = getParent();
		if (parent != null && parent.isHidden())
			return true;

		return ref.isHidden();
	}

	public int[] getItemIds() {
		return ref.getItemIds();
	}

	public int[] getItemQuantities() {
		return ref.getItemQuantities();
	}

	public Collection<String> getActions() {
		ArrayList<String> actions = new ArrayList<String>();

		String[] arr = ref.getActions();
		if (arr == null || arr.length == 0)
			return actions;

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != null) {
				actions.add((String) arr[i]);
			}
		}

		return actions;
	}

	public Widget getParent() {
		IWidget parent = ref.getParent();
		if (parent != null)
			return new Widget(parent);

		int parentId = getParentWidgetId();
		if (parentId == -1)
			return null;

		int containerIndex = parentId >> 16;
		int parentIndex = parentId & 0xFFFF;
		return Widgets.getWidget(containerIndex, parentIndex);
	}

	public int getX() {
		Widget parent = getParent();
		if (parent != null) {
			return parent.getX() + getAbsoluteX() - getRelativeX();
		} else {
			return Widgets.getBoundsX(getBoundsIndex());
		}
	}

	public int getY() {
		Widget parent = getParent();
		if (parent != null) {
			return parent.getY() + getAbsoluteY() - getRelativeY();
		} else {
			return Widgets.getBoundsY(getBoundsIndex());
		}
	}

	public Widget getChild(int index) {
		if (index < 0)
			return null;

		IWidget[] arr = ref.getChildren();
		if (arr == null || arr.length <= index)
			return null;

		Object o = arr[index];
		if (o == null)
			return null;

		return new Widget(arr[index]);
	}

	public boolean hasChild(int index) {
		if (index < 0)
			return false;

		IWidget[] arr = ref.getChildren();
		if (arr == null)
			return false;
		if (arr.length <= index)
			return false;

		return arr[index] != null;
	}

	public Widget[] getChildren() {
		IWidget[] arr = ref.getChildren();

		if (arr == null)
			return new Widget[0];

		Widget[] array = new Widget[arr.length];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != null)
				array[i] = new Widget(arr[i]);
		}

		return array;
	}

	public Rectangle toScreen() {
		return new Rectangle(getX(), getY(), getWidth(), getHeight());
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%s: %d \n", "id", getWidgetId()));
		sb.append(String.format("%s: %s \n", "parentId", getParentWidgetId()));

		sb.append(String.format("%s: %s \n", "textureId", getTextureId()));
		// sb.append(String.format("%s: %s \n", "type", getType()));

		sb.append(String.format("%s: %s \n", "name", getName()));
		sb.append(String.format("%s: %s \n", "text", getText()));
		sb.append(String.format("%s: %s \n", "hidden", isHidden()));

		sb.append(String.format("%s: %s \n", "x", getX()));
		sb.append(String.format("%s: %s \n", "y", getY()));

		sb.append(String.format("%s: %s \n", "width", getWidth()));
		sb.append(String.format("%s: %s \n", "height", getHeight()));

		sb.append(String.format("%s: %s \n", "boundaryIndex", getBoundsIndex()));
		sb.append(String.format("%s: %s \n", "boundsX", Widgets.getBoundsX(getBoundsIndex())));
		sb.append(String.format("%s: %s \n", "boundsY", Widgets.getBoundsY(getBoundsIndex())));
		sb.append(String.format("%s: %s \n", "absoluteX", getAbsoluteX()));
		sb.append(String.format("%s: %s \n", "absoluteY", getAbsoluteY()));
		sb.append(String.format("%s: %s \n", "relativeX", getRelativeX()));
		sb.append(String.format("%s: %s \n", "relativeY", getRelativeY()));

		sb.append(String.format("%s: %s \n", "itemIds", Arrays.toString(getItemIds())));
		sb.append(String.format("%s: %s \n", "itemQuantities", Arrays.toString(getItemQuantities())));

		sb.append(String.format("%s: %s \n", "itemId", getItemId()));
		sb.append(String.format("%s: %s \n", "itemQuantity", getItemQuantity()));

		sb.append(String.format("%s: %s \n", "actions", getActions().toString()));
		return sb.toString();
	}

}
