package org.iinc.osbot.api.injection.accessors.renderable;

import java.util.Collection;
import java.util.HashSet;

import org.iinc.osbot.api.injection.interfaces.renderable.IPlayer;
import org.iinc.osbot.bot.Bot;

public class Players {
	
	public static Collection<Player> getAll() {
		HashSet<Player> result = new HashSet<Player>();
		IPlayer[] arr = Bot.game.getPlayers();
		if (arr == null)
			return result;

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != null){
				result.add(new Player(arr[i]));
			}
		}

		return result;
	}
	
	public static IPlayer[] getPlayerObjects() {
		return Bot.game.getPlayers();
	}
	
}
