package org.iinc.osbot.api.injection.accessors.renderable;

import org.iinc.osbot.api.injection.accessors.collection.DualNode;
import org.iinc.osbot.api.injection.interfaces.renderable.IRenderable;

public class Renderable extends DualNode {

	private IRenderable ref;
	
	public Renderable(IRenderable ref) {
		super(ref);
		this.ref = ref;
	}

	public int getModelHeight() {
		return ref.getModelHeight();
	}
}
