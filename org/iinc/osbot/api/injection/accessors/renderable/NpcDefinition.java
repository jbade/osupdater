package org.iinc.osbot.api.injection.accessors.renderable;

import org.iinc.osbot.api.injection.accessors.collection.DualNode;
import org.iinc.osbot.api.injection.interfaces.renderable.INpcDefinition;

public class NpcDefinition extends DualNode {

	private INpcDefinition ref;
	
	public NpcDefinition(INpcDefinition ref) {
		super(ref);
		this.ref = ref;
	}

	public String getName() {
		return ref.getName();
	}

	public int getNpcId() {
		return ref.getNpcId();
	}
}
