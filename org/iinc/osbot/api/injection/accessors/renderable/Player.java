package org.iinc.osbot.api.injection.accessors.renderable;

import org.iinc.osbot.api.injection.interfaces.renderable.IPlayer;

public class Player extends Character {
	
	private IPlayer ref;
	
	public Player(IPlayer ref) {
		super(ref);
		this.ref = ref;
	}
	
	public int getCombatLevel(){
		return ref.getCombatLevel();
	}
	
	// -1 = no skull
	// 0 = normal skull
	public int getSkullIcon(){
		return ref.getSkullIcon();
	}
	
	public boolean isSkulled(){
		return getSkullIcon() != -1;
	}
	
	public IPlayer getReference(){
		return ref;
	}
	
}
