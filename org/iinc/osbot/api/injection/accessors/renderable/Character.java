package org.iinc.osbot.api.injection.accessors.renderable;

import java.awt.Point;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Clickable;
import org.iinc.osbot.api.base.positioning.PreciseTile;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.injection.interfaces.renderable.ICharacter;
import org.iinc.osbot.api.injection.accessors.Client;

public class Character extends Renderable implements Clickable {

	private ICharacter ref;

	public Character(ICharacter ref) {
		super(ref);
		this.ref = ref;
	}

	public int getLocalX() {
		return ref.getLocalX();
	}

	public int getLocalY() {
		return ref.getLocalY();
	}
	
	public double getPreciseX() {
		return Client.getBaseX() + getLocalX() / 128.0;
	}

	public double getPreciseY() {
		return Client.getBaseY() + getLocalY() / 128.0;
	}
	
	public PreciseTile getPreciseTile(){
		return new PreciseTile(getPreciseX(), getPreciseY(), Client.getPlane());
	}
	
	public int getX() {
		return Client.getBaseX() + (getLocalX() >> 7);
	}

	public int getY() {
		return Client.getBaseY() + (getLocalY() >> 7);
	}

	public Tile getTile() {
		return new Tile(getX(), getY(), Client.getPlane());
	}

	public Point toScreen() {
		return Calculations.worldToScreen(getLocalX(), getLocalY(), Client.getPlane(), getModelHeight() / 2);
	}

	public Point toMiniMap() {
		return Calculations.worldToMiniMap(getLocalX(), getLocalY());
	}

	public int getAnimation() {
		return ref.getAnimation();
	}

	public boolean isInteracting() {
		return getInteractingIndex() != -1;
	}

	public int getInteractingIndex() {
		return ref.getInteractingIndex();
	}

	public Character getInteracting() {
		/*
		 rev 119 x.av(Law;)V 393
		 	Character aw2 = null;
		    if (aw.interactingIndex * 545141459 < 32768) {
		        aw2 = client.npcs[aw.bx * 545141459];
		    }
		    else if (aw.interactingIndex * 545141459 >= 32768) {
		        aw2 = client.players[aw.interactingIndex * 545141459 - 32768];
		    }
		 */

		int index = getInteractingIndex();
		if (index == -1)
			return null;

		if (index < 32768) { // npc
			return Npcs.getNpc(index);
		} else if (index >= 32768) { // player
			// TODO client.players[index - 32768];
		}
		return null;
	}

	public Object getInteractingReference() {
		int index = getInteractingIndex();
		if (index == -1)
			return null;

		if (index < 32768) { // npc
			return Npcs.getNpcObjects()[index];
		} else if (index >= 32768) { // player
			return Players.getPlayerObjects()[index - 32768];
		}

		return null;
	}

}
