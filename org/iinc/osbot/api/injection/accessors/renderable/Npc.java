package org.iinc.osbot.api.injection.accessors.renderable;

import java.util.Collection;

import org.iinc.osbot.api.injection.interfaces.renderable.INpc;
import org.iinc.osbot.api.injection.interfaces.renderable.INpcDefinition;

public class Npc extends Character {
	private INpc ref;
	private int index;

	public Npc(INpc ref) {
		this(ref, -1);
	}

	public Npc(INpc ref, int index) {
		super(ref);
		this.ref = ref;
		this.index = index;
	}

	public NpcDefinition getNpcDefinition() {
		INpcDefinition o = ref.getNpcDefinition();
		if (o == null)
			return null;

		return new NpcDefinition(o);
	}

	public int getIndex() {
		if (index != -1) {
			Npc npc = Npcs.getNpc(index);
			if (this.equals(npc))
				return index;
		}

		Object[] npcs = Npcs.getNpcObjects();
		for (Integer i : Npcs.getNpcIndicies()) {
			if (i <= 0)
				break;

			if (npcs[i] == ref) {
				index = i;
				return i;
			}
		}

		index = -1;
		return index;
	}

	public boolean isInteracting() {
		return getInteractingIndex() != -1;
	}

	public boolean isBeingInteractedWith() {
		return isBeingInteractedWith(Players.getAll());
	}

	public boolean isBeingInteractedWith(Collection<Player> players) {
		int index = getIndex();
		if (index == -1)
			return false;

		for (Player player : players) {
			if (index == player.getInteractingIndex()) {
				return true;
			}
		}
		return false;
	}
	
	public INpc getReference(){
		return ref;
	}
}
