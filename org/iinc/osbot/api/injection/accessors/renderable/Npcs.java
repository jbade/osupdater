package org.iinc.osbot.api.injection.accessors.renderable;

import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Stream;

import org.iinc.osbot.api.injection.interfaces.renderable.INpc;
import org.iinc.osbot.bot.Bot;

public class Npcs {
	
	public static Npc getNpc(int index) {
		if (index < 0)
			return null;

		INpc[] arr = Bot.game.getNpcs();
		if (arr == null || arr.length <= index || arr[index] == null)
			return null;

		return new Npc(arr[index]);
	}

	public static Collection<Npc> getAll() {
		HashSet<Npc> result = new HashSet<Npc>();
		INpc[] arr = Bot.game.getNpcs();
		if (arr == null)
			return result;

		int[] indicies = getNpcIndicies();
		for (int i = 0; i < indicies.length; i++) {
			if (indicies[i] <= 0)
				break;

			if (arr[indicies[i]] != null)
				result.add(new Npc(arr[indicies[i]]));
		}

		return result;
	}

	public static int[] getNpcIndicies() {
		return Bot.game.getNpcIndices();
	}

	public static Object[] getNpcObjects() {
		return Bot.game.getNpcs();
	}

	public static Stream<Npc> getClosestStream(String name) {
		return getAll().parallelStream().filter((npc) -> {
			NpcDefinition def = npc.getNpcDefinition();
			if (def == null)
				return false;
			return name.equals(def.getName());
		}).sorted();
	}
	
	
	public static Npc getClosest(String name) {
		return getClosestStream(name).findFirst().orElse(null);
	}
	
}
