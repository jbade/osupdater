package org.iinc.osbot.api.injection.accessors;

import java.awt.Canvas;

import org.iinc.osbot.api.injection.accessors.object.Region;
import org.iinc.osbot.api.injection.accessors.renderable.Player;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.api.injection.accessors.widget.Widgets;
import org.iinc.osbot.api.injection.interfaces.object.IRegion;
import org.iinc.osbot.api.injection.interfaces.renderable.IPlayer;
import org.iinc.osbot.bot.Bot;

public class Client {

	public static int getBaseX() {
		return Bot.game.getBaseX();
	}

	public static int getBaseY() {
		return Bot.game.getBaseY();
	}

	public static int getCameraPitch() {
		return Bot.game.getCameraPitch();
	}

	public static int getCameraX() {
		return Bot.game.getCameraX();
	}

	public static int getCameraY() {
		return Bot.game.getCameraY();
	}

	public static int getCameraYaw() {
		return Bot.game.getCameraYaw();
	}

	public static int getCameraZ() {
		return Bot.game.getCameraZ();
	}

	public static Canvas getCanvas() {
		return Bot.game.getCanvas();
	}

	public static int[] getCurrentLevels() {
		return Bot.game.getCurrentLevels();
	}

	public static int getDestinationX() {
		return Bot.game.getDestinationX();
	}

	public static int getDestinationY() {
		return Bot.game.getDestinationY();
	}

	public static int getGameState() {
		return Bot.game.getGameState();
	}

	public static int getItemSelectionState() {
		return Bot.game.getItemSelectionState();
	}

	public static int[] getLevels() {
		return Bot.game.getLevels();
	}

	public static Player getLocalPlayer() {
		IPlayer p = Bot.game.getLocalPlayer();
		if (p == null)
			return null;
		
		return new Player(p);
	}

	public static int getLoginState() {
		return Bot.game.getLoginState();
	}

	public static int getMapAngle() {
		return Bot.game.getMapAngle();
	}

	public static String[] getMenuActions() {
		return Bot.game.getMenuActions();
	}

	public static int getMenuCount() {
		return Bot.game.getMenuCount();
	}

	public static int getMenuHeight() {
		return Bot.game.getMenuHeight();
	}

	public static String[] getMenuOptions() {
		return Bot.game.getMenuOptions();
	}

	public static boolean isMenuVisible() {
		return Bot.game.isMenuVisible();
	}

	public static int getMenuWidth() {
		return Bot.game.getMenuWidth();
	}

	public static int getMenuX() {
		return Bot.game.getMenuX();
	}

	public static int getMenuY() {
		return Bot.game.getMenuY();
	}

	public static int[] getNpcIndices() {
		return Bot.game.getNpcIndices();
	}

	public static int getPlane() {
		return Bot.game.getPlane();
	}

	public static Region getRegion(){
		IRegion r = Bot.game.getRegion();
		if (r == null)
			return null;
		
		return new Region(r);
	}
	
	public static int[] getSettings() {
		return Bot.game.getSettings();
	}

	public static boolean isSpellSelected() {
		return Bot.game.isSpellSelected();
	}

	public static int[][][] getTileHeights() {
		return Bot.game.getTileHeights();
	}

	public static byte[][][] getTileSettings() {
		return Bot.game.getTileSettings();
	}

	public static int getViewportHeight() {
		return Bot.game.getViewportHeight();
	}

	public static int getViewportScale() {
		return Bot.game.getViewportScale();
	}

	public static int getViewportWidth() {
		return Bot.game.getViewportWidth();
	}

	public static int[] getWidgetBoundsHeight() {
		return Bot.game.getWidgetBoundsHeight();
	}

	public static int[] getWidgetBoundsWidth() {
		return Bot.game.getWidgetBoundsWidth();
	}

	public static int[] getWidgetBoundsX() {
		return Bot.game.getWidgetBoundsX();
	}

	public static int[] getWidgetBoundsY() {
		return Bot.game.getWidgetBoundsY();
	}

	public static int getCursorState() {
		return Bot.game.getCursorState();
	}
	
	public static boolean didRedClick(){
		return getCursorState() == 2;
	}
	
	public static boolean didYellowClick(){
		return getCursorState() == 1;
	}
	
	
	public static int getSetting(int index) {
		int[] settings = getSettings();
		if (settings == null || index >= settings.length)
			return -1;

		return settings[index];
	}

	
	private static final int[] WILDERNESS_WIDGET = new int[] { 90 };
	private static final int[] WILDERNESS_LEVEL_WIDGET = new int[] { 90, 46 };

	public static int getWildernessLevel() {
		// text: Level: 21
		Widget w = Widgets.getWidget(WILDERNESS_LEVEL_WIDGET);
		if (w == null)
			return -1;
		String text = w.getText();
		if (text == null)
			return -1;
		if (text.length() < 7)
			return -1;
		
		text = text.substring(7);
		if (text == null){
			return -1;
		}
		text = text.replaceAll("[^\\d.]", "");
		
		if (text.equals("")){
			return -1;
		}
		
		return Integer.valueOf(text);
	}

	public static boolean isInWilderness() {
		return getWildernessLevel() != -1;
	}

	// inactive: textureId=1060, active: textureId=1061
	private static final int[] POISON_WIDGET = { 160, 6 };

	public static boolean isPoisoned() {
		Widget widget = Widgets.getWidget(POISON_WIDGET);
		return widget != null && widget.getTextureId() == 1061;
	}

	public static boolean isLoggedIn() {
		return getGameState() > 25;
	}

	public static boolean atLoginScreen() {
		return getGameState() == 10;
	}
	
}
