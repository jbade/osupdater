package org.iinc.osbot.api.injection.accessors.grandexchange;

import org.iinc.osbot.api.injection.interfaces.IGrandExchangeOffer;

public class GrandExchangeOffer {

	public static enum STATES {
		EMPTY, BUYING, SELLING, COMPLETE, UNKNOWN
	}

	private final int index;
	private IGrandExchangeOffer ref;

	public GrandExchangeOffer(IGrandExchangeOffer ref, int index) {
		this.ref = ref;
		this.index = index;
	}

	public int getTransfered() {
		return ref.getTransfered();
	}

	public int getItemId() {
		return ref.getItemId();
	}

	public int getQuantity() {
		return ref.getQuantity();
	}

	public int getPrice() {
		return ref.getPrice();
	}

	public int getSpent() {
		return ref.getSpent();
	}

	/*
		0 - empty
		1 - buying initializing
		2 - buying progress
		3 - buying updating
		4 - buying aborting
		5 - buying complete/aborted
		6
		7
		8
		9 - selling initializing
		10 - selling progress
		11 - selling updating
		12 - selling aborting
		13 - selling complete/aborted
		14
	 */
	public STATES getState() {
		byte state = ref.getState();
		switch (state) {
		case 0:
			return STATES.EMPTY;
		case 1:
		case 2:
		case 3:
		case 4:
			return STATES.BUYING;
		case 9:
		case 10:
		case 11:
		case 12:
			return STATES.SELLING;
		case 5:
		case 13:
			return STATES.COMPLETE;
		default:
			return STATES.UNKNOWN;
		}
	}

	public boolean isSellOffer() {
		byte state = ref.getState();
		return state >= 9 && state <= 13;
	}

	public boolean isBuyOffer() {
		byte state = ref.getState();
		return state >= 1 && state <= 5;
	}

	public int getIndex() {
		return index;
	}

	@Override
	public String toString() {
		return "GrandExchangeOffer [index: " + index + ", transfered: " + getTransfered() + ", itemId: " + getItemId()
				+ ", quantity: " + getQuantity() + ", price: " + getPrice() + ", spent: " + getSpent() + ", state: "
				+ getState() + "]";
	}

}
