package org.iinc.osbot.api.injection.accessors.grandexchange;

import java.util.ArrayList;

import org.iinc.osbot.api.injection.interfaces.IGrandExchangeOffer;
import org.iinc.osbot.bot.Bot;

public class GrandExchange {

	public static GrandExchangeOffer getOffer(int index) {
		IGrandExchangeOffer[] arr = Bot.game.getGrandExchangeOffers();
		
		if (arr == null || arr.length == 0 || arr.length <= index)
			return null;

		if (arr[index] != null) {
			return new GrandExchangeOffer(arr[index], index);
		}

		return null;
	}

	public static ArrayList<GrandExchangeOffer> getOffers() {
		ArrayList<GrandExchangeOffer> offers = new ArrayList<GrandExchangeOffer>(8);

		IGrandExchangeOffer[] arr = Bot.game.getGrandExchangeOffers();
		if (arr == null || arr.length == 0)
			return offers;

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != null) {
				GrandExchangeOffer offer = new GrandExchangeOffer(arr[i], i);
				if (offer.getState() != GrandExchangeOffer.STATES.EMPTY)
					offers.add(offer);
			}
		}

		return offers;
	}

	public static GrandExchangeOffer[] getOffersArray() {
		IGrandExchangeOffer[] arr = Bot.game.getGrandExchangeOffers();
		if (arr == null || arr.length == 0)
			return null;

		GrandExchangeOffer[] offers = new GrandExchangeOffer[arr.length];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != null) {
				offers[i] = new GrandExchangeOffer(arr[i], i);
			}
		}

		return offers;
	}

	public static int getEmptySlot() {
		IGrandExchangeOffer[] arr = Bot.game.getGrandExchangeOffers();
		if (arr == null || arr.length == 0)
			return 0;

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != null) {
				GrandExchangeOffer offer = new GrandExchangeOffer(arr[i], i);
				if (offer.getState() == GrandExchangeOffer.STATES.EMPTY)
					return i;
			}
		}

		return -1;
	}

	public static int getEmptySlots() {
		IGrandExchangeOffer[] arr = Bot.game.getGrandExchangeOffers();
		if (arr == null || arr.length == 0)
			return 0;

		int count = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != null) {
				GrandExchangeOffer offer = new GrandExchangeOffer(arr[i], i);
				if (offer.getState() != GrandExchangeOffer.STATES.EMPTY)
					count++;
			}
		}

		return arr.length - count;
	}

	public static int getTotalSlots() {
		IGrandExchangeOffer[] arr = Bot.game.getGrandExchangeOffers();
		if (arr == null)
			return 0;
		return arr.length;
	}
	
}
