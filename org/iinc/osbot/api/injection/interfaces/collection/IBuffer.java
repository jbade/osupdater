package org.iinc.osbot.api.injection.interfaces.collection;

public interface IBuffer {

	public byte[] getPayload();

	public int getPosition();

}
