package org.iinc.osbot.api.injection.interfaces.collection;

public interface IHashTable {

	public INode[] getBuckets();

	public int getIndex();

	public int getSize();

}
