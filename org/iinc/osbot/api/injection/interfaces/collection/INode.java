package org.iinc.osbot.api.injection.interfaces.collection;

public interface INode {

	public long getId();

	public INode getNext();

	public INode getPrev();

}
