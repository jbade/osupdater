package org.iinc.osbot.api.injection.interfaces.collection;

public interface ILinkedList {

	public INode getHead();

}
