package org.iinc.osbot.api.injection.interfaces;

import org.iinc.osbot.api.injection.interfaces.renderable.IRenderable;

public interface IGroundItem extends IRenderable {

	public int getItemId();
	
	public int getStackSize();

}
