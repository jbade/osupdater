package org.iinc.osbot.api.injection.interfaces.renderable;

public interface INpc extends ICharacter {
	
	public INpcDefinition getNpcDefinition();

}
