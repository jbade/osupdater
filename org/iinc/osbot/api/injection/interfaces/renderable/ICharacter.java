package org.iinc.osbot.api.injection.interfaces.renderable;

public interface ICharacter extends IRenderable {
	
	public int getAnimation();

	public int getInteractingIndex();

	public int getLocalX();

	public int getLocalY();

}
