package org.iinc.osbot.api.injection.interfaces.renderable;

import org.iinc.osbot.api.injection.interfaces.collection.IDualNode;

public interface INpcDefinition extends IDualNode {

	public String getName();

	public int getNpcId();
	
}
