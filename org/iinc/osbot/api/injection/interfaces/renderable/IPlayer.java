package org.iinc.osbot.api.injection.interfaces.renderable;

public interface IPlayer extends ICharacter {

	public int getCombatLevel();

	public int getSkullIcon();

}
