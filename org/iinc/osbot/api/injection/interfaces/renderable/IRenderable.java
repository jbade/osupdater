package org.iinc.osbot.api.injection.interfaces.renderable;

import org.iinc.osbot.api.injection.interfaces.collection.IDualNode;

public interface IRenderable extends IDualNode {

	public int getModelHeight();

}
