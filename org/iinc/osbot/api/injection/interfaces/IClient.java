package org.iinc.osbot.api.injection.interfaces;

import java.awt.Canvas;

import org.iinc.osbot.api.injection.interfaces.collection.IHashTable;
import org.iinc.osbot.api.injection.interfaces.collection.ILinkedList;
import org.iinc.osbot.api.injection.interfaces.object.IRegion;
import org.iinc.osbot.api.injection.interfaces.renderable.INpc;
import org.iinc.osbot.api.injection.interfaces.renderable.IPlayer;
import org.iinc.osbot.api.injection.interfaces.widget.IWidget;

public interface IClient {

	public int getBaseX();

	public int getBaseY();

	public int getCameraPitch();

	public int getCameraX();

	public int getCameraY();

	public int getCameraYaw();

	public int getCameraZ();

	public Canvas getCanvas();

	public int[] getCurrentLevels();

	public int getDestinationX();

	public int getDestinationY();

	public int[] getExperiences();

	public int getGameState();

	public IGrandExchangeOffer[] getGrandExchangeOffers();

	public ILinkedList[][][] getGroundItems();

	public int getItemSelectionState();

	public int[] getLevels();

	public IPlayer getLocalPlayer();

	public int getLoginState();

	public int getMapAngle();
	
	public String[] getMenuActions();

	public int getMenuCount();

	public int getMenuHeight();

	public String[] getMenuOptions();

	public boolean isMenuVisible();

	public int getMenuWidth();

	public int getMenuX();

	public int getMenuY();

	public int[] getNpcIndices();

	public INpc[] getNpcs();

	public int getPlane();

	public IPlayer[] getPlayers();

	public IRegion getRegion();

	public int[] getSettings();

	public boolean isSpellSelected();

	public int[][][] getTileHeights();

	public byte[][][] getTileSettings();

	public int getViewportHeight();

	public int getViewportScale();

	public int getViewportWidth();

	public int[] getWidgetBoundsHeight();

	public int[] getWidgetBoundsWidth();

	public int[] getWidgetBoundsX();

	public int[] getWidgetBoundsY();

	public IHashTable getWidgetNodeCache();

	public IWidget[][] getWidgets();

	public int getCursorState();
}
