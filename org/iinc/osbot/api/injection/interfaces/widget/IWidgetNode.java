package org.iinc.osbot.api.injection.interfaces.widget;

import org.iinc.osbot.api.injection.interfaces.collection.INode;

public interface IWidgetNode extends INode {

	public int getWidgetNodeId();

}
