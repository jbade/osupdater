package org.iinc.osbot.api.injection.interfaces.widget;

import org.iinc.osbot.api.injection.interfaces.collection.INode;

public interface IWidget extends INode {
	
	public int getAbsoluteX();

	public int getAbsoluteY();

	public String[] getActions();

	public int getBoundsIndex();

	public IWidget[] getChildren();

	public int getHeight();

	public boolean isHidden();

	public int getItemId();

	public int[] getItemIds();

	public int[] getItemQuantities();

	public int getItemQuantity();

	public String getName();

	public IWidget getParent();

	public int getParentId();

	public int getRelativeX();

	public int getRelativeY();

	public String getText();

	public int getTextureId();

	public int getWidgetId();

	public int getWidth();

}
