package org.iinc.osbot.api.injection.interfaces.object;

public interface IRegion {

	public ISceneTile[][][] getSceneTiles();

}
