package org.iinc.osbot.api.injection.interfaces.object;

import org.iinc.osbot.api.injection.interfaces.renderable.IRenderable;

public interface IGameObject {

	public int getFlags();

	public int getHeight();

	public int getLocalX();

	public int getLocalY();

	public int getObjectId();

	public int getOffsetX();

	public int getOffsetY();

	public int getOrientation();

	public int getPlane();

	public IRenderable getRender();

	public int getWorldX();

	public int getWorldY();

}
