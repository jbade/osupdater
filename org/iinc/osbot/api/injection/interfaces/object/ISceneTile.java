package org.iinc.osbot.api.injection.interfaces.object;

public interface ISceneTile {
	
	public IBoundaryObject getBoundaryObject();

	public IGameObject[] getGameObjects();

}
