package org.iinc.osbot.api.injection.interfaces.object;

import org.iinc.osbot.api.injection.interfaces.renderable.IRenderable;

public interface IBoundaryObject {

	public int getFlags();

	public int getHeight();

	public int getLocalX();

	public int getLocalY();

	public int getObjectId();

	public int getOrientation();

	public int getPlane();

	public IRenderable getRender();

	public IRenderable getRender2();

}