package org.iinc.osbot.api.injection.interfaces;

public interface IGrandExchangeOffer {
	
	public int getItemId();

	public int getPrice();

	public int getQuantity();

	public int getSpent();

	public byte getState();

	public int getTransfered();

}
