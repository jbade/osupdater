package org.iinc.osbot.api.injection;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.gui.GUI;

public class ReplacedCanvas extends Canvas {
	private static final long serialVersionUID = 1L;

	@Override
	public Graphics getGraphics() {
		if (Bot.gameImage == null || this.getWidth() != Bot.gameImage.getWidth()
				|| this.getHeight() != Bot.gameImage.getHeight()) {
			Bot.gameImage = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
		}
		GUI.repaint();

		return Bot.gameImage.getGraphics();
	}
}
