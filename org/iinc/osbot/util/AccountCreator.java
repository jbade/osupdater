package org.iinc.osbot.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.util.internet.Internet;
import org.iinc.osbot.util.internet.Response;

public class AccountCreator {

	private final static String TWO_CAPTCHA_API_KEY = "b7fbabdb83043703212d7f0382cd1ce5";

	public static String getCaptchaSolution() {
		HashMap<String, String> form = new HashMap<String, String>();
		form.put("key", TWO_CAPTCHA_API_KEY);
		form.put("method", "userrecaptcha");
		form.put("googlekey", "6LccFA0TAAAAAHEwUJx_c1TfTBWMTAOIphwTtd1b");
		form.put("pageurl",
				"https://secure.runescape.com/m=account-creation/g=oldscape/create_account?trialactive=true");
		Response res = Internet.post("https://2captcha.com/in.php", form);
		if (res == null || res.getResponseBody() == null || res.getResponseBody().indexOf("OK|") != 0)
			return null;

		form.clear();
		form.put("key", TWO_CAPTCHA_API_KEY);
		form.put("action", "get");
		form.put("id", res.getResponseBody().substring(3));
		String qs = form.entrySet().stream().map(p -> {
			try {
				return URLEncoder.encode(p.getKey(), "UTF-8") + "=" + URLEncoder.encode(p.getValue(), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				return "";
			}
		}).reduce((p1, p2) -> p1 + "&" + p2).orElse("");

		Timer t = new Timer(5 * 60 * 1000);
		while (t.isRunning()) {
			res = Internet.post("https://2captcha.com/res.php?" + qs, null);
			if (res != null && res.getResponseBody() != null && res.getResponseBody().indexOf("OK|") == 0) {
				return res.getResponseBody().substring(3);
			}

			Time.sleep(5000);
		}

		return null;
	}

	public static boolean createAccount(String email, String accountName, String password, String captchaSolution) {
		HashMap<String, String> form = new HashMap<String, String>();
		form.put("trialactive", "true");
		form.put("onlyOneEmail", "1");
		form.put("displayname_present", "true");
		form.put("age", Integer.toString(Random.randomRange(18, 30)));
		form.put("displayname", accountName);
		form.put("email1", email);
		form.put("password1", password);
		form.put("password2", password);
		form.put("agree_email", "on");
		form.put("g-recaptcha-response", captchaSolution);
		form.put("submit", "Join Now");

		Response res = Internet.post("https://secure.runescape.com/m=account-creation/g=oldscape/create_account", form);
		Bot.LOGGER.log(Level.FINEST, res.toString());

		if (res == null || res.getResponseCode() != 200 || res.getResponseBody() == null)
			return false;

		return res.getResponseBody().indexOf("<title>Account Created") != -1;
	}

	public static String getFreeName() {
		String s = "cat";

		while (true) {
			HashMap<String, String> form = new HashMap<String, String>();
			form.put("displayname", s);
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put("X-Requested-With", "XMLHttpRequest");
			headers.put("Referer", "https://secure.runescape.com/m=account-creation/g=oldscape/create_account");
			headers.put("Origin", "https://secure.runescape.com");

			Response res = Internet.post(
					"https://secure.runescape.com/m=account-creation/g=oldscapeg=oldscape/check_displayname.ajax",
					headers, form);
			if (res == null || res.getResponseCode() != 200 || res.getResponseBody() == null)
				return null;

			String body = res.getResponseBody();
			System.out.println(body);
			
			if (body.contains("\"displayNameIsValid\":\"true\"")) {
				return s;
			}

			Pattern p = Pattern.compile(
					"\"nameSuggestions\":\\[\"([^\"]*)\"(?:,\"([^\"]*)\"(?:,\"([^\"]*)\"(?:,\"([^\"]*)\"(?:,\"([^\"]*)\")?)?)?)?");
			Matcher m = p.matcher(body);

			if (m.find() && m.groupCount() > 0) {
				s = m.group(1);
				//s = m.group(1).replaceAll("[^A-z]", ""); // we dont want numbers in the name
			} else {
				return null;
			}
		}
	}
}
