package org.iinc.osbot.util.internet;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Internet {
	
	public static Response post(String url, Map<String, String> form) {
		return post(url, null, form);
	}
	
	public static Response post(String url, Map<String, String> headers, Map<String, String> form) {
		try {
			URL u = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) u.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
			
			if (headers != null){
				for (Entry<String, String> e : headers.entrySet()){
					conn.setRequestProperty(e.getKey(), e.getValue());
				}
			}
			
			if (form != null){
				conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				String formData = form.entrySet().stream().map(p -> {
					try {
						return URLEncoder.encode(p.getKey(), "UTF-8") + "=" + URLEncoder.encode(p.getValue(), "UTF-8");
					} catch (UnsupportedEncodingException e) {
						return "";
					}
				}).reduce((p1, p2) -> p1 + "&" + p2).orElse("");
				conn.setRequestProperty("Content-Length", String.valueOf(formData.length()));
				OutputStream os = conn.getOutputStream();
				os.write(formData.getBytes());
			}
			

			return new Response(conn);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static List<String> read(String url) {
		LinkedList<String> list = new LinkedList<String>();
		try {
			HttpURLConnection connection = HttpURLConnection.class.cast(new URL(url).openConnection());
			BufferedReader stream = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String read;
			while ((read = stream.readLine()) != null) {
				list.add(read);
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return list;
	}

	public static File download(String location, Path path) {
		return download(location, path, new InternetCallback() {

			@Override
			public void onDownload(double p) {
			}

			@Override
			public void onComplete() {
			}
		});
	}

	public static File download(String location, Path path, InternetCallback callback) {
		BufferedInputStream in = null;
		FileOutputStream out = null;

		try {
			URL url = new URL(location);
			URLConnection conn = url.openConnection();
			int size = conn.getContentLength();

			in = new BufferedInputStream(url.openStream());
			out = new FileOutputStream(path.toFile());
			byte data[] = new byte[1024];
			int count;
			double sumCount = 0.0;

			while ((count = in.read(data, 0, 1024)) != -1) {
				out.write(data, 0, count);

				sumCount += count;
				if (size > 0) {
					if (callback != null)
						callback.onDownload(sumCount / size);
				}
			}

			if (callback != null && sumCount == size) {
				callback.onComplete();
			}

		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e3) {
					e3.printStackTrace();
				}
			if (out != null)
				try {
					out.close();
				} catch (IOException e4) {
					e4.printStackTrace();
				}
		}

		return path.toFile();
	}

}
