package org.iinc.osbot.util.internet;

public interface InternetCallback {
	public void onDownload(double p);
	
	public void onComplete();
}
