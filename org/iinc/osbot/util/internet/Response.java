package org.iinc.osbot.util.internet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;

public class Response {
	final int responseCode;
	final String responseMessage;
	final Map<String, List<String>> reponseHeaders;
	final String responseBody;

	public Response(HttpURLConnection conn) throws IOException {
		BufferedReader stream = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		StringBuilder sb = new StringBuilder();
		String read;
		while ((read = stream.readLine()) != null) {
			sb.append(read).append("\n");
		}
		this.responseBody = sb.toString();

		this.reponseHeaders = conn.getHeaderFields();
		this.responseCode = conn.getResponseCode();
		this.responseMessage = conn.getResponseMessage();
	}

	public int getResponseCode() {
		return responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public Map<String, List<String>> getReponseHeaders() {
		return reponseHeaders;
	}

	public String getResponseBody() {
		return responseBody;
	}

	@Override
	public String toString() {
		return "Response [responseCode=" + responseCode + ", responseMessage=" + responseMessage + ", reponseHeaders="
				+ reponseHeaders + ", responseBody=" + responseBody + "]";
	}
	
	
	
}
