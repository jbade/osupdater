package org.iinc.osbot.util;

import java.applet.Applet;
import java.applet.AppletContext;
import java.applet.AppletStub;
import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.worlds.World;
import org.iinc.osbot.util.internet.Internet;
import org.iinc.osbot.util.internet.InternetCallback;
import org.iinc.osbot.util.pattern.OpcodeGroups;
import org.iinc.osbot.util.pattern.Pattern;
import org.iinc.osbot.util.pattern.PatternElement;
import org.iinc.osbot.util.pattern.PatternMatchResult;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.MethodNode;

/**
 * @author Tyler Sedlar
 * @since 10/21/15
 */
public class Crawler {

	private final Map<String, String> parameters = new HashMap<>();
	private final String home;
	private final String config;
	private int hash = -1;

	public Crawler() {
		this(369);
	}

	public Crawler(int world) {
		home = String.format("http://oldschool%d.runescape.com/", world - 300);
		config = home + "jav_config.ws";
	}

	public Crawler(World world) {
		home = "http://" + world.getDomain() + "/";
		config = home + "jav_config.ws";
	}

	public URL getFileURL() {
		try {
			return Paths.get(Settings.TEMPORARY_DIRECTORY, "gamepack.jar").toUri().toURL();
		} catch (MalformedURLException e) {
			return null;
		}
	}

	public Applet start(ClassLoader classloader) {
		try {
			String main = parameters.get("initial_class").replace(".class", "");
			Applet applet = (Applet) classloader.loadClass(main).getConstructor().newInstance();
			applet.setBackground(Color.BLACK);
			applet.setPreferredSize(appletSize());
			applet.resize(appletSize());
			applet.setLayout(null);
			AppletStub stub = stub(applet);
			applet.setStub(stub);
			applet.init();
			applet.start();
			applet.setVisible(true);
			return applet;
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | ClassNotFoundException e) {
			e.printStackTrace();
			if(e instanceof java.lang.reflect.InvocationTargetException){
				 InvocationTargetException i = (java.lang.reflect.InvocationTargetException) e;
				 System.err.println("SPACE");
				 i.getTargetException().printStackTrace(System.out);
			}
			
			System.exit(0);
			return null;
		}
	}

	/**
	 * Creates the AppletStub for the given Applet.
	 *
	 * @param applet
	 *            The game Applet.
	 * @return The AppletStub for the given Applet.
	 */
	public AppletStub stub(final Applet applet) {
		return new AppletStub() {

			@Override
			public boolean isActive() {
				return true;
			}

			@Override
			public URL getDocumentBase() {
				try {
					return new URL(parameters.get("codebase"));
				} catch (MalformedURLException e) {
					e.printStackTrace();
					return null;
				}
			}

			@Override
			public URL getCodeBase() {
				try {
					return new URL(parameters.get("codebase"));
				} catch (MalformedURLException ignored) {
					ignored.printStackTrace();
					return null;
				}
			}

			@Override
			public String getParameter(String name) {
				return parameters.get(name);
			}

			@Override
			public void appletResize(int width, int height) {
				Dimension size = new Dimension(width, height);
				applet.setSize(size);
			}

			@Override
			public AppletContext getAppletContext() {
				return null;
			}
		};
	}

	/**
	 * Gets the hash of the local game jar.
	 *
	 * @return The hash of the local game jar.
	 */
	private int localHash() {
		try {
			URL url = Paths.get(Settings.TEMPORARY_DIRECTORY, "gamepack.jar").toUri().toURL();
			try (JarInputStream stream = new JarInputStream(url.openStream())) {
				return stream.getManifest().hashCode();
			} catch (Exception ignored) {
				return -1;
			}
		} catch (MalformedURLException ignored) {
			return -1;
		}
	}

	/**
	 * Gets the current game hash.
	 *
	 * @return The current game hash.
	 */
	public int hash() {
		return hash;
	}

	/**
	 * Gets the game hash on the server.
	 *
	 * @return The game hash on the server.
	 */
	public int remoteHash() {
		try {
			URL url = new URL(home + parameters.get("initial_jar"));
			try (JarInputStream stream = new JarInputStream(url.openStream())) {
				return stream.getManifest().hashCode();
			} catch (Exception ignored) {
				return -1;
			}
		} catch (IOException ignored) {
			return -1;
		}
	}

	/**
	 * Sets the hash object to the local jar hash.
	 */
	public void initHash() {
		hash = localHash();
	}

	/**
	 * Checks whether the local jar is outdated or not.
	 * 
	 * @return <t>true</t> if the local hash doesn't match the remote hash,
	 *         otherwise <t>false</t>.
	 */
	public boolean outdated() {
		File gamepack = Paths.get(Settings.TEMPORARY_DIRECTORY, "gamepack.jar").toFile();
		if (!gamepack.exists()) {
			return true;
		}
		if (hash == -1) {
			hash = localHash();
		}
		boolean outdated = hash == -1 || hash != remoteHash();
		return outdated;
	}

	/**
	 * Reads the server configuration.
	 *
	 * @return <t>true</t> if the server configuration was read, otherwise
	 *         <t>false</t>.
	 */
	public boolean crawl() {
		try {
			List<String> source = Internet.read(config);
			for (String line : source) {
				// System.out.println("line " + line);
				if (line.startsWith("param=")) {
					line = line.substring(6);
				}
				int idx = line.indexOf("=");
				if (idx == -1) {
					continue;
				}
				parameters.put(line.substring(0, idx), line.substring(idx + 1));
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Downloads the game jar to the given target.
	 *
	 * @param path
	 *            The path to download to.
	 * @param callback
	 *            The callback to be run upon download percentage change.
	 * @return <t>true</t> if the game was downloaded, otherwise <t>false</t>.
	 */
	public boolean download(Path path, final InternetCallback callback) {
		hash = remoteHash();
		System.out.println(home + parameters.get("initial_jar"));
		return Internet.download(home + parameters.get("initial_jar"), path, new InternetCallback() {

			@Override
			public void onDownload(double p) {
				if (callback != null)
					callback.onDownload(p);
			}

			@Override
			public void onComplete() {
				if (callback != null)
					callback.onComplete();
			}

		}) != null;
	}

	/**
	 * Downloads the game jar to the given target.
	 *
	 * @param path
	 *            The path to download to.
	 * @return <t>true</t> if the game was downloaded, otherwise <t>false</t>.
	 */
	public boolean download(Path path) {
		return download(path, null);
	}

	/**
	 * Downloads the game jar.
	 */
	public boolean download() {
		return download(Paths.get(Settings.TEMPORARY_DIRECTORY, "gamepack.jar"));
	}

	/**
	 * Downloads the game jar.
	 *
	 * @param callback
	 *            The callback to be run upon download percentage change.
	 * @return <t>true</t> if the game was downloaded, otherwise <t>false</t>.
	 */
	public boolean download(InternetCallback callback) {
		return download(Paths.get(Settings.TEMPORARY_DIRECTORY, "gamepack.jar"), callback);
	}

	/**
	 * Gets the default Applet dimensions based on its parameters.
	 *
	 * @return The default Applet dimensions based on its parameters.
	 */
	public Dimension appletSize() {
		try {
			return new Dimension(Integer.parseInt(parameters.get("applet_minwidth")),
					Integer.parseInt(parameters.get("applet_minheight")));
		} catch (NumberFormatException e) {
			System.out.println("appletSize");
			return null;
		}
	}

	
	private static final Pattern REVISION_PATTERN = new Pattern(
			PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).capture().b(),
			PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).capture().b(),
			PatternElement.b().opcodes(OpcodeGroups.PUSH_CONSTANT).capture().b());
	
	@SuppressWarnings({ "resource", "unchecked" })
	public int revision() {

		try {
			JarFile jar = new JarFile(Paths.get(Settings.TEMPORARY_DIRECTORY, "gamepack.jar").toFile());
			HashMap<String, ClassNode> classes = new HashMap<String, ClassNode>();

			jar.stream().forEach(entry -> {
				if (entry.getName().endsWith(".class")) {
					try {
						ClassReader reader = new ClassReader(jar.getInputStream(entry));
						ClassNode node = new ClassNode();
						reader.accept(node, ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES);
						classes.put(node.name, node);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});

			ClassNode client = classes.get(parameters.get("initial_class").replace(".class", ""));
			if (client != null) {
				for (MethodNode mn : (Iterable<MethodNode>) client.methods) {
					if (mn.name.equals("init") && mn.desc.equals("()V")) {
						List<PatternMatchResult> result = REVISION_PATTERN.match(mn);
						if (!result.isEmpty()) {
							return ((IntInsnNode) result.get(0).getCaptures().get(2)).operand;
						}	
					}
				}
			}

			return -1;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}
}