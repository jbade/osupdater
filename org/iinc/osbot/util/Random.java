package org.iinc.osbot.util;

public class Random {
	
	public static java.util.Random random = new java.util.Random(System.currentTimeMillis());

	public static int randomRange(int min, int max, java.util.Random random) {
		if (min >= max)
			return min;

		int range = (max - min) + 1;
		return (int) (random.nextDouble() * range) + min;
	}

	public static int randomRange(int min, int max) {
		if (min >= max)
			return min;

		int range = (max - min) + 1;
		return (int) (random.nextDouble() * range + min);
	}

	public static int modifiedRandomRange(int min, int max, double modifier) {
		int val = (min + max) / 4;
		if (val > min)
			val = min;
		return modifiedRandomRange(min, max, val, modifier);
	}

	/**
	 * Produces a random integer usually between min and max.
	 * 
	 * @param min
	 * @param max
	 * @param modifierRate
	 *            The value that the modifier is multiplied to before it is
	 *            added to the range.
	 * @param modifier
	 *            -1.0 to 1.0
	 * @return
	 */
	public static int modifiedRandomRange(int min, int max, int modifierRate, double modifier) {
		double offset = modifier * modifierRate;
		if (min >= max)
			return (int) (min + offset);

		int range = (max - min) + 1;
		return (int) (offset + random.nextDouble() * range + min);
	}

	public static int randomGuassianRange(int num) {
		return randomGuassianRange(-1 * num, num, ConfidenceIntervals.CI_90);
	}

	public static int randomGuassianRange(int min, int max, double confidenceLevel) {
		if (min >= max)
			return min;

		double mid = (min + max) / 2.0;
		return (int) (Math.floor(mid + ((max - min) / (confidenceLevel * 2) * random.nextGaussian()) + 0.5));
	}

	public static int modifiedRandomGuassianRange(int min, int max, double confidenceLevel, double modifier) {
		int val = (min + max) / 4;
		if (val > min)
			val = min;
		return modifiedRandomGuassianRange(min, max, confidenceLevel, modifier, val);
	}

	public static int modifiedRandomGuassianRange(int min, int max, double confidenceLevel, double modifierRate,
			double modifier) {
		if (min >= max)
			return min;

		double mid = (min + max) / 2.0;
		return (int) (Math.floor(
				modifierRate * modifier + mid + ((max - min) / (confidenceLevel * 2) * random.nextGaussian()) + 0.5));
	}

	public static boolean roll(double chance) {
		return random.nextDouble() < chance;
	}
}
