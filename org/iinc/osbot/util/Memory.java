package org.iinc.osbot.util;

import java.text.NumberFormat;
import java.util.logging.Level;

import org.iinc.osbot.bot.Bot;

public class Memory {

	public static void checkMemory(long threshold){
		Runtime runtime = Runtime.getRuntime();
		long totalFreeMemory = runtime.freeMemory() + runtime.maxMemory() - runtime.totalMemory();
		if (totalFreeMemory < threshold) {
			Bot.LOGGER.log(Level.INFO, "Running low on memory, killing bot");
			NumberFormat format = NumberFormat.getInstance();
			long maxMemory = runtime.maxMemory();
			long allocatedMemory = runtime.totalMemory();
			long freeMemory = runtime.freeMemory();
			Email.sendEmail(Email.TYPE.MEMORY_ALERT,
					String.format("Memory Stats\n\tfree memory: %s\n\tallocated memory: %s\n\tmax memory: %s\n\ttotal free memory: %s\n",
							format.format(freeMemory / 1024), format.format(allocatedMemory / 1024),
							format.format(maxMemory / 1024),
							format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024)));
			System.exit(0);
		}
	}
}
