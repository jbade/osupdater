package org.iinc.osbot.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map.Entry;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.ReplacedCanvas;
import org.iinc.osbot.hooks.Hooks;
import org.iinc.osbot.hooks.log.ClassInfo;
import org.iinc.osbot.hooks.log.FieldInfo;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;

public class Injector {
	private URL gamepack;
	private Class<?>[] accessors;
	private HashMap<String, ClassNode> classes;

	public Injector(URL fileURL) {
		this.gamepack = fileURL;
	}

	public void inject() {
		try {
			// doesnt work on linux for some reason
			/* 
			java.lang.NullPointerException
			at org.iinc.osbot.util.ClassFinder.find(ClassFinder.java:26)
			at org.iinc.osbot.util.Injector.inject(Injector.java:47)
			at org.iinc.osbot.AppHeadless.main(AppHeadless.java:186)*/
			// accessors = ClassFinder.find(IClient.class.getPackage().getName());
			accessors = new Class<?> [] {
				org.iinc.osbot.api.injection.interfaces.collection.IBuffer.class,
				org.iinc.osbot.api.injection.interfaces.collection.IDualNode.class,
				org.iinc.osbot.api.injection.interfaces.collection.IHashTable.class,
				org.iinc.osbot.api.injection.interfaces.collection.ILinkedList.class,
				org.iinc.osbot.api.injection.interfaces.collection.INode.class,
				org.iinc.osbot.api.injection.interfaces.IClient.class,
				org.iinc.osbot.api.injection.interfaces.IGrandExchangeOffer.class,
				org.iinc.osbot.api.injection.interfaces.IGroundItem.class,
				org.iinc.osbot.api.injection.interfaces.object.IBoundaryObject.class,
				org.iinc.osbot.api.injection.interfaces.object.IGameObject.class,
				org.iinc.osbot.api.injection.interfaces.object.IRegion.class,
				org.iinc.osbot.api.injection.interfaces.object.ISceneTile.class,
				org.iinc.osbot.api.injection.interfaces.renderable.ICharacter.class,
				org.iinc.osbot.api.injection.interfaces.renderable.INpc.class,
				org.iinc.osbot.api.injection.interfaces.renderable.INpcDefinition.class,
				org.iinc.osbot.api.injection.interfaces.renderable.IPlayer.class,
				org.iinc.osbot.api.injection.interfaces.renderable.IRenderable.class,
				org.iinc.osbot.api.injection.interfaces.widget.IWidget.class,
				org.iinc.osbot.api.injection.interfaces.widget.IWidgetNode.class
			};
			classes = loadGamepack();

			for (Entry<String, ClassInfo> ci : Hooks.getHooks().getClasses().entrySet()) {
				ClassNode cn = classes.get(ci.getValue().getName());

				// add accessor interface
				for (Class<?> c : accessors) {
					if (c.getSimpleName().equalsIgnoreCase("I" + ci.getKey())) {
						cn.interfaces.add(Type.getInternalName(c));
					}
				}

				// add field getters
				for (Entry<String, FieldInfo> fi : ci.getValue().getFields().entrySet()) {
					String name = getGetterMethodName(fi);
					String desc = getGetterMethodDesc(fi.getValue());
					// System.out.println(name + " " + desc);

					MethodNode mn = new MethodNode(Opcodes.ACC_PUBLIC, name, desc, null, null);
					mn.visitCode();
					mn.visitVarInsn(Opcodes.ALOAD, 0);
					mn.visitFieldInsn(Opcodes.GETFIELD, fi.getValue().getClassName(), fi.getValue().getName(),
							fi.getValue().getDesc());
					if (fi.getValue().getMultiplier() != null) {
						mn.visitLdcInsn(fi.getValue().getMultiplier());
						mn.visitInsn(Opcodes.IMUL);
					}
					if (!desc.substring(2).equalsIgnoreCase(fi.getValue().getDesc())) {
						mn.visitTypeInsn(Opcodes.CHECKCAST, Type.getReturnType(desc).getInternalName());
					}
					mn.visitInsn(Type.getReturnType(desc).getOpcode(Opcodes.IRETURN));
					mn.visitMaxs(0, 0);
					mn.visitEnd();

					cn.methods.add(mn);
				}
			}

			ClassNode cn = classes.get("client");
//			for (Class<?> c : accessors) {
//				if (c.getSimpleName().equalsIgnoreCase("IClient")) {
//					cn.interfaces.add(Type.getInternalName(c));
//				}
//			}

			// add static field getters
			for (Entry<String, FieldInfo> fi : Hooks.getHooks().getStatic().getFields().entrySet()) {
				String name = getGetterMethodName(fi);
				String desc = getGetterMethodDesc(fi.getValue());
				// System.out.println(name + " " + desc);

				MethodNode mn = new MethodNode(Opcodes.ACC_PUBLIC, name, desc, null, null);
				mn.visitCode();
				mn.visitFieldInsn(Opcodes.GETSTATIC, fi.getValue().getClassName(), fi.getValue().getName(),
						fi.getValue().getDesc());
				if (fi.getValue().getMultiplier() != null) {
					mn.visitLdcInsn(fi.getValue().getMultiplier());
					mn.visitInsn(Opcodes.IMUL);
				}

				if (!desc.substring(2).equalsIgnoreCase(fi.getValue().getDesc())) {
					mn.visitTypeInsn(Opcodes.CHECKCAST, Type.getReturnType(desc).getInternalName());
				}

				mn.visitInsn(Type.getReturnType(desc).getOpcode(Opcodes.IRETURN));
				mn.visitMaxs(0, 0);
				mn.visitEnd();

				cn.methods.add(mn);
			}

			replaceCanvas(classes);

			saveGamepack(classes);
		} catch (IOException e) {
			e.printStackTrace();
			Email.sendEmail(Email.TYPE.SEVERE, "Error injecting", e);
			Time.sleep(5000);
			System.exit(0);
		}
	}

	private void replaceCanvas(HashMap<String, ClassNode> classes) {
		ClassNode canvasClassNode = null;
		for (ClassNode cn : classes.values()) {
			if (cn.superName.equals("java/awt/Canvas")) {
				canvasClassNode = cn;
				break;
			}
		}

		canvasClassNode.superName = Type.getInternalName(ReplacedCanvas.class);

		// cheddy
		ListIterator<MethodNode> mnli = canvasClassNode.methods.listIterator();
		while (mnli.hasNext()) {
			MethodNode mn = mnli.next();
			if (mn.name.equals("<init>")) {
				ListIterator<AbstractInsnNode> ainli = mn.instructions.iterator();
				while (ainli.hasNext()) {
					AbstractInsnNode ain = ainli.next();
					if (ain.getOpcode() == Opcodes.INVOKESPECIAL) {
						MethodInsnNode min = (MethodInsnNode) ain;
						if (min.owner.equals("java/awt/Canvas") && min.name.equals("<init>")) {
							mn.instructions.set(min, new MethodInsnNode(Opcodes.INVOKESPECIAL,
									Type.getInternalName(ReplacedCanvas.class), "<init>", "()V", false));
							return;
						}
					}
				}
			}
		}
	}

	private String getGetterMethodDesc(FieldInfo fi) {
		String type = fi.getDesc();
		int array = type.length() - type.replace("[", "").length();
		if (type.contains("[")) {
			type = Type.getType(fi.getDesc()).getElementType().getClassName();
		} else {
			type = Type.getType(fi.getDesc()).getClassName();
		}

		// replace return type with out accessor
		for (Entry<String, ClassInfo> ci : Hooks.getHooks().getClasses().entrySet()) {
			if (ci.getValue().getName().equalsIgnoreCase(type)) {
				for (Class<?> c : accessors) {
					if (c.getSimpleName().equalsIgnoreCase("I" + ci.getKey())) {
						StringBuilder sb = new StringBuilder();
						sb.append("()");
						for (int i = 0; i < array; i++)
							sb.append("[");
						sb.append(Type.getType(c));
						return sb.toString();
					}
				}
			}
		}

		return "()" + fi.getDesc();
	}

	private String getGetterMethodName(Entry<String, FieldInfo> fi) {
		String prefix = "get";
		if (fi.getValue().getDesc().equalsIgnoreCase("Z")) {
			prefix = "is";
		}

		return prefix + fi.getKey().substring(0, 1).toUpperCase() + fi.getKey().substring(1);
	}

	public URL getFileURL() {
		try {
			return Paths.get(Settings.TEMPORARY_DIRECTORY, "injected.jar").toUri().toURL();
		} catch (MalformedURLException e) {
			return null;
		}
	}

	private HashMap<String, ClassNode> loadGamepack() throws IOException {
		HashMap<String, ClassNode> classes = new HashMap<String, ClassNode>();
		JarFile jar = new JarFile(gamepack.getFile());
		jar.stream().forEach(entry -> {
			if (entry.getName().endsWith(".class")) {
				try {
					ClassReader reader = new ClassReader(jar.getInputStream(entry));
					ClassNode node = new ClassNode();
					reader.accept(node, ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES);
					classes.put(node.name, node);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		jar.close();
		return classes;
	}

	private void saveGamepack(HashMap<String, ClassNode> classes) throws FileNotFoundException, IOException {
		JarOutputStream jos = new JarOutputStream(
				new FileOutputStream(Paths.get(Settings.TEMPORARY_DIRECTORY, "injected.jar").toFile()));
		classes.values().stream().forEach((cn) -> {
			ClassWriter cw = new ClassWriter(1);
			cn.accept(cw);
			JarEntry entry = new JarEntry(cn.name + ".class");
			try {
				jos.putNextEntry(entry);
				jos.write(cw.toByteArray());
				jos.closeEntry();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		jos.close();
	}
}
