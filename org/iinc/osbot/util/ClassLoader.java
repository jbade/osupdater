package org.iinc.osbot.util;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;

/**
 * Caching Class loader.
 */
public class ClassLoader {
	private HashMap<String, Class<?>> classes = new HashMap<String, Class<?>>();
	private URLClassLoader classLoader;

	public ClassLoader(URL url) {
		classLoader = new URLClassLoader(new URL[] { url });
	}

	public Class<?> loadClass(String name) throws ClassNotFoundException {
		if (classes.containsKey(name)) {
			return classes.get(name);
		}

		classes.put(name, classLoader.loadClass(name));
		return classes.get(name);
	}
}
