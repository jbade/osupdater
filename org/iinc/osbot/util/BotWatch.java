package org.iinc.osbot.util;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.Login;
import org.iinc.osbot.api.base.positioning.Tile;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.input.Mouse;

public class BotWatch {
	private static FileWriter bot, human;

	static {
		long time = System.currentTimeMillis();
		try {
			bot = new FileWriter(Paths.get(Settings.BOTWATCH_DIRECTORY + "/" + time + ".bot.csv").toString());
			human = new FileWriter(Paths.get(Settings.BOTWATCH_DIRECTORY + "/" + time + ".human.csv").toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void logMouseClick(boolean bot, MouseEvent e) {
		logMouseClick(bot, e.getPoint(), e.getButton());
	}

	public static void logMouseClick(boolean bot, Point p, int button) {
		if (!Settings.BOTWATCH_ENABLED)
			return;

		if (button == Mouse.LEFT_BUTTON) {
			String action = "", option = "";

			if (Login.isLoggedIn()) {
				// walking using minimap
				if (Calculations.distance(642, 84, (int) p.getX(), (int) p.getY()) <= 62){
					action = "Walk here";
				} else {
					if (Menu.isOpen()) {
						Point menu = Menu.getLocation();

						// within x bounds
						if (p.getX() >= menu.getX() && p.getY() <= menu.getX() + Menu.getSize().width) {

							int index = (int) ((p.getY() - menu.getY() - 19) / 15);
							int count = Client.getMenuCount();
							if (index >= 0 && index < count) {
								action = Client.getMenuActions()[count - index - 1].replaceAll("<col=[0-9a-z]{1,6}>", "")
										.replaceAll("<\\/col>", "").trim();
								option = Client.getMenuOptions()[count - index - 1].replaceAll("<col=[0-9a-z]{1,6}>", "")
										.replaceAll("<\\/col>", "").trim();
							}
						}
					} else {
						int count = Client.getMenuCount();
						action = Client.getMenuActions()[count - 1].replaceAll("<col=[0-9a-z]{1,6}>", "")
								.replaceAll("<\\/col>", "").trim();
						option = Client.getMenuOptions()[count - 1].replaceAll("<col=[0-9a-z]{1,6}>", "")
								.replaceAll("<\\/col>", "").trim();
					}
				}
			}

			int regionX = -1, regionY = -1, plane = -1;

			if (Login.isLoggedIn()) {
				Tile loc = Client.getLocalPlayer().getTile();
				regionX = loc.getX() / 64;
				regionY = loc.getY() / 64;
				plane = loc.getPlane();
			}

			long time = System.currentTimeMillis();

			System.out.println(String.format("%d %d,%d,%d %s %s", time, regionX, regionY, plane, action, option));
			if (bot) {
				try {
					BotWatch.bot.write(
							String.format("%d,%d,%d,%d,%s,%s\n", time, regionX, regionY, plane, action, option));
					BotWatch.bot.flush();
				} catch (IOException e) {
				}
			} else {
				try {
					BotWatch.human.write(
							String.format("%d,%d,%d,%d,%s,%s\n", time, regionX, regionY, plane, action, option));
					BotWatch.human.flush();
				} catch (IOException e) {
				}
			}

		}
	}
}
