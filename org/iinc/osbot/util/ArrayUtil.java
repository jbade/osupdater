package org.iinc.osbot.util;

import java.util.Arrays;

public class ArrayUtil {

	public static boolean contains(int[] stack, int needle) {
		if (stack == null)
			return false;

		for (int i = 0; i < stack.length; i++)
			if (stack[i] == needle)
				return true;

		return false;
	}

	public static boolean contains(Object[] stack, Object needle) {
		if (stack == null)
			return false;

		for (int i = 0; i < stack.length; i++)
			if (needle == null && stack[i] == null || stack[i] != null && stack[i].equals(needle))
				return true;

		return false;
	}

	// if needle starts with something in stack
	public static boolean startsWith(String needle, String[] stack) {
		if (stack == null)
			return false;

		for (int i = 0; i < stack.length; i++)
			if (needle == null && stack[i] == null || needle != null && needle.startsWith(stack[i]))
				return true;

		return false;
	}

	public static int sum(int[] arr) {
		int a = 0;
		for (int i = 0; i < arr.length; i++)
			a += arr[i];
		return a;
	}

	// http://stackoverflow.com/a/784842
	@SuppressWarnings("unchecked")
	public static <T> T[] concatAll(T[] first, T[]... rest) {
		int totalLength = first.length;
		for (T[] array : rest) {
			totalLength += array.length;
		}
		T[] result = Arrays.copyOf(first, totalLength);
		int offset = first.length;
		for (T[] array : rest) {
			System.arraycopy(array, 0, result, offset, array.length);
			offset += array.length;
		}
		return result;
	}
}
