package org.iinc.osbot.util;

import java.applet.Applet;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.base.worlds.World;
import org.iinc.osbot.api.base.worlds.Worlds;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.hooks.Hooks;
import org.iinc.osbot.util.internet.Internet;

import net.sargue.mailgun.Configuration;
import net.sargue.mailgun.Mail;
import net.sargue.mailgun.MailBuilder;

public class Email {
	private static final Configuration configuration = new Configuration()
			.domain("")
			.apiKey("")
			.from("osbot", "");

	private static final Destination[] DESTINATIONS = new Destination[] {
			new Destination("", TYPE.INFO, TYPE.SEVERE) };

	public enum TYPE {
		MEMORY_ALERT("Memory alert"), INFO("Info"), SEVERE("Severe");

		private String subject;

		TYPE(String subject) {
			this.subject = subject;
		}
	}

	private static class Destination {
		String email;
		TYPE[] type;

		public Destination(String email, TYPE... type) {
			this.email = email;
			this.type = type;
		}
	}

	public static void sendEmail(TYPE type, String body, Throwable exception) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		exception.printStackTrace(pw);
		String error = sw.toString();
		Bot.LOGGER.log(Level.INFO, "Sending email");
		sendEmail(type, body + "\n\n" + error);
		Bot.LOGGER.log(Level.INFO, "Done sending email");
	}

	public static void sendEmail(TYPE type, String body) {
		MailBuilder mb = Mail.using(configuration).subject(type.subject + " - " + Bot.username).text(Bot.username + " "
				+ Internet.read("https://api.ipify.org").stream().collect(Collectors.joining("")) + "\n\n" + body);
		for (Destination dest : DESTINATIONS) {
			if (ArrayUtil.contains(dest.type, type)) {
				mb = mb.to(dest.email);
			}
		}

		try {
			File outputfile = new File("image" + System.currentTimeMillis() + ".jpg");
			ImageIO.write(Bot.gameImage, "png", outputfile);
			mb.multipart().attachment(outputfile).build().send();
		} catch (Exception e) {
			e.printStackTrace();
			mb.build().send();
		}
	}

	public static void main(String[] args) {
		Hooks.loadFromFile("hooks.json");
		World world = Worlds.getRandomNormalWorld(true);
		System.out.println(world);
		Crawler crawler = new Crawler(world);
		crawler.crawl();
		if (crawler.outdated()) {
			crawler.download();
		}

		if (Hooks.getRevision() != crawler.revision()) {
			System.err.println("Hooks not updated");
			System.exit(1);
		}

		ClassLoader cl = new ClassLoader(crawler.getFileURL());
		Hooks.buildHooks(cl);

		Runtime.getRuntime().gc();
		System.out.println("ready");

		Applet game = crawler.start(cl);
		Bot.start(game);

		Time.sleep(5000);
		Email.sendEmail(Email.TYPE.SEVERE, "awefaweflst");
	}
}
