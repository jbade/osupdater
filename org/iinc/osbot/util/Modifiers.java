package org.iinc.osbot.util;

public class Modifiers {
	private static double[] modifiers = new double[10000];

	private static Thread updateThread;

	public static void init(int hash) {
		if (updateThread != null) {
			try {
				updateThread.interrupt();
				// updateThread.join();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		java.util.Random random = new java.util.Random(hash);

		for (int i = 0; i < modifiers.length; i++) {
			modifiers[i] = random.nextDouble() > 0.5 ? random.nextDouble() : -1 * random.nextDouble();
		}

		updateThread = new Thread(new Runnable() {

			@Override
			public void run() {
				java.util.Random random = new java.util.Random();

				while (!Thread.interrupted()) {
					for (int i = 0; i < modifiers.length; i++) {
						// .8 per hr = 4 * 60*60/30 * .5/x
						modifiers[i] += (random.nextDouble() > 0.4 ? 1 : -1) * 4 * random.nextDouble() / 300;

						if (modifiers[i] < -1) {
							modifiers[i] = -1;
						} else if (modifiers[i] > 1) {
							modifiers[i] = 1;
						}
					}

					try {
						Thread.sleep(30 * 1000);
					} catch (InterruptedException e) {
					}
				}
			}
		});
		updateThread.setDaemon(true);
		updateThread.start();
	}

	public static double get(String string) {
		return modifiers[Math.abs(string.hashCode()) % modifiers.length];
	}
}
