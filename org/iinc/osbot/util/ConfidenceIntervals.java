package org.iinc.osbot.util;

public class ConfidenceIntervals {
	public static final double CI_50 = .674;
	public static final double CI_60 = .841;
	public static final double CI_70 = 1.036;
	public static final double CI_80 = 1.282;
	public static final double CI_90 = 1.645;
	public static final double CI_95 = 1.96;
	public static final double CI_96 = 2.054;
	public static final double CI_98 = 2.326;
	public static final double CI_99 = 2.576;
	public static final double CI_99p5 = 2.807;
	public static final double CI_99p8 = 3.091;
	public static final double CI_99p9 = 3.291;
}
