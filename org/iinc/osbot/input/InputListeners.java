package org.iinc.osbot.input;

import java.applet.Applet;
import java.awt.Canvas;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.SwingUtilities;

import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.util.BotWatch;

/**
 * Created by IntelliJ IDEA. User: Jan Ove / Kosaki Date: 04.apr.2009 Time: 13:56:54
 */
public class InputListeners
		implements MouseListener, MouseMotionListener, MouseWheelListener, KeyListener, FocusListener {
	private static boolean inputEnabled = false;
	private Applet applet;

	public InputListeners(Applet applet) {
		this.applet = applet;
	}

	public static void enableInput() {
		inputEnabled = true;
	}

	public static void disableInput() {
		inputEnabled = false;
	}

	public static boolean isInputEnabled() {
		return inputEnabled;
	}

	public void mouseClicked(MouseEvent e) {
		if (inputEnabled) {
			e.setSource(applet);
			Client.getCanvas().dispatchEvent(e);
		} else {
			new Thread(() -> Mouse.click(e.getPoint(), e.getButton())).start();
		}
	}

	public void mousePressed(MouseEvent e) {
		if (inputEnabled) {
			BotWatch.logMouseClick(false, e);
			e.setSource(applet);
			Canvas canvas = Client.getCanvas();
			if (!canvas.hasFocus())
				canvas.requestFocus();
			canvas.dispatchEvent(e);
		}
	}

	public void mouseReleased(MouseEvent e) {
		if (inputEnabled) {
			e.setSource(applet);
			Client.getCanvas().dispatchEvent(e);
		}
	}

	public void mouseEntered(MouseEvent e) {
		if (inputEnabled) {
			e.setSource(applet);
			Client.getCanvas().dispatchEvent(e);
		}
	}

	public void mouseExited(MouseEvent e) {
		if (inputEnabled) {
			e.setSource(applet);
			Client.getCanvas().dispatchEvent(e);
		}
	}

	public void mouseDragged(MouseEvent e) {
		if (inputEnabled) {
			e.setSource(applet);
			Client.getCanvas().dispatchEvent(e);
			Mouse.cx = e.getX();
			Mouse.cy = e.getY();
		}
	}

	public void mouseMoved(MouseEvent e) {
		if (inputEnabled) {
			e.setSource(applet);
			Client.getCanvas().dispatchEvent(e);
			Mouse.cx = e.getX();
			Mouse.cy = e.getY();
		}
	}

	public void mouseWheelMoved(MouseWheelEvent e) {
		if (inputEnabled) {
			e.setSource(applet);
			Client.getCanvas().dispatchEvent(e);
		}
	}

	public void keyTyped(KeyEvent e) {
		e.setSource(applet);
		Client.getCanvas().dispatchEvent(e);
	}

	public void keyPressed(KeyEvent e) {
		e.setSource(applet);
		Client.getCanvas().dispatchEvent(e);
	}

	public void keyReleased(KeyEvent e) {
		e.setSource(applet);
		Client.getCanvas().dispatchEvent(e);
	}

	public void focusGained(FocusEvent e) {
		if (inputEnabled) {
			e.setSource(applet);
			Client.getCanvas().dispatchEvent(e);
		}
	}

	public void focusLost(FocusEvent e) {
		if (inputEnabled) {
			e.setSource(applet);
			Client.getCanvas().dispatchEvent(e);
		}
	}

}
