package org.iinc.osbot.input;

import java.awt.Point;
import java.awt.Rectangle;

import org.iinc.osbot.api.base.Calculations;
import org.iinc.osbot.api.base.Clickable;
import org.iinc.osbot.api.base.Menu;
import org.iinc.osbot.api.base.interfaces.tabs.Inventory;
import org.iinc.osbot.api.base.item.Item;
import org.iinc.osbot.api.injection.accessors.widget.Widget;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.util.Random;

public class Clicking {

	public static boolean hover(Rectangle rec) {
		if (rec == null)
			return false;

		Bot.mouseLock.lock();
		try {
			Menu.close();

			int stops = Random.randomRange(0, 1);

			for (int i = 0; i < stops; i++)
				Mouse.moveGeneral(rec);

			if (!rec.contains(Mouse.getLocation()))
				Mouse.move(rec);

			return rec.contains(Mouse.getLocation());
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static boolean hover(Widget widget) {
		if (widget == null || widget.isHidden())
			return false;

		return hover(widget.toScreen());
	}

	private static boolean interactRectangle(Rectangle rec, String[] actions, String[] options) {
		if (rec == null)
			return false;

		Bot.mouseLock.lock();
		try {
			Menu.close();

			if (!rec.contains(Mouse.getLocation()) || !Menu.contains(actions, options)) {
				int stops = Random.randomRange(0, 1);

				for (int i = 0; i < stops; i++) {
					Mouse.moveGeneral(rec);
				}

				Mouse.move(rec);
				Mouse.sleepBeforeClick();
			}
			
			return Menu.interact(actions, options);
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static boolean interactItem(Item item, String[] actions, String[] options) {
		if (item == null || !Inventory.open())
			return false;

		return interactRectangle(Inventory.getBounds(item.getIndex()), actions, options);
	}

	private static boolean interactWidget(Widget widget, String[] actions, String[] options) {
		if (widget == null)
			return false;

		return interactRectangle(widget.toScreen(), actions, options);
	}

	private static boolean interactClickable(Clickable entity, String[] actions, String[] options) {
		if (entity == null)
			return false;

		Bot.mouseLock.lock();
		try {
			Menu.close();

			Point p = entity.toScreen();
			if (!Calculations.onViewport(p)) {
				return false;
			}

			int stops = Random.randomRange(0, 1);

			for (int i = 0; i < stops; i++) {
				Mouse.moveGeneral(p);
			}

			p = entity.toScreen();

			Mouse.move(p, 4);
			Mouse.sleepBeforeClick();
			return Menu.interact(actions, options);
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static boolean interact(Widget widget) {
		return interactWidget(widget, null, null);
	}

	public static boolean interact(Widget widget, String action) {
		return interactWidget(widget, action == null ? null : new String[] { action }, null);
	}

	public static boolean interact(Widget widget, String action, String option) {
		return interactWidget(widget, action == null ? null : new String[] { action },
				option == null ? null : new String[] { option });
	}

	public static boolean interact(Widget widget, String[] actions, String[] options) {
		return interactWidget(widget, actions == null ? null : actions, options == null ? null : options);
	}

	public static boolean interact(Item item) {
		return interactItem(item, null, null);
	}

	public static boolean interact(Item item, String action) {
		return interactItem(item, action == null ? null : new String[] { action }, null);
	}

	public static boolean interact(Item item, String action, String option) {
		return interactItem(item, action == null ? null : new String[] { action },
				option == null ? null : new String[] { option });
	}

	public static boolean interact(Item item, String[] actions, String[] options) {
		return interactItem(item, actions == null ? null : actions, options == null ? null : options);
	}

	public static boolean interact(Clickable entity) {
		return interactClickable(entity, null, null);
	}

	public static boolean interact(Clickable entity, String action) {
		return interactClickable(entity, action == null ? null : new String[] { action }, null);
	}

	public static boolean interact(Clickable entity, String action, String option) {
		return interactClickable(entity, action == null ? null : new String[] { action },
				option == null ? null : new String[] { option });
	}

	public static boolean interact(Clickable entity, String[] actions, String[] options) {
		return interactClickable(entity, actions == null ? null : actions, options == null ? null : options);
	}

}
