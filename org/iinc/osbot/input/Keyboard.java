package org.iinc.osbot.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;
import java.util.Set;

import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

// https://github.com/BenLand100/SMART
public class Keyboard {
	static Set<int[]> keysHeld = new HashSet<int[]>();
	static boolean shiftDown = false; // TODO shift modifies mouse btnMask while scrolling

	/**
	 * Converts a char into a KeyCode value for KeyEvent
	 * 
	 * @param c
	 *            Char to convert
	 * @result c's KeyCode
	 */
	public static int toKeyCode(char c) {
		final String special = "~!@#$%^&*()_+|{}:\"<>?";
		final String normal = "`1234567890-=\\[];',./";
		int index = special.indexOf(c);
		return Character.toUpperCase(index == -1 ? c : normal.charAt(index));
	}

	/**
	 * Tests if a character requires the shift key to be pressed.
	 * 
	 * @param c
	 *            Char to check for
	 * @result True if shift is required
	 */
	private static boolean isShiftChar(char c) {
		String special = "~!@#$%^&*()_+|{}:\"<>?";
		return special.indexOf(c) != -1 || (c - 'A' >= 0 && c - 'A' <= 25);
	}

	/**
	 * Converts a vk code into a char
	 * 
	 * @param code
	 *            KeyCode to convert
	 * @result the char
	 */
	private static char toChar(int vk, boolean shift) {
		int code = typable_vk_keycode[vk];
		final String special = "~!@#$%^&*()_+|{}:\"<>?";
		final String normal = "`1234567890-=\\[];',./";
		int index = normal.indexOf((char) code);
		if (index == -1) {
			return shift ? Character.toUpperCase((char) code) : Character.toLowerCase((char) code);
		} else {
			return shift ? special.charAt(index) : (char) code;
		}
	}

	/**
	 * Returns true if the vk code is typable
	 */
	private static boolean isTypableCode(int vk) {
		return vk < 0xff && typable_vk_keycode[vk] != 0;
	}

	public static int[] typable_vk_keycode = new int[0xff];
	static {
		typable_vk_keycode[32] = 32;
		for (int c = (int) 'A'; c <= (int) 'Z'; c++)
			typable_vk_keycode[c] = c;
		for (int c = (int) '0'; c <= (int) '9'; c++)
			typable_vk_keycode[c] = c;
		typable_vk_keycode[186] = ';'; // ;:
		typable_vk_keycode[187] = '='; // =+
		typable_vk_keycode[188] = ','; // hack: ,
		typable_vk_keycode[189] = '-'; // -_
		typable_vk_keycode[190] = '.'; // .>
		typable_vk_keycode[191] = '/'; // /?
		typable_vk_keycode[192] = '`'; // `~
		typable_vk_keycode[219] = '['; // [{
		typable_vk_keycode[220] = '\\';// \|
		typable_vk_keycode[221] = ']'; // ]}
		typable_vk_keycode[222] = '\'';// '"
		typable_vk_keycode[226] = ','; // hack: <
	}

	public static boolean isKeyDown(int code) {
		int[] dat = new int[] { code };
		return isKeyHeld(dat);
	}

	/**
	 * Holds a key. Should be used for any key that needs to be held, not sending text.
	 * 
	 * @param code
	 *            KeyCode for the key
	 */
	public static void holdKey(int code) {
		Bot.keyboardLock.lock();
		try {
			long startTime = System.currentTimeMillis();
			int[] dat = new int[] { code, (int) (startTime & 0xFFFFFFFF) };
			if (!isKeyHeld(dat)) {
				if (KeyEvent.VK_SHIFT == code)
					shiftDown = true;
				dispatchEvent(new KeyEvent(Bot.applet, KeyEvent.KEY_PRESSED, startTime,
						shiftDown ? KeyEvent.SHIFT_DOWN_MASK : 0, code, KeyEvent.CHAR_UNDEFINED,
						KeyEvent.KEY_LOCATION_STANDARD));
				if (isTypableCode(code)) {
					// System.out.println("Trying to type " + code + " as '" +
					// toChar(code,shiftDown) +
					// "'");
					dispatchEvent(new KeyEvent(Bot.applet, KeyEvent.KEY_TYPED, startTime,
							shiftDown ? KeyEvent.SHIFT_DOWN_MASK : 0, 0, toChar(code, shiftDown),
							KeyEvent.KEY_LOCATION_UNKNOWN));
				}
				setKeyHeld(dat, true);
			}
		} finally {
			Bot.keyboardLock.unlock();
		}
	}

	/**
	 * Release a key. Should be used for any key that needs to be held, not sending text. Will only
	 * release it if its already held.
	 * 
	 * @param code
	 *            KeyCode for the key
	 */
	public static void releaseKey(int code) {
		Bot.keyboardLock.lock();
		try {
			long startTime = System.currentTimeMillis();
			int[] dat = new int[] { code };
			if (isKeyHeld(dat)) {
				setKeyHeld(dat, false);
				dispatchEvent(new KeyEvent(Bot.applet, KeyEvent.KEY_RELEASED, startTime, 0, code, KeyEvent.CHAR_UNDEFINED,
						KeyEvent.KEY_LOCATION_STANDARD));
				if (KeyEvent.VK_SHIFT == code)
					shiftDown = false;
			}
		} finally {
			Bot.keyboardLock.unlock();
		}
	}

	private static void setKeyHeld(int[] dat, boolean held) {
		Bot.keyboardLock.lock();
		try {
			if (held) {
				keysHeld.add(dat);
			} else {
				HashSet<int[]> remove = new HashSet<int[]>();
				for (int[] entry : keysHeld) {
					if (entry[0] == dat[0]) {
						remove.add(entry);
					}
				}
				keysHeld.removeAll(remove);
			}
		} finally {
			Bot.keyboardLock.unlock();
		}
	}

	private static boolean isKeyHeld(int[] dat) {
		Bot.keyboardLock.lock();
		try {
			for (int[] entry : keysHeld) {
				if (entry[0] == dat[0]) {
					return true;
				}
			}
			return false;
		} finally {
			Bot.keyboardLock.unlock();
		}
	}

	public static void sendKey(int code) {
		Bot.keyboardLock.lock();
		try {
			Keyboard.holdKey(code);
			Time.sleep(100, 300);
			Keyboard.releaseKey(code);
		} finally {
			Bot.keyboardLock.unlock();
		}
	}

	public static void sendKeys(String text) {
		sendKeys(text,
				Random.modifiedRandomGuassianRange(50, 70, ConfidenceIntervals.CI_80, 10, Modifiers.get("keyboardKeywait")),
				Random.modifiedRandomGuassianRange(20, 30, ConfidenceIntervals.CI_80, 5,
						Modifiers.get("keyboardKeymodwait")));
	}

	public static void sendKeys(String text, int keywait, int keymodwait) {
		Bot.keyboardLock.lock();
		try {
			char[] chars = text.toCharArray();

			for (char c : chars) {
				int code = toKeyCode(c);
				int keyLoc = Character.isDigit(c)
						? Math.random() > 0.5D ? KeyEvent.KEY_LOCATION_NUMPAD : KeyEvent.KEY_LOCATION_STANDARD
						: KeyEvent.KEY_LOCATION_STANDARD;
				if (isShiftChar(c)) {
					int shiftLoc = Math.random() > 0.5D ? KeyEvent.KEY_LOCATION_RIGHT : KeyEvent.KEY_LOCATION_LEFT;
					dispatchEvent(new KeyEvent(Bot.applet, KeyEvent.KEY_PRESSED, System.currentTimeMillis(),
							KeyEvent.SHIFT_MASK, KeyEvent.VK_SHIFT, KeyEvent.CHAR_UNDEFINED, shiftLoc));

					Time.sleep((int) ((Math.random() * 0.1 + 1) * keymodwait));

					long time = System.currentTimeMillis();
					dispatchEvent(
							new KeyEvent(Bot.applet, KeyEvent.KEY_PRESSED, time, KeyEvent.SHIFT_MASK, code, c, keyLoc));
					dispatchEvent(new KeyEvent(Bot.applet, KeyEvent.KEY_TYPED, time, KeyEvent.SHIFT_MASK, 0, c,
							KeyEvent.KEY_LOCATION_UNKNOWN));

					Time.sleep((int) ((Math.random() * 0.1 + 1) * keywait));

					dispatchEvent(new KeyEvent(Bot.applet, KeyEvent.KEY_RELEASED, System.currentTimeMillis(),
							KeyEvent.SHIFT_MASK, code, c, keyLoc));
					Time.sleep((int) ((Math.random() * 0.1 + 1) * keymodwait));

					Client.getCanvas().dispatchEvent(new KeyEvent(Bot.applet, KeyEvent.KEY_RELEASED,
							System.currentTimeMillis(), 0, KeyEvent.VK_SHIFT, KeyEvent.CHAR_UNDEFINED, shiftLoc));
				} else {
					long time = System.currentTimeMillis();
					dispatchEvent(new KeyEvent(Bot.applet, KeyEvent.KEY_PRESSED, time, 0, code, c, keyLoc));
					dispatchEvent(
							new KeyEvent(Bot.applet, KeyEvent.KEY_TYPED, time, 0, 0, c, KeyEvent.KEY_LOCATION_UNKNOWN));

					Time.sleep((int) ((Math.random() * 0.1 + 1) * keywait));

					dispatchEvent(new KeyEvent(Bot.applet, KeyEvent.KEY_RELEASED, System.currentTimeMillis(), 0, code, c,
							keyLoc));
				}
			}
		} finally {
			Bot.keyboardLock.unlock();
		}
	}

	private static void dispatchEvent(KeyEvent event) {
		Bot.keyboardLock.lock();
		try {
			KeyListener[] listeners = Client.getCanvas().getKeyListeners();

			switch (event.getID()) {
			case KeyEvent.KEY_PRESSED:
				for (KeyListener kl : listeners)
					kl.keyPressed(event);
				break;
			case KeyEvent.KEY_RELEASED:
				for (KeyListener kl : listeners)
					kl.keyReleased(event);
				break;
			case KeyEvent.KEY_TYPED:
				for (KeyListener kl : listeners)
					kl.keyTyped(event);
				break;
			}
		} finally {
			Bot.keyboardLock.unlock();
		}
	}

}
