package org.iinc.osbot.input;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import org.iinc.osbot.Settings;
import org.iinc.osbot.api.base.timing.Time;
import org.iinc.osbot.api.injection.accessors.Client;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.script.PaintListener;
import org.iinc.osbot.script.ScriptHandler;
import org.iinc.osbot.util.BotWatch;
import org.iinc.osbot.util.ConfidenceIntervals;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.Random;

// https://github.com/BenLand100/SMART
public class Mouse implements PaintListener {
	public static final int LEFT_BUTTON = 1;
	public static final int MIDDLE_BUTTON = 2;
	public static final int RIGHT_BUTTON = 3;

	static int cx, cy;
	private static boolean mousein, leftDown, midDown, rightDown, focused;

	static {
		ScriptHandler.painters.add(new Mouse()); // TODO
	}

	public static void wait(int min, int max) {
		Bot.mouseLock.lock();
		try {
			int time = min == max ? min : (int) ((Math.random() * Math.abs(max - min)) + Math.min(min, max));
			try {
				Thread.sleep(time);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static void wait(int mills) {
		wait(mills, mills);
	}

	public Point getMousePos() {
		Bot.mouseLock.lock();
		try {
			return new Point(cx, cy);
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	private static Point moveMouseImpl(int x, int y) {
		Bot.mouseLock.lock();
		try {
			int btnMask = (leftDown ? MouseEvent.BUTTON1_DOWN_MASK : 0)
					| (rightDown ? (MouseEvent.BUTTON3_DOWN_MASK | MouseEvent.META_DOWN_MASK) : 0);
			if (isDragging()) {
				Client.getCanvas().dispatchEvent(new MouseEvent(Bot.applet, MouseEvent.MOUSE_DRAGGED,
						System.currentTimeMillis(), btnMask, x, y, 0, false, 0));
			} else if (x >= 0 && x < Bot.applet.getWidth() && y >= 0 && y < Bot.applet.getHeight()) {
				if (mousein) {
					Client.getCanvas().dispatchEvent(new MouseEvent(Bot.applet, MouseEvent.MOUSE_MOVED,
							System.currentTimeMillis(), btnMask, x, y, 0, false, 0));
				} else {
					mousein = true;
					Client.getCanvas().dispatchEvent(new MouseEvent(Bot.applet, MouseEvent.MOUSE_ENTERED,
							System.currentTimeMillis(), btnMask, x, y, 0, false, 0));
				}
			} else {
				if (mousein) {
					Client.getCanvas().dispatchEvent(new MouseEvent(Bot.applet, MouseEvent.MOUSE_EXITED,
							System.currentTimeMillis(), btnMask, x, y, 0, false, 0));
					mousein = false;
				} else {
					// Mouse outside and still out, no event.
				}
			}
			cx = x;
			cy = y;
			return new Point(cx, cy);
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	/**
	 * Sets the location of the mouse
	 * 
	 * @param x
	 *            The x destination
	 * @param y
	 *            The y destination
	 * @result The actual end point
	 */
	public static Point setLocation(int x, int y) {
		return moveMouseImpl(x, y);
	}

	/**
	 * Holds the mouse at the specified position after moving from the current position to the
	 * specified position.
	 * 
	 * @param x
	 *            The x destination
	 * @param y
	 *            The y destination
	 * @result The actual end point
	 */
	public static Point holdMouse(int x, int y, int button) {
		Bot.mouseLock.lock();
		try {
			if (canHold(button)) {
				int btnMask = ((leftDown || button == 1) ? MouseEvent.BUTTON1_DOWN_MASK : 0)
						| ((midDown || button == 2) ? (MouseEvent.BUTTON2_DOWN_MASK | MouseEvent.META_DOWN_MASK) : 0)
						| ((rightDown || button == 3) ? (MouseEvent.BUTTON3_DOWN_MASK | MouseEvent.META_DOWN_MASK) : 0);
				int btn = 0;
				switch (button) {
				case 1:
					btn = MouseEvent.BUTTON1;
					break;
				case 2:
					btn = MouseEvent.BUTTON2;
					break;
				case 3:
					btn = MouseEvent.BUTTON3;
					break;
				}
				Point end = setLocation(x, y);
				if (mousein) {
					Client.getCanvas().dispatchEvent(new MouseEvent(Bot.applet, MouseEvent.MOUSE_PRESSED,
							System.currentTimeMillis(), btnMask, cx, cy, 1, false, btn));
					if (!focused) {
						wait(25, 50);
						getFocus();
					}
					switch (button) {
					case 1:
						leftDown = true;
						break;
					case 2:
						midDown = true;
						break;
					case 3:
						rightDown = true;
						break;
					}
				}
				return end;
			}
			return null;
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	/**
	 * Releases the mouse at the specified position after moving from the current position to the
	 * specified position.
	 * 
	 * @param x
	 *            The x destination
	 * @param y
	 *            The y destination
	 * @result The actual end point
	 */
	public static Point releaseMouse(int x, int y, int button) {
		Bot.mouseLock.lock();
		try {
			if (canRelease(button)) {
				int btnMask = ((leftDown || button == 1) ? MouseEvent.BUTTON1_DOWN_MASK : 0)
						| ((midDown || button == 2) ? (MouseEvent.BUTTON2_DOWN_MASK | MouseEvent.META_DOWN_MASK) : 0)
						| ((rightDown || button == 3) ? (MouseEvent.BUTTON3_DOWN_MASK | MouseEvent.META_DOWN_MASK) : 0);
				int btn = 0;
				switch (button) {
				case 1:
					btn = MouseEvent.BUTTON1;
					break;
				case 2:
					btn = MouseEvent.BUTTON2;
					break;
				case 3:
					btn = MouseEvent.BUTTON3;
					break;
				}
				Point end = setLocation(x, y);
				if (mousein) {
					long time = System.currentTimeMillis();
					Client.getCanvas().dispatchEvent(new MouseEvent(Bot.applet, MouseEvent.MOUSE_RELEASED, time,
							btnMask, end.x, end.y, 1, false, btn));
					Client.getCanvas().dispatchEvent(new MouseEvent(Bot.applet, MouseEvent.MOUSE_CLICKED, time, btnMask,
							end.x, end.y, 1, false, btn));
					switch (button) {
					case 1:
						leftDown = false;
						break;
					case 2:
						midDown = false;
						break;
					case 3:
						rightDown = false;
						break;
					}
				} else {
					loseFocus(false);
				}
				return end;
			}
			return null;
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	/**
	 * Clicks the mouse at the specified position after moving from the current position to the
	 * specified position.
	 * 
	 * @param x
	 *            The x destination
	 * @param y
	 *            The y destination
	 * @result The actual end point
	 */
	public static Point clickMouse(int x, int y, int button) {
		Bot.mouseLock.lock();
		try {
			if (canClick(button)) {
				int btnMask = ((leftDown || button == 1) ? MouseEvent.BUTTON1_DOWN_MASK : 0)
						| ((midDown || button == 2) ? (MouseEvent.BUTTON2_DOWN_MASK | MouseEvent.META_DOWN_MASK) : 0)
						| ((rightDown || button == 3) ? (MouseEvent.BUTTON3_DOWN_MASK | MouseEvent.META_DOWN_MASK) : 0);
				int btn = 0;
				switch (button) {
				case 1:
					btn = MouseEvent.BUTTON1;
					break;
				case 2:
					btn = MouseEvent.BUTTON2;
					break;
				case 3:
					btn = MouseEvent.BUTTON3;
					break;
				}
				Point end = setLocation(x, y);
				if (mousein) {
					MouseEvent e = new MouseEvent(Bot.applet, MouseEvent.MOUSE_PRESSED, System.currentTimeMillis(),
							btnMask, cx, cy, 1, false, btn);
					BotWatch.logMouseClick(true, e);
					Client.getCanvas().dispatchEvent(e);
					switch (button) {
					case 1:
						leftDown = true;
						break;
					case 2:
						midDown = true;
						break;
					case 3:
						rightDown = true;
						break;
					}
					if (!focused) {
						wait(25, 50);
						getFocus();
					}

					Time.sleep(Random.modifiedRandomGuassianRange(20, 40, ConfidenceIntervals.CI_60, 10,
							Modifiers.get("mouseClickTime")));

					// drag mouse
					if (Random.modifiedRandomRange(0, 100, 15, Modifiers.get("mouseAccidentalDrag")) < 25) {
						Mouse.move(Mouse.getLocation(), Random.randomGuassianRange(0, 3, ConfidenceIntervals.CI_60));
					}

					Time.sleep(Random.modifiedRandomGuassianRange(30, 55, ConfidenceIntervals.CI_60, 20,
							Modifiers.get("mouseClickTime")));

					long time = System.currentTimeMillis();
					Canvas canvas = Client.getCanvas();
					canvas.dispatchEvent(new MouseEvent(Bot.applet, MouseEvent.MOUSE_RELEASED, time, btnMask, end.x,
							end.y, 1, false, btn));
					canvas.dispatchEvent(new MouseEvent(Bot.applet, MouseEvent.MOUSE_CLICKED, time, btnMask, end.x,
							end.y, 1, false, btn));
					switch (button) {
					case 1:
						leftDown = false;
						break;
					case 2:
						midDown = false;
						break;
					case 3:
						rightDown = false;
						break;
					}
				} else {
					loseFocus(false);
				}

				if (Settings.MAKE_CLICKING_ERRORS
						&& Random.modifiedRandomRange(0, 100, 5, Modifiers.get("mouseAccidentalClick")) > 98)
					clickMouse(x, y, button);

				return end;
			}
			return null;
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static boolean isMouseButtonHeld(int button) {
		Bot.mouseLock.lock();
		try {
			switch (button) {
			case 1:
				return leftDown;
			case 2:
				return midDown;
			case 3:
				return rightDown;
			}
			return false;
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static Point scrollMouse(int x, int y, int lines) {
		Bot.mouseLock.lock();
		try {
			int btnMask = (isDown(KeyEvent.VK_SHIFT) ? KeyEvent.SHIFT_MASK : 0)
					| (isDown(KeyEvent.VK_ALT) ? KeyEvent.ALT_MASK : 0)
					| (isDown(KeyEvent.VK_CONTROL) ? KeyEvent.CTRL_MASK : 0);
			Point end = setLocation(x, y);
			if (mousein) {
				Client.getCanvas()
						.dispatchEvent(new MouseWheelEvent(Bot.applet, MouseEvent.MOUSE_WHEEL,
								System.currentTimeMillis(), btnMask, x, y, 0, false, MouseWheelEvent.WHEEL_UNIT_SCROLL,
								Math.abs(lines), lines < 0 ? -1 : 1));
				return new Point(x, y);
			}
			return end;
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	/**
	 * Sends an event that emulates a user alt+tabbing into RuneScape.
	 */

	private static void getFocus() {
		Bot.mouseLock.lock();
		try {
			if (!focused) {
				Client.getCanvas().dispatchEvent(new FocusEvent(Bot.applet, FocusEvent.FOCUS_GAINED, false, null));
				focused = true;
				wait(100, 200);
			}
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	/**
	 * Sends an event that emulates a user alt+tabbing out of RuneScape. tabbed = true will send a
	 * tab key, so avoid that.
	 */
	public static void loseFocus(boolean tabbed) {
		Bot.mouseLock.lock();
		try {
			if (focused) {
				if (tabbed) {
					Client.getCanvas()
							.dispatchEvent(new KeyEvent(Bot.applet, KeyEvent.KEY_PRESSED, System.currentTimeMillis(),
									KeyEvent.ALT_DOWN_MASK, KeyEvent.VK_ALT, KeyEvent.CHAR_UNDEFINED,
									KeyEvent.KEY_LOCATION_LEFT));
					wait(100, 200);
					Client.getCanvas()
							.dispatchEvent(new KeyEvent(Bot.applet, KeyEvent.KEY_PRESSED, System.currentTimeMillis(),
									KeyEvent.ALT_DOWN_MASK, KeyEvent.VK_TAB, KeyEvent.CHAR_UNDEFINED,
									KeyEvent.KEY_LOCATION_STANDARD));
					wait(10, 50);
				}
				Client.getCanvas().dispatchEvent(new FocusEvent(Bot.applet, FocusEvent.FOCUS_LOST, false, null));
				focused = false;
				wait(100, 200);
			}
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static boolean isFocused() {
		Bot.mouseLock.lock();
		try {
			return focused;
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static boolean isDragging() {
		Bot.mouseLock.lock();
		try {
			return leftDown || midDown || rightDown;
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static boolean isDown(int button) {
		Bot.mouseLock.lock();
		try {
			switch (button) {
			case 1:
				return leftDown;
			case 2:
				return midDown;
			case 3:
				return rightDown;
			case KeyEvent.VK_SHIFT:
				return Keyboard.shiftDown;
			case KeyEvent.VK_ALT:
				return false;
			case KeyEvent.VK_CONTROL:
				return false;
			}
			return false;
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static boolean canClick(int button) {
		Bot.mouseLock.lock();
		try {
			switch (button) {
			case 1:
				return !leftDown;
			case 2:
				return !midDown;
			case 3:
				return !rightDown;
			}
			return false;
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static boolean canHold(int button) {
		Bot.mouseLock.lock();
		try {
			switch (button) {
			case 1:
				return !leftDown;
			case 2:
				return !midDown;
			case 3:
				return !rightDown;
			}
			return false;
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static boolean canRelease(int button) {
		Bot.mouseLock.lock();
		try {
			switch (button) {
			case 1:
				return leftDown;
			case 2:
				return midDown;
			case 3:
				return rightDown;
			}
			return false;
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static Point getLocation() {
		Bot.mouseLock.lock();
		try {
			return new Point(cx, cy);
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static void moveGeneral(Rectangle r) {
		int x = r.x + (int) (r.width / 2.0);
		int y = r.y + (int) (r.height / 2.0);

		Mouse.moveGeneral(new Point(x, y));
	}

	public static void moveGeneral(Point p) {
		Bot.mouseLock.lock();
		try {
			Point start = Mouse.getLocation();
			int distx = Math.abs(start.x - p.x);
			int disty = Math.abs(start.y - p.y);

			int randx = (int) (distx / 3);
			int randy = (int) (disty / 3);

			MouseMovementHandler.moveMouse(p, randx, randy);
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static void move(Point p) {
		MouseMovementHandler.moveMouse(p, 0, 0);
	}

	public static void move(Point p, int rand) {
		MouseMovementHandler.moveMouse(p, rand, rand);
	}

	public static void click(Rectangle rect, int button) {
		Bot.mouseLock.lock();
		try {
			Mouse.move(rect);
			Mouse.sleepBeforeClick();
			Mouse.click(button);
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static void click(Point p, int button) {
		Bot.mouseLock.lock();
		try {
			Mouse.move(p);
			Mouse.sleepBeforeClick();
			Mouse.clickMouse(cx, cy, button);
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static void click(int button) {
		Mouse.clickMouse(cx, cy, button);
	}

	public static void move(java.awt.Rectangle rectangle) {
		move(rectangle.x, rectangle.y, rectangle.x + rectangle.width - 1, rectangle.y + rectangle.height - 1);
	}

	public static void move(int x1, int y1, int x2, int y2) {
		MouseMovementHandler.moveMouse((x1 + x2) / 2, (y1 + y2) / 2, (x2 - x1) / 2, (y2 - y1) / 2);
	}

	public static void moveMouse(Point p, int randx, int randy) {
		MouseMovementHandler.moveMouse(p, randx, randy);
	}

	public static void click(int x1, int y1, int x2, int y2, int button) {
		Bot.mouseLock.lock();
		try {
			Mouse.move(x1, y1, x2, y2);
			Mouse.sleepBeforeClick();
			Mouse.clickMouse(cx, cy, button);
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	public static void sleepBeforeClick() {
		Time.sleep(Random.modifiedRandomGuassianRange(20, 60, ConfidenceIntervals.CI_70, 40,
				Modifiers.get("mouseSleepBeforeClickTime")));
	}

	public static void moveOffscreen() {
		Bot.mouseLock.lock();
		try {
			Rectangle bounds = Client.getCanvas().getBounds();

			int x = Random.modifiedRandomGuassianRange(-200, 200, ConfidenceIntervals.CI_90, 200,
					Modifiers.get("mouseMoveOffscreenX"));
			int y = Random.modifiedRandomGuassianRange(-200, 200, ConfidenceIntervals.CI_90, 200,
					Modifiers.get("mouseMoveOffscreenY"));

			Mouse.click(new Point(x < 0 ? x : bounds.width + x, y < 0 ? y : bounds.height + y), 1);
		} finally {
			Bot.mouseLock.unlock();
		}
	}

	@Override
	public void onDraw(Graphics g) {
		g.setColor(Color.green);
		g.drawLine(cx - 4, cy - 4, cx + 4, cy + 4);
		g.drawLine(cx - 4, cy + 4, cx + 4, cy - 4);
	}

}
