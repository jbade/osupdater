package org.iinc.osbot.gui;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.Supplier;

import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import org.iinc.osbot.input.InputListeners;
import org.iinc.osbot.script.Script;
import org.iinc.osbot.script.ScriptHandler;
import org.iinc.osbot.scripts.AccountCreatorScript;
import org.iinc.osbot.scripts.EscapeScript;
import org.iinc.osbot.scripts.LesserDemon;
import org.iinc.osbot.scripts.LumbridgeOffloader;
import org.iinc.osbot.scripts.Multi;
import org.iinc.osbot.scripts.NewAccountScript;
import org.iinc.osbot.scripts.ShopScript;
import org.iinc.osbot.scripts.TestScript;
import org.iinc.osbot.scripts.basic.PickpocketScript;
import org.iinc.osbot.scripts.bond.RecieveCreateBondOfferScript;
import org.iinc.osbot.scripts.bond.RedeemBondOfferScript;
import org.iinc.osbot.scripts.common.Antiban;
import org.iinc.osbot.scripts.debug.DebugBoundaryObjects;
import org.iinc.osbot.scripts.debug.DebugCamera;
import org.iinc.osbot.scripts.debug.DebugEquipment;
import org.iinc.osbot.scripts.debug.DebugGameObjects;
import org.iinc.osbot.scripts.debug.DebugGameState;
import org.iinc.osbot.scripts.debug.DebugGrandExchangeOffers;
import org.iinc.osbot.scripts.debug.DebugGroundItems;
import org.iinc.osbot.scripts.debug.DebugInventory;
import org.iinc.osbot.scripts.debug.DebugLocalPlayer;
import org.iinc.osbot.scripts.debug.DebugMap;
import org.iinc.osbot.scripts.debug.DebugMenu;
import org.iinc.osbot.scripts.debug.DebugNpcs;
import org.iinc.osbot.scripts.debug.DebugPlayers;
import org.iinc.osbot.scripts.debug.DebugTrading;
import org.iinc.osbot.scripts.debug.DebugWidgets;
import org.iinc.osbot.scripts.debug.PathRecorder;
import org.iinc.osbot.scripts.dmm.DMMArdyOffloaderScript;
import org.iinc.osbot.scripts.dmm.DMMArdyThievingScript;
import org.iinc.osbot.scripts.dmm.silkseller.DMMSilkSellerScript;
import org.iinc.osbot.scripts.fishing.barbarian.BarbarianFishingScript;
import org.iinc.osbot.scripts.fishing.catherby.CatherbyFishingScript;
import org.iinc.osbot.scripts.fishing.fishing_guild.FishingGuildFishingScript;
import org.iinc.osbot.scripts.fishing.lumbridge_river.LumbridgeRiverFishingScript;
import org.iinc.osbot.scripts.fishing.lumbridge_swamp.LumbridgeSwampFishingScript;
import org.iinc.osbot.scripts.gdk.GDKScript;
import org.iinc.osbot.scripts.ge.Flipper;
import org.iinc.osbot.scripts.herblore.Herblore;
import org.iinc.osbot.scripts.hunter.Hunter;
import org.iinc.osbot.scripts.shop.zaff.ZaffScript;
import org.iinc.osbot.scripts.thieving.ardyguards.ArdyGuardsScript;
import org.iinc.osbot.scripts.thieving.ardyknights.ArdyKnightsScript;
import org.iinc.osbot.scripts.thieving.ardywarriors.ArdyWarriorsScript;
import org.iinc.osbot.scripts.thieving.cakestall.CakeStallScript;
import org.iinc.osbot.scripts.thieving.castleknights.CastleKnightsScript;
import org.iinc.osbot.scripts.thieving.farmers.ArdyFarmersScript;
import org.iinc.osbot.scripts.thieving.theguns.TheGunsScript;
import org.iinc.osbot.scripts.thieving.watchtower.WatchtowerScript;
import org.iinc.osbot.scripts.tutorial.TutorialIslandScript;
import org.iinc.osbot.scripts.basic.FightingScript;

public class MenuBar {

	private static JMenuItem createJMenuItem(String text, Supplier<Script> s, boolean disableInput) {
		JMenuItem jmi = new JMenuItem(text);
		jmi.addActionListener((e) -> {
			SwingUtilities.invokeLater(() -> {
				if (disableInput) {
					InputListeners.disableInput();
				}
				ScriptHandler.addScript(s.get());
			});
		});
		return jmi;
	}

	public static JMenuBar getJMenuBar() {
		JMenuBar jMenuBar = new JMenuBar();

		JMenu menu;
		JMenuItem menuItem;

		jMenuBar = new JMenuBar();

		JButton button = new JButton("Stop scripts");
		button.addActionListener((e) -> {
			SwingUtilities.invokeLater(() -> {
				ScriptHandler.stopScripts();
				InputListeners.enableInput();
			});
		});
		button.setMargin(new Insets(0, 0, 0, 0));
		button.setFocusable(false);
		jMenuBar.add(button);

		// debug
		menu = new JMenu("Debug");
		jMenuBar.add(menu);
		menu.add(createJMenuItem("Game state", () -> new DebugGameState(), false));
		menu.add(createJMenuItem("Widget explorer", () -> new DebugWidgets(), false));
		menu.add(createJMenuItem("Npcs", () -> new DebugNpcs(), false));
		menu.add(createJMenuItem("Players", () -> new DebugPlayers(), false));
		menu.add(createJMenuItem("Inventory", () -> new DebugInventory(), false));
		menu.add(createJMenuItem("Grand Exchange Offers", () -> new DebugGrandExchangeOffers(), false));
		menu.add(createJMenuItem("Menu", () -> new DebugMenu(), false));
		menu.add(createJMenuItem("Camera", () -> new DebugCamera(), false));
		menu.add(createJMenuItem("Map", () -> new DebugMap(), false));
		menu.add(createJMenuItem("Local player", () -> new DebugLocalPlayer(), false));
		menu.add(createJMenuItem("Path recorder", () -> new PathRecorder(), false));
		menu.add(createJMenuItem("Ground items", () -> new DebugGroundItems(), false));
		menu.add(createJMenuItem("Equipment", () -> new DebugEquipment(), false));
		menu.add(createJMenuItem("Trading", () -> new DebugTrading(), false));
		menu.add(createJMenuItem("BoundaryObjects", () -> new DebugBoundaryObjects(), false));
		menu.add(createJMenuItem("GameObjects", () -> new DebugGameObjects(), false));

		// input
		menu = new JMenu("Input");
		jMenuBar.add(menu);

		menuItem = new JMenuItem("Enable");
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				InputListeners.enableInput();
			}

		});
		menu.add(menuItem);

		menuItem = new JMenuItem("Disable");
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				InputListeners.disableInput();
			}

		});
		menu.add(menuItem);

		// scripts
		menu = new JMenu("Scripts");
		jMenuBar.add(menu);

		menu.add(createJMenuItem("Antiban", () -> new Antiban(), false));
		menu.add(createJMenuItem("TestScript", () -> new TestScript(), false));
		menu.add(createJMenuItem("Flipper", () -> new Flipper(), true));
		menu.add(createJMenuItem("RatFrog", () -> new org.iinc.osbot.scripts.ratfrog.RatFrogScript(), true));
		menu.add(createJMenuItem("RatFrogStr", () -> new org.iinc.osbot.scripts.ratfrog.RatFrogStrScript(), true));
		menu.add(createJMenuItem("Lesser demon", () -> new LesserDemon(), true));
		menu.add(createJMenuItem("CatherbyFishing", () -> new CatherbyFishingScript(), true));
		menu.add(createJMenuItem("FishingGuildFishing", () -> new FishingGuildFishingScript(), true));
		menu.add(createJMenuItem("Callisto2", () -> new org.iinc.osbot.scripts.callisto.Callisto(), true));
		menu.add(createJMenuItem("GDK", () -> new GDKScript(), true));
		menu.add(createJMenuItem("Herblore", () -> new Herblore(), true));
		menu.add(createJMenuItem("Hunter", () -> new Hunter(), true));
		menu.add(createJMenuItem("Multi", () -> new Multi(), true));
		menu.add(createJMenuItem("LumbridgeOffloader", () -> new LumbridgeOffloader(), true));
		menu.add(createJMenuItem("TutorialIsland", () -> new TutorialIslandScript(), true));
		menu.add(createJMenuItem("Zaff", () -> new ZaffScript(), true));
		menu.add(createJMenuItem("NewAccountScript", () -> new NewAccountScript(), true));
		menu.add(createJMenuItem("LumbridgeRiverFishingScript", () -> new LumbridgeRiverFishingScript(), true));
		menu.add(createJMenuItem("LumbridgeSwampFishingScript", () -> new LumbridgeSwampFishingScript(), true));
		menu.add(createJMenuItem("AccountCreatorScript", () -> new AccountCreatorScript(), true));
		menu.add(createJMenuItem("TheGuns", () -> new TheGunsScript(), true));
		menu.add(createJMenuItem("CakeStallScript", () -> new CakeStallScript(), true));
		menu.add(createJMenuItem("ArdyWarriorsScript", () -> new ArdyWarriorsScript(), true));
		menu.add(createJMenuItem("ArdyFarmersScript", () -> new ArdyFarmersScript(), true));
		menu.add(createJMenuItem("ArdyGuardsScript", () -> new ArdyGuardsScript(), true));
		menu.add(createJMenuItem("ArdyKnightsScript", () -> new ArdyKnightsScript(), true));
		menu.add(createJMenuItem("WatchtowerScript", () -> new WatchtowerScript(), true));
		menu.add(createJMenuItem("CastleKnightsScript", () -> new CastleKnightsScript(), true));
		menu.add(createJMenuItem("BarbarianFishingScript", () -> new BarbarianFishingScript(), true));
		menu.add(createJMenuItem("RedeemBondOffer", () -> new RedeemBondOfferScript(), true));
		menu.add(createJMenuItem("RecieveCreateBondOffer", () -> new RecieveCreateBondOfferScript(), true));
		menu.add(createJMenuItem("DMMArdy", () -> new DMMArdyThievingScript(), true));
		menu.add(createJMenuItem("FightingScript", () -> new FightingScript(), true));
		menu.add(createJMenuItem("EscapeScript", () -> new EscapeScript(), true));

		return jMenuBar;
	}

}
