package org.iinc.osbot.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import org.iinc.osbot.bot.Game;

public class GUI {

	private static JFrame frame;
	private static Game game;

	public static void start() {

		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setJMenuBar(MenuBar.getJMenuBar());
		frame.setLayout(new BorderLayout());

		game = new Game();
		frame.add(game, BorderLayout.CENTER);
		
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void repaint() {
		if (game != null)
			game.repaint();
	}
}
