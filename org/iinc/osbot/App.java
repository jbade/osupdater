package org.iinc.osbot;

import java.applet.Applet;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.iinc.osbot.api.base.worlds.World;
import org.iinc.osbot.api.base.worlds.Worlds;
import org.iinc.osbot.bot.Bot;
import org.iinc.osbot.bot.BreakHandler;
import org.iinc.osbot.gui.GUI;
import org.iinc.osbot.hooks.Hooks;
import org.iinc.osbot.input.InputListeners;
import org.iinc.osbot.util.ClassLoader;
import org.iinc.osbot.util.Crawler;
import org.iinc.osbot.util.Injector;
import org.iinc.osbot.util.Modifiers;
import org.iinc.osbot.util.internet.Internet;

public class App {
	static boolean PROXY = false;
	static boolean RESET = true;

	public static void main(String[] args) throws InterruptedException, IOException {

		Settings.TEMPORARY_DIRECTORY = "tmp";
		Settings.BOTWATCH_DIRECTORY = "botwatch";
		Settings.LOG_TO_CONSOLE = true;
		Settings.LOG_LEVEL = Level.ALL;

		if (RESET) {
			deleteFile(Paths.get(System.getProperty("user.home"), "random.dat").toFile());

			// Don't delete gamepack and cache
			// deleteFile(Paths.get(Settings.TEMPORARY_DIRECTORY).toFile());
			// deleteFile(Paths.get(System.getProperty("user.home"), "jagexcache",
			// "oldschool").toFile());
			deleteFile(Paths.get(System.getProperty("user.home"), "jagexappletviewer.preferences").toFile());
			deleteFile(Paths.get(System.getProperty("user.home"), "jagex_cl_oldschool_LIVE.dat").toFile());
		}

		try {
			Files.createDirectories(Paths.get(Settings.TEMPORARY_DIRECTORY));
			Files.createDirectories(Paths.get(Settings.BOTWATCH_DIRECTORY));
		} catch (IOException e) {
			System.err.println("Unable to create directories");
			System.exit(1);
		}

		if (PROXY) {
			System.setProperty("socksProxyHost", "localhost");
			System.setProperty("socksProxyPort", "1080");
		}
		
		System.out.println("ip: " + Internet.read("https://api.ipify.org").stream().collect(Collectors.joining("")));

		Hooks.loadFromFile("hooks.json");

		World world = Worlds.getRandomNormalWorld(true);
		//World world = Worlds.getWorlds().stream().filter((w)-> w.getId() == 394).findFirst().orElse(null);
		System.out.println(world);
		Crawler crawler = new Crawler(world);
		crawler.crawl();
		if (crawler.outdated()) {
			crawler.download();
		}

		if (Hooks.getRevision() != crawler.revision()) {
			System.err.println("Hooks not updated");
			System.exit(1);
		}

		Injector inj = new Injector(crawler.getFileURL());
		inj.inject();

		ClassLoader cl = new ClassLoader(inj.getFileURL());
		// Hooks.buildHooks(cl);

		Runtime.getRuntime().gc();
		System.out.println("ready");

		Applet game = crawler.start(cl);
		Bot.start(game);
		GUI.start();
		InputListeners.enableInput();

		Modifiers.init(Bot.username.hashCode());
		BreakHandler.init(Bot.username.hashCode());

		//ScriptHandler.addScript(new DMMSilkSellerScript());
	}

	public static void deleteFile(File element) {
		if (element.isDirectory()) {
			for (File sub : element.listFiles()) {
				deleteFile(sub);
			}
		}
		element.delete();
	}
}
